# IGS Abschlusstendenzrechner.go
Dieses Programm berechnet Abschlusstendenzen für Schüler:Innen hessischer integrierter Gesamtschulen. Das Programm ist in go geschrieben und Open Source.
Es liest die von der LUSD exportierten Excel-Dateien ein, berechnet die Abschlusstendenzen und gibt die berechneten Tendenzen als neue Excel-Dateien aus.
Dabei werden alle Daten nur lokal verarbeitet.

## Disclaimer
WICHTIG: Die von diesem Programm berechneten Tendenzen können Fehler enthalten! Ich garantiere nicht für die Richtigkeit der berechneten Tendenzen. Deshalb ist es unerlässlich, dass alle automatisch berechneten Tendenzen manuell geprüft werden!

## Installation
- Lade das Programm für das entsprechende Betriebssystem herunter.
  - [Direktlink zur Windows-Version](https://gitlab.com/Kuddelmuddel/igs-abschlussrechner/-/jobs/artifacts/master/download?job=build-windows-amd64)
  - [Direktlink zur MacOS-Version](https://gitlab.com/Kuddelmuddel/igs-abschlussrechner/-/jobs/artifacts/master/download?job=build-macOS-amd64)
  - [Direktlink zur Linux-Version](https://gitlab.com/Kuddelmuddel/igs-abschlussrechner/-/jobs/artifacts/master/download?job=build-linux-amd64)
- entpacke das Archiv an einen Ort deiner Wahl

## Bedienungsanleitung
- exportiere die Daten aus der LUSD im Excel-Format
  - Zeugniskonferenzliste:
  ![Export Zeugniskonferenzliste](Export_Zeugniskonferenzliste.png)
  - Abschlusszeugnis-Konferenzliste (optional):
  ![Export Abschlusszeugnis-Konferenzliste](Export_Abschlusszeugnis-Konferenzliste.png)
  - Ergebnisse der Abschlussprüfungen (für Haupt- und Realschulabschluss):
  ![Export Ergebnisse der Abschlussprüfungen](Export_Ergebnisse_Abschlussprüfungen.png)
  - alternativ (bzw. auch zusätzlich) zu den LUSD-Daten kann das Programm auch Tendenzen aus manuell eingegebene Noten berechnen. Nutze dazu die Vorlage 'manuelle-Noteneingabe_Template.xlsx'. Das Programm verarbeitet alle Tabellenblätter dieser Datei, wenn sie im *input-Ordner* liegt.
- speichere alle zu verarbeitenden Excel-Dateien im *input-Ordner*
- führe das Programm aus:
  - Windows: mit einem Doppelklick auf igs-abschlussrechner.exe (oder über die Eingabeaufforderung)
  - MacOS/Linux: Doppelklick auf igs-abschlussrechner oder über das Terminal:
    - gehe in den Ordner des Programms: `cd Pfad/zum/igs-abschlussrechner-Ordner/`
    - führe die Datei aus: `./igs-abschlussrechner`
- die Ergebnisdateien werden im *output-Ordner* gespeichert
  - Hinweis: Die berufsorientierten Abschlüsse werden vom Programm nicht berechnet. Auch die Tabelle mit Fördermaßnahmen wird nicht vom Programm gefüllt. Die entsprechenden Tabelleneinträge sind dazu gedacht, die Abschlüsse bzw. Fördermaßnahmen manuell hinzuzufügen.

## Programm konfigurieren
Das Programm kann über die *config.yaml*-Datei konfiguriert werden. Öffne die Datei mit einem Texteditor deiner Wahl, um sie zu editieren.

### Elternwünsche aus einer Excle-Datei importieren
- Speichere eine Excle-Datei mit folgenden Spalten zustätzlich im *input-Ordner*:

    | Nachname | Vorname | Klasse | Elternwunsch|
    | -------- | ------- | ------ | ----------- |
    | Muster   |   Lars  |   8g   |     v11     |
    |    ...   |   ...   |   ...  |     ...     |

- Gib folgende Informationen über die Excel-Tabelle in der yaml-Konfigurationsdatei unter "ExternalInfos" an:
  - xlsxSheetname:
    - gib den Namen des Excel-Tabellenblattes an, das die erstellte Tabelle enthält
  - detectfilebyCell:
    - gib eine einmalige Zelle aus dem Tabellenblatt an (z.B. P2), mit der die Excel-Datei identifiziert werden kann (einmalig heißt, diese Zelle sollte in anderen Excel-Dateien im *input-Ordner* nicht den gleichen Wert enthalten)
  - detectfilebyContent:
    - gib den Wert an, der in der oben angegebenen einmaligen Zelle steht (z.B. Elternwunsch)
  - detectionStringLastname:
    - gib die Kopfzeile der Spalte an, in der die Nachnamen stehen (im Beispiel: Nachname)
  - detectionStringForename:
    - gib die Kopfzeile der Spalte an, in der die Vornamen stehen (im Beispiel: Vorname)
  - detectionStringClass:
    - gib die Kopfzeile der Spalte an, in der die Klassen der Schüler:Innen stehen (im Beispiel: Klasse)

Wenn keine Excel-Datei mit der angegebenen Zelle erkannt wird, wird diese Option ignoriert. (Solltest du diese Funktion nicht nutzen wollen, kannst du die Excel-Datei mit den Elternwünschen im *input-Ordner* einfach weglassen)

### Weitere 'externe' Daten (z.B. eine Excel-Datei mit Umstufungsinformationen)
- Speichere eine Excle-Datei mit folgenden Spalten zustätzlich im *input-Ordner*:

    | Schülername  | Klasse |  Fach  | ... | Elternantrag |
    | ------------ | ------ | ------ | --- | ------------ |
    | Muster, Lars |   8g   |  Mathe | ... |       x      |
    |      ...     |   ...  |   ...  | ... |      ...     |

- Gib die Informationen über die Excel-Tabelle in der yaml-Konfigurationsdatei unter "ExternalInfosLevelChanges" an (analog zu den Elternwünschen (Details siehe oben)):
  - detectfilebyCell: gib eine einmalige Zelle aus dem Tabellenblatt an
  - detectfilebyContent: gib den Wert an, der in der oben angegebenen einmaligen Zelle steht. Hier muss dieser Wert (anders als bei den Elternwünschen) nicht genau der Wert der Zelle sein, aber der Wert muss (unter Anderem) in der Zelle enthalten sein.
  - detectionStringName: gib die Kopfzeile der Spalte an, in der die Namen der Schüler:Innen stehen
  - detectionStringClass: gib die Kopfzeile der Spalte an, in der die Klassen der Schüler:Innen stehen
  - detectionStringSubject: gib die Kopfzeile der Spalte an, in der das Fächer der Umstufung stehen
  - detectionStringTeacher: gib die Kopfzeile der Spalte an, in der die Kurslehrkräfte stehen
  - detectionStringCurrentLevel: gib die Kopfzeile der Spalte an, in der die aktuellen aktuelle Kursbezeichnungen stehen
  - detectionStringObjection: gib die Kopfzeile der Spalte an, in der steht, ob bereits ein Elterneinspruch vorliegt
  - detectionStringGrade: gib die Kopfzeile der Spalte an, in der die Halbjahresnoten stehen (optional)
  - detectionStringFutureLevel: gib die Kopfzeile der Spalte an, in der die gewünschten Umstufungen stehen
  - detectionStringParentChangeRequest: gib die Kopfzeile der Spalte an, in der steht, ob ein Elternantrag auf freiwillige Abstufung vorliegt