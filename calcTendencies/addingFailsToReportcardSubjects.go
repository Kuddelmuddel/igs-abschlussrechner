package calctendencies

import (
	config "igs-abschlussrechner/config"
	"igs-abschlussrechner/global"
)

// TODO: Add Test
func AddingFailsToSubjectsInReportcard(student *global.Student) {
	var failsAndComps global.FailsAndCompensations
	cfg := config.GetConfig()

	switch student.Tendency.GraduationTendency {
	case cfg.Versetztung11:
		failsAndComps = student.CalcResults.V11Results.FailsAndComps
	case cfg.Realschulabschluss:
		failsAndComps = student.CalcResults.RAResults.FailsAndComps
	default:
		if student.ClassYear == 10 {
			failsAndComps = student.CalcResults.RAResults.FailsAndComps
		} else {
			failsAndComps = student.CalcResults.HAResults.FailsAndComps
		}
	}

	for _, majorfc := range failsAndComps.MajorFail {
		student.ReportCardInfo.Subjects = addFailsAndCompsToReportCard(student.ReportCardInfo.Subjects, majorfc)
	}

	for _, minorfc := range failsAndComps.MinorFail {
		student.ReportCardInfo.Subjects = addFailsAndCompsToReportCard(student.ReportCardInfo.Subjects, minorfc)
	}
}

func addFailsAndCompsToReportCard(subjectList []global.SubjectStruc, failedSubject global.FailedSubject) []global.SubjectStruc {
	for i := 0; i < len(subjectList); i++ {
		if subjectList[i].SubjectAbbreviation == failedSubject.Name && subjectList[i].Teacher == failedSubject.Teacher {
			subjectList[i].FailString = "Ausfall"
			subjectList[i].CompenstaionBy = failedSubject.Compensatedby
		}
	}
	return subjectList
}
