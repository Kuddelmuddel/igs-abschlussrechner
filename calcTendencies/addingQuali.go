package calctendencies

import (
	config "igs-abschlussrechner/config"
	misc "igs-abschlussrechner/miscellaneous"

	"igs-abschlussrechner/global"
)

// TODO: Add Test
func AddingQuali(student *global.Student) {
	cfg := config.GetConfig()

	// V11
	student.CalcResults.V11Results.Quali = cfg.NoQuali

	// Realschulabschluss
	switch {
	case student.CalcResults.RAResults.Averages.AverageDME <= 3.0 && student.CalcResults.RAResults.Averages.AverageAllButDME <= 3.0:
		student.CalcResults.RAResults.Quali = cfg.Quali
	case student.CalcResults.RAResults.Averages.AverageNaWiQualiDMEandPHorCH < 3.0 && student.CalcResults.RAResults.Averages.AverageNaWiQualiRestOfSubjects < 3.0:
		student.CalcResults.RAResults.Quali = cfg.NaWiQuali
		student.CalcResults.RAResults.Averages.BestAverageDME = student.CalcResults.RAResults.Averages.AverageNaWiQualiDMEandPHorCH
		student.CalcResults.RAResults.Averages.BestAverageRestOfSubjects = student.CalcResults.RAResults.Averages.AverageNaWiQualiRestOfSubjects
	default:
		student.CalcResults.RAResults.Quali = cfg.NoQuali
	}

	// correcting Quali: in class 10 quali is only valid, if RA-Exam was taken (checking that)
	if (student.CalcResults.RAResults.Quali == cfg.Quali || student.CalcResults.RAResults.Quali == cfg.NaWiQuali) && student.ClassYear == 10 && !student.ReportCardInfo.FinalExams.TookExams && student.Term == 2 && !student.CalcResults.V11Results.ConditionsMet {
		student.CalcResults.RAResults.Quali = cfg.NoQuali
		addComment(student, config.CommentNoRAPNoQuali)
	} else if (student.CalcResults.RAResults.Quali == cfg.Quali || student.CalcResults.RAResults.Quali == cfg.NaWiQuali) && student.ClassYear == 10 && !student.ReportCardInfo.FinalExams.TookExams && student.Term == 1 && !student.CalcResults.V11Results.ConditionsMet {
		misc.AddWarning(student, config.WarningTakeRAP)
	}

	// Hauptschulabschluss
	if student.CalcResults.HAResults.Averages.AverageAllSubjects <= 3.0 {
		student.CalcResults.HAResults.Quali = cfg.Quali
	}

	// correcting Quali: in class 9 quali is only valid, if HA-Exam was taken (checking that)
	if (student.CalcResults.HAResults.Quali == cfg.Quali || student.CalcResults.HAResults.Quali == cfg.NaWiQuali) && student.ClassYear == 9 && !student.ReportCardInfo.FinalExams.TookExams && student.Term == 2 && !student.CalcResults.RAResults.ConditionsMet && !student.CalcResults.V11Results.ConditionsMet {
		student.CalcResults.HAResults.Quali = cfg.NoQuali
		addComment(student, config.CommentNoHAPNoQuali)
	} else if (student.CalcResults.HAResults.Quali == cfg.Quali || student.CalcResults.HAResults.Quali == cfg.NaWiQuali) && student.ClassYear == 9 && !student.ReportCardInfo.FinalExams.TookExams && student.Term == 1 && !student.CalcResults.RAResults.ConditionsMet && !student.CalcResults.V11Results.ConditionsMet {
		misc.AddWarning(student, config.WarningTakeHRP)
	}
}
