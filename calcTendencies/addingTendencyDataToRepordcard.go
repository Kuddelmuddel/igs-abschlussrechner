package calctendencies

import (
	config "igs-abschlussrechner/config"
	misc "igs-abschlussrechner/miscellaneous"

	"igs-abschlussrechner/global"
)

func AddingTendencyDataToReportcard(student *global.Student) {
	cfg := config.GetConfig()

	// function to set tendency data
	setTendencyData := func(tendencyData global.TendencyCalcResults) {
		student.Tendency.WithCompensation = tendencyData.WithCompensation
		student.Tendency.Quali = tendencyData.Quali
		student.Tendency.Averages = tendencyData.Averages
	}

	switch {
	case student.Warnings.NoCalculationPossible:
		student.Tendency.GraduationTendency = "nicht berechenbar" // TODO: hardcode string in config file
		misc.AddWarning(student, config.NoTendencyCalcPossible)
	case student.CalcResults.V11Results.ConditionsMet:
		student.Tendency.GraduationTendency = cfg.Versetztung11
		setTendencyData(student.CalcResults.V11Results)
	case student.CalcResults.RAResults.ConditionsMet:
		student.Tendency.GraduationTendency = cfg.Realschulabschluss
		setTendencyData(student.CalcResults.RAResults)
	case student.CalcResults.HAResults.ConditionsMet:
		student.Tendency.GraduationTendency = cfg.Hauptschulabschluss
		if student.ClassYear == 10 {
			setTendencyData(student.CalcResults.RAResults)
		} else {
			setTendencyData(student.CalcResults.HAResults)
		}
	default:
		student.Tendency.GraduationTendency = cfg.OhneAbschluss
		if student.ClassYear == 10 {
			student.Tendency.Averages = student.CalcResults.RAResults.Averages
		} else {
			student.Tendency.Averages = student.CalcResults.HAResults.Averages
		}
	}

	student.Tendency.Log = append(student.Tendency.Log, student.CalcResults.V11Results.Log...)
	student.Tendency.Log = append(student.Tendency.Log, student.CalcResults.V11Results.FailsLog...)
	student.Tendency.Log = append(student.Tendency.Log, student.CalcResults.RAResults.Log...)
	student.Tendency.Log = append(student.Tendency.Log, student.CalcResults.RAResults.FailsLog...)
	student.Tendency.Log = append(student.Tendency.Log, student.CalcResults.HAResults.Log...)
	student.Tendency.Log = append(student.Tendency.Log, student.CalcResults.HAResults.FailsLog...)
}
