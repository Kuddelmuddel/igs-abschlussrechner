package calctendencies

// TODO: Create test for addingTendencyData ...

// import (
// 	struc "igs-abschlussrechner/globalStructs"
// )

// type testCasesForTendencyDataToReportCardFunction struct {
// 	description    string
// 	testStudents   struc.Student
// 	expectedResult struc.Student
// }

// using TestSubjects from ...

// func TestAddingTendencyDataToReportcard(t *testing.T) {
// 	// Create a sample student
// 	student := struc.Student{
// 		Warnings: struc.WarningStruc{
// 			NoCalculationPossible: false,
// 		},
// 		CalcResults: struc.CalcResultsStruc{
// 			V11Results: struc.TendencyCalcResults{
// 				ConditionsMet:    true,
// 				WithCompensation: true,
// 				Quali:            Quali,
// 				Averages: struc.AverageStruct{
// 					AverageDME:                     0,
// 					AverageAllButDME:               0,
// 					AverageAllSubjects:             0,
// 					AverageNaWiQualiDMEandPHorCH:   0,
// 					AverageNaWiQualiRestOfSubjects: 0,
// 					BestAverageDME:                 0,
// 					BestAverageRestOfSubjects:      0,
// 				},
// 				Log: [][3]string{
// 					{"Fail Log V11 Result 1"},
// 				},
// 				FailsLog: [][3]string{
// 					{"Fail Log V11 Result 1"},
// 				},
// 			},
// 			RAResults: struc.TendencyCalcResults{
// 				ConditionsMet:    false,
// 				WithCompensation: false,
// 				Quali:            10,
// 				Averages:         70,
// 				Log:              []string{"Log RA Result 1", "Log RA Result 2"},
// 				FailsLog:         []string{"Fail Log RA Result 1"},
// 			},
// 			HAResults: struc.TendencyCalcResults{
// 				ConditionsMet:    false,
// 				WithCompensation: false,
// 				Quali:            8,
// 				Averages:         60,
// 				Log:              []string{"Log HA Result 1", "Log HA Result 2"},
// 				FailsLog:         []string{"Fail Log HA Result 1"},
// 			},
// 		},
// 		Tendency:  struc.TendencyStruc{},
// 		ClassYear: 11,
// 	}

// 	// Call the function
// 	result := AddingTendencyDataToReportcard(student)

// 	// Check if the graduation tendency is set correctly
// 	expectedTendency := Versetztung11
// 	if result.Tendency.GraduationTendency != expectedTendency {
// 		t.Errorf("Expected graduation tendency %s, got %s", expectedTendency, result.Tendency.GraduationTendency)
// 	}

// 	// Check if the tendency data is set correctly
// 	expectedWithCompensation := true
// 	if result.Tendency.WithCompensation != expectedWithCompensation {
// 		t.Errorf("Expected with compensation %t, got %t", expectedWithCompensation, result.Tendency.WithCompensation)
// 	}
// 	expectedQuali := 12
// 	if result.Tendency.Quali != expectedQuali {
// 		t.Errorf("Expected quali %d, got %d", expectedQuali, result.Tendency.Quali)
// 	}
// 	expectedAverages := 80
// 	if result.Tendency.Averages != expectedAverages {
// 		t.Errorf("Expected averages %d, got %d", expectedAverages, result.Tendency.Averages)
// 	}

// 	// Check if the logs are appended correctly
// 	expectedLogs := []string{"Log V11 Result 1", "Log V11 Result 2", "Fail Log V11 Result 1"}
// 	if len(result.Tendency.Log) != len(expectedLogs) {
// 		t.Errorf("Expected log length %d, got %d", len(expectedLogs), len(result.Tendency.Log))
// 	} else {
// 		for i := range expectedLogs {
// 			if result.Tendency.Log[i] != expectedLogs[i] {
// 				t.Errorf("Expected log entry %s, got %s", expectedLogs[i], result.Tendency.Log[i])
// 			}
// 		}
// 	}
// }
