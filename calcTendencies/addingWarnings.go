package calctendencies

import (
	config "igs-abschlussrechner/config"
	"igs-abschlussrechner/global"
	misc "igs-abschlussrechner/miscellaneous"
	"strconv"
	"strings"

	"github.com/TwiN/go-color"
	"github.com/mohae/deepcopy"
)

func AddingWarningsAndCommentsToStudentData(classData *[]global.Student) []string {
	cfg := config.GetConfig()

	var infosAndWarningsLog []string

	for idx := range *classData {
		student := (*classData)[idx]

		// Add warning, if any level downgrades threaten current tendency
		mockTendency := deepcopy.Copy(student).(global.Student)
		applyCourceLvlDowngrade(&mockTendency)
		CheckingTendencyCriteria(&mockTendency)
		AddingTendencyDataToReportcard(&mockTendency)

		if isGraduationLower(mockTendency.Tendency.GraduationTendency, student.Tendency.GraduationTendency) {
			misc.AddWarning(&student, config.CriticalCourseLvlDowngrade)
		}

		// Add warning, if v11 tendency depends on grade in biology (which lacks next year)
		if (checkBioWarning(student.ReportCardInfo.Subjects, student.ClassYear) || checkBioWarning(mockTendency.ReportCardInfo.Subjects, student.ClassYear)) && student.CalcResults.V11Results.ConditionsMet {
			misc.AddWarning(&student, config.WarningMissingE3forV11NextYear)
		}

		// Add warning, if V11 tendency in 8th grade depends on E3 in Nawi-Subject next year
		if check8GradeNaWiCoursWarning(student.ReportCardInfo.Subjects) && student.ClassYear == 8 && student.Tendency.GraduationTendency == cfg.Versetztung11 {
			misc.AddWarning(&student, config.WarningMissingACourseAndE3forV11NextYear)
		}

		// Add warning to check for HA, if after 10th grade no graduation is present
		if student.ClassYear == 10 && student.Tendency.GraduationTendency == cfg.Hauptschulabschluss {
			student.Tendency.Quali = cfg.NoQuali
			misc.AddWarning(&student, config.WarningCheckHA)
		}

		// Check for missing B-courses at the end of year 9 and add warning
		if student.ClassYear == 9 && student.Term == 2 && student.Tendency.GraduationTendency == cfg.Hauptschulabschluss && student.Tendency.Quali == cfg.Quali && checkIfToFewBCoursesFor10thGrade(student.ReportCardInfo.Subjects) {
			misc.AddWarning(&student, config.WarningPaedLevelIncrease)
		}

		// add Warning, if many unexcused absents
		if student.ReportCardInfo.AbsentsTotal.UnexcusedAbsents >= config.UnexcusedAbsentsWarningLimit {
			misc.AddWarning(&student, config.WarningUnexcusedAbsents)
		}

		// Add comment for HAGradeIncrease
		if student.ClassYear == 9 && student.Term == 1 && (student.Tendency.GraduationTendency == cfg.Hauptschulabschluss || student.Tendency.GraduationTendency == cfg.OhneAbschluss) {
			addComment(&student, config.CommentHAGradeIncreaseInfo)
		}

		// Check for missing exam data and missing course-lvl (v-courses)
		for _, subject := range student.ReportCardInfo.Subjects {
			// checking for missing exam data
			if subject.Grade == 0 {
				misc.AddWarning(&student, config.WarningMissingGrades)
				misc.ConsoleLogPrintln(color.InRed("    * ")+student.Name+" ("+student.Class+", "+student.ClassTeacher+"): missing (exam) grades - check exported LUSD-excel-file!", true, true)
				infosAndWarningsLog = append(infosAndWarningsLog, "- "+student.Name+" ("+student.Class+", "+student.ClassTeacher+"): Fehlende (Abschluss)Noten - prüfe exportierte LUSD-Excel-Datei!")
			}

			// checking for missing course lvl assignments
			if subject.CourseLvl == "v" {
				misc.AddWarning(&student, config.WarningMissingCourseSpecifications)
				misc.ConsoleLogPrintln(color.InRed("    * ")+student.Name+" ("+student.Class+", "+student.ClassTeacher+"): missing course assignment(s) - v-course(s)", true, true)
				infosAndWarningsLog = append(infosAndWarningsLog, "- "+student.Name+" ("+student.Class+", "+student.ClassTeacher+"): Fehlende Kurszuweisung(en) - v-Kurs(e)")
			}
		}

		// checking if all exam subjects are present
		if student.ReportCardInfo.FinalExams.TookExams {
			if len(student.ReportCardInfo.FinalExams.ExamSubjects) < 4 {
				misc.AddWarning(&student, config.MissingExamSubject)
				misc.ConsoleLogPrintln(color.InRed("    * ")+student.Name+" ("+student.Class+", "+student.ClassTeacher+"): at least one graduation subject grade is missing.", true, true)
				infosAndWarningsLog = append(infosAndWarningsLog, "- "+student.Name+" ("+student.Class+", "+student.ClassTeacher+"): Es fehlt mindestens eine der vier Prüfungsnoten.")
			}
		}

		// Add warning, if any exam data was replaced by backup data
		if student.ReportCardInfo.FinalExams.ReplacedMissingData {
			misc.AddWarning(&student, config.ExamDataReplacedByBackup+strings.Join(student.ReportCardInfo.FinalExams.DataReplacedBy, " | "))
			misc.ConsoleLogPrintln(color.InRed("    * ")+student.Name+" ("+student.Class+", "+student.ClassTeacher+"): missing ExamData, copied replacement data from backup:", true, true)
			misc.ConsoleLogPrintln("        * "+strings.Join(student.ReportCardInfo.FinalExams.DataReplacedBy, "\n        * "), true, true)
			infosAndWarningsLog = append(infosAndWarningsLog, "- "+student.Name+" ("+student.Class+", "+student.ClassTeacher+"): Fehlende LUSD-Abschlussprüfungsdaten wurden aus Backupdatensatz ersetzt: "+strings.Join(student.ReportCardInfo.FinalExams.DataReplacedBy, " | "))
		}

		// comparing graduation
		if student.ReportCardInfo.FinalExams.LUSDExamData.DataPresent && student.ReportCardInfo.FinalExams.LUSDExamData.TotalAverage > 0 && !compareGradTypes(student.Tendency.GraduationTendency+student.Tendency.Quali, student.ReportCardInfo.FinalExams.GraduationFullName) {
			misc.AddWarning(&student, config.DifferenceInLUSDGradiationAndClacGraduation)
			misc.ConsoleLogPrintln(color.InRed("    * ")+student.Name+" ("+student.Class+", "+student.ClassTeacher+"): LUSD-tendency and calculated tendency do not match.", true, true)
			infosAndWarningsLog = append(infosAndWarningsLog, "- "+student.Name+" ("+student.Class+", "+student.ClassTeacher+"): LUSD-Abschlusstendenz und berechnete Abschlusstendenz stimmen nicht überein.")
		}

		// comparing averages
		if student.ReportCardInfo.FinalExams.LUSDExamData.DataPresent && !areAveragesEqual(student.CalcResults, student.ReportCardInfo.FinalExams) {
			comparedAverages := getComparedAverages(student.CalcResults, student.ReportCardInfo.FinalExams)

			var comparedAveragesAsStringList []string
			for _, num := range comparedAverages {
				comparedAveragesAsStringList = append(comparedAveragesAsStringList, strconv.FormatFloat(num, 'f', -1, 64))
			}
			concatenatedAveragesString := strings.Join(comparedAveragesAsStringList, ", ")

			misc.AddWarning(&student, config.AverageMissmatchBetweenLUSDandCalc)
			misc.ConsoleLogPrintln(color.InRed("    * ")+student.Name+" ("+student.Class+", "+student.ClassTeacher+"): LUSD-Average and calculated average do not match: "+concatenatedAveragesString, true, true)
			infosAndWarningsLog = append(infosAndWarningsLog, "- "+student.Name+" ("+student.Class+", "+student.ClassTeacher+"): LUSD-Gesamtleistung und berechnete Gesamtleistung (Durchschnitt) stimmen nicht überein: "+concatenatedAveragesString)
		}

		// TODO: compare graduation tendency with parental goal
		// if student.Tendency.ParentsGraduationGoal != "" && student.Tendency.ParentsGraduationGoal != student.Tendency.GraduationTendency {
		// 	// convert parental graduation goal Tendency
		// 	// add Warning
		// }

		// add FAQs
		if student.ClassYear == 8 && student.Tendency.GraduationTendency == cfg.Realschulabschluss && containsFailsInV11BioChPh(student) {
			addComment(&student, config.NaWiInGrade8) // NaWiInGrade8 explains the criteria for NaWi subjects without courses in 8th grade
		}

		if student.ReportCardInfo.FinalExams.TookExams && student.ClassYear == 9 {
			addComment(&student, config.HauptschulabschlussAverage)
		}
		if student.ReportCardInfo.FinalExams.TookExams && student.ClassYear == 10 {
			addComment(&student, config.RealschulabschlussAverage)
		}

		// update student back into classData
		(*classData)[idx] = student
	}

	return infosAndWarningsLog
}

func areAveragesEqual(calcAverage global.CalcResultsStruc, examData global.ExamsStruc) bool {
	if examData.TotalAverage <= 0 {
		return true
	}
	switch examData.GraduationFullName {
	case config.LusdHaGrad, config.LusdHaGradQuali:
		return calcAverage.HAResults.Averages.AverageAllSubjects == examData.LUSDExamData.TotalAverage
	case config.LusdRaGrad, config.LusdRaGradQuali:
		return calcAverage.RAResults.Averages.AverageAllSubjects == examData.LUSDExamData.TotalAverage && calcAverage.RAResults.Averages.AverageDME == examData.LUSDExamData.MajorsAverage && calcAverage.RAResults.Averages.AverageAllButDME == examData.LUSDExamData.MinorsAverage
	default:
		return false
	}
}

func getComparedAverages(calcAverage global.CalcResultsStruc, examDa global.ExamsStruc) []float64 {
	if examDa.TotalAverage <= 0 {
		return []float64{}
	}

	switch examDa.GraduationFullName {
	case config.LusdHaGrad, config.LusdHaGradQuali:
		return []float64{calcAverage.HAResults.Averages.AverageAllSubjects, examDa.LUSDExamData.TotalAverage}
	case config.LusdRaGrad, config.LusdRaGradQuali:
		return []float64{calcAverage.RAResults.Averages.AverageAllSubjects, calcAverage.RAResults.Averages.AverageDME, calcAverage.RAResults.Averages.AverageAllButDME, examDa.LUSDExamData.TotalAverage, examDa.LUSDExamData.MajorsAverage, examDa.LUSDExamData.MinorsAverage}
	default:
		return []float64{}
	}
}

func compareGradTypes(calcGradPlusQualiString, lusdGrad string) bool {
	cfg := config.GetConfig()

	switch lusdGrad {
	case config.LusdHaGrad:
		return calcGradPlusQualiString == cfg.Hauptschulabschluss+cfg.NoQuali
	case config.LusdHaGradQuali:
		return calcGradPlusQualiString == cfg.Hauptschulabschluss+cfg.Quali || calcGradPlusQualiString == cfg.Realschulabschluss+cfg.NoQuali || calcGradPlusQualiString == cfg.Realschulabschluss+cfg.Quali || calcGradPlusQualiString == cfg.Versetztung11+cfg.NoQuali
	case config.LusdRaGrad:
		return calcGradPlusQualiString == cfg.Realschulabschluss+cfg.NoQuali
	case config.LusdRaGradQuali:
		return calcGradPlusQualiString == cfg.Realschulabschluss+cfg.Quali || calcGradPlusQualiString == cfg.Versetztung11+cfg.NoQuali
	default:
		return false
	}
}

// function checks, if first graduation (grad1) is lower than second graduation (grad2)
func isGraduationLower(grad1, grad2 string) bool {
	cfg := config.GetConfig()

	grad1Index, grad1Exists := cfg.GraduationOrder[grad1]
	grad2Index, grad2Exists := cfg.GraduationOrder[grad2]

	// Ensure both graduation tendencies exist in the defined order
	if !grad1Exists || !grad2Exists {
		return false
	}

	return grad1Index < grad2Index
}

func containsFailsInV11BioChPh(student global.Student) bool {
	for _, subject := range student.CalcResults.V11Results.FailsAndComps.MinorFail {
		if subject.Name == "CH" || subject.Name == "BIO" || subject.Name == "PH" {
			return true
		}
	}
	return false
}

func checkIfToFewBCoursesFor10thGrade(subjectList []global.SubjectStruc) bool {
	numberOfSufficientCourseLvls := 0

	for _, subject := range subjectList {
		switch {
		case subject.CourseLvl == "A":
			numberOfSufficientCourseLvls++
		case subject.CourseLvl == "B" && !strings.Contains(subject.LvlChange, "↧"):
			numberOfSufficientCourseLvls++
		case subject.CourseLvl == "C" && subject.LvlChange == "↥":
			numberOfSufficientCourseLvls++
		}
	}
	return numberOfSufficientCourseLvls < 2
}

// function checks, if BIO is needed for v11 tendency
// TODO: write Test
func checkBioWarning(subjects []global.SubjectStruc, class int) bool {
	var aLevelCoursesInMajors, eLevelCoursesInMinorsWithoutBio, eLevelBio int

	for _, subject := range subjects {
		switch {
		case subjectIsMajor(subject.SubjectAbbreviation) && subject.CourseLvl == "C":
			return false
		case subjectIsMajor(subject.SubjectAbbreviation) && subject.CourseLvl == "A":
			aLevelCoursesInMajors++
		case class <= 8 &&
			subjectIsNaWi(subject.SubjectAbbreviation) &&
			subject.SubjectAbbreviation != "BIO" &&
			subject.Grade <= 3:
			eLevelCoursesInMinorsWithoutBio++
		case class <= 8 &&
			subject.SubjectAbbreviation == "BIO" &&
			subject.Grade <= 3:
			eLevelBio++
		case subjectIsNaWi(subject.SubjectAbbreviation) &&
			subject.SubjectAbbreviation != "BIO" &&
			subject.CourseLvl == "E" &&
			subject.Grade <= 3:
			eLevelCoursesInMinorsWithoutBio++
		case subject.SubjectAbbreviation == "BIO" &&
			subject.CourseLvl == "E" &&
			subject.Grade <= 3:
			eLevelBio++
		}
	}

	return aLevelCoursesInMajors == 2 && eLevelCoursesInMinorsWithoutBio == 0 && eLevelBio == 1
}

func check8GradeNaWiCoursWarning(subjects []global.SubjectStruc) bool {
	var majorALevels int
	var naWiBetter3 int

	for _, subject := range subjects {
		if subjectIsMajor(subject.SubjectAbbreviation) && subject.CourseLvl == "A" {
			majorALevels++
		}
		if subjectIsNaWi(subject.SubjectAbbreviation) && subject.Grade < 3 {
			naWiBetter3++
		}
	}
	return majorALevels == 2 && naWiBetter3 == 0
}

// TODO: simplify and write Test
func AuditNonLUSDData(nonLUSDData global.NonLUSDData) []string {
	var warningLog []string

	if len(nonLUSDData.NonLUSDLvlChangeInfo) > 0 || len(nonLUSDData.NonLUSDAdditionalInfo) > 0 {
		warningLog = append(warningLog, "\n## Eingelesene 'nicht-LUSD' Daten")
	}

	// TODO: rewrite as own function
	if len(nonLUSDData.NonLUSDLvlChangeInfo) > 0 {
		warningLog = append(warningLog, "### Folgende Umstufungsinformationen aus externer Excel-Umstufungs-Datei konnten nicht zugeordnet werden:")
		notUsed := auditExternalLeveldataUsage(nonLUSDData.NonLUSDLvlChangeInfo)
		if len(notUsed) > 0 {
			for i := 0; i < len(notUsed); i++ {
				warningLog = append(warningLog, "- "+notUsed[i].Class+": "+notUsed[i].Name+" "+notUsed[i].SubjectFullName+" Umstufung vom "+notUsed[i].CurrentCourseLvl+" zum "+notUsed[i].FutureCourseLvl)
				misc.ConsoleLogPrintln(color.InYellow("    * ")+notUsed[i].Name+" ("+notUsed[i].Class+"): "+color.InCyan(notUsed[i].SubjectFullName)+" level change from "+color.InCyan(notUsed[i].CurrentCourseLvl)+" to "+color.InCyan(notUsed[i].FutureCourseLvl)+" could not be assigned to any student", true, true)
			}
			warningLog = append(warningLog, "")
		} else {
			warningLog = append(warningLog, "Es konnten alle Daten zugeordnet werden.\n") // ever in use?
		}
	}

	if len(nonLUSDData.NonLUSDAdditionalInfo) > 0 {
		warningLog = append(warningLog, "### Folgende Informationen (z.B. Elternwunschinformationen) aus externer Excel-Datei konnten nicht zugeordnet werden:")
		notUsed := auditExternalGradGoaldataUsage(nonLUSDData.NonLUSDAdditionalInfo)
		if len(notUsed) > 0 {
			for i := 0; i < len(notUsed); i++ {
				warningLog = append(warningLog, "- "+notUsed[i].Class+": "+notUsed[i].Lastname+", "+notUsed[i].Forename+" - Elternwunsch: "+notUsed[i].GraduationGoal)
				misc.ConsoleLogPrintln(color.InYellow("    * ")+notUsed[i].Lastname+", "+notUsed[i].Forename+" ("+notUsed[i].Class+"): "+"parent graduation goal "+color.InCyan(notUsed[i].GraduationGoal)+" could not be assigned to any student", true, true)
			}
			warningLog = append(warningLog, "")
		} else {
			warningLog = append(warningLog, "Es konnten alle Daten zugeordnet werden.\n")
		}
	}
	return warningLog
}

// function searches and returns external level change data that could not be matched to LUSD data
func auditExternalLeveldataUsage(externalLvLChangeData []global.ImportedLvlChanges) []global.ImportedLvlChanges {
	var studentsNotMatched []global.ImportedLvlChanges

	for i := 0; i < len(externalLvLChangeData); i++ {
		if !externalLvLChangeData[i].MatchFound {
			studentsNotMatched = append(studentsNotMatched, externalLvLChangeData[i])
		}
	}

	return studentsNotMatched
}

// function searches and returns external level change data that could not be matched to LUSD data
func auditExternalGradGoaldataUsage(externalGradGoalChangeData []global.ImportedAdditionalInfo) []global.ImportedAdditionalInfo {
	var studentsNotMatched []global.ImportedAdditionalInfo

	for i := 0; i < len(externalGradGoalChangeData); i++ {
		if !externalGradGoalChangeData[i].MatchFound {
			studentsNotMatched = append(studentsNotMatched, externalGradGoalChangeData[i])
		}
	}

	return studentsNotMatched
}

func applyCourceLvlDowngrade(student *global.Student) {
	for j, subject := range student.ReportCardInfo.Subjects {
		if subject.LvlChange == "↧" { // "normal" level changes (without parent requested changes)
			student.ReportCardInfo.Subjects[j].CourseLvl = misc.DetermineChangedLevel(subject.CourseLvl, "↧")
			student.ReportCardInfo.Subjects[j].Grade = 4 // change, so in mock reportcard its not a fail
		}
		if subject.LvlChangeNonLUSDData.ParentChangeRequest { // changes requested by parents
			student.ReportCardInfo.Subjects[j].CourseLvl = misc.DetermineChangedLevel(subject.CourseLvl, "↧")
		}
	}
}
