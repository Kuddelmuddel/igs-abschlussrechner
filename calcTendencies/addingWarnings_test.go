package calctendencies

import (
	"igs-abschlussrechner/global"
	"reflect"
	"testing"
)

func TestCheckIfToFewBCoursesFor10thGrade(t *testing.T) {
	testcases := []struct {
		description     string
		subjectTestList []global.SubjectStruc
		expectedResult  bool
	}{
		{
			description: "All A courses",
			subjectTestList: []global.SubjectStruc{
				{
					SubjectAbbreviation: "D",
					CourseLvl:           "A",
					LvlChange:           "",
				},
				{
					SubjectAbbreviation: "M",
					CourseLvl:           "A",
					LvlChange:           "",
				},
				{
					SubjectAbbreviation: "E",
					CourseLvl:           "A",
					LvlChange:           "",
				},
			},
			expectedResult: false,
		},
		{
			description: "All B courses",
			subjectTestList: []global.SubjectStruc{
				{
					SubjectAbbreviation: "D",
					CourseLvl:           "B",
					LvlChange:           "",
				},
				{
					SubjectAbbreviation: "M",
					CourseLvl:           "B",
					LvlChange:           "",
				},
				{
					SubjectAbbreviation: "E",
					CourseLvl:           "B",
					LvlChange:           "",
				},
			},
			expectedResult: false,
		},
		{
			description: "All C courses",
			subjectTestList: []global.SubjectStruc{
				{
					SubjectAbbreviation: "D",
					CourseLvl:           "C",
					LvlChange:           "",
				},
				{
					SubjectAbbreviation: "M",
					CourseLvl:           "C",
					LvlChange:           "",
				},
				{
					SubjectAbbreviation: "E",
					CourseLvl:           "C",
					LvlChange:           "",
				},
			},
			expectedResult: true,
		},
		{
			description: "ACC",
			subjectTestList: []global.SubjectStruc{
				{
					SubjectAbbreviation: "D",
					CourseLvl:           "A",
					LvlChange:           "",
				},
				{
					SubjectAbbreviation: "M",
					CourseLvl:           "C",
					LvlChange:           "",
				},
				{
					SubjectAbbreviation: "E",
					CourseLvl:           "C",
					LvlChange:           "",
				},
			},
			expectedResult: true,
		},
		{
			description: "BCC",
			subjectTestList: []global.SubjectStruc{
				{
					SubjectAbbreviation: "D",
					CourseLvl:           "B",
					LvlChange:           "",
				},
				{
					SubjectAbbreviation: "M",
					CourseLvl:           "C",
					LvlChange:           "",
				},
				{
					SubjectAbbreviation: "E",
					CourseLvl:           "C",
					LvlChange:           "",
				},
			},
			expectedResult: true,
		},
		{
			description: "BB↧C",
			subjectTestList: []global.SubjectStruc{
				{
					SubjectAbbreviation: "D",
					CourseLvl:           "B",
					LvlChange:           "",
				},
				{
					SubjectAbbreviation: "M",
					CourseLvl:           "B",
					LvlChange:           "↧",
				},
				{
					SubjectAbbreviation: "E",
					CourseLvl:           "C",
					LvlChange:           "",
				},
			},
			expectedResult: true,
		},
		{
			description: "BB↧B↧",
			subjectTestList: []global.SubjectStruc{
				{
					SubjectAbbreviation: "D",
					CourseLvl:           "B",
					LvlChange:           "",
				},
				{
					SubjectAbbreviation: "M",
					CourseLvl:           "B",
					LvlChange:           "↧",
				},
				{
					SubjectAbbreviation: "E",
					CourseLvl:           "B",
					LvlChange:           "↧",
				},
			},
			expectedResult: true,
		},
		{
			description: "BC↥C",
			subjectTestList: []global.SubjectStruc{
				{
					SubjectAbbreviation: "D",
					CourseLvl:           "B",
					LvlChange:           "",
				},
				{
					SubjectAbbreviation: "M",
					CourseLvl:           "C",
					LvlChange:           "↥",
				},
				{
					SubjectAbbreviation: "E",
					CourseLvl:           "C",
					LvlChange:           "",
				},
			},
			expectedResult: false,
		},
	}

	for _, test := range testcases {
		t.Run(test.description, func(t *testing.T) {
			// Call the function to be tested
			result := checkIfToFewBCoursesFor10thGrade(test.subjectTestList)
			// Compare the actual result with the expected result
			if !reflect.DeepEqual(result, test.expectedResult) {
				t.Errorf("For %s, expected %v but got %v", test.description, test.expectedResult, result)
			}
		})
	}
}
