package calctendencies

// type testCourseLvlCountStruc struct {
// 	description      string
// 	inputSubjectList []struc.SubjectStruc
// 	inputClassYear   int
// 	expectedResult   bool
// }

// var testcasesForV11SufficientCourseLVLs = []testCourseLvlCountStruc{
// 	{
// 		description: "Only A courses",
// 		inputSubjectList: []struc.SubjectStruc{
// 			{
// 				SubjectAbbreviation: "D",
// 				Grade:               2,
// 				CourseLvl:           "A"},
// 			{
// 				SubjectAbbreviation: "E",
// 				Grade:               2,
// 				CourseLvl:           "A"},
// 			{
// 				SubjectAbbreviation: "M",
// 				Grade:               2,
// 				CourseLvl:           "A"},
// 		},
// 		inputClassYear: 9,
// 		expectedResult: true},
// 	{
// 		description: "Two A courses but one E3 course to compensate",
// 		inputSubjectList: []struc.SubjectStruc{
// 			{
// 				SubjectAbbreviation: "D",
// 				Grade:               2,
// 				CourseLvl:           "B"},
// 			{
// 				SubjectAbbreviation: "E",
// 				Grade:               2,
// 				CourseLvl:           "A"},
// 			{
// 				SubjectAbbreviation: "M",
// 				Grade:               2,
// 				CourseLvl:           "A"},
// 			{
// 				SubjectAbbreviation: "CH",
// 				Grade:               2,
// 				CourseLvl:           "E"},
// 		},
// 		inputClassYear: 9,
// 		expectedResult: true},
// 	{
// 		description: "Two A courses and an A course in F which doesn't count",
// 		inputSubjectList: []struc.SubjectStruc{
// 			{
// 				SubjectAbbreviation: "D",
// 				Grade:               2,
// 				CourseLvl:           "A"},
// 			{
// 				SubjectAbbreviation: "E",
// 				Grade:               2,
// 				CourseLvl:           "B"},
// 			{
// 				SubjectAbbreviation: "M",
// 				Grade:               2,
// 				CourseLvl:           "A"},
// 			{
// 				SubjectAbbreviation: "F",
// 				Grade:               2,
// 				CourseLvl:           "A"},
// 		},
// 		inputClassYear: 9,
// 		expectedResult: false},
// 	{
// 		description: "Two A courses with no E course to compensate",
// 		inputSubjectList: []struc.SubjectStruc{
// 			{
// 				SubjectAbbreviation: "D",
// 				Grade:               2,
// 				CourseLvl:           "A"},
// 			{
// 				SubjectAbbreviation: "E",
// 				Grade:               2,
// 				CourseLvl:           "B"},
// 			{
// 				SubjectAbbreviation: "M",
// 				Grade:               2,
// 				CourseLvl:           "A"},
// 			{
// 				SubjectAbbreviation: "CH",
// 				Grade:               2,
// 				CourseLvl:           "G"},
// 			{
// 				SubjectAbbreviation: "BIO",
// 				Grade:               4,
// 				CourseLvl:           "E"},
// 		},
// 		inputClassYear: 9,
// 		expectedResult: false},
// 	{
// 		description: "Student with C courses",
// 		inputSubjectList: []struc.SubjectStruc{
// 			{
// 				SubjectAbbreviation: "D",
// 				Grade:               2,
// 				CourseLvl:           "C"},
// 			{
// 				SubjectAbbreviation: "E",
// 				Grade:               2,
// 				CourseLvl:           "A"},
// 			{
// 				SubjectAbbreviation: "M",
// 				Grade:               2,
// 				CourseLvl:           "A"},
// 		},
// 		inputClassYear: 9,
// 		expectedResult: false},
// 	{
// 		description: "Just one A course and two E courses (with grade 3 or better)",
// 		inputSubjectList: []struc.SubjectStruc{
// 			{
// 				SubjectAbbreviation: "D",
// 				Grade:               2,
// 				CourseLvl:           "A"},
// 			{
// 				SubjectAbbreviation: "E",
// 				Grade:               2,
// 				CourseLvl:           "B"},
// 			{
// 				SubjectAbbreviation: "M",
// 				Grade:               2,
// 				CourseLvl:           "B"},
// 			{
// 				SubjectAbbreviation: "CH",
// 				Grade:               2,
// 				CourseLvl:           "E"},
// 			{
// 				SubjectAbbreviation: "BIO",
// 				Grade:               2,
// 				CourseLvl:           "E"},
// 		},
// 		inputClassYear: 9,
// 		expectedResult: false},
// 	{
// 		description: "Can't use Subjects without course lvl to compensate for missing A courses",
// 		inputSubjectList: []struc.SubjectStruc{
// 			{
// 				SubjectAbbreviation: "D",
// 				Grade:               2,
// 				CourseLvl:           "A"},
// 			{
// 				SubjectAbbreviation: "E",
// 				Grade:               2,
// 				CourseLvl:           "B"},
// 			{
// 				SubjectAbbreviation: "M",
// 				Grade:               2,
// 				CourseLvl:           "A"},
// 			{
// 				SubjectAbbreviation: "GL",
// 				Grade:               1,
// 				CourseLvl:           ""},
// 			{
// 				SubjectAbbreviation: "SPO",
// 				Grade:               1,
// 				CourseLvl:           ""},
// 		},
// 		inputClassYear: 9,
// 		expectedResult: false},
// 	{
// 		description: "Two A courses in grade 8 and grade 3 or better in NaWi-subjects",
// 		inputSubjectList: []struc.SubjectStruc{
// 			{
// 				SubjectAbbreviation: "D",
// 				Grade:               2,
// 				CourseLvl:           "A"},
// 			{
// 				SubjectAbbreviation: "E",
// 				Grade:               2,
// 				CourseLvl:           "B"},
// 			{
// 				SubjectAbbreviation: "M",
// 				Grade:               2,
// 				CourseLvl:           "A"},
// 			{
// 				SubjectAbbreviation: "CH",
// 				Grade:               3,
// 				CourseLvl:           ""},
// 			{
// 				SubjectAbbreviation: "BIO",
// 				Grade:               3,
// 				CourseLvl:           ""},
// 		},
// 		inputClassYear: 8,
// 		expectedResult: true},
// 	{
// 		description: "Two A courses in grade 8 and grades less than 3 in NaWi-subjects",
// 		inputSubjectList: []struc.SubjectStruc{
// 			{
// 				SubjectAbbreviation: "D",
// 				Grade:               2,
// 				CourseLvl:           "A"},
// 			{
// 				SubjectAbbreviation: "E",
// 				Grade:               2,
// 				CourseLvl:           "B"},
// 			{
// 				SubjectAbbreviation: "M",
// 				Grade:               2,
// 				CourseLvl:           "A"},
// 			{
// 				SubjectAbbreviation: "CH",
// 				Grade:               4,
// 				CourseLvl:           ""},
// 			{
// 				SubjectAbbreviation: "BIO",
// 				Grade:               4,
// 				CourseLvl:           ""},
// 		},
// 		inputClassYear: 8,
// 		expectedResult: false},
// }

// func TestV11SufficientCourseLVLs(t *testing.T) {
// 	for _, testcase := range testcasesForV11SufficientCourseLVLs {
// 		t.Run(testcase.description, func(t *testing.T) {
// 			result := v11SufficientCourseLVLs(testcase.inputSubjectList, testcase.inputClassYear)
// 			if result != testcase.expectedResult {
// 				t.Errorf("Expected %T, got %T./n", testcase.expectedResult, result)
// 			}
// 		})
// 	}
// }

// var v11EinerCase = struc.Student{
// 	DateOfFile:        "",
// 	School:            "",
// 	YearsOfData:       "",
// 	Term:              "",
// 	ClassTeacher:      "",
// 	SubstitudeTeacher: "-",
// 	Class:             "",
// 	ClassYear:         9,
// 	ClassChar:         "",
// 	Name:              "V11, EinerCase",
// 	ReportCardInfo: struc.ReportCardInformation{
// 		AV:           2,
// 		SV:           3,
// 		AbsentsTotal: struc.AbsentStruc{},
// 		Subjects: []struc.SubjectStruc{
// 			{
// 				SubjectAbbreviation:        "D",
// 				SubjectFullName:            "",
// 				Teacher:                    "",
// 				Absents:                    struc.AbsentStruc{},
// 				CourseLvl:                  "A",
// 				LvlChange:                  "",
// 				Grade:                      1,
// 				GradeJump:                  false,
// 				GradeNotIncludingExamGrade: 0,
// 				AlternativeGrade:           "",
// 				Fail:                       "",
// 				CompenstaionBy:             "",
// 				ExamTaken:                  false,
// 				LvlChangeExternData:        struc.ExternalLvlChangeInfoStruc{},
// 			},
// 			{
// 				SubjectAbbreviation:        "E",
// 				SubjectFullName:            "",
// 				Teacher:                    "",
// 				Absents:                    struc.AbsentStruc{},
// 				CourseLvl:                  "A",
// 				LvlChange:                  "",
// 				Grade:                      1,
// 				GradeJump:                  false,
// 				GradeNotIncludingExamGrade: 0,
// 				AlternativeGrade:           "",
// 				Fail:                       "",
// 				CompenstaionBy:             "",
// 				ExamTaken:                  false,
// 				LvlChangeExternData:        struc.ExternalLvlChangeInfoStruc{},
// 			},
// 			{
// 				SubjectAbbreviation:        "M",
// 				SubjectFullName:            "",
// 				Teacher:                    "",
// 				Absents:                    struc.AbsentStruc{},
// 				CourseLvl:                  "A",
// 				LvlChange:                  "",
// 				Grade:                      1,
// 				GradeJump:                  false,
// 				GradeNotIncludingExamGrade: 0,
// 				AlternativeGrade:           "",
// 				Fail:                       "",
// 				CompenstaionBy:             "",
// 				ExamTaken:                  false,
// 				LvlChangeExternData:        struc.ExternalLvlChangeInfoStruc{},
// 			},
// 			{
// 				SubjectAbbreviation:        "GL",
// 				SubjectFullName:            "",
// 				Teacher:                    "",
// 				Absents:                    struc.AbsentStruc{},
// 				CourseLvl:                  "",
// 				LvlChange:                  "",
// 				Grade:                      1,
// 				GradeJump:                  false,
// 				GradeNotIncludingExamGrade: 0,
// 				AlternativeGrade:           "",
// 				Fail:                       "",
// 				CompenstaionBy:             "",
// 				ExamTaken:                  false,
// 				LvlChangeExternData:        struc.ExternalLvlChangeInfoStruc{},
// 			},
// 			{
// 				SubjectAbbreviation:        "BIO",
// 				SubjectFullName:            "",
// 				Teacher:                    "",
// 				Absents:                    struc.AbsentStruc{},
// 				CourseLvl:                  "E",
// 				LvlChange:                  "",
// 				Grade:                      1,
// 				GradeJump:                  false,
// 				GradeNotIncludingExamGrade: 0,
// 				AlternativeGrade:           "",
// 				Fail:                       "",
// 				CompenstaionBy:             "",
// 				ExamTaken:                  false,
// 				LvlChangeExternData:        struc.ExternalLvlChangeInfoStruc{},
// 			},
// 			{
// 				SubjectAbbreviation:        "CH",
// 				SubjectFullName:            "",
// 				Teacher:                    "",
// 				Absents:                    struc.AbsentStruc{},
// 				CourseLvl:                  "E",
// 				LvlChange:                  "",
// 				Grade:                      1,
// 				GradeJump:                  false,
// 				GradeNotIncludingExamGrade: 0,
// 				AlternativeGrade:           "",
// 				Fail:                       "",
// 				CompenstaionBy:             "",
// 				ExamTaken:                  false,
// 				LvlChangeExternData:        struc.ExternalLvlChangeInfoStruc{},
// 			},
// 			{
// 				SubjectAbbreviation:        "F",
// 				SubjectFullName:            "",
// 				Teacher:                    "",
// 				Absents:                    struc.AbsentStruc{},
// 				CourseLvl:                  "A",
// 				LvlChange:                  "",
// 				Grade:                      1,
// 				GradeJump:                  false,
// 				GradeNotIncludingExamGrade: 0,
// 				AlternativeGrade:           "",
// 				Fail:                       "",
// 				CompenstaionBy:             "",
// 				ExamTaken:                  false,
// 				LvlChangeExternData:        struc.ExternalLvlChangeInfoStruc{},
// 			},
// 			{
// 				SubjectAbbreviation:        "AL",
// 				SubjectFullName:            "",
// 				Teacher:                    "",
// 				Absents:                    struc.AbsentStruc{},
// 				CourseLvl:                  "",
// 				LvlChange:                  "",
// 				Grade:                      1,
// 				GradeJump:                  false,
// 				GradeNotIncludingExamGrade: 0,
// 				AlternativeGrade:           "",
// 				Fail:                       "",
// 				CompenstaionBy:             "",
// 				ExamTaken:                  false,
// 				LvlChangeExternData:        struc.ExternalLvlChangeInfoStruc{},
// 			},
// 			{
// 				SubjectAbbreviation:        "KU",
// 				SubjectFullName:            "",
// 				Teacher:                    "",
// 				Absents:                    struc.AbsentStruc{},
// 				CourseLvl:                  "",
// 				LvlChange:                  "",
// 				Grade:                      1,
// 				GradeJump:                  false,
// 				GradeNotIncludingExamGrade: 0,
// 				AlternativeGrade:           "",
// 				Fail:                       "",
// 				CompenstaionBy:             "",
// 				ExamTaken:                  false,
// 				LvlChangeExternData:        struc.ExternalLvlChangeInfoStruc{},
// 			},
// 			{
// 				SubjectAbbreviation:        "SPO",
// 				SubjectFullName:            "",
// 				Teacher:                    "",
// 				Absents:                    struc.AbsentStruc{},
// 				CourseLvl:                  "",
// 				LvlChange:                  "",
// 				Grade:                      1,
// 				GradeJump:                  false,
// 				GradeNotIncludingExamGrade: 0,
// 				AlternativeGrade:           "",
// 				Fail:                       "",
// 				CompenstaionBy:             "",
// 				ExamTaken:                  false,
// 				LvlChangeExternData:        struc.ExternalLvlChangeInfoStruc{},
// 			},
// 			{
// 				SubjectAbbreviation:        "ETHI",
// 				SubjectFullName:            "",
// 				Teacher:                    "",
// 				Absents:                    struc.AbsentStruc{},
// 				CourseLvl:                  "",
// 				LvlChange:                  "",
// 				Grade:                      1,
// 				GradeJump:                  false,
// 				GradeNotIncludingExamGrade: 0,
// 				AlternativeGrade:           "",
// 				Fail:                       "",
// 				CompenstaionBy:             "",
// 				ExamTaken:                  false,
// 				LvlChangeExternData:        struc.ExternalLvlChangeInfoStruc{},
// 			},
// 			{
// 				SubjectAbbreviation:        "WPU",
// 				SubjectFullName:            "",
// 				Teacher:                    "",
// 				Absents:                    struc.AbsentStruc{},
// 				CourseLvl:                  "",
// 				LvlChange:                  "",
// 				Grade:                      1,
// 				GradeJump:                  false,
// 				GradeNotIncludingExamGrade: 0,
// 				AlternativeGrade:           "",
// 				Fail:                       "",
// 				CompenstaionBy:             "",
// 				ExamTaken:                  false,
// 				LvlChangeExternData:        struc.ExternalLvlChangeInfoStruc{},
// 			},
// 		},
// 		UnratedSubjects: []struc.SubjectStruc{},
// 		FinalExams:      struc.ExamsStruc{},
// 		GradeJumps:      false,
// 	},
// 	Tendency:      struc.TendencyStruc{},
// 	TendencyCheck: struc.TendencyCheckStruc{},
// 	ProtocolInfo:  struc.ProtocolStruc{},
// 	History:       struc.StudentHistory{},
// 	Warnings:      struc.WarningStruc{},
// }
