package calctendencies

import (
	config "igs-abschlussrechner/config"
	"igs-abschlussrechner/global"
	"math"
)

// TODO: improve and simplify function
func CalculatingAllAverages(student *global.Student) {
	cfg := config.GetConfig()
	student.CalcResults.V11Results.Averages = calcAveragesOfSubjects(student.ReportCardInfo.Subjects, cfg.Versetztung11, false)
	student.CalcResults.RAResults.Averages = calcAveragesOfSubjects(student.ReportCardInfo.Subjects, cfg.Realschulabschluss, false)
	haIncrease := student.Term == 1 || (student.Term == 2 && student.ReportCardInfo.FinalExams.TookExams)
	haAverageSubjectList := student.ReportCardInfo.Subjects
	if student.ClassYear == 9 && student.Term == 2 && student.ReportCardInfo.FinalExams.TookExams { // adding ha presentation list to subject list for average calculation
		hAPresentation := findPresentationSubject(student.ReportCardInfo.FinalExams.ExamSubjects)
		if hAPresentation.ExamTaken {
			haAverageSubjectList = append(haAverageSubjectList, hAPresentation)
		}
	}
	student.CalcResults.HAResults.Averages = calcAveragesOfSubjects(haAverageSubjectList, cfg.Hauptschulabschluss, haIncrease)
}

func findPresentationSubject(examSubjectList []global.ExamSubjectStruct) global.SubjectStruc {
	var haPresentationSubject global.SubjectStruc
	for _, examSubject := range examSubjectList {
		if examSubject.SubjectFullName == "Hauptschulprojektprüfung" {
			haPresentationSubject.Grade = examSubject.ExamGrade
			haPresentationSubject.CourseLvl = "P" // add "P" as course Level to prevent haIncrease when calculating average
			haPresentationSubject.ExamTaken = true
			return haPresentationSubject
		}
	}
	return haPresentationSubject
}

func calcAveragesOfSubjects(subjects []global.SubjectStruc, grad string, haIncrease bool) global.AverageStruct {
	var averages global.AverageStruct
	cfg := config.GetConfig()

	switch grad {
	case cfg.Versetztung11:
		gradeSlice := collectGradesAllSubjects(subjects, grad, false)
		averages.AverageAllSubjects = calcTruncatedAverage(gradeSlice)
	case cfg.Realschulabschluss:
		gradeSlice := collectGradesAllSubjects(subjects, grad, false)
		gradeSliceDME := collectGradesDME(subjects, grad)
		gradeSliceAllButDME := collectAllGradesButDME(subjects, grad)
		gradeSliceDMEandCHorPH := collectDMEandCHorPHForNawiQuali(subjects, grad)
		gradeSliceAllButDMEandCHorPH := collectAllButDMEandCHorPHForNawiQuali(subjects, grad)

		averages.AverageAllSubjects = calcTruncatedAverage(gradeSlice)
		averages.AverageDME = calcTruncatedAverage(gradeSliceDME)
		averages.AverageAllButDME = calcTruncatedAverage(gradeSliceAllButDME)
		averages.AverageNaWiQualiDMEandPHorCH = calcTruncatedAverage(gradeSliceDMEandCHorPH)
		averages.AverageNaWiQualiRestOfSubjects = calcTruncatedAverage(gradeSliceAllButDMEandCHorPH)

		if !(averages.AverageDME <= 3 && averages.AverageAllButDME <= 3) && (averages.AverageNaWiQualiDMEandPHorCH < 3 && averages.AverageNaWiQualiRestOfSubjects < 3) {
			averages.BestAverageDME = averages.AverageNaWiQualiDMEandPHorCH
			averages.BestAverageRestOfSubjects = averages.AverageNaWiQualiRestOfSubjects
		} else {
			averages.BestAverageDME = averages.AverageDME
			averages.BestAverageRestOfSubjects = averages.AverageAllButDME
		}

	case cfg.Hauptschulabschluss:
		gradeSlice := collectGradesAllSubjects(subjects, grad, haIncrease)
		averages.AverageAllSubjects = calcTruncatedAverage(gradeSlice)
	}

	return averages
}

// function takes a slice of positive integers and calculates the average of those integers. Function returns the flat rounded float with one decimal place. If slice is empty function returns -1. If input slice contains a negative integer, function return -99.
func calcTruncatedAverage(intSlice []int) float64 {
	if len(intSlice) == 0 {
		return -1
	}

	var addup int
	for i := 0; i < len(intSlice); i++ {
		if intSlice[i] < 0 {
			return -1
		}
		addup += intSlice[i]
	}

	average := float64(addup) / float64(len(intSlice))
	flatRoundedAverage := math.Trunc(average*10) / 10
	return flatRoundedAverage
}

func collectGradesAllSubjects(subjectList []global.SubjectStruc, grad string, haIncrease bool) []int {
	var gradeSlice []int

	for i := 0; i < len(subjectList); i++ {
		gradeSlice = append(gradeSlice, ConvertGrade(subjectList[i], grad, haIncrease))
		if subjectList[i].ExamTaken { // if exam taken, append again (counts double)
			gradeSlice = append(gradeSlice, ConvertGrade(subjectList[i], grad, haIncrease))
		}
	}

	return gradeSlice
}

func collectGradesDME(subjectList []global.SubjectStruc, grad string) []int {
	var gradeSlice []int

	for i := 0; i < len(subjectList); i++ {
		if subjectList[i].SubjectAbbreviation == "D" || subjectList[i].SubjectAbbreviation == "M" || subjectList[i].SubjectAbbreviation == "E" {
			gradeSlice = append(gradeSlice, ConvertGrade(subjectList[i], grad, false))
		}
	}

	return gradeSlice
}

func collectAllGradesButDME(subjectList []global.SubjectStruc, grad string) []int {
	var gradeSlice []int

	for i := 0; i < len(subjectList); i++ {
		if subjectList[i].SubjectAbbreviation != "D" && subjectList[i].SubjectAbbreviation != "M" && subjectList[i].SubjectAbbreviation != "E" {
			gradeSlice = append(gradeSlice, ConvertGrade(subjectList[i], grad, false))
		}
	}

	return gradeSlice
}

func collectDMEandCHorPHForNawiQuali(subjects []global.SubjectStruc, grad string) []int {
	var filteredGrades []int
	var ph, ch global.SubjectStruc

	// Criteria for filtering subjects
	isDME := map[string]bool{"D": true, "M": true, "E": true}

	for _, subject := range subjects {
		if isDME[subject.SubjectAbbreviation] {
			filteredGrades = append(filteredGrades, ConvertGrade(subject, grad, false))
		}

		// Keep track of "PH" and "CH"
		if subject.SubjectAbbreviation == "PH" {
			ph = subject
		}
		if subject.SubjectAbbreviation == "CH" {
			ch = subject
		}
	}

	// Append either "PH" or "CH" based on the lower grade
	convertedCHGrade := ConvertGrade(ch, grad, false)
	convertedPHGrade := ConvertGrade(ph, grad, false)
	switch {
	case ph.Grade == 0 && ch.Grade == 0:
		return filteredGrades
	case ph.Grade == 0 && ch.Grade != 0, ph.Grade >= convertedCHGrade:
		filteredGrades = append(filteredGrades, convertedCHGrade)
	case ph.Grade != 0 && ch.Grade == 0, convertedPHGrade < convertedCHGrade:
		filteredGrades = append(filteredGrades, convertedPHGrade)
	}

	return filteredGrades
}

func collectAllButDMEandCHorPHForNawiQuali(subjects []global.SubjectStruc, grad string) []int {
	// Criteria for filtering subjects
	isDMEandPHorCH := map[string]bool{"D": true, "M": true, "E": true, "PH": true, "CH": true}
	var filteredGrades []int
	var ph, ch global.SubjectStruc

	for _, subject := range subjects {
		if !isDMEandPHorCH[subject.SubjectAbbreviation] {
			filteredGrades = append(filteredGrades, ConvertGrade(subject, grad, false))
		}

		// Keep track of "PH" and "CH"
		if subject.SubjectAbbreviation == "PH" {
			ph = subject
		}
		if subject.SubjectAbbreviation == "CH" {
			ch = subject
		}
	}

	// Append the grade of the subject with the worse grade between "PH" and "CH"
	convertedCHGrade := ConvertGrade(ch, grad, false)
	convertedPHGrade := ConvertGrade(ph, grad, false)
	switch {
	case ph.Grade == 0, ch.Grade == 0: // if one of both is not present (unconverted =0), nothing is added (case the remaining subject is in NawiQuali already)
		return filteredGrades
	case convertedPHGrade <= convertedCHGrade:
		filteredGrades = append(filteredGrades, convertedCHGrade)
	case convertedPHGrade > convertedCHGrade:
		filteredGrades = append(filteredGrades, convertedPHGrade)
	}

	return filteredGrades
}
