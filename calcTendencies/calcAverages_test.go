package calctendencies

import (
	config "igs-abschlussrechner/config"
	"igs-abschlussrechner/global"
	"math"

	"testing"
)

func TestCalcAveragesOfSubjects(t *testing.T) {
	cfg := config.LoadGlobalConfigVariables("../")
	testcases := []struct {
		description    string
		inputSubjects  []global.SubjectStruc
		haIncrease     bool
		inputGrad      string
		expectedResult global.AverageStruct
	}{
		{
			description:    "empty subject slice",
			inputSubjects:  []global.SubjectStruc{},
			inputGrad:      "",
			expectedResult: global.AverageStruct{}},
		{
			description: "averages for Versetzung11",
			inputSubjects: []global.SubjectStruc{
				{
					SubjectAbbreviation: "D",
					CourseLvl:           "B",
					Grade:               2, // 3
					ExamTaken:           true,
				},
				{
					SubjectAbbreviation: "M",
					CourseLvl:           "A",
					Grade:               3,
					ExamTaken:           true,
				},
				{
					SubjectAbbreviation: "E",
					CourseLvl:           "A",
					Grade:               3,
					ExamTaken:           true,
				},
				{
					SubjectAbbreviation: "GL",
					CourseLvl:           "",
					Grade:               2,
					ExamTaken:           true,
				},
				{
					SubjectAbbreviation: "PH",
					CourseLvl:           "E",
					Grade:               3,
					ExamTaken:           false,
				},
				{
					SubjectAbbreviation: "CH",
					CourseLvl:           "G",
					Grade:               3, // 5
					ExamTaken:           false,
				},
				{
					SubjectAbbreviation: "AL",
					CourseLvl:           "",
					Grade:               3,
					ExamTaken:           false,
				},
				{
					SubjectAbbreviation: "KU",
					CourseLvl:           "",
					Grade:               2,
					ExamTaken:           false,
				},
				{
					SubjectAbbreviation: "SPO",
					CourseLvl:           "",
					Grade:               1,
					ExamTaken:           false,
				},
				{
					SubjectAbbreviation: "REV",
					CourseLvl:           "",
					Grade:               3,
					ExamTaken:           false,
				},
				{
					SubjectAbbreviation: "WPU",
					CourseLvl:           "",
					Grade:               3,
					ExamTaken:           false,
				},
			},
			inputGrad: cfg.Versetztung11,
			expectedResult: global.AverageStruct{
				AverageAllSubjects: 2.8,
			}},
		{
			description: "averages for Realschulabschluss",
			inputSubjects: []global.SubjectStruc{
				{
					SubjectAbbreviation: "D",
					CourseLvl:           "B",
					Grade:               2,
					ExamTaken:           true,
				},
				{
					SubjectAbbreviation: "M",
					CourseLvl:           "A",
					Grade:               3, // 2
					ExamTaken:           true,
				},
				{
					SubjectAbbreviation: "E",
					CourseLvl:           "C",
					Grade:               3, // 4
					ExamTaken:           true,
				},
				{
					SubjectAbbreviation: "GL",
					CourseLvl:           "",
					Grade:               3,
					ExamTaken:           false,
				},
				{
					SubjectAbbreviation: "PH",
					CourseLvl:           "E",
					Grade:               2,
					ExamTaken:           true,
				},
				{
					SubjectAbbreviation: "CH",
					CourseLvl:           "G",
					Grade:               3, // 4
					ExamTaken:           false,
				},
				{
					SubjectAbbreviation: "AL",
					CourseLvl:           "",
					Grade:               4,
					ExamTaken:           false,
				},
				{
					SubjectAbbreviation: "KU",
					CourseLvl:           "",
					Grade:               2,
					ExamTaken:           false,
				},
				{
					SubjectAbbreviation: "SPO",
					CourseLvl:           "",
					Grade:               3,
					ExamTaken:           false,
				},
				{
					SubjectAbbreviation: "REV",
					CourseLvl:           "",
					Grade:               2,
					ExamTaken:           false,
				},
				{
					SubjectAbbreviation: "WPU",
					CourseLvl:           "",
					Grade:               3,
					ExamTaken:           false,
				},
			}, inputGrad: cfg.Realschulabschluss,
			expectedResult: global.AverageStruct{
				AverageDME:                     2.6,
				AverageAllButDME:               2.8,
				AverageAllSubjects:             2.7,
				AverageNaWiQualiDMEandPHorCH:   2.5,
				AverageNaWiQualiRestOfSubjects: 3.0,
				BestAverageDME:                 2.6,
				BestAverageRestOfSubjects:      2.8,
			}},
		{
			description: "averages for Hauptschulabschluss",
			inputSubjects: []global.SubjectStruc{
				{
					SubjectAbbreviation: "D",
					CourseLvl:           "C",
					Grade:               2,
					ExamTaken:           true,
				},
				{
					SubjectAbbreviation: "M",
					CourseLvl:           "B",
					Grade:               3, // 2
					ExamTaken:           true,
				},
				{
					SubjectAbbreviation: "E",
					CourseLvl:           "C",
					Grade:               3,
					ExamTaken:           true,
				},
				{
					SubjectAbbreviation: "GL",
					CourseLvl:           "",
					Grade:               3, // 2
					ExamTaken:           false,
				},
				{
					SubjectAbbreviation: "PH",
					CourseLvl:           "E",
					Grade:               4, // 3
					ExamTaken:           false,
				},
				{
					SubjectAbbreviation: "CH",
					CourseLvl:           "G",
					Grade:               3,
					ExamTaken:           false,
				},
				{
					SubjectAbbreviation: "AL",
					CourseLvl:           "",
					Grade:               4, // 3
					ExamTaken:           false,
				},
				{
					SubjectAbbreviation: "KU",
					CourseLvl:           "",
					Grade:               3, // 2
					ExamTaken:           false,
				},
				{
					SubjectAbbreviation: "SPO",
					CourseLvl:           "",
					Grade:               2, // 1
					ExamTaken:           false,
				},
				{
					SubjectAbbreviation: "REV",
					CourseLvl:           "",
					Grade:               3, // 2
					ExamTaken:           false,
				},
				{
					SubjectAbbreviation: "WPU",
					CourseLvl:           "",
					Grade:               4, // 3
					ExamTaken:           false,
				},
				{
					SubjectAbbreviation: "Hauptschulprojektprüfung",
					CourseLvl:           "P",
					Grade:               2,
					ExamTaken:           true,
				},
			},
			haIncrease: true,
			inputGrad:  cfg.Hauptschulabschluss,
			expectedResult: global.AverageStruct{
				AverageAllSubjects: 2.3,
			}},
		// TODO: add testcases:
		// HA, RA, V11 without Exam (all false)
	}

	for _, test := range testcases {
		t.Run(test.description, func(t *testing.T) {
			result := calcAveragesOfSubjects(test.inputSubjects, test.inputGrad, test.haIncrease)
			if result != test.expectedResult {
				t.Errorf("For %s, expected %f but got %f", test.description, test.expectedResult, result)
			}
		})
	}
}

func TestCalcTruncatedAverage(t *testing.T) {
	testcases := []struct {
		description    string
		input          []int
		expectedResult float64
	}{
		{"empty slice", []int{}, -1.0},
		{"slice with negative integers", []int{1, 4, -2}, -99},
		{"test case 1", []int{1, 2, 3, 4, 5, 6, 7, 8, 9}, 5.0},
		{"test case 2", []int{1, 1, 1, 1, 1, 1, 1, 1, 1}, 1.0},
		{"test case 3", []int{2, 2, 3, 5, 4, 1}, 2.8},
		{"test case 4", []int{1}, 1.0},
	}

	const tolerance = 0.0001 // Define a tolerance level for floating-point comparison

	for _, test := range testcases {
		t.Run(test.description, func(t *testing.T) {
			result := calcTruncatedAverage(test.input)
			if !floatEquals(result, test.expectedResult, tolerance) {
				t.Errorf("For %s, expected %f but got %f", test.description, test.expectedResult, result)
			}
		})
	}
}

// Helper function to compare floating-point numbers with tolerance
func floatEquals(a, b, tolerance float64) bool {
	return math.Abs(a-b) < tolerance
}
