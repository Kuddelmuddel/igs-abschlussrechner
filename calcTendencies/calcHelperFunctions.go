package calctendencies

import (
	config "igs-abschlussrechner/config"
	"igs-abschlussrechner/global"
)

// function checks if a subject is a major
func subjectIsMajor(str string) bool {
	majorSubjects := map[string]bool{"E": true, "D": true, "M": true, "GL": true}
	return majorSubjects[str]
}

// function checks if a subject is a NaWi subject
func subjectIsNaWi(str string) bool {
	naWis := map[string]bool{"CH": true, "PH": true, "BIO": true}
	return naWis[str]
}

// ConvertGrade converts all grades to grades for v11, "Realschulabschluss" and "Hauptschulabschluss" graduation, respectively
func ConvertGrade(course global.SubjectStruc, targetGraduation string, haIncrease bool) int {
	cfg := config.GetConfig()

	if course.Grade < 1 || course.Grade > 6 {
		return -1
	}

	switch targetGraduation {
	case cfg.Versetztung11:
		convertedGrade := handleVersetztung11(course)
		return makeGradeValidAgain(convertedGrade)
	case cfg.Realschulabschluss:
		convertedGrade := handleRealschulabschluss(course)
		return makeGradeValidAgain(convertedGrade)
	case cfg.Hauptschulabschluss, cfg.OhneAbschluss:
		convertedGrade := handleHauptschulabschluss(course, haIncrease)
		return makeGradeValidAgain(convertedGrade)
	default:
		return course.Grade
	}
}

func handleVersetztung11(course global.SubjectStruc) int {
	switch course.CourseLvl {
	case "B":
		return course.Grade + 1
	case "C", "G":
		return course.Grade + 2
	default:
		return course.Grade
	}
}

func handleRealschulabschluss(course global.SubjectStruc) int {
	switch course.CourseLvl {
	case "A":
		return course.Grade - 1
	case "C", "G":
		return course.Grade + 1
	default:
		return course.Grade
	}
}

func handleHauptschulabschluss(course global.SubjectStruc, haIncrease bool) int {
	switch {
	case course.CourseLvl == "A":
		return course.Grade - 2
	case course.CourseLvl == "B", course.CourseLvl == "E", course.CourseLvl == "" && haIncrease:
		return course.Grade - 1
	default:
		return course.Grade
	}
}

// makeGradeValidAgain checks if converted grades are in range 1-6 and corrects them if they are not
func makeGradeValidAgain(grade int) int {
	if grade > 6 {
		return 6
	} else if grade < 1 {
		return 1
	}
	return grade
}

// function checks, if a subject will be FLD (fachleistungsdifferenziert) in a later school year
func isFuturFLD(str string) bool {
	cfg := config.GetConfig()
	for _, a := range cfg.FuturFLD {
		if a == str {
			return true
		}
	}
	return false
}
