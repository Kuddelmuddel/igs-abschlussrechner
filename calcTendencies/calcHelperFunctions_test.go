package calctendencies

import (
	"igs-abschlussrechner/config"
	"igs-abschlussrechner/global"
	"testing"
)

func TestConvertGrades(t *testing.T) {
	cfg := config.LoadGlobalConfigVariables("../")

	testCase := []struct {
		description     string
		inputSubject    global.SubjectStruc
		inputGrad       string
		inputHAIncrease bool
		expectedResult  int
	}{
		{
			description:     "invalid input",
			inputSubject:    global.SubjectStruc{CourseLvl: "", Grade: 0},
			inputGrad:       cfg.Hauptschulabschluss,
			inputHAIncrease: false,
			expectedResult:  -1,
		},
		// Hauptschulabschluss cases
		{
			description:     "A course Hauptschulabschluss, no HA increase",
			inputSubject:    global.SubjectStruc{CourseLvl: "A", Grade: 4},
			inputGrad:       cfg.Hauptschulabschluss,
			inputHAIncrease: false,
			expectedResult:  2,
		},
		{
			description:     "A course Hauptschulabschluss, with HA increase",
			inputSubject:    global.SubjectStruc{CourseLvl: "A", Grade: 4},
			inputGrad:       cfg.Hauptschulabschluss,
			inputHAIncrease: true,
			expectedResult:  2,
		},
		{
			description:     "A course Hauptschulabschluss, no HA increase - no increase possible",
			inputSubject:    global.SubjectStruc{CourseLvl: "A", Grade: 1},
			inputGrad:       cfg.Hauptschulabschluss,
			inputHAIncrease: false,
			expectedResult:  1,
		},
		{
			description:     "A course Hauptschulabschluss, with HA increase - no increase possible",
			inputSubject:    global.SubjectStruc{CourseLvl: "A", Grade: 1},
			inputGrad:       cfg.Hauptschulabschluss,
			inputHAIncrease: true,
			expectedResult:  1,
		},
		{
			description:     "B course Hauptschulabschluss, no HA increase",
			inputSubject:    global.SubjectStruc{CourseLvl: "B", Grade: 4},
			inputGrad:       cfg.Hauptschulabschluss,
			inputHAIncrease: false,
			expectedResult:  3,
		},
		{
			description:     "B course Hauptschulabschluss, with HA increase",
			inputSubject:    global.SubjectStruc{CourseLvl: "B", Grade: 4},
			inputGrad:       cfg.Hauptschulabschluss,
			inputHAIncrease: true,
			expectedResult:  3,
		},
		{
			description:     "B course Hauptschulabschluss, no HA increase - no increase possible",
			inputSubject:    global.SubjectStruc{CourseLvl: "B", Grade: 1},
			inputGrad:       cfg.Hauptschulabschluss,
			inputHAIncrease: false,
			expectedResult:  1,
		},
		{
			description:     "B course Hauptschulabschluss, with HA increase - no increase possible",
			inputSubject:    global.SubjectStruc{CourseLvl: "B", Grade: 1},
			inputGrad:       cfg.Hauptschulabschluss,
			inputHAIncrease: true,
			expectedResult:  1,
		},
		{
			description:     "C course Hauptschulabschluss, no HA increase",
			inputSubject:    global.SubjectStruc{CourseLvl: "C", Grade: 4},
			inputGrad:       cfg.Hauptschulabschluss,
			inputHAIncrease: false,
			expectedResult:  4,
		},
		{
			description:     "C course Hauptschulabschluss, with HA increase",
			inputSubject:    global.SubjectStruc{CourseLvl: "C", Grade: 4},
			inputGrad:       cfg.Hauptschulabschluss,
			inputHAIncrease: true,
			expectedResult:  4,
		},
		{
			description:     "no course levels, Hauptschulabschluss, no HA increase",
			inputSubject:    global.SubjectStruc{CourseLvl: "", Grade: 3},
			inputGrad:       cfg.Hauptschulabschluss,
			inputHAIncrease: false,
			expectedResult:  3,
		},
		{
			description:     "no course level, Hauptschulabschluss, with HA increase",
			inputSubject:    global.SubjectStruc{CourseLvl: "", Grade: 3},
			inputGrad:       cfg.Hauptschulabschluss,
			inputHAIncrease: true,
			expectedResult:  2,
		},
		{
			description:     "G course Hauptschulabschluss, no HA increase",
			inputSubject:    global.SubjectStruc{CourseLvl: "G", Grade: 4},
			inputGrad:       cfg.Hauptschulabschluss,
			inputHAIncrease: false,
			expectedResult:  4,
		},
		{
			description:     "G course Hauptschulabschluss, with HA increase",
			inputSubject:    global.SubjectStruc{CourseLvl: "G", Grade: 4},
			inputGrad:       cfg.Hauptschulabschluss,
			inputHAIncrease: true,
			expectedResult:  4,
		},
		{
			description:     "E course Hauptschulabschluss, no HA increase",
			inputSubject:    global.SubjectStruc{CourseLvl: "E", Grade: 4},
			inputGrad:       cfg.Hauptschulabschluss,
			inputHAIncrease: false,
			expectedResult:  3,
		},
		{
			description:     "E course Hauptschulabschluss, with HA increase",
			inputSubject:    global.SubjectStruc{CourseLvl: "E", Grade: 4},
			inputGrad:       cfg.Hauptschulabschluss,
			inputHAIncrease: true,
			expectedResult:  3,
		},
		// Realschulabschluss cases
		{
			description:     "A course Realschulabschluss",
			inputSubject:    global.SubjectStruc{CourseLvl: "A", Grade: 4},
			inputGrad:       cfg.Realschulabschluss,
			inputHAIncrease: false,
			expectedResult:  3,
		},
		{
			description:     "A course Realschulabschluss - no increase possible",
			inputSubject:    global.SubjectStruc{CourseLvl: "A", Grade: 1},
			inputGrad:       cfg.Realschulabschluss,
			inputHAIncrease: false,
			expectedResult:  1,
		},
		{
			description:     "B course Realschulabschluss",
			inputSubject:    global.SubjectStruc{CourseLvl: "B", Grade: 4},
			inputGrad:       cfg.Realschulabschluss,
			inputHAIncrease: false,
			expectedResult:  4,
		},
		{
			description:     "C course Realschulabschluss",
			inputSubject:    global.SubjectStruc{CourseLvl: "C", Grade: 4},
			inputGrad:       cfg.Realschulabschluss,
			inputHAIncrease: false,
			expectedResult:  5,
		},
		{
			description:     "C course Realschulabschluss no decrease possible",
			inputSubject:    global.SubjectStruc{CourseLvl: "C", Grade: 6},
			inputGrad:       cfg.Realschulabschluss,
			inputHAIncrease: false,
			expectedResult:  6,
		},
		{
			description:     "no course levels, Realschulabschluss",
			inputSubject:    global.SubjectStruc{CourseLvl: "", Grade: 3},
			inputGrad:       cfg.Realschulabschluss,
			inputHAIncrease: false,
			expectedResult:  3,
		},
		{
			description:     "G course Realschulabschluss",
			inputSubject:    global.SubjectStruc{CourseLvl: "G", Grade: 4},
			inputGrad:       cfg.Realschulabschluss,
			inputHAIncrease: false,
			expectedResult:  5,
		},
		{
			description:     "E course Realschulabschluss",
			inputSubject:    global.SubjectStruc{CourseLvl: "E", Grade: 4},
			inputGrad:       cfg.Realschulabschluss,
			inputHAIncrease: false,
			expectedResult:  4,
		},
		// v11 cases
		{
			description:     "A course Versetztung11",
			inputSubject:    global.SubjectStruc{CourseLvl: "A", Grade: 4},
			inputGrad:       cfg.Versetztung11,
			inputHAIncrease: false,
			expectedResult:  4,
		},
		{
			description:     "B course Versetztung11",
			inputSubject:    global.SubjectStruc{CourseLvl: "B", Grade: 4},
			inputGrad:       cfg.Versetztung11,
			inputHAIncrease: false,
			expectedResult:  5,
		},
		{
			description:     "B course Versetztung11 - no decrease possible",
			inputSubject:    global.SubjectStruc{CourseLvl: "B", Grade: 6},
			inputGrad:       cfg.Versetztung11,
			inputHAIncrease: false,
			expectedResult:  6,
		},
		{
			description:     "C course Versetztung11",
			inputSubject:    global.SubjectStruc{CourseLvl: "C", Grade: 4},
			inputGrad:       cfg.Versetztung11,
			inputHAIncrease: false,
			expectedResult:  6,
		},
		{
			description:     "C course Versetztung11, only one grade decrease possible",
			inputSubject:    global.SubjectStruc{CourseLvl: "C", Grade: 5},
			inputGrad:       cfg.Versetztung11,
			inputHAIncrease: false,
			expectedResult:  6,
		},
		{
			description:     "C course Versetztung11 no decrease possible",
			inputSubject:    global.SubjectStruc{CourseLvl: "C", Grade: 6},
			inputGrad:       cfg.Versetztung11,
			inputHAIncrease: false,
			expectedResult:  6,
		},
		{
			description:     "no course levels, Versetztung11",
			inputSubject:    global.SubjectStruc{CourseLvl: "", Grade: 3},
			inputGrad:       cfg.Versetztung11,
			inputHAIncrease: false,
			expectedResult:  3,
		},
		{
			description:     "G course Versetztung11",
			inputSubject:    global.SubjectStruc{CourseLvl: "G", Grade: 4},
			inputGrad:       cfg.Versetztung11,
			inputHAIncrease: false,
			expectedResult:  6,
		},
		{
			description:     "E course Versetztung11",
			inputSubject:    global.SubjectStruc{CourseLvl: "E", Grade: 4},
			inputGrad:       cfg.Versetztung11,
			inputHAIncrease: false,
			expectedResult:  4,
		},
	}

	for _, test := range testCase {
		t.Run(test.description, func(t *testing.T) {
			result := ConvertGrade(test.inputSubject, test.inputGrad, test.inputHAIncrease)
			if result != test.expectedResult {
				t.Errorf("For %s, expected %d but got %d", test.description, test.expectedResult, result)
			}
		})
	}
}
