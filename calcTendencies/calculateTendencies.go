package calctendencies

import (
	config "igs-abschlussrechner/config"
	misc "igs-abschlussrechner/miscellaneous"

	"igs-abschlussrechner/global"
)

// TODO: simplify(?) und testen?
func CheckingTendencyCriteria(student *global.Student) {
	var logV11, logRA, logHA [][3]string
	var noGradWithoutHaIncrease bool

	student.CalcResults.V11Results.ConditionsMet, logV11 = checkV11(*student)
	student.CalcResults.V11Results.Log = logV11

	student.CalcResults.RAResults.ConditionsMet, logRA = checkRA(*student)
	student.CalcResults.RAResults.Log = logRA

	student.CalcResults.HAResults.ConditionsMet, logHA, noGradWithoutHaIncrease = checkHA(*student)
	student.CalcResults.HAResults.Log = logHA
	if noGradWithoutHaIncrease {
		misc.AddWarning(student, config.WarningNoGradWithoutHaIncrease)
	}
}

func checkV11(studentData global.Student) (bool, [][3]string) {
	v11ConditionsMet := true
	var log [][3]string
	cfg := config.GetConfig()

	log = append(log, [3]string{cfg.Versetztung11 + "-Bedingungen", "", ""})
	log = append(log, [3]string{"Erfüllt?", "Ausnahme?", "Bedingung"})

	// hard criteria (cannot be compensated)
	v11SufficientCourseLVLsText := "3x A-Kurs in Hauptfächern oder 2x A-Kurs in Hauptfächern + 1x min. E3 in Chemie oder Physik (oder Biologie) - kein C-Kurs!"
	if v11SufficientCourseLVLs(studentData.ReportCardInfo.Subjects, studentData.ClassYear) {
		log = append(log, [3]string{"✓", "---", v11SufficientCourseLVLsText})
	} else {
		log = append(log, [3]string{"✗", "nein", v11SufficientCourseLVLsText})
		v11ConditionsMet = false
	}

	twoMajorFailsText := "kein Ausfall (= nicht hinreichende Leistung) in zwei (oder mehr) Hauptfächern"
	failsInMajors := len(studentData.CalcResults.V11Results.FailsAndComps.MajorFail)
	if failsInMajors >= 2 {
		log = append(log, [3]string{"✗", "nein", twoMajorFailsText})
		v11ConditionsMet = false
	} else {
		log = append(log, [3]string{"✓", "---", twoMajorFailsText})
	}

	noMajorSixText := "keine Note 6 in einem Hauptfach (oder mehreren)"
	sixesInMajors := countSixesInMajors(studentData.ReportCardInfo.Subjects, cfg.Versetztung11)
	sixInMajor := sixesInMajors >= 1
	if sixInMajor {
		log = append(log, [3]string{"✗", "nein", noMajorSixText})
		v11ConditionsMet = false
	} else {
		log = append(log, [3]string{"✓", "---", noMajorSixText})
	}

	tooManySixesAndfailsText := "keine Note 6 + weiteren Ausfall (= nicht hinreichende Leistung) in je einem beliebigem Fach"
	sixesInMinors := countSixesInMinors(studentData.ReportCardInfo.Subjects, cfg.Versetztung11)
	failsInMinors := len(studentData.CalcResults.V11Results.FailsAndComps.MinorFail)
	tooManySixesAndfails := (sixesInMinors) >= 1 && (failsInMajors+failsInMinors) >= 2 // Ausschlusskriterium: Note 6 und einem Ausfall in jeweils einem beliebigen Fach
	if tooManySixesAndfails {
		log = append(log, [3]string{"✗", "möglich", tooManySixesAndfailsText})
		v11ConditionsMet = false
	} else {
		log = append(log, [3]string{"✓", "---", tooManySixesAndfailsText})
	}

	moreThan3FailsText := "kein Ausfall (= nicht hinreichende Leistung) in 3 (oder mehr) Fächern"
	moreThan3Fails := (failsInMajors + failsInMinors) >= 3
	if moreThan3Fails {
		log = append(log, [3]string{"✗", "möglich", moreThan3FailsText})
		v11ConditionsMet = false
	} else {
		log = append(log, [3]string{"✓", "---", moreThan3FailsText})
	}

	// checking soft criteria (can be compensated): checked in studentData.CalcResults.V11Results.AllCompensated
	return (v11ConditionsMet && studentData.CalcResults.V11Results.AllCompensated), log
}

// function checks if conditions for "Realschulabschluss" are met and returns respective boolean, a list of documented conditions and a string array with possible subjects for compensation
// TODO: write a test for function
func checkRA(studentData global.Student) (bool, [][3]string) {
	RAConditionsMet := true
	var log [][3]string
	cfg := config.GetConfig()

	// log
	log = append(log, [3]string{cfg.Realschulabschluss + "bedingungen", "", ""})
	log = append(log, [3]string{"Erfüllt?", "Ausnahme?", "Bedingung"})

	// hard criteria (cannot be compensated)
	raSufficientCourseLVLsText := "2 B-Kurse in Deutsch, Mathe oder Englisch"
	if raSufficientCourseLVLs(studentData.ReportCardInfo.Subjects) {
		log = append(log, [3]string{"✓", "---", raSufficientCourseLVLsText})
	} else {
		log = append(log, [3]string{"✗", "nein", raSufficientCourseLVLsText})
		RAConditionsMet = false
	}

	raSufficientGradesinNoLvlSubjectsText := "min. 2x Note 3 in Fächern ohne Fachleistungsdifferenzierung"
	if raSufficientGradesinNoLvlSubjects(studentData.ReportCardInfo.Subjects) {
		log = append(log, [3]string{"✓", "---", raSufficientGradesinNoLvlSubjectsText})
	} else {
		log = append(log, [3]string{"✗", "nein", raSufficientGradesinNoLvlSubjectsText})
		RAConditionsMet = false
	}

	twoMajorFailsText := "keine Note 5 oder 6 in zwei Hauptfächern"
	failsMajor := len(studentData.CalcResults.RAResults.FailsAndComps.MajorFail)
	twoMajorFails := failsMajor >= 2
	if twoMajorFails {
		log = append(log, [3]string{"✗", "nein", twoMajorFailsText})
		RAConditionsMet = false
	} else {
		log = append(log, [3]string{"✓", "---", twoMajorFailsText})
	}

	tooManySixesAndfailsText := "keine Note 5 oder 6 in einem Hauptfach und zwei weiteren Fächern"
	failsMinor := len(studentData.CalcResults.RAResults.FailsAndComps.MinorFail)
	tooManySixesAndfails := failsMajor >= 1 && (failsMajor+failsMinor) >= 3
	if tooManySixesAndfails {
		log = append(log, [3]string{"✗", "möglich", tooManySixesAndfailsText})
		RAConditionsMet = false
	} else {
		log = append(log, [3]string{"✓", "---", tooManySixesAndfailsText})
	}

	sixInMajorText := "keine Note 6 in einem Hauptfach (Deutsch, Mathe, Englisch oder Gesellschaftslehre)"
	sixesMajor := countSixesInMajors(studentData.ReportCardInfo.Subjects, cfg.Realschulabschluss)
	sixInMajor := sixesMajor >= 1
	if sixInMajor {
		log = append(log, [3]string{"✗", "nein", sixInMajorText})
		RAConditionsMet = false
	} else {
		log = append(log, [3]string{"✓", "---", sixInMajorText})
	}

	sixAndFailInMinorText := "keine Note 6 und Note 5 in den Nebenfächern"
	sixesMinor := countSixesInMinors(studentData.ReportCardInfo.Subjects, cfg.Realschulabschluss)
	sixAndFailInMinor := sixesMinor >= 1 && failsMinor >= 2
	if sixAndFailInMinor {
		log = append(log, [3]string{"✗", "möglich", sixAndFailInMinorText})
		RAConditionsMet = false
	} else {
		log = append(log, [3]string{"✓", "---", sixAndFailInMinorText})
	}

	moreThan3FailsText := "keine Note 5 in drei (oder mehr) Nebenfächern"
	moreThan3Fails := failsMinor >= 3
	if moreThan3Fails {
		log = append(log, [3]string{"✗", "möglich", moreThan3FailsText})
		RAConditionsMet = false
	} else {
		log = append(log, [3]string{"✓", "---", moreThan3FailsText})
	}

	// checking soft criteria (can be compensated)
	return (RAConditionsMet && studentData.CalcResults.RAResults.AllCompensated), log
}

// function checks if conditions for "Hauptschulabschluss" are met and returns respective boolean, a list of documented conditions and a string array with possible subjects for compensation
// TODO: write Test for function
func checkHA(studentData global.Student) (bool, [][3]string, bool) {
	haConditionsMet := true
	var log [][3]string
	cfg := config.GetConfig()

	// Logging
	log = append(log, [3]string{cfg.Hauptschulabschluss + "bedingungen", "", ""})
	log = append(log, [3]string{"Erfüllt?", "Ausnahme?", "Bedingung"})

	// Hard criteria (cannot be compensated)
	haIncrease := (studentData.Term == 1 || (studentData.Term == 2 && studentData.ReportCardInfo.FinalExams.TookExams))
	failsDMGl := countFailsInDMGl(studentData.ReportCardInfo.Subjects, cfg.Hauptschulabschluss, haIncrease)
	failsRest := countFailsInRest(studentData.ReportCardInfo.Subjects, cfg.Hauptschulabschluss, haIncrease)

	moreThan3FailsInDMGlText := "kein Ausfall (= nicht hinreichende Leistung) in 3 Fächern, falls (mindestens) eines davon Deutsch, Mathe oder Gesellschaftslehre ist"
	if failsDMGl >= 1 && (failsDMGl+failsRest >= 3) {
		log = append(log, [3]string{"✗", "nein", moreThan3FailsInDMGlText})
		haConditionsMet = false
	} else {
		log = append(log, [3]string{"✓", "---", moreThan3FailsInDMGlText})
	}

	moreThan5FailsText := "kein Ausfall (= nicht hinreichende Leistung) in 5 oder mehr Fächern"
	if failsDMGl+failsRest >= 5 {
		log = append(log, [3]string{"✗", "möglich", moreThan5FailsText})
		haConditionsMet = false
	} else {
		log = append(log, [3]string{"✓", "---", moreThan5FailsText})
	}

	// checks, if HA conditions are met also without haGradeIncrease
	var noGradWithoutHaIncrease bool
	if haIncrease && studentData.Term == 1 {
		failsDMGlWithoutHaIncrease := countFailsInDMGl(studentData.ReportCardInfo.Subjects, cfg.Hauptschulabschluss, false)
		failsRestWithoutHaIncrease := countFailsInRest(studentData.ReportCardInfo.Subjects, cfg.Hauptschulabschluss, false)
		noGradWithoutHaIncrease = failsDMGlWithoutHaIncrease >= 1 && (failsDMGlWithoutHaIncrease+failsRestWithoutHaIncrease >= 3) || failsDMGlWithoutHaIncrease+failsRestWithoutHaIncrease >= 5
	}

	// checking soft criteria (can be compensated) and returning results
	return haConditionsMet && studentData.CalcResults.HAResults.AllCompensated, log, (haConditionsMet && noGradWithoutHaIncrease)
}

func v11SufficientCourseLVLs(subjects []global.SubjectStruc, class int) bool {
	lvlAInMajorCount := 0
	lvlEInMinorCount := 0

	for i := 0; i < len(subjects); i++ {
		switch {
		case subjectIsMajor(subjects[i].SubjectAbbreviation) && subjects[i].CourseLvl == "C": // condition: no C level course allowed
			return false
		case subjectIsMajor(subjects[i].SubjectAbbreviation) && subjects[i].CourseLvl == "A":
			lvlAInMajorCount++
		case class <= 8 &&
			subjectIsNaWi(subjects[i].SubjectAbbreviation) &&
			subjects[i].Grade <= 3:
			lvlEInMinorCount++
		case subjectIsNaWi(subjects[i].SubjectAbbreviation) &&
			subjects[i].CourseLvl == "E" &&
			subjects[i].Grade <= 3:
			lvlEInMinorCount++
		}
	}
	return lvlAInMajorCount == 3 || (lvlAInMajorCount == 2 && lvlEInMinorCount >= 1)
}

// function counts and returns the number of sixes in majors (M,D,E,GL)
// TODO: simplify and write Test -> simpligy by deleting switch case statement by just puttin grad in Convert function!
func countSixesInMajors(subjects []global.SubjectStruc, grad string) int {
	cfg := config.GetConfig()

	countSixes := 0
	for i := 0; i < len(subjects); i++ {
		if isMajor(subjects[i].SubjectAbbreviation) {
			switch grad { // remove this and just put grad in ConvertGrade function?
			case cfg.Versetztung11:
				if ConvertGrade(subjects[i], cfg.Versetztung11, false) == 6 {
					countSixes++
				}
			case cfg.Realschulabschluss:
				if ConvertGrade(subjects[i], cfg.Realschulabschluss, false) == 6 {
					countSixes++
				}
			}
		}
	}
	return countSixes
}

// checks if a subject is a major
func isMajor(str string) bool {
	cfg := config.GetConfig()
	for _, a := range cfg.MajorSubjects {
		if a == str {
			return true
		}
	}
	return false
}

// counts and returns number of sixes in minors (= all subjects that are not majors)
// TODO: simplify(?) and write Test
func countSixesInMinors(subjects []global.SubjectStruc, grad string) int {
	cfg := config.GetConfig()
	countSixes := 0
	for i := 0; i < len(subjects); i++ {
		if !isMajor(subjects[i].SubjectAbbreviation) {
			switch grad {
			case cfg.Versetztung11:
				if ConvertGrade(subjects[i], cfg.Versetztung11, false) == 6 {
					countSixes++
				}
			case cfg.Realschulabschluss:
				if ConvertGrade(subjects[i], cfg.Realschulabschluss, false) == 6 {
					countSixes++
				}
			}
		}
	}
	return countSixes
}

// checks if the course levels for "Realschulabschluss" are met (2x min B level in M,E,D)
// TODO: write a Test
func raSufficientCourseLVLs(subjects []global.SubjectStruc) bool {
	majorCount := 0 // number of B-levels in majors

	for i := 0; i < len(subjects); i++ {
		if subjects[i].SubjectAbbreviation == "D" || subjects[i].SubjectAbbreviation == "M" || subjects[i].SubjectAbbreviation == "E" {
			if subjects[i].CourseLvl == "A" || subjects[i].CourseLvl == "B" {
				majorCount++
			}
		}
	}
	return majorCount >= 2
}

// checks if grades in courses without course levels are sufficient (2x min 3 in courses without course-levels)
func raSufficientGradesinNoLvlSubjects(subjects []global.SubjectStruc) bool {
	noLvlCourseCount := 0 // number of grade 3 in courses without course-levels

	for i := 0; i < len(subjects); i++ {
		if subjects[i].CourseLvl == "" {
			if subjects[i].Grade <= 3 {
				noLvlCourseCount++
			}
		}
	}

	return noLvlCourseCount >= 2
}

// counts and returns number of failed courses in M,D,GL (grade 5 or 6) for "Hauptschulabschluss"
// TODO: write a Test
func countFailsInDMGl(subjects []global.SubjectStruc, grad string, haIncrease bool) int {
	countFails := 0
	for i := 0; i < len(subjects); i++ {
		if subjects[i].SubjectAbbreviation == "D" || subjects[i].SubjectAbbreviation == "M" || subjects[i].SubjectAbbreviation == "GL" {
			if ConvertGrade(subjects[i], grad, haIncrease) >= 5 {
				countFails++
			}
		}
	}
	return countFails
}

// counts and returns number of failed courses for "Hauptschulabschluss", when courses are NOT M,D,GL (grade 5 or 6)
func countFailsInRest(subjects []global.SubjectStruc, grad string, haIncrease bool) int {
	countFails := 0
	for i := 0; i < len(subjects); i++ {
		if subjects[i].SubjectAbbreviation != "D" && subjects[i].SubjectAbbreviation != "M" && subjects[i].SubjectAbbreviation != "GL" {
			if ConvertGrade(subjects[i], grad, haIncrease) >= 5 {
				countFails++
			}
		}
	}
	return countFails
}
