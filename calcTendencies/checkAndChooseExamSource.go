package calctendencies

import (
	config "igs-abschlussrechner/config"
	"igs-abschlussrechner/global"
	misc "igs-abschlussrechner/miscellaneous"
	"math"
	"strconv"
	"strings"
)

func ChoseAndCheckExamData(student *global.Student) {
	student.ReportCardInfo.FinalExams.FillDataFromSources()

	if student.ReportCardInfo.FinalExams.TookExams {
		ReplaceMissingValuesByBackupData(student)
		addExamInfoToSubjectStruct(student)
	}
}

func addExamInfoToSubjectStruct(studentData *global.Student) {
	finalExams := &studentData.ReportCardInfo.FinalExams
	subjectList := &studentData.ReportCardInfo.Subjects

	for _, examSubject := range finalExams.ExamSubjects {
		examSubjectAbbreviation := examSubject.SubjectAbbreviation

		// TODO: change hardcoded "Prop" -> config file (Prop -> from Abschlusszeugnis-Konferenzliste, PPR -> from Ergebnisse_Abschlussprüfung)
		if examSubjectAbbreviation == "ProP" || examSubjectAbbreviation == "PPR" {
			continue
		}

		// Update existing subject if found
		if updateExistingSubject(subjectList, examSubject, studentData.ClassYear) {
			continue
		}

		addNewSubject(subjectList, examSubject)
	}
}

func addNewSubject(subjectList *[]global.SubjectStruc, examSubject global.ExamSubjectStruct) {
	newSubject := global.SubjectStruc{
		SubjectAbbreviation:          examSubject.SubjectAbbreviation,
		SubjectFullName:              misc.ConvertSubAbbToFullName(examSubject.SubjectAbbreviation),
		Teacher:                      examSubject.Teacher,
		Absents:                      global.AbsentStruc{},
		CourseLvl:                    "",
		LvlChange:                    "",
		Grade:                        examSubject.ReportCardGrade,
		GradeNotIncludingExamGrade:   -1,
		GradeJump:                    false,
		GrageFromPreviousYear:        true,
		HAGrade:                      0,
		HAGradeDifferentThanOrginial: false,
		AlternativeGrade:             "",
		FailString:                   "",
		CompenstaionBy:               "",
		ExamTaken:                    true,
		LvlChangeNonLUSDData:         global.NonLUSDLvlChangeStruc{},
	}
	*subjectList = append(*subjectList, newSubject)
}

func updateExistingSubject(subjectList *[]global.SubjectStruc, examSubject global.ExamSubjectStruct, year int) bool {
	cfg := config.GetConfig()

	for i := range *subjectList {
		subject := &(*subjectList)[i]
		if subject.SubjectAbbreviation == examSubject.SubjectAbbreviation {
			subject.GradeNotIncludingExamGrade = subject.Grade

			// update subject and calculate reportcard grade (including exam) if that is not included yet
			if examSubject.ReportCardGrade <= 0 {
				switch year {
				case 9:
					convertedSubjectGrad := ConvertGrade(*subject, cfg.Hauptschulabschluss, false)
					subject.Grade = calcReportCardGradeOfExamSubject(convertedSubjectGrad, examSubject.ExamGrade)
				case 10:
					convertedSubjectGrad := ConvertGrade(*subject, cfg.Realschulabschluss, false)
					repordcardGradIncludingExam := calcReportCardGradeOfExamSubject(convertedSubjectGrad, examSubject.ExamGrade)

					// calculate back to original course level
					switch subject.CourseLvl {
					case "A":
						subject.Grade = repordcardGradIncludingExam + 1
					case "C":
						subject.Grade = repordcardGradIncludingExam - 1
					default:
						subject.Grade = repordcardGradIncludingExam
					}
				default:
					subject.Grade = calcReportCardGradeOfExamSubject(subject.GradeNotIncludingExamGrade, examSubject.ExamGrade)
				}
			} else {
				subject.Grade = examSubject.ReportCardGrade
			}
			subject.ExamTaken = true
			return true // Subject updated
		}
	}
	return false // No subject found
}

func calcReportCardGradeOfExamSubject(subjectGrade, examgrade int) int {
	average := (2*float64(subjectGrade) + float64(examgrade)) / 3.0
	return int(math.Round(average))
}

func ReplaceMissingValuesByBackupData(student *global.Student) {
	examData := &student.ReportCardInfo.FinalExams

	if !examData.BackupExamData.DataPresent {
		return
	}

	if examData.GraduationAbbreviation == "" && examData.BackupExamData.GraduationAbbreviation != "" {
		examData.GraduationAbbreviation = examData.BackupExamData.GraduationAbbreviation
		examData.ReplacedMissingData = true
		examData.DataReplacedBy = append(examData.DataReplacedBy, "Abschlusskürzel "+examData.BackupExamData.GraduationAbbreviation+" ergänzt")

	}

	if examData.GraduationFullName == "" && examData.BackupExamData.GraduationFullName != "" {
		examData.GraduationFullName = examData.BackupExamData.GraduationFullName
		examData.ReplacedMissingData = true
		examData.DataReplacedBy = append(examData.DataReplacedBy, "Abschluss "+examData.BackupExamData.GraduationFullName+" ergänzt")
	}
	if (examData.TotalAverage == 0 || examData.TotalAverage == -1) && !(examData.BackupExamData.TotalAverage == 0 || examData.BackupExamData.TotalAverage == -1) {
		examData.TotalAverage = examData.BackupExamData.TotalAverage
		examData.ReplacedMissingData = true
		examData.DataReplacedBy = append(examData.DataReplacedBy, "Gesamtschnitt ("+strconv.FormatFloat(examData.BackupExamData.TotalAverage, 'f', 2, 64)+") ergänzt")
	}

	if len(examData.ExamSubjects) < 4 {
		addedSubjects := addMissingSubjects(&examData.ExamSubjects, examData.BackupExamData.ExamSubjects)
		examData.ReplacedMissingData = true
		examData.DataReplacedBy = append(examData.DataReplacedBy, "Prüfungsfächer ergänzt: "+strings.Join(addedSubjects, ","))
	}
}

// Helper function to check if a subject abbreviation matches "E", "D", or "M"
func isRequiredSubject(abb string) bool {
	return abb == "E" || abb == "D" || abb == "M"
}

// Helper function to check if the subject from the backup is valid for the missing abbreviation
func isValidBackupSubject(subject, requiredAbb string) bool {
	if requiredAbb == "other" {
		return subject != "E" && subject != "D" && subject != "M"
	}
	return subject == requiredAbb
}

// Function to find and add missing subjects from the backup
func addMissingSubjects(subjects *[]global.ExamSubjectStruct, backup []global.ExamSubjectStruct) []string {
	requiredSubjects := map[string]bool{"E": false, "D": false, "M": false, "other": false}
	var addedSubjects []string

	// Iterate over the existing subjects and mark the ones that are already present
	for _, subject := range *subjects {
		if isRequiredSubject(subject.SubjectAbbreviation) {
			requiredSubjects[subject.SubjectAbbreviation] = true
		} else {
			requiredSubjects["other"] = true
		}
	}

	// Iterate over the required subjects to find out which ones are missing
	for requiredAbb, found := range requiredSubjects {
		if !found {
			// Find a subject from the backup that matches the missing abbreviation
			for _, backupSubject := range backup {
				if isValidBackupSubject(backupSubject.SubjectAbbreviation, requiredAbb) {
					*subjects = append(*subjects, backupSubject)
					addedSubjects = append(addedSubjects, backupSubject.SubjectFullName)
					break
				}
			}
		}
	}
	return addedSubjects
}
