package calctendencies

import (
	"fmt"
	config "igs-abschlussrechner/config"
	"igs-abschlussrechner/global"
	"reflect"
	"testing"
)

func TestIsRequiredSubject(t *testing.T) {
	testCases := []struct {
		abb      string
		expected bool
	}{
		{"E", true},
		{"D", true},
		{"M", true},
		{"S", false},
		{"A", false},
		{"", false},
	}

	for _, test := range testCases {
		t.Run(test.abb, func(t *testing.T) {
			result := isRequiredSubject(test.abb)
			if result != test.expected {
				t.Errorf("For abbreviation %q, expected %v, but got %v", test.abb, test.expected, result)
			}
		})
	}
}

func TestIsValidBackupSubject(t *testing.T) {
	testCases := []struct {
		subjectAbb string
		backupAbb  string
		expected   bool
	}{
		{"E", "E", true},
		{"D", "D", true},
		{"M", "M", true},
		{"S", "other", true},
		{"X", "other", true},
		{"E", "other", false},
		{"D", "other", false},
		{"M", "other", false},
	}

	for _, test := range testCases {
		t.Run(test.subjectAbb, func(t *testing.T) {
			result := isValidBackupSubject(test.subjectAbb, test.backupAbb)
			if result != test.expected {
				t.Errorf("For subject %q and required abbreviation %q, expected %v, but got %v", test.subjectAbb, test.backupAbb, test.expected, result)
			}
		})
	}
}

func TestAddMissingSubjects(t *testing.T) {
	type testCase struct {
		name             string
		inputSubjects    []global.ExamSubjectStruct
		inputBackup      []global.ExamSubjectStruct
		expectedSubjects []global.ExamSubjectStruct
	}

	testCases := []testCase{
		{
			name: "Missing M subject, backup has M",
			inputSubjects: []global.ExamSubjectStruct{
				{SubjectAbbreviation: "E", SubjectFullName: "English", Teacher: "Mr. Smith", ReportCardGrade: 2, ExamGrade: 1},
				{SubjectAbbreviation: "D", SubjectFullName: "Dutch", Teacher: "Mrs. Johnson", ReportCardGrade: 3, ExamGrade: 2},
				{SubjectAbbreviation: "SPO", SubjectFullName: "Sport", Teacher: "Mr. Black", ReportCardGrade: 4, ExamGrade: 3},
			},
			inputBackup: []global.ExamSubjectStruct{
				{SubjectAbbreviation: "E", SubjectFullName: "English", Teacher: "Mr. Smith", ReportCardGrade: 2, ExamGrade: 1},
				{SubjectAbbreviation: "D", SubjectFullName: "Dutch", Teacher: "Mrs. Johnson", ReportCardGrade: 3, ExamGrade: 2},
				{SubjectAbbreviation: "M", SubjectFullName: "Mathematics", Teacher: "Mr. White", ReportCardGrade: 1, ExamGrade: 1},
				{SubjectAbbreviation: "SPO", SubjectFullName: "Sport", Teacher: "Mr. Black", ReportCardGrade: 4, ExamGrade: 3},
			},
			expectedSubjects: []global.ExamSubjectStruct{
				{SubjectAbbreviation: "E", SubjectFullName: "English", Teacher: "Mr. Smith", ReportCardGrade: 2, ExamGrade: 1},
				{SubjectAbbreviation: "D", SubjectFullName: "Dutch", Teacher: "Mrs. Johnson", ReportCardGrade: 3, ExamGrade: 2},
				{SubjectAbbreviation: "SPO", SubjectFullName: "Sport", Teacher: "Mr. Black", ReportCardGrade: 4, ExamGrade: 3},
				{SubjectAbbreviation: "M", SubjectFullName: "Mathematics", Teacher: "Mr. White", ReportCardGrade: 1, ExamGrade: 1},
			},
		},
		{
			name: "No subjects are missing",
			inputSubjects: []global.ExamSubjectStruct{
				{SubjectAbbreviation: "E", SubjectFullName: "English", Teacher: "Mr. Smith", ReportCardGrade: 2, ExamGrade: 1},
				{SubjectAbbreviation: "D", SubjectFullName: "Dutch", Teacher: "Mrs. Johnson", ReportCardGrade: 3, ExamGrade: 2},
				{SubjectAbbreviation: "M", SubjectFullName: "Mathematics", Teacher: "Mr. White", ReportCardGrade: 1, ExamGrade: 1},
				{SubjectAbbreviation: "BIO", SubjectFullName: "Biology", Teacher: "Mrs. Green", ReportCardGrade: 2, ExamGrade: 3},
			},
			inputBackup: []global.ExamSubjectStruct{
				{SubjectAbbreviation: "E", SubjectFullName: "English", Teacher: "Mr. Smith", ReportCardGrade: 2, ExamGrade: 1},
				{SubjectAbbreviation: "D", SubjectFullName: "Dutch", Teacher: "Mrs. Johnson", ReportCardGrade: 3, ExamGrade: 2},
				{SubjectAbbreviation: "M", SubjectFullName: "Mathematics", Teacher: "Mr. White", ReportCardGrade: 1, ExamGrade: 1},
				{SubjectAbbreviation: "BIO", SubjectFullName: "Biology", Teacher: "Mrs. Green", ReportCardGrade: 2, ExamGrade: 3}},
			expectedSubjects: []global.ExamSubjectStruct{
				{SubjectAbbreviation: "E", SubjectFullName: "English", Teacher: "Mr. Smith", ReportCardGrade: 2, ExamGrade: 1},
				{SubjectAbbreviation: "D", SubjectFullName: "Dutch", Teacher: "Mrs. Johnson", ReportCardGrade: 3, ExamGrade: 2},
				{SubjectAbbreviation: "M", SubjectFullName: "Mathematics", Teacher: "Mr. White", ReportCardGrade: 1, ExamGrade: 1},
				{SubjectAbbreviation: "BIO", SubjectFullName: "Biology", Teacher: "Mrs. Green", ReportCardGrade: 2, ExamGrade: 3}},
		},
		{
			name: "Missing 'other' subject, backup has non-required subject",
			inputSubjects: []global.ExamSubjectStruct{
				{SubjectAbbreviation: "E", SubjectFullName: "English", Teacher: "Mr. Smith", ReportCardGrade: 2, ExamGrade: 1},
				{SubjectAbbreviation: "D", SubjectFullName: "Dutch", Teacher: "Mrs. Johnson", ReportCardGrade: 3, ExamGrade: 2},
				{SubjectAbbreviation: "M", SubjectFullName: "Mathematics", Teacher: "Mr. White", ReportCardGrade: 1, ExamGrade: 1},
			},
			inputBackup: []global.ExamSubjectStruct{
				{SubjectAbbreviation: "E", SubjectFullName: "English", Teacher: "Mr. Smith", ReportCardGrade: 2, ExamGrade: 1},
				{SubjectAbbreviation: "D", SubjectFullName: "Dutch", Teacher: "Mrs. Johnson", ReportCardGrade: 3, ExamGrade: 2},
				{SubjectAbbreviation: "M", SubjectFullName: "Mathematics", Teacher: "Mr. White", ReportCardGrade: 1, ExamGrade: 1},
				{SubjectAbbreviation: "SPO", SubjectFullName: "Sport", Teacher: "Mr. Black", ReportCardGrade: 4, ExamGrade: 3},
			},
			expectedSubjects: []global.ExamSubjectStruct{
				{SubjectAbbreviation: "E", SubjectFullName: "English", Teacher: "Mr. Smith", ReportCardGrade: 2, ExamGrade: 1},
				{SubjectAbbreviation: "D", SubjectFullName: "Dutch", Teacher: "Mrs. Johnson", ReportCardGrade: 3, ExamGrade: 2},
				{SubjectAbbreviation: "M", SubjectFullName: "Mathematics", Teacher: "Mr. White", ReportCardGrade: 1, ExamGrade: 1},
				{SubjectAbbreviation: "SPO", SubjectFullName: "Sport", Teacher: "Mr. Black", ReportCardGrade: 4, ExamGrade: 3},
			},
		},
		{
			name: "Empty backup struct",
			inputSubjects: []global.ExamSubjectStruct{
				{SubjectAbbreviation: "E", SubjectFullName: "English", Teacher: "Mr. Smith", ReportCardGrade: 2, ExamGrade: 1},
				{SubjectAbbreviation: "D", SubjectFullName: "Dutch", Teacher: "Mrs. Johnson", ReportCardGrade: 3, ExamGrade: 2},
				{SubjectAbbreviation: "M", SubjectFullName: "Mathematics", Teacher: "Mr. White", ReportCardGrade: 1, ExamGrade: 1},
				{SubjectAbbreviation: "BIO", SubjectFullName: "Biology", Teacher: "Mrs. Green", ReportCardGrade: 2, ExamGrade: 3},
			},
			inputBackup: []global.ExamSubjectStruct{},
			expectedSubjects: []global.ExamSubjectStruct{
				{SubjectAbbreviation: "E", SubjectFullName: "English", Teacher: "Mr. Smith", ReportCardGrade: 2, ExamGrade: 1},
				{SubjectAbbreviation: "D", SubjectFullName: "Dutch", Teacher: "Mrs. Johnson", ReportCardGrade: 3, ExamGrade: 2},
				{SubjectAbbreviation: "M", SubjectFullName: "Mathematics", Teacher: "Mr. White", ReportCardGrade: 1, ExamGrade: 1},
				{SubjectAbbreviation: "BIO", SubjectFullName: "Biology", Teacher: "Mrs. Green", ReportCardGrade: 2, ExamGrade: 3}},
		},
		{
			name:          "Empty original data struct",
			inputSubjects: []global.ExamSubjectStruct{},
			inputBackup: []global.ExamSubjectStruct{
				{SubjectAbbreviation: "E", SubjectFullName: "English", Teacher: "Mr. Smith", ReportCardGrade: 2, ExamGrade: 1},
				{SubjectAbbreviation: "D", SubjectFullName: "Dutch", Teacher: "Mrs. Johnson", ReportCardGrade: 3, ExamGrade: 2},
				{SubjectAbbreviation: "M", SubjectFullName: "Mathematics", Teacher: "Mr. White", ReportCardGrade: 1, ExamGrade: 1},
				{SubjectAbbreviation: "SPO", SubjectFullName: "Sport", Teacher: "Mr. Black", ReportCardGrade: 4, ExamGrade: 3},
			},
			expectedSubjects: []global.ExamSubjectStruct{
				{SubjectAbbreviation: "E", SubjectFullName: "English", Teacher: "Mr. Smith", ReportCardGrade: 2, ExamGrade: 1},
				{SubjectAbbreviation: "D", SubjectFullName: "Dutch", Teacher: "Mrs. Johnson", ReportCardGrade: 3, ExamGrade: 2},
				{SubjectAbbreviation: "M", SubjectFullName: "Mathematics", Teacher: "Mr. White", ReportCardGrade: 1, ExamGrade: 1},
				{SubjectAbbreviation: "SPO", SubjectFullName: "Sport", Teacher: "Mr. Black", ReportCardGrade: 4, ExamGrade: 3},
			},
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			subjects := append([]global.ExamSubjectStruct{}, tc.inputSubjects...)
			addMissingSubjects(&subjects, tc.inputBackup)
			if !reflect.DeepEqual(subjects, tc.expectedSubjects) {
				t.Errorf("For test case %q, expected subjects %+v, but got %+v", tc.name, tc.expectedSubjects, subjects)
			}
		})
	}
}

func TestAddExamInfoToSubjectStruct(t *testing.T) {
	config.LoadGlobalConfigVariables("../")

	testcases := []struct {
		description string
		input       global.Student
		expected    global.Student
	}{
		{
			description: "Update existing subject grades",
			input: global.Student{
				ReportCardInfo: global.ReportCardInformation{
					Subjects: []global.SubjectStruc{{
						SubjectAbbreviation:        "M",
						Grade:                      2,
						GradeNotIncludingExamGrade: 0,
						ExamTaken:                  false,
					}, {
						SubjectAbbreviation:        "D",
						Grade:                      3,
						GradeNotIncludingExamGrade: 0,
						ExamTaken:                  false,
					}},
					FinalExams: global.ExamsStruc{
						TookExams: true,
						ExamSubjects: []global.ExamSubjectStruct{{
							SubjectAbbreviation: "M",
							ReportCardGrade:     2,
							ExamGrade:           1,
						}, {
							SubjectAbbreviation: "D",
							ReportCardGrade:     2,
							ExamGrade:           1,
						}},
					},
				},
			},
			expected: global.Student{
				ReportCardInfo: global.ReportCardInformation{
					Subjects: []global.SubjectStruc{{
						SubjectAbbreviation:        "M",
						Grade:                      2,
						GradeNotIncludingExamGrade: 2,
						ExamTaken:                  true,
					}, {
						SubjectAbbreviation:        "D",
						Grade:                      2,
						GradeNotIncludingExamGrade: 3,
						ExamTaken:                  true,
					}},
					FinalExams: global.ExamsStruc{
						TookExams: true,
						ExamSubjects: []global.ExamSubjectStruct{{
							SubjectAbbreviation: "M",
							ReportCardGrade:     2,
							ExamGrade:           1,
						}, {
							SubjectAbbreviation: "D",
							ReportCardGrade:     2,
							ExamGrade:           1,
						}},
					},
				},
			},
		},
		{
			description: "Append new exam subject",
			input: global.Student{
				ReportCardInfo: global.ReportCardInformation{
					Subjects: []global.SubjectStruc{{
						SubjectAbbreviation:        "M",
						Grade:                      2,
						GradeNotIncludingExamGrade: 0,
						ExamTaken:                  false,
					}, {
						SubjectAbbreviation:        "D",
						Grade:                      3,
						GradeNotIncludingExamGrade: 0,
						ExamTaken:                  false,
					}},
					FinalExams: global.ExamsStruc{
						TookExams: true,
						ExamSubjects: []global.ExamSubjectStruct{{
							SubjectAbbreviation: "M",
							ReportCardGrade:     2,
							ExamGrade:           1,
						}, {
							SubjectAbbreviation: "BIO",
							ReportCardGrade:     2,
							ExamGrade:           1,
						}},
					},
				},
			},
			expected: global.Student{
				ReportCardInfo: global.ReportCardInformation{
					Subjects: []global.SubjectStruc{{
						SubjectAbbreviation:        "M",
						Grade:                      2,
						GradeNotIncludingExamGrade: 2,
						ExamTaken:                  true,
					}, {
						SubjectAbbreviation:        "D",
						Grade:                      3,
						GradeNotIncludingExamGrade: 0,
						ExamTaken:                  false,
					}, {
						SubjectAbbreviation:        "BIO",
						SubjectFullName:            "Biologie",
						Grade:                      2,
						GradeNotIncludingExamGrade: -1,
						GrageFromPreviousYear:      true,
						ExamTaken:                  true,
					}},
					FinalExams: global.ExamsStruc{
						TookExams: true,
						ExamSubjects: []global.ExamSubjectStruct{{
							SubjectAbbreviation: "M",
							ReportCardGrade:     2,
							ExamGrade:           1,
						}, {
							SubjectAbbreviation: "BIO",
							ReportCardGrade:     2,
							ExamGrade:           1,
						}},
					},
				},
			},
		},
	}

	for _, test := range testcases {
		t.Run(test.description, func(t *testing.T) {
			addExamInfoToSubjectStruct(&test.input)
			if !reflect.DeepEqual(test.input, test.expected) {
				t.Errorf("For %s, expected\n%v\n but got\n%v", test.description, test.expected, &test.input)
			}
		})
	}
}

func TestCalcReportCardGradeOfExamSubject(t *testing.T) {
	tests := []struct {
		a, b int
		want int
	}{
		{3, 2, 3},
		{5, 10, 7},
		{0, 0, 0},
		{10, 5, 8},
	}

	for _, tt := range tests {
		t.Run(fmt.Sprintf("WeightedAverage(%d, %d)", tt.a, tt.b), func(t *testing.T) {
			got := calcReportCardGradeOfExamSubject(tt.a, tt.b)
			if got != tt.want {
				t.Errorf("WeightedAverage(%d, %d) = %d; want %d", tt.a, tt.b, got, tt.want)
			}
		})
	}
}
