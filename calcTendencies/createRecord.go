package calctendencies

import (
	"igs-abschlussrechner/config"
	misc "igs-abschlussrechner/miscellaneous"

	"igs-abschlussrechner/global"
	"sort"
	"strconv"
	"strings"
)

func AddingRecordToStudentData(student *global.Student) {
	for i := 0; i < len(student.ReportCardInfo.Subjects); i++ {
		// TODO: add level changes (as function)
		if strings.Contains(student.ReportCardInfo.Subjects[i].LvlChange, "↥") || strings.Contains(student.ReportCardInfo.Subjects[i].LvlChange, "↧") || strings.Contains(student.ReportCardInfo.Subjects[i].LvlChange, "↦") || student.ReportCardInfo.Subjects[i].LvlChangeNonLUSDData.ListedinNonLUSDData {
			var tmpLvlChange global.LevelChangeStruc

			tmpLvlChange.StudentName = student.Name
			tmpLvlChange.SubjectFullName = student.ReportCardInfo.Subjects[i].SubjectFullName
			tmpLvlChange.SubjectAbbreviation = student.ReportCardInfo.Subjects[i].SubjectAbbreviation
			tmpLvlChange.CourseLvl = student.ReportCardInfo.Subjects[i].CourseLvl
			tmpLvlChange.LvlChange = student.ReportCardInfo.Subjects[i].LvlChange
			if student.ReportCardInfo.Subjects[i].HAGrade > 0 {
				tmpLvlChange.Grade = student.ReportCardInfo.Subjects[i].HAGrade
			} else {
				tmpLvlChange.Grade = student.ReportCardInfo.Subjects[i].Grade
			}
			tmpLvlChange.Teacher = student.ReportCardInfo.Subjects[i].Teacher
			tmpLvlChange.ListedInLUSD = student.ReportCardInfo.Subjects[i].LvlChangeNonLUSDData.ListedInLUSD
			tmpLvlChange.ListedInNonLUSD = student.ReportCardInfo.Subjects[i].LvlChangeNonLUSDData.ListedinNonLUSDData
			if student.ReportCardInfo.Subjects[i].LvlChangeNonLUSDData.ListedInLUSD != student.ReportCardInfo.Subjects[i].LvlChangeNonLUSDData.ListedinNonLUSDData {
				tmpLvlChange.NotListed = true
			}
			if student.ReportCardInfo.Subjects[i].LvlChangeNonLUSDData.ParentChangeRequest {
				tmpLvlChange.ParentChangeRequest = true
			}

			student.Record.LevelChanges = append(student.Record.LevelChanges, tmpLvlChange)
		}

		// TODO: add support plans for failed subjects as function
		if student.ReportCardInfo.Subjects[i].Grade >= 5 {
			var tmpSupportPlan global.SupportPlanStruc

			tmpSupportPlan.StudentName = student.Name
			tmpSupportPlan.SubjectAbbreviation = student.ReportCardInfo.Subjects[i].SubjectAbbreviation
			tmpSupportPlan.SubjectFullName = student.ReportCardInfo.Subjects[i].SubjectFullName
			tmpSupportPlan.CourseLvlGrade = student.ReportCardInfo.Subjects[i].CourseLvl + strconv.Itoa(student.ReportCardInfo.Subjects[i].Grade)
			tmpSupportPlan.Teacher = student.ReportCardInfo.Subjects[i].Teacher

			student.Record.SupportPlan = append(student.Record.SupportPlan, tmpSupportPlan)
		}

		// TODO: add GradeJumps for subjects as function
		if student.ReportCardInfo.Subjects[i].GradeJump {
			var tmpGradeJumo global.GradeJumpStruc

			tmpGradeJumo.StudentName = student.Name
			tmpGradeJumo.SubjectAbbreviation = student.ReportCardInfo.Subjects[i].SubjectAbbreviation
			tmpGradeJumo.SubjectFullName = student.ReportCardInfo.Subjects[i].SubjectFullName
			tmpGradeJumo.CourseLvl = student.ReportCardInfo.Subjects[i].CourseLvl
			tmpGradeJumo.Grade = student.ReportCardInfo.Subjects[i].Grade
			tmpGradeJumo.Teacher = student.ReportCardInfo.Subjects[i].Teacher

			if student.Term == 2 { // only add grade jumps to procotoll and as warning in second term!
				student.Record.GradeJumps = append(student.Record.GradeJumps, tmpGradeJumo)
				misc.AddWarning(student, config.WarningGradejump+student.ReportCardInfo.Subjects[i].SubjectFullName)
			}
		}
	}
}

func SortSubjectsOfStudentData(classData *[]global.Student) {
	cfg := config.GetConfig()
	for i, student := range *classData {
		(*classData)[i].ReportCardInfo.Subjects = sortSubjectsByOrder(student.ReportCardInfo.Subjects, cfg.FachSortierungInAusgabe)
	}
}

// sortSubjectsByOrder sorts the slice 'subjectlist' based on the order specified in slice 'orderReference'.
// If an element in 'subjectlist' is present in 'orderReference', the sorting is done according to the order in 'orderReference'.
// If an element is not found in 'orderReference', it retains its original order in 'subjectlist'.
// Returns:
//   - The sorted slice 'subjectlist' based on the order in 'orderReference', preserving the original order for non-matching elements.
func sortSubjectsByOrder(subjectlist []global.SubjectStruc, orderReference []string) []global.SubjectStruc {
	// Create a map to store the index of each element in reference slice
	indexMap := make(map[string]int)
	for i, val := range orderReference {
		indexMap[val] = i
	}

	// Sort array a based on the order in slice orderReference
	sort.Slice(subjectlist, func(i, j int) bool {
		indexI, existsI := indexMap[subjectlist[i].SubjectAbbreviation]
		indexJ, existsJ := indexMap[subjectlist[j].SubjectAbbreviation]

		// If both elements exist in slice orderReference, compare their indices
		// Otherwise, move the element that doesn't exist to the end
		switch {
		case existsI && existsJ:
			return indexI < indexJ
		case existsI:
			return true
		case existsJ:
			return false
		default:
			return i < j // preserve original order of elements not in reference array
		}
	})
	return subjectlist
}
