package calctendencies

import (
	"igs-abschlussrechner/global"
	"reflect"
	"testing"
)

func TestSortSubjects(t *testing.T) {
	testcases := []struct {
		description    string
		sliceToOrder   []global.SubjectStruc
		orderReference []string
		expectedResult []global.SubjectStruc
	}{
		{
			description:    "sort a slice according to a reference list",
			sliceToOrder:   []global.SubjectStruc{{SubjectAbbreviation: "D"}, {SubjectAbbreviation: "GL"}, {SubjectAbbreviation: "PH"}, {SubjectAbbreviation: "WPU"}, {SubjectAbbreviation: "M"}, {SubjectAbbreviation: "WPU"}, {SubjectAbbreviation: "CH"}},
			orderReference: []string{"D", "M", "E", "GL", "BIO", "CH", "PH", "F", "L", "AL", "KU", "MU", "SPO", "ETHI", "REV", "RKA", "WPU", "GTAB", "TUT"},
			expectedResult: []global.SubjectStruc{{SubjectAbbreviation: "D"}, {SubjectAbbreviation: "M"}, {SubjectAbbreviation: "GL"}, {SubjectAbbreviation: "CH"}, {SubjectAbbreviation: "PH"}, {SubjectAbbreviation: "WPU"}, {SubjectAbbreviation: "WPU"}},
		},
		{
			description:    "slice contains Names, that are not listed in the reference slice",
			sliceToOrder:   []global.SubjectStruc{{SubjectAbbreviation: "D"}, {SubjectAbbreviation: "GL"}, {SubjectAbbreviation: "PH"}, {SubjectAbbreviation: "WPU"}, {SubjectAbbreviation: "M"}, {SubjectAbbreviation: "WPU"}, {SubjectAbbreviation: "CH"}},
			orderReference: []string{"M", "E", "GL", "BIO", "CH", "PH", "F", "L", "AL", "KU", "MU", "SPO", "ETHI", "REV", "RKA", "WPU", "GTAB", "TUT"},
			expectedResult: []global.SubjectStruc{{SubjectAbbreviation: "M"}, {SubjectAbbreviation: "GL"}, {SubjectAbbreviation: "CH"}, {SubjectAbbreviation: "PH"}, {SubjectAbbreviation: "WPU"}, {SubjectAbbreviation: "WPU"}, {SubjectAbbreviation: "D"}},
		},
		{
			description:    "reference slice is shorter than slice to order",
			sliceToOrder:   []global.SubjectStruc{{SubjectAbbreviation: "D"}, {SubjectAbbreviation: "GL"}, {SubjectAbbreviation: "PH"}, {SubjectAbbreviation: "WPU"}, {SubjectAbbreviation: "M"}, {SubjectAbbreviation: "WPU"}, {SubjectAbbreviation: "CH"}},
			orderReference: []string{"D", "M", "E"},
			expectedResult: []global.SubjectStruc{{SubjectAbbreviation: "D"}, {SubjectAbbreviation: "M"}, {SubjectAbbreviation: "GL"}, {SubjectAbbreviation: "PH"}, {SubjectAbbreviation: "WPU"}, {SubjectAbbreviation: "WPU"}, {SubjectAbbreviation: "CH"}},
		},
	}

	for _, test := range testcases {
		t.Run(test.description, func(t *testing.T) {
			result := sortSubjectsByOrder(test.sliceToOrder, test.orderReference)
			if !reflect.DeepEqual(result, test.expectedResult) {
				t.Errorf("For %s, expected %v but got %v", test.description, test.expectedResult, result)
			}
		})
	}
}
