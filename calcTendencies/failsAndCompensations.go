package calctendencies

import (
	config "igs-abschlussrechner/config"
	"igs-abschlussrechner/global"
	"strings"
)

// input and output change to list
// TODO: Add Test
func FindAllFails(student *global.Student) {
	cfg := config.GetConfig()

	student.CalcResults.V11Results.FailsAndComps = detectFails(student.ReportCardInfo.Subjects, cfg.Versetztung11, false)
	student.CalcResults.RAResults.FailsAndComps = detectFails(student.ReportCardInfo.Subjects, cfg.Realschulabschluss, false)
	haIncrease := student.Term == 1 || (student.Term == 2 && student.ReportCardInfo.FinalExams.TookExams)
	student.CalcResults.HAResults.FailsAndComps = detectFails(student.ReportCardInfo.Subjects, cfg.Hauptschulabschluss, haIncrease)
}

// TODO: Add Test
func FindPossibleCompensations(student *global.Student) {
	cfg := config.GetConfig()

	student.CalcResults.V11Results.FailsAndComps = findAndAddPossibleComps(student.ReportCardInfo.Subjects, student.CalcResults.V11Results.FailsAndComps, cfg.Versetztung11, false)
	student.CalcResults.RAResults.FailsAndComps = findAndAddPossibleComps(student.ReportCardInfo.Subjects, student.CalcResults.RAResults.FailsAndComps, cfg.Realschulabschluss, false)
	haIncrease := student.Term == 1 || (student.Term == 2 && student.ReportCardInfo.FinalExams.TookExams)
	student.CalcResults.HAResults.FailsAndComps = findAndAddPossibleComps(student.ReportCardInfo.Subjects, student.CalcResults.HAResults.FailsAndComps, cfg.Hauptschulabschluss, haIncrease)
}

// TODO: before simplifying - write a test first (so you can directly see, if it still works!)
// TODO: simplify - nur eingeben: V11Results und Subjects (eigentlich weder students Data noch Subjects needed?)
// TODO: ailComp, Log <-- wird hier überschrieben(?) Ausfälle und so müssen nur appended werden! (und auch die Ausgleichflag wird nicht gesetzt ...)
func CompensateFails(student *global.Student) {
	cfg := config.GetConfig()

	// try to compensate for all V11 fails and add compensation flags
	student.CalcResults.V11Results.AllCompensated, student.CalcResults.V11Results.FailsAndComps, student.CalcResults.V11Results.FailsLog = compensateFails(student.CalcResults.V11Results.FailsAndComps, cfg.Versetztung11)
	if len(student.CalcResults.V11Results.FailsAndComps.MajorFail)+len(student.CalcResults.V11Results.FailsAndComps.MinorFail) > 0 {
		student.CalcResults.V11Results.WithCompensation = true
	}
	// try to compensate for all RA fails and add compensation flags
	student.CalcResults.RAResults.AllCompensated, student.CalcResults.RAResults.FailsAndComps, student.CalcResults.RAResults.FailsLog = compensateFails(student.CalcResults.RAResults.FailsAndComps, cfg.Realschulabschluss)
	if len(student.CalcResults.RAResults.FailsAndComps.MajorFail)+len(student.CalcResults.RAResults.FailsAndComps.MinorFail) > 0 {
		student.CalcResults.RAResults.WithCompensation = true
	}
	// try to compensate for all HA fails and add compensation flags
	student.CalcResults.HAResults.AllCompensated, student.CalcResults.HAResults.FailsAndComps, student.CalcResults.HAResults.FailsLog = compensateFails(student.CalcResults.HAResults.FailsAndComps, cfg.Hauptschulabschluss)
	if len(student.CalcResults.HAResults.FailsAndComps.MajorFail)+len(student.CalcResults.HAResults.FailsAndComps.MinorFail) > 0 {
		student.CalcResults.HAResults.WithCompensation = true
	}
}

// TODO: before simplifying - write a test first (so you can directly see, if it still works!)
// TODO: Check and simplify (add Tests)
// function detects fails for the different graduation levels
func detectFails(subjects []global.SubjectStruc, grad string, haIncrease bool) global.FailsAndCompensations {
	var fails global.FailsAndCompensations
	cfg := config.GetConfig()

	for i := 0; i < len(subjects); i++ {
		switch grad {
		case cfg.Versetztung11:
			switch {
			case subjects[i].CourseLvl != "": // Fächer mit Fachleistungsdifferenzierung
				if ConvertGrade(subjects[i], cfg.Versetztung11, false) > 4 { // Ausfall, wenn die (für v11 umgerechnete Note) schlechter als 4 ist
					switch subjects[i].SubjectAbbreviation {
					case "D", "E", "M": // Wenn der Ausfall in Deutsch, Englisch oder Mathe ist -> Ausfall eines Hauptfaches
						fails.MajorFail = append(fails.MajorFail, newFail(subjects[i].SubjectAbbreviation, subjects[i].Teacher))
					default: // Wenn der Ausfall in einem anderen Fach (als Deutsch, Englisch oder Mathe) ist -> Ausfall eines Nebenfachs)
						fails.MinorFail = append(fails.MinorFail, newFail(subjects[i].SubjectAbbreviation, subjects[i].Teacher))
					}
				}
			default: // Fächer ohne Fachleistungsdifferenzierung
				if ConvertGrade(subjects[i], cfg.Versetztung11, false) > 3 { // Ausfall, wenn die (für v11 umgerechnete Note) schlechter als 3 ist
					switch subjects[i].SubjectAbbreviation {
					case "GL": // Wenn der Ausfall in Gesellschaftslehre ist -> Ausfall eines Hauptfaches
						fails.MajorFail = append(fails.MajorFail, newFail(subjects[i].SubjectAbbreviation, subjects[i].Teacher))
					default: // Wenn der Ausfall in einem anderen Fach (als Gesellschaftslehre) ist -> Ausfall eines Nebenfachs)
						fails.MinorFail = append(fails.MinorFail, newFail(subjects[i].SubjectAbbreviation, subjects[i].Teacher))
					}
				}
			}
		case cfg.Realschulabschluss: // Notenkriterien für Realschulabschlusses
			switch {
			case subjects[i].SubjectAbbreviation == "F" || subjects[i].SubjectAbbreviation == "L": // Im Fach Latein oder Französisch
				if ConvertGrade(subjects[i], cfg.Realschulabschluss, false) > 5 { // Ausfall (Nebenfach), wenn die (für Realschulabschluss umgerechnete Note) schlechter als 5 ist
					fails.MinorFail = append(fails.MinorFail, newFail(subjects[i].SubjectAbbreviation, subjects[i].Teacher))
				}
			default: // Alle anderen Fächer (nicht Latein oder Französisch)
				if ConvertGrade(subjects[i], cfg.Realschulabschluss, false) > 4 { // Ausfall, wenn die (für Realschulabschluss umgerechnete Note) schlechter als 4 ist
					switch subjects[i].SubjectAbbreviation {
					case "D", "E", "M", "GL": // Wenn der Ausfall in Deutsch, Englisch, Mathe oder Gesellschaftslehre ist -> Ausfall eines Hauptfaches
						fails.MajorFail = append(fails.MajorFail, newFail(subjects[i].SubjectAbbreviation, subjects[i].Teacher))
					default: // Wenn der Ausfall in einem anderen Fach (als Deutsch, Englisch, Mathe oder Gesellschaftslehre) ist -> Ausfall eines Nebenfachs
						fails.MinorFail = append(fails.MinorFail, newFail(subjects[i].SubjectAbbreviation, subjects[i].Teacher))
					}
				}
			}
		case cfg.Hauptschulabschluss: // Notenkriterien für Hauptschulabschlusses
			if ConvertGrade(subjects[i], cfg.Hauptschulabschluss, haIncrease) > 4 && subjects[i].SubjectAbbreviation != "F" && subjects[i].SubjectAbbreviation != "L" { // Ausfall, wenn Note schlechter als 4 ist (außer das Fach ist Französisch oder Latein, denn diese sind A-Kursniveau)
				fail := newFail(subjects[i].SubjectAbbreviation, subjects[i].Teacher)
				fails.MajorFail = append(fails.MajorFail, fail)
			}
		}
	}
	return fails
}

func newFail(name string, teacher string) global.FailedSubject {
	var fail global.FailedSubject
	fail.Name = name
	fail.Teacher = teacher
	fail.Compensated = false
	return fail
}

// function determines, if subjects with fails can be compensated
// TODO: before simplifying - write a test first (so you can directly see, if it still works!)
// TODO: simplify and write Test(s)
func findAndAddPossibleComps(subjects []global.SubjectStruc, failComps global.FailsAndCompensations, grad string, haIncrease bool) global.FailsAndCompensations {
	cfg := config.GetConfig()

	for i := 0; i < len(subjects); i++ {
		switch grad {
		case cfg.Versetztung11:
			switch {
			// compensation for major failes
			case subjects[i].CourseLvl == "A" && subjects[i].Grade <= 2: // A-Kurs mit Note 2 oder besser
				failComps.PossibleMajorCompensation = append(failComps.PossibleMajorCompensation, newComp(subjects[i].SubjectAbbreviation))
			case subjects[i].SubjectAbbreviation == "GL" && subjects[i].Grade <= 2: // Gesellschaftslehre mit Note 2 oder besser
				failComps.PossibleMajorCompensation = append(failComps.PossibleMajorCompensation, newComp(subjects[i].SubjectAbbreviation))
			case subjects[i].CourseLvl == "E" && subjects[i].Grade == 1: // E-Kurs mit Note 1 oder besser
				failComps.PossibleMajorCompensation = append(failComps.PossibleMajorCompensation, newComp(subjects[i].SubjectAbbreviation))
			// Ausgleich für Ausfälle in Haupt- und Nebenfächer
			case subjects[i].CourseLvl == "" && subjects[i].Grade <= 2 && subjects[i].Grade > 0: // Fächer ohne Fachleistungsdifferenzierung mit Note 2 oder besser
				failComps.MultiMajorCompNoLvl = append(failComps.MultiMajorCompNoLvl, newComp(subjects[i].SubjectAbbreviation))
			// Ausgleich nur für Ausfälle in Nebenfächern
			case (subjects[i].CourseLvl == "A" || subjects[i].CourseLvl == "B") && ConvertGrade(subjects[i], cfg.Versetztung11, false) == 3: // A-Kurs mit Note 3 (bzw. B-Kurs mit Note 2) oder besser
				failComps.PossibleMinorCompensation = append(failComps.PossibleMinorCompensation, newComp(subjects[i].SubjectAbbreviation))
			case subjects[i].CourseLvl == "E" && (subjects[i].Grade == 3 || subjects[i].Grade == 2): // E-Kurs mit Note 3 oder besser
				failComps.PossibleMinorCompensation = append(failComps.PossibleMinorCompensation, newComp(subjects[i].SubjectAbbreviation))
			case subjects[i].CourseLvl == "G" && subjects[i].Grade == 1: // G-Kurs mit Note 1
				failComps.PossibleMinorCompensation = append(failComps.PossibleMinorCompensation, newComp(subjects[i].SubjectAbbreviation))
			}
		case cfg.Realschulabschluss: // Ausgleichsfächer bei Realschulabschluss-Bedingungen
			switch {
			// Ausgleich für mögliche Ausfälle in Hauptfächern
			case (subjects[i].CourseLvl == "B" && subjects[i].Grade <= 2) || (subjects[i].CourseLvl == "A" && subjects[i].Grade <= 3): // B-Kurs mit Note 2 (oder besser) bzw. A-Kurs mit Note 3 (oder besser)
				failComps.PossibleMajorCompensation = append(failComps.PossibleMajorCompensation, newComp(subjects[i].SubjectAbbreviation))
			case subjects[i].CourseLvl == "E" && subjects[i].Grade <= 2: // E-Kurs mit Note 2 (oder besser)
				failComps.PossibleMajorCompensation = append(failComps.PossibleMajorCompensation, newComp(subjects[i].SubjectAbbreviation))
			case subjects[i].CourseLvl == "G" && subjects[i].Grade == 1: // G-Kurs mit Note 1
				failComps.PossibleMajorCompensation = append(failComps.PossibleMajorCompensation, newComp(subjects[i].SubjectAbbreviation))
			case subjects[i].SubjectAbbreviation == "GL" && subjects[i].Grade <= 2: // Gesellschaftslehre mit Note 2 (oder besser)
				failComps.PossibleMajorCompensation = append(failComps.PossibleMajorCompensation, newComp(subjects[i].SubjectAbbreviation))
			// Ausgleich für Ausfälle in Haupt- und Nebenfächer
			case subjects[i].CourseLvl == "" && subjects[i].SubjectAbbreviation != "GL" && (subjects[i].Grade <= 2 && subjects[i].Grade > 0): // Fächer ohne Fachleistungsdifferenzierung (und nicht Gesellschaftslehre!) mit Note 2 (oder besser)
				failComps.MultiMajorCompNoLvl = append(failComps.MultiMajorCompNoLvl, newComp(subjects[i].SubjectAbbreviation))
			case (subjects[i].CourseLvl == "B" && subjects[i].Grade == 3) || (subjects[i].CourseLvl == "A" && subjects[i].Grade <= 4): // B-Kurs mit Note 3 (oder A-Kurs mit Note 4)
				failComps.MultiMajorCompB = append(failComps.MultiMajorCompB, newComp(subjects[i].SubjectAbbreviation))
			case subjects[i].CourseLvl == "E" && subjects[i].Grade == 3: // E-Kurs mit Note 3
				failComps.MultiMajorCompGE = append(failComps.MultiMajorCompGE, newComp(subjects[i].SubjectAbbreviation))
			case subjects[i].CourseLvl == "G" && subjects[i].Grade == 2: // G-Kurs mit Note 2
				failComps.MultiMajorCompGE = append(failComps.MultiMajorCompGE, newComp(subjects[i].SubjectAbbreviation))
			// Ausgleich nur für Ausfälle in Nebenfächern (alle in der Liste "multiMajorComp" können auch für Nebenfächer genutzt werden)
			case (subjects[i].SubjectAbbreviation == "F" || subjects[i].SubjectAbbreviation == "L") && subjects[i].Grade <= 4 && subjects[i].Grade > 0: // Französisch oder Latein mit Note 4 (oder besser)
				failComps.PossibleMinorCompensation = append(failComps.PossibleMinorCompensation, newComp(subjects[i].SubjectAbbreviation))
			case subjects[i].CourseLvl == "" && subjects[i].Grade == 3: // Fächer ohne Fachleistungsdifferenzierung mit Note 3
				failComps.MultiMinorComp = append(failComps.MultiMinorComp, newComp(subjects[i].SubjectAbbreviation))
			}

		case cfg.Hauptschulabschluss: // Ausgleichsfächer bei Hauptschulabschluss-Bedingungen
			if subjects[i].Grade > 0 && ConvertGrade(subjects[i], cfg.Hauptschulabschluss, haIncrease) <= 3 { // Fächer mit Note 3 auf Hauptschulniveau (oder besser)
				failComps.PossibleMajorCompensation = append(failComps.PossibleMajorCompensation, newComp(subjects[i].SubjectAbbreviation))
			}
		}
	}

	return failComps
}

func newComp(name string) global.CompensationSubject {
	var comp global.CompensationSubject
	comp.Name = name
	comp.Used = false
	return comp
}

// function checks, if subjects with fails can be compensated
// TODO: before simplifying - write a test first (so you can directly see, if it still works!)
// TODO: simplify and add a Test
func compensateFails(failComp global.FailsAndCompensations, grad string) (bool, global.FailsAndCompensations, [][3]string) {
	// add table Header (if fails are present):
	logHeader := addCompLogHeader(failComp)

	failCompCompensated, log := compSubject(failComp, logHeader, grad)
	log = append(log, [3]string{"", "", ""}) // add empty row at the end of log

	return allCompensated(failCompCompensated), failCompCompensated, log
}

func addCompLogHeader(failComp global.FailsAndCompensations) [][3]string {
	var logHeader [][3]string
	if (len(failComp.MajorFail) + len(failComp.MinorFail)) > 0 {
		logHeader = append(logHeader, [3]string{"Ausfälle", "", ""})
		logHeader = append(logHeader, [3]string{"Fach", "Ausgleich", "durch"})
	}
	return logHeader
}

// function loops through all fails, finds a possible compensation (if present) and returns the failed subject and the subject used to compensate it.
// TODO: before simplifying - write a test first (so you can directly see, if it still works!)
// TODO: Simplify and write Test for function
func compSubject(failComp global.FailsAndCompensations, log [][3]string, grad string) (global.FailsAndCompensations, [][3]string) {
	cfg := config.GetConfig()

	// loop through and compensate major fails
	for i := 0; i < len(failComp.MajorFail); i++ {
		switch {
		case checkCompList(failComp.PossibleMajorCompensation, 1): // tries to compensate from "possibleMajorCompensation"-list
			log, failComp.MajorFail[i] = compensateSub(failComp.MajorFail[i], failComp.PossibleMajorCompensation, 1, log)
		case checkCompList(failComp.MultiMajorCompNoLvl, 2): // tries to compensate from "multiMajorCompNoLvl"-list
			log, failComp.MajorFail[i] = compensateSub(failComp.MajorFail[i], failComp.MultiMajorCompNoLvl, 2, log)
		case checkCompList(failComp.MultiMajorCompB, 2): // tries to compensate from "multiMajorCompB"-list
			log, failComp.MajorFail[i] = compensateSub(failComp.MajorFail[i], failComp.MultiMajorCompB, 2, log)
		case checkCompList(failComp.MultiMajorCompGE, 2): // tries to compensate from "multiMajorCompGE"-list
			log, failComp.MajorFail[i] = compensateSub(failComp.MajorFail[i], failComp.MultiMajorCompGE, 2, log)
		default: // no compensation possible
			log, failComp.MajorFail[i] = markAndLogFailstructs(false, failComp.MajorFail[i], "---", log)
		}
	}

	// loop through and compensate minor fails (only relevant for v11 and Realschulabschluss)
	for i := 0; i < len(failComp.MinorFail); i++ {
		switch {
		case checkCompList(failComp.PossibleMajorCompensation, 1): // tries to compensate from "possibleMajorCompensation"-list
			log, failComp.MinorFail[i] = compensateSub(failComp.MinorFail[i], failComp.PossibleMajorCompensation, 1, log)
		case grad == cfg.Versetztung11 && checkCompList(failComp.MultiMajorCompNoLvl, 2): // tries to compensate from "multiMajorCompNoLvl"-list
			log, failComp.MinorFail[i] = compensateSub(failComp.MinorFail[i], failComp.MultiMajorCompNoLvl, 2, log)
		case grad == cfg.Realschulabschluss && checkCompList(failComp.MultiMajorCompNoLvl, 1): // tries to compensate from "multiMajorCompNoLvl"-list
			log, failComp.MinorFail[i] = compensateSub(failComp.MinorFail[i], failComp.MultiMajorCompNoLvl, 1, log)
		case grad == cfg.Realschulabschluss && checkCompList(failComp.MultiMajorCompB, 1): // tries to compensate from "multiMajorCompB"-list
			log, failComp.MinorFail[i] = compensateSub(failComp.MinorFail[i], failComp.MultiMajorCompB, 1, log)
		case grad == cfg.Realschulabschluss && checkCompList(failComp.MultiMajorCompGE, 1): // tries to compensate from "multiMajorCompGE"-list
			log, failComp.MinorFail[i] = compensateSub(failComp.MinorFail[i], failComp.MultiMajorCompGE, 1, log)

		case checkCompList(failComp.PossibleMinorCompensation, 1): // tries to compensate from "possibleMinorCompensation"-list
			log, failComp.MinorFail[i] = compensateSub(failComp.MinorFail[i], failComp.PossibleMinorCompensation, 1, log)
		case checkCompList(failComp.MultiMinorComp, 2): // tries to compensate from "multiMinorComp"-list
			log, failComp.MinorFail[i] = compensateSub(failComp.MinorFail[i], failComp.MultiMinorComp, 2, log)
		default: // no compensation possible
			log, failComp.MinorFail[i] = markAndLogFailstructs(false, failComp.MinorFail[i], "---", log)
		}
	}
	return failComp, log
}

// TODO: write a Test
func allCompensated(failComp global.FailsAndCompensations) bool {
	for sub := 0; sub < len(failComp.MajorFail); sub++ {
		if !failComp.MajorFail[sub].Compensated {
			return false
		}
	}

	for sub := 0; sub < len(failComp.MinorFail); sub++ {
		if !failComp.MinorFail[sub].Compensated {
			return false
		}
	}

	return true
}

// function checks, if compList has enough subjects listed to compensate a failed subject. Number of needed compSubjects can be specified
func checkCompList(compList []global.CompensationSubject, numberOfCompNeeded int) bool {
	counter := 0
	for i := 0; i < len(compList); i++ {
		if !compList[i].Used {
			counter++
		}
	}
	return numberOfCompNeeded <= counter
}

// function compensates and sets all respective flags. Function takes failedSubject and a list of subjects to compensate, the number of subjects needed to compensate the fail and the igs.Student struct to add results to the log
func compensateSub(failedSub global.FailedSubject, compList []global.CompensationSubject, numberOfSubjectsToCompensate int, log [][3]string) ([][3]string, global.FailedSubject) {
	unusedPosition := findUnusedPosition(compList)
	for i := 0; i < numberOfSubjectsToCompensate; i++ {
		compList[unusedPosition+i].Used = true
	}
	compSub := conCatCompNames(compList, unusedPosition, numberOfSubjectsToCompensate)
	logAppended, failedSubMarked := markAndLogFailstructs(true, failedSub, compSub, log)
	return logAppended, failedSubMarked
}

func markAndLogFailstructs(compensated bool, failedSub global.FailedSubject, compensationSub string, log [][3]string) ([][3]string, global.FailedSubject) {
	var compensatedSign string
	if compensated {
		compensatedSign = "✓"
	} else {
		compensatedSign = "✗"
	}

	failedSub.Compensated = compensated
	failedSub.Compensatedby = compensationSub
	log = append(log, [3]string{failedSub.Name, compensatedSign, failedSub.Compensatedby})
	return log, failedSub
}

// function finds and returns position of unused compensation subject in slice of compensation subjects
// TODO: before simplifying - write a test first (so you can directly see, if it still works!)
// TODO: simplify and write a Test
func findUnusedPosition(compList []global.CompensationSubject) int {
	for i := 0; i < len(compList); i++ {
		if !compList[i].Used {
			return i
		}
	}
	return -1
}

// TODO: write a test
func conCatCompNames(multiComp []global.CompensationSubject, k int, numberofConcats int) string {
	var subjects []string
	for i := 0; i < numberofConcats; i++ {
		subjects = append(subjects, multiComp[k+i].Name)
	}
	return strings.Join(subjects, "+")
}
