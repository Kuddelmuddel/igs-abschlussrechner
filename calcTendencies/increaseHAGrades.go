package calctendencies

import "igs-abschlussrechner/global"

// TODO: write Test!
func IncreaseGradesForEligibleHAStudents(student *global.Student) {
	// Skip students who are not in 9th grade or in the wrong term
	if student.ClassYear != 9 || (student.Term == 2 && !student.ReportCardInfo.FinalExams.TookExams) {
		return
	}
	for j, subject := range student.ReportCardInfo.Subjects {
		// Skip all subjects with course levels or future FLD subjects
		if subject.CourseLvl != "" || isFuturFLD(subject.SubjectAbbreviation) {
			continue
		}

		// Increase HAGrade if conditions met
		subject.HAGrade = makeGradeValidAgain(subject.Grade - 1)
		subject.HAGradeDifferentThanOrginial = subject.HAGrade != subject.Grade

		// Update subject in student's report card
		student.ReportCardInfo.Subjects[j] = subject
	}
}
