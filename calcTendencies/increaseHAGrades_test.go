package calctendencies

import (
	"igs-abschlussrechner/global"
	"reflect"
	"testing"
)

func TestIncreaseGradesForEligibleHAStudents(t *testing.T) {
	testcases := []struct {
		description     string
		classTestStruct global.Student
		expectedResult  global.Student
	}{
		{
			description: "Test for eligible HA student (with grade increase), first Term (no Exam)",
			classTestStruct: global.Student{
				Term:      1,
				ClassYear: 9,
				ReportCardInfo: global.ReportCardInformation{
					Subjects: []global.SubjectStruc{
						{
							SubjectAbbreviation:          "D",
							CourseLvl:                    "C",
							Grade:                        3,
							HAGrade:                      0,
							HAGradeDifferentThanOrginial: false,
						},
						{
							SubjectAbbreviation:          "KU",
							CourseLvl:                    "",
							Grade:                        2,
							HAGrade:                      0,
							HAGradeDifferentThanOrginial: false,
						},
						{
							SubjectAbbreviation:          "SPO",
							CourseLvl:                    "",
							Grade:                        1,
							HAGrade:                      0,
							HAGradeDifferentThanOrginial: false,
						},
					},
					FinalExams: global.ExamsStruc{TookExams: false},
				},
			},
			expectedResult: global.Student{
				Term:      1,
				ClassYear: 9,
				ReportCardInfo: global.ReportCardInformation{
					Subjects: []global.SubjectStruc{
						{
							SubjectAbbreviation:          "D",
							CourseLvl:                    "C",
							Grade:                        3,
							HAGrade:                      0,
							HAGradeDifferentThanOrginial: false,
						},
						{
							SubjectAbbreviation:          "KU",
							CourseLvl:                    "",
							Grade:                        2,
							HAGrade:                      1,
							HAGradeDifferentThanOrginial: true,
						},
						{
							SubjectAbbreviation:          "SPO",
							CourseLvl:                    "",
							Grade:                        1,
							HAGrade:                      1,
							HAGradeDifferentThanOrginial: false,
						},
					},
					FinalExams: global.ExamsStruc{TookExams: false},
				},
			},
		},

		{
			description: "Test for eligible HA student (with grade increase), first Term (with Exam)",
			classTestStruct: global.Student{
				Term:      1,
				ClassYear: 9,
				ReportCardInfo: global.ReportCardInformation{
					Subjects: []global.SubjectStruc{
						{
							SubjectAbbreviation:          "D",
							CourseLvl:                    "C",
							Grade:                        3,
							HAGrade:                      0,
							HAGradeDifferentThanOrginial: false,
						},
						{
							SubjectAbbreviation:          "KU",
							CourseLvl:                    "",
							Grade:                        2,
							HAGrade:                      0,
							HAGradeDifferentThanOrginial: false,
						},
						{
							SubjectAbbreviation:          "SPO",
							CourseLvl:                    "",
							Grade:                        1,
							HAGrade:                      0,
							HAGradeDifferentThanOrginial: false,
						},
					},
					FinalExams: global.ExamsStruc{TookExams: true},
				},
			},
			expectedResult: global.Student{

				Term:      1,
				ClassYear: 9,
				ReportCardInfo: global.ReportCardInformation{
					Subjects: []global.SubjectStruc{
						{
							SubjectAbbreviation:          "D",
							CourseLvl:                    "C",
							Grade:                        3,
							HAGrade:                      0,
							HAGradeDifferentThanOrginial: false,
						},
						{
							SubjectAbbreviation:          "KU",
							CourseLvl:                    "",
							Grade:                        2,
							HAGrade:                      1,
							HAGradeDifferentThanOrginial: true,
						},
						{
							SubjectAbbreviation:          "SPO",
							CourseLvl:                    "",
							Grade:                        1,
							HAGrade:                      1,
							HAGradeDifferentThanOrginial: false,
						},
					},
					FinalExams: global.ExamsStruc{TookExams: true},
				},
			},
		},

		{
			description: "Test for eligible HA student (with grade increase), second Term, with Exam",
			classTestStruct: global.Student{
				Term:      1,
				ClassYear: 9,
				ReportCardInfo: global.ReportCardInformation{
					Subjects: []global.SubjectStruc{
						{
							SubjectAbbreviation:          "D",
							CourseLvl:                    "C",
							Grade:                        3,
							HAGrade:                      0,
							HAGradeDifferentThanOrginial: false,
						},
						{
							SubjectAbbreviation:          "KU",
							CourseLvl:                    "",
							Grade:                        2,
							HAGrade:                      0,
							HAGradeDifferentThanOrginial: false,
						},
						{
							SubjectAbbreviation:          "SPO",
							CourseLvl:                    "",
							Grade:                        1,
							HAGrade:                      0,
							HAGradeDifferentThanOrginial: false,
						},
					},
					FinalExams: global.ExamsStruc{TookExams: true},
				},
			},
			expectedResult: global.Student{
				Term:      1,
				ClassYear: 9,
				ReportCardInfo: global.ReportCardInformation{
					Subjects: []global.SubjectStruc{
						{
							SubjectAbbreviation:          "D",
							CourseLvl:                    "C",
							Grade:                        3,
							HAGrade:                      0,
							HAGradeDifferentThanOrginial: false,
						},
						{
							SubjectAbbreviation:          "KU",
							CourseLvl:                    "",
							Grade:                        2,
							HAGrade:                      1,
							HAGradeDifferentThanOrginial: true,
						},
						{
							SubjectAbbreviation:          "SPO",
							CourseLvl:                    "",
							Grade:                        1,
							HAGrade:                      1,
							HAGradeDifferentThanOrginial: false,
						},
					},
					FinalExams: global.ExamsStruc{TookExams: true},
				},
			},
		},
		{
			description: "Test for not eligible HA student (no grade increase), second Term, no Exam",
			classTestStruct: global.Student{
				Term:      2,
				ClassYear: 9,
				ReportCardInfo: global.ReportCardInformation{
					Subjects: []global.SubjectStruc{
						{
							SubjectAbbreviation:          "D",
							CourseLvl:                    "C",
							Grade:                        3,
							HAGrade:                      0,
							HAGradeDifferentThanOrginial: false,
						},
						{
							SubjectAbbreviation:          "KU",
							CourseLvl:                    "",
							Grade:                        2,
							HAGrade:                      0,
							HAGradeDifferentThanOrginial: false,
						},
						{
							SubjectAbbreviation:          "SPO",
							CourseLvl:                    "",
							Grade:                        1,
							HAGrade:                      0,
							HAGradeDifferentThanOrginial: false,
						},
					},
					FinalExams: global.ExamsStruc{TookExams: false},
				},
			},
			expectedResult: global.Student{
				Term:      2,
				ClassYear: 9,
				ReportCardInfo: global.ReportCardInformation{
					Subjects: []global.SubjectStruc{
						{
							SubjectAbbreviation:          "D",
							CourseLvl:                    "C",
							Grade:                        3,
							HAGrade:                      0,
							HAGradeDifferentThanOrginial: false,
						},
						{
							SubjectAbbreviation:          "KU",
							CourseLvl:                    "",
							Grade:                        2,
							HAGrade:                      0,
							HAGradeDifferentThanOrginial: false,
						},
						{
							SubjectAbbreviation:          "SPO",
							CourseLvl:                    "",
							Grade:                        1,
							HAGrade:                      0,
							HAGradeDifferentThanOrginial: false,
						},
					},
					FinalExams: global.ExamsStruc{TookExams: false},
				},
			},
		},
		{
			description: "Test for not eligible HA student (no grade increase), not in 9th grade",
			classTestStruct: global.Student{
				Term:      2,
				ClassYear: 8,
				ReportCardInfo: global.ReportCardInformation{
					Subjects: []global.SubjectStruc{
						{
							SubjectAbbreviation:          "D",
							CourseLvl:                    "C",
							Grade:                        3,
							HAGrade:                      0,
							HAGradeDifferentThanOrginial: false,
						},
						{
							SubjectAbbreviation:          "KU",
							CourseLvl:                    "",
							Grade:                        2,
							HAGrade:                      0,
							HAGradeDifferentThanOrginial: false,
						},
						{
							SubjectAbbreviation:          "SPO",
							CourseLvl:                    "",
							Grade:                        1,
							HAGrade:                      0,
							HAGradeDifferentThanOrginial: false,
						},
					},
					FinalExams: global.ExamsStruc{TookExams: false},
				},
			},
			expectedResult: global.Student{
				Term:      2,
				ClassYear: 8,
				ReportCardInfo: global.ReportCardInformation{
					Subjects: []global.SubjectStruc{
						{
							SubjectAbbreviation:          "D",
							CourseLvl:                    "C",
							Grade:                        3,
							HAGrade:                      0,
							HAGradeDifferentThanOrginial: false,
						},
						{
							SubjectAbbreviation:          "KU",
							CourseLvl:                    "",
							Grade:                        2,
							HAGrade:                      0,
							HAGradeDifferentThanOrginial: false,
						},
						{
							SubjectAbbreviation:          "SPO",
							CourseLvl:                    "",
							Grade:                        1,
							HAGrade:                      0,
							HAGradeDifferentThanOrginial: false,
						},
					},
					FinalExams: global.ExamsStruc{TookExams: false},
				},
			},
		},
	}

	for _, test := range testcases {
		t.Run(test.description, func(t *testing.T) {
			// Call the function to be tested
			IncreaseGradesForEligibleHAStudents(&test.classTestStruct)

			// Compare the actual result with the expected result
			if !reflect.DeepEqual(test.classTestStruct, test.expectedResult) {
				t.Errorf("For %s, expected %v but got %v", test.description, test.expectedResult, test.classTestStruct)

				t.Errorf("Test case: %s\nGot result: %v\nExpected result: %v\n", test.description, test.classTestStruct, test.expectedResult)
			}
		})
	}
}
