package calctendencies

import (
	"igs-abschlussrechner/global"
)

const (
	upperLimitForLvlChange = 2
	lowerLimitForLvlChange = 5
)

// TODO: Write Test for function
// Auf- und Abstufungsgrenzen (const oben) in config schreiben?
func MarkLVLChance(student *global.Student) {
	for i := 0; i < len(student.ReportCardInfo.Subjects); i++ {
		subject := &student.ReportCardInfo.Subjects[i]
		ignoreIf := student.ClassYear == 10 && student.Term == 2
		if subject.CourseLvl != "" {
			switch {
			case subject.Grade <= upperLimitForLvlChange && !isExcludedLvl(subject.CourseLvl, []string{"A", "E"}):
				setLvlChange(subject, "↥", ignoreIf)
			case subject.Grade >= lowerLimitForLvlChange && !isExcludedLvl(subject.CourseLvl, []string{"C", "G"}) && !isExcludedSubject(subject.SubjectAbbreviation, []string{"F", "L"}):
				setLvlChange(subject, "↧", ignoreIf)
			case subject.LvlChangeNonLUSDData.ParentChangeRequest:
				setLvlChange(subject, "↧*", ignoreIf)
			}
		}
	}
}

func isExcludedLvl(lvl string, exclusions []string) bool {
	for _, exclusion := range exclusions {
		if lvl == exclusion {
			return true
		}
	}
	return false
}

func isExcludedSubject(subjectAbbreviation string, exclusions []string) bool {
	for _, exclusion := range exclusions {
		if subjectAbbreviation == exclusion {
			return true
		}
	}
	return false
}

func setLvlChange(subject *global.SubjectStruc, lvlChange string, ignoreChange bool) {
	subject.LvlChange = lvlChange

	if ignoreChange {
		subject.LvlChange = " ↦"
	}
	subject.LvlChangeNonLUSDData.ListedInLUSD = true
}
