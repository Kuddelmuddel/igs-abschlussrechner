package calctendencies

import (
	"igs-abschlussrechner/global"
	"reflect"
	"testing"
)

func TestMarkLVLChance(t *testing.T) {
	testcases := []struct {
		description    string
		input          global.Student
		expectedResult global.Student
	}{
		{
			description:    "emty struc",
			input:          global.Student{},
			expectedResult: global.Student{},
		},

		{
			description: "major subject, with level change (up)",
			input: global.Student{
				Term:      2,
				ClassYear: 9,
				ReportCardInfo: global.ReportCardInformation{
					Subjects: []global.SubjectStruc{{
						SubjectAbbreviation: "D",
						CourseLvl:           "B",
						LvlChange:           "",
						Grade:               1,
						LvlChangeNonLUSDData: global.NonLUSDLvlChangeStruc{
							ListedInLUSD: false,
						}}}}},
			expectedResult: global.Student{
				Term:      2,
				ClassYear: 9,
				ReportCardInfo: global.ReportCardInformation{
					Subjects: []global.SubjectStruc{{
						SubjectAbbreviation: "D",
						CourseLvl:           "B",
						LvlChange:           "↥",
						Grade:               1,
						LvlChangeNonLUSDData: global.NonLUSDLvlChangeStruc{
							ListedInLUSD: true,
						}}}}}},
		{
			description: "major subject, with level change (down)",
			input: global.Student{
				Term:      2,
				ClassYear: 9,
				ReportCardInfo: global.ReportCardInformation{
					Subjects: []global.SubjectStruc{{
						SubjectAbbreviation: "E",
						CourseLvl:           "B",
						LvlChange:           "",
						Grade:               5,
						LvlChangeNonLUSDData: global.NonLUSDLvlChangeStruc{
							ListedInLUSD: false,
						}}}}},
			expectedResult: global.Student{
				Term:      2,
				ClassYear: 9,
				ReportCardInfo: global.ReportCardInformation{
					Subjects: []global.SubjectStruc{{
						SubjectAbbreviation: "E",
						CourseLvl:           "B",
						LvlChange:           "↧",
						Grade:               5,
						LvlChangeNonLUSDData: global.NonLUSDLvlChangeStruc{
							ListedInLUSD: true,
						}}}}}},
		{
			description: "major subject, already highest course",
			input: global.Student{
				Term:      2,
				ClassYear: 9,
				ReportCardInfo: global.ReportCardInformation{
					Subjects: []global.SubjectStruc{{
						SubjectAbbreviation: "D",
						CourseLvl:           "A",
						LvlChange:           "",
						Grade:               1,
						LvlChangeNonLUSDData: global.NonLUSDLvlChangeStruc{
							ListedInLUSD: false,
						}}}}},
			expectedResult: global.Student{
				Term:      2,
				ClassYear: 9,
				ReportCardInfo: global.ReportCardInformation{
					Subjects: []global.SubjectStruc{{
						SubjectAbbreviation: "D",
						CourseLvl:           "A",
						LvlChange:           "",
						Grade:               1,
						LvlChangeNonLUSDData: global.NonLUSDLvlChangeStruc{
							ListedInLUSD: false,
						}}}}},
		},
		{
			description: "major subject, already lowest course",
			input: global.Student{
				Term:      2,
				ClassYear: 9,
				ReportCardInfo: global.ReportCardInformation{
					Subjects: []global.SubjectStruc{{
						SubjectAbbreviation: "E",
						CourseLvl:           "C",
						LvlChange:           "",
						Grade:               5,
						LvlChangeNonLUSDData: global.NonLUSDLvlChangeStruc{
							ListedInLUSD: false,
						}}}}},
			expectedResult: global.Student{
				Term:      2,
				ClassYear: 9,
				ReportCardInfo: global.ReportCardInformation{
					Subjects: []global.SubjectStruc{{
						SubjectAbbreviation: "E",
						CourseLvl:           "C",
						LvlChange:           "",
						Grade:               5,
						LvlChangeNonLUSDData: global.NonLUSDLvlChangeStruc{
							ListedInLUSD: false,
						}}}}}},
		{
			description: "major subject, ignore level change, 10th grade",
			input: global.Student{
				Term:      2,
				ClassYear: 10,
				ReportCardInfo: global.ReportCardInformation{
					Subjects: []global.SubjectStruc{{
						SubjectAbbreviation: "D",
						CourseLvl:           "B",
						LvlChange:           "",
						Grade:               1,
						LvlChangeNonLUSDData: global.NonLUSDLvlChangeStruc{
							ListedInLUSD: false,
						}}}}},
			expectedResult: global.Student{
				Term:      2,
				ClassYear: 10,
				ReportCardInfo: global.ReportCardInformation{
					Subjects: []global.SubjectStruc{{
						SubjectAbbreviation: "D",
						CourseLvl:           "B",
						LvlChange:           " ↦",
						Grade:               1,
						LvlChangeNonLUSDData: global.NonLUSDLvlChangeStruc{
							ListedInLUSD: true,
						}}}}}},
		{
			description: "minor subject, with level change (up)",
			input: global.Student{
				Term:      2,
				ClassYear: 9,
				ReportCardInfo: global.ReportCardInformation{
					Subjects: []global.SubjectStruc{{
						SubjectAbbreviation: "CH",
						CourseLvl:           "G",
						LvlChange:           "",
						Grade:               1,
						LvlChangeNonLUSDData: global.NonLUSDLvlChangeStruc{
							ListedInLUSD: false,
						}}}}},
			expectedResult: global.Student{
				Term:      2,
				ClassYear: 9,
				ReportCardInfo: global.ReportCardInformation{
					Subjects: []global.SubjectStruc{{
						SubjectAbbreviation: "CH",
						CourseLvl:           "G",
						LvlChange:           "↥",
						Grade:               1,
						LvlChangeNonLUSDData: global.NonLUSDLvlChangeStruc{
							ListedInLUSD: true,
						}}}}}},
		{
			description: "minor subject, with level change (down)",
			input: global.Student{
				Term:      2,
				ClassYear: 9,
				ReportCardInfo: global.ReportCardInformation{
					Subjects: []global.SubjectStruc{{
						SubjectAbbreviation: "PH",
						CourseLvl:           "E",
						LvlChange:           "",
						Grade:               5,
						LvlChangeNonLUSDData: global.NonLUSDLvlChangeStruc{
							ListedInLUSD: false,
						}}}}},
			expectedResult: global.Student{
				Term:      2,
				ClassYear: 9,
				ReportCardInfo: global.ReportCardInformation{
					Subjects: []global.SubjectStruc{{
						SubjectAbbreviation: "PH",
						CourseLvl:           "E",
						LvlChange:           "↧",
						Grade:               5,
						LvlChangeNonLUSDData: global.NonLUSDLvlChangeStruc{
							ListedInLUSD: true,
						}}}}}},
		{
			description: "minor subject, already highest course",
			input: global.Student{
				Term:      2,
				ClassYear: 9,
				ReportCardInfo: global.ReportCardInformation{
					Subjects: []global.SubjectStruc{{
						SubjectAbbreviation: "BIO",
						CourseLvl:           "E",
						LvlChange:           "",
						Grade:               1,
						LvlChangeNonLUSDData: global.NonLUSDLvlChangeStruc{
							ListedInLUSD: false,
						}}}}},
			expectedResult: global.Student{
				Term:      2,
				ClassYear: 9,
				ReportCardInfo: global.ReportCardInformation{
					Subjects: []global.SubjectStruc{{
						SubjectAbbreviation: "BIO",
						CourseLvl:           "E",
						LvlChange:           "",
						Grade:               1,
						LvlChangeNonLUSDData: global.NonLUSDLvlChangeStruc{
							ListedInLUSD: false,
						}}}}}},
		{
			description: "minor subject, already lowest course",
			input: global.Student{
				Term:      2,
				ClassYear: 9,
				ReportCardInfo: global.ReportCardInformation{
					Subjects: []global.SubjectStruc{{
						SubjectAbbreviation: "CH",
						CourseLvl:           "G",
						LvlChange:           "",
						Grade:               5,
						LvlChangeNonLUSDData: global.NonLUSDLvlChangeStruc{
							ListedInLUSD: false,
						}}}}},
			expectedResult: global.Student{
				Term:      2,
				ClassYear: 9,
				ReportCardInfo: global.ReportCardInformation{
					Subjects: []global.SubjectStruc{{
						SubjectAbbreviation: "CH",
						CourseLvl:           "G",
						LvlChange:           "",
						Grade:               5,
						LvlChangeNonLUSDData: global.NonLUSDLvlChangeStruc{
							ListedInLUSD: false,
						}}}}}},
		{
			description: "minor subject, ignore level change, 10th grade",
			input: global.Student{
				Term:      2,
				ClassYear: 10,
				ReportCardInfo: global.ReportCardInformation{
					Subjects: []global.SubjectStruc{{
						SubjectAbbreviation: "PH",
						CourseLvl:           "G",
						LvlChange:           "",
						Grade:               1,
						LvlChangeNonLUSDData: global.NonLUSDLvlChangeStruc{
							ListedInLUSD: false,
						}}}}},
			expectedResult: global.Student{
				Term:      2,
				ClassYear: 10,
				ReportCardInfo: global.ReportCardInformation{
					Subjects: []global.SubjectStruc{{
						SubjectAbbreviation: "PH",
						CourseLvl:           "G",
						LvlChange:           " ↦",
						Grade:               1,
						LvlChangeNonLUSDData: global.NonLUSDLvlChangeStruc{
							ListedInLUSD: true,
						}}}}}},
		{
			description: "subject without course level (no change)",
			input: global.Student{
				Term:      2,
				ClassYear: 9,
				ReportCardInfo: global.ReportCardInformation{
					Subjects: []global.SubjectStruc{{
						SubjectAbbreviation: "KU",
						CourseLvl:           "",
						LvlChange:           "",
						Grade:               1,
						LvlChangeNonLUSDData: global.NonLUSDLvlChangeStruc{
							ListedInLUSD: false,
						}}}}},
			expectedResult: global.Student{
				Term:      2,
				ClassYear: 9,
				ReportCardInfo: global.ReportCardInformation{
					Subjects: []global.SubjectStruc{{
						SubjectAbbreviation: "KU",
						CourseLvl:           "",
						LvlChange:           "",
						Grade:               1,
						LvlChangeNonLUSDData: global.NonLUSDLvlChangeStruc{
							ListedInLUSD: false,
						}}}}}},
	}
	for _, test := range testcases {
		t.Run(test.description, func(t *testing.T) {
			MarkLVLChance(&test.input)
			if !reflect.DeepEqual(test.input, test.expectedResult) {
				t.Errorf("For %s, expected %v but got %v", test.description, test.expectedResult, test.input)
			}
		})
	}
}
