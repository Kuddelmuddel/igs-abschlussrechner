package calctendencies

import "igs-abschlussrechner/global"

func addComment(student *global.Student, comentMsg string) {
	student.Comments.CommentMsg = append(student.Comments.CommentMsg, comentMsg)
	student.Comments.ContainsComment = true
}
