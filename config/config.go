package config

import (
	"log"
	"strings"

	"github.com/spf13/viper"
)

var cfg *Config

func LoadGlobalConfigVariables(path string) *Config {
	if cfg == nil {
		viper.AddConfigPath(path)
		viper.SetConfigName("config")
		viper.SetConfigType("yaml")
		// viper.AutomaticEnv()          // read in environment variables that match - for

		if err := viper.ReadInConfig(); err != nil {
			log.Fatalf("Error reading config file: %v", err)
		}

		// load variables from config file
		cfg = &Config{
			SchoolName:                         viper.GetString("RecordSpecifications.schoolName"),
			SchoolDescription:                  viper.GetString("RecordSpecifications.schoolDescription"),
			OhneAbschluss:                      viper.GetString("Abschlussbezeichnungen.ohneAbschluss"),
			Hauptschulabschluss:                viper.GetString("Abschlussbezeichnungen.Hauptschulabschluss"),
			Realschulabschluss:                 viper.GetString("Abschlussbezeichnungen.Realschulabschluss"),
			Versetztung11:                      viper.GetString("Abschlussbezeichnungen.Versetztung11"),
			Quali:                              viper.GetString("Abschlussbezeichnungen.Quali"),
			NaWiQuali:                          viper.GetString("Abschlussbezeichnungen.NaWiQuali"),
			NoQuali:                            viper.GetString("Abschlussbezeichnungen.NoQuali"),
			MajorSubjects:                      strings.Split(strings.ReplaceAll(viper.GetString("SchoolSpecifics.Hauptfächer"), " ", ""), ","),
			Encrypt:                            viper.GetBool("Setup.encryptExcelFiles"),
			Passphrase:                         viper.GetString("Setup.passphrase"),
			MinimumSubjectsToCalculateTendency: viper.GetInt("Setup.minimumSubjectsToCalculateTendency"),
			AutoPrintArea:                      viper.GetBool("Setup.autoPrintArea"),
			PrintArea:                          viper.GetString("Setup.printArea"),
			ZoomScale:                          viper.GetFloat64("Setup.ZoomScale"),
			ZoomScaleOverview:                  viper.GetFloat64("Setup.ZoomScaleOverview"),
			PrintRecordSheet:                   viper.GetBool("Setup.printRecordSheet"),
			IncludeFoerdermassnahmenHeader:     viper.GetBool("Setup.includeFoerdermassnahmenHeader"),
			SpellOutFOSandQualiInOverview:      viper.GetBool("Setup.spellOutFOSandQualiInOverview"),
			GenerateLvlChangeList:              viper.GetBool("Setup.generateLvlChangeList"),
			// PrintMissingAbsentsWarning:         false,
			ClearMCourses:           viper.GetBool("DataManipulation.clearMCourses"),
			Fachabkürzungen:         viper.GetStringMapString("Fachabkürzungen"),
			FachSortierungInAusgabe: viper.GetStringSlice("FachSortierungInAusgabe"),
			IgnorelvlChange:         viper.GetString("exceptions.ignorelvlChange"),
			FuturFLD:                strings.Split(viper.GetString("exceptions.futurFLD"), ", "),
			// NoGradeIncreaseforHA:               []string{},
			DetectConferenceFile: ConferencefileStruc{
				Detect: FileDetectionStruc{
					Cell:    viper.GetString("detectConferenceListBy.cell"),
					Content: viper.GetString("detectConferenceListBy.content"),
				},
				DetectClassTeacherRowBy: "",
				DetectSubjectRowBy:      viper.GetString("conferenceListRowDetectionStrings.detectSubjectRowBy"),
			},
			DetectFinalExamFile: FileDetectionStruc{
				Cell:    viper.GetString("detectFinalExamListBy.cell"),
				Content: viper.GetString("detectFinalExamListBy.content"),
			},
			DetectCourseLvlChangeFile: CourseLvlChanceFileStruc{
				Detect: FileDetectionStruc{
					Cell:    viper.GetString("CourseLvlChanceFile.detectfilebyCell"),
					Content: viper.GetString("CourseLvlChanceFile.detectfilebyContent"),
				},
				DetectionStringName:                viper.GetString("CourseLvlChanceFile.detectionStringName"),
				DetectionStringClass:               viper.GetString("CourseLvlChanceFile.detectionStringClass"),
				DetectionStringSubject:             viper.GetString("CourseLvlChanceFile.detectionStringSubject"),
				DetectionStringTeacher:             viper.GetString("CourseLvlChanceFile.detectionStringTeacher"),
				DetectionStringCurrentLevel:        viper.GetString("CourseLvlChanceFile.detectionStringCurrentLevel"),
				DetectionStringObjection:           viper.GetString("CourseLvlChanceFile.detectionStringObjection"),
				DetectionStringGrade:               viper.GetString("CourseLvlChanceFile.detectionStringGrade"),
				DetectionStringFutureLevel:         viper.GetString("CourseLvlChanceFile.detectionStringFutureLevel"),
				DetectionStringParentChangeRequest: viper.GetString("CourseLvlChanceFile.detectionStringParentChangeRequest"),
				IgnoreIfFirstZellInRowIsEmpty:      viper.GetBool("CourseLvlChanceFile.ignoreIfFirstZellInRowIsEmpty"),
			},
			DetectAdditionalInfoFile: AdditionalInfoFileStruc{
				Detect: FileDetectionStruc{
					Cell:    viper.GetString("AdditionalInfoFile.detectfilebyCell"),
					Content: viper.GetString("AdditionalInfoFile.detectfilebyContent"),
				},
				XlsxSheetname:              viper.GetString("AdditionalInfoFile.xlsxSheetname"),
				DetectionStringLastname:    viper.GetString("AdditionalInfoFile.detectionStringLastname"),
				DetectionStringForename:    viper.GetString("AdditionalInfoFile.detectionStringForename"),
				DetectionStringClass:       viper.GetString("AdditionalInfoFile.detectionStringClass"),
				DetectionStringNotenschutz: viper.GetString("AdditionalInfoFile.detectionStringNotenschutz"),
			},
			DetectManualInputFile: ManualInputFileStruc{
				Detect: FileDetectionStruc{
					Cell:    viper.GetString("manualInputFile.detectfilebyCell"),
					Content: viper.GetString("manualInputFile.detectfilebyContent"),
				},
				DetectSubjectRowBy: viper.GetString("manualInputFile.detectSubjectRowBy"),
			},
			DetectLUSDGraduationResults: FileDetectionStruc{
				Cell:    viper.GetString("detectLUSDGraduationResultstBy.cell"),
				Content: viper.GetString("detectLUSDGraduationResultstBy.content"),
			},
		}

		cfg.GraduationOrder = map[string]int{
			cfg.OhneAbschluss:       0,
			cfg.Hauptschulabschluss: 1,
			cfg.Realschulabschluss:  2,
			cfg.Versetztung11:       3,
		}
	}
	return cfg
}

func GetConfig() *Config {
	if cfg == nil {
		log.Fatal("Configuration not loaded. Call LoadGlobalConfigVariables first.")
	}
	return cfg
}

// constants and variables
const LusdHaGradAbbr = "H"
const LusdHaGrad = "Hauptschulabschluss"

const LusdHaGradQualiAbbr = "HQ"
const LusdHaGradQuali = "qualifizierender Hauptschulabschluss"

const LusdRaGradAbbr = "MA"
const LusdRaGrad = "mittlerer Abschluss (Realschulabschluss)"

const LusdRaGradQualiAbbr = "MAQ"
const LusdRaGradQuali = "qualifizierender mittlerer Abschluss (Realschulabschluss)"

// TODO: add constants (variables only in yaml-config-file)
// Hauptschulprojektprüfung (used in average calculation)

// FAQ
const NaWiInGrade8 = "• Die unverkursten Naturwissenschaften in Klasse 8 werden bei den Abschlusskriterien noch zu den 'übrigen Fächern' gezählt. Für V11 ist Note 4 deshalb ein Ausfall."
const HauptschulabschlussAverage = "Für den Hauptschulabschluss-Schnitt werden die Endnoten aller Fächer und die Note der Präsentationsprüfung gewertet, wobei die 3 Prüfungsfächer sowie die Note der Präsentationsprüfung doppelt gewichtet werden."
const RealschulabschlussAverage = "Für den Realschulabschluss-Schnitt werden die Endnoten aller Fächer gewertet, wobei die 4 Prüfungsfächer doppelt gewichtet werden. Wurde ein Prüfungsfach im 10. Schuljahr nicht unterrichtet, zählt die Vorjahresnote (sie taucht aber nicht im Zeugnis auf)."

// Comments
const CommentNoRAPNoQuali = "• Keinen Quali, da Schüler*In nicht an Realschulprüfung teilgenommen hat"
const CommentNoHAPNoQuali = "• Keinen Quali, da Schüler*In nicht an Hauptschulprüfung teilgenommen hat"
const CommentHAGradeIncreaseInfo = "• Notenverbesserungen nicht differenzierter Fächer (^) im 2. Halbjahr nur bei Teilnahme an Hauptschulprüfung."

// Warnings
const WarningTakeRAP = "• Schüler*In sollte ggf. an Realschulprüfungen teilnehmen, um den Quali zu erhalten!"
const WarningTakeHRP = "• Schüler*In sollte ggf. an Hauptschulprüfung teilnehmen, um den Quali zu erhalten!"
const WarningNoGradWithoutHaIncrease = "• Schüler*In sollte an HA-Prüfungen teilnehmen, da die aktuelle Hauptschulabschlusstendenz nur aufgrund HA-Notenverbesserung besteht."
const WarningMissingE3forV11NextYear = "• v11 (ggf. nach Abstufungen) nur erreicht über eine E3 (oder besser) in Bio. Bio fällt in Klasse 10 aber weg!"
const WarningMissingACourseAndE3forV11NextYear = "• Schüler*In hat nur zwei A-Kurse und nur Note 3 oder schlechter in NaWi-Fächern. Für V11-Tendenz im nächsten Jahr wird ein E-Kurs mit mindestens Note 3 benötigt (ggf. Einspruch bei Einstufung der NaWi-Fächer bedenken)."
const WarningCheckHA = "• Kein Realschulabschluss, aber Hauptschulabschluss möglich (bzw. Hauptschulabschluss bereits vorhanden?)"
const WarningPaedLevelIncrease = "• Nicht genügend A & B-Kurse für Klasse 10 (HA+Quali), ggf. pädagogische Aufstufung bedenken!"
const WarningMissingGrades = "• Fehlende Abschluss- oder Zeugnisnoten - Stufenleitung muss LUSD-Export-Datei prüfen"
const WarningMissingCourseSpecifications = "• Fehlende Kurszuweisung: v-Kurs(e), Tendenzberechnung wahrscheinlich fehlerhaft!"
const WarningGradejump = "• Notensprung in Fach " // subject will be added automatically to string
const CriticalCourseLvlDowngrade = "• Abstufung(en) könnte(n) aktuelle Abschlusstendenz gefährden, Widerspruch empfehlen?"
const NoTendencyCalcPossible = "• Es konnte keine automatische Tendenz berechnet werden"
const MissingExamSubject = "• Es fehlt mindestens eine der vier Prüfungsnoten"
const DifferenceInLUSDGradiationAndClacGraduation = "• LUSD-Abschlusstendenz und berechnete Abschlusstendenz stimmen nicht überein."
const AverageMissmatchBetweenLUSDandCalc = "• LUSD-Gesamtleistung und berechnete Gesamtleistung (Durchschnitt) stimmen nicht überein."
const ExamDataReplacedByBackup = "• Fehlende LUSD-Abschlussprüfungsdaten aus Backupdatensatz (Ergebnisse_Abschlussprüfung*.xlsx) ergänzt: "
const UnexcusedAbsentsWarningLimit = 15
const WarningUnexcusedAbsents = "• Viele unentschuldigte Fehlstunden: Attestpflicht androhen/verhängen?"
