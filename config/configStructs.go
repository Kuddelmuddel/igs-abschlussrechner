package config

type Config struct {
	SchoolName                         string
	SchoolDescription                  string
	OhneAbschluss                      string
	Hauptschulabschluss                string
	Realschulabschluss                 string
	Versetztung11                      string
	Quali                              string
	NaWiQuali                          string
	NoQuali                            string
	MajorSubjects                      []string
	Encrypt                            bool
	Passphrase                         string
	MinimumSubjectsToCalculateTendency int
	AutoPrintArea                      bool
	PrintArea                          string
	ZoomScale                          float64
	ZoomScaleOverview                  float64
	PrintRecordSheet                   bool
	IncludeFoerdermassnahmenHeader     bool
	SpellOutFOSandQualiInOverview      bool
	GenerateLvlChangeList              bool
	PrintMissingAbsentsWarning         bool
	ClearMCourses                      bool
	Fachabkürzungen                    map[string]string
	FachSortierungInAusgabe            []string
	IgnorelvlChange                    string
	FuturFLD                           []string
	NoGradeIncreaseforHA               []string
	DetectConferenceFile               ConferencefileStruc
	DetectFinalExamFile                FileDetectionStruc
	DetectCourseLvlChangeFile          CourseLvlChanceFileStruc
	DetectAdditionalInfoFile           AdditionalInfoFileStruc
	DetectManualInputFile              ManualInputFileStruc
	DetectLUSDGraduationResults        FileDetectionStruc
	GraduationOrder                    map[string]int
}

type FileDetectionStruc struct {
	Cell    string
	Content string
}

type ConferencefileStruc struct {
	Detect                  FileDetectionStruc
	DetectClassTeacherRowBy string
	DetectSubjectRowBy      string
}

type CourseLvlChanceFileStruc struct {
	Detect                             FileDetectionStruc
	DetectionStringName                string
	DetectionStringClass               string
	DetectionStringSubject             string
	DetectionStringTeacher             string
	DetectionStringCurrentLevel        string
	DetectionStringObjection           string
	DetectionStringGrade               string
	DetectionStringFutureLevel         string
	DetectionStringParentChangeRequest string
	IgnoreIfFirstZellInRowIsEmpty      bool
}
type AdditionalInfoFileStruc struct {
	Detect                     FileDetectionStruc
	XlsxSheetname              string
	DetectionStringLastname    string
	DetectionStringForename    string
	DetectionStringClass       string
	DetectionStringNotenschutz string
}

type ManualInputFileStruc struct {
	Detect             FileDetectionStruc
	DetectSubjectRowBy string
}
