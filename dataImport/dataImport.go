package dataimport

import (
	misc "igs-abschlussrechner/miscellaneous"

	"github.com/TwiN/go-color"
	"github.com/xuri/excelize/v2"
)

// GetSheetInfo fetches sheet names of an excle file.
func GetSheetInfo(filename string) map[int]string {
	f, err := excelize.OpenFile(filename)
	misc.CheckError("Failed to open excel file in GetSheetInfo function, dataImport.go", err)

	sheetMap := f.GetSheetMap()
	return sheetMap
}

// ImportSheetOfXLSXfile imports one sheet of an XLSX-file and returns the data as a [][]string. If the specified sheetstring is empty, the first sheet is used.
func ImportSheetOfXLSXfile(filename string, sheetname string) [][]string {
	f, err := excelize.OpenFile(filename)
	misc.CheckError("Error reading xlsx-file in ReadXLSX function, dataImport.go", err)

	if sheetname == "" {
		sheetMap := f.GetSheetMap()
		sheetname = sheetMap[1]
	}

	sheetData, err := f.GetRows(sheetname)
	misc.CheckError("Error getting row content while reading xlsx-file in ReadXLSX function, dataImport.go", err)
	return sheetData
}

// ImportAllSheetsOfXLSXfile imports all sheets of a XLSX-file and returns the data as [sheet][row][col]string.
func ImportAllSheetsOfXLSXfile(file string) [][][]string {
	var sheetsList [][][]string
	sheets := GetSheetInfo(file)
	for _, sheet := range sheets {
		misc.ConsoleLogPrintln(color.InGreen("    * ")+"Importing data from sheet "+color.InCyan(sheet), true, true)
		sheetsList = append(sheetsList, ImportSheetOfXLSXfile(file, sheet))
	}
	return sheetsList
}
