package dataimport

import (
	"igs-abschlussrechner/global"
	"testing"
)

// TODO: biger ones: write test for ParseConferenceList

// try not use file for testing ...
// func TestParseTemporalInformation(t *testing.T) {
// 	dummyFile := ImportAllSheetsOfXLSXfile("../testData/dummy_Zeugniskonferenzliste.xlsx")

// 	dateOfFile, years, term := parseTemporalInformation(dummyFile[0][0][0])
// 	switch {
// 	case dateOfFile != "28.06.2021":
// 		t.Errorf("parseTemporalInformation FAILED. Expected %s, got %s./n", "28.06.2021", dateOfFile)
// 	case years != "2020/2021":
// 		t.Errorf("parseTemporalInformation FAILED. Expected %s, got %s./n", "2020/2021", years)
// 	case term != 2:
// 		t.Errorf("parseTemporalInformation FAILED. Expected %s, got %s./n", 2, term)
// 	}

// 	dateOfFileEmpty, yearsEmpty, termEmpty := parseTemporalInformation("")
// 	switch {
// 	case dateOfFileEmpty != "":
// 		t.Errorf("parseTemporalInformation FAILED. Expected %s, got %s./n", "", dateOfFileEmpty)
// 	case yearsEmpty != "":
// 		t.Errorf("parseTemporalInformation FAILED. Expected %s, got %s./n", "", yearsEmpty)
// 	case termEmpty != "":
// 		t.Errorf("parseTemporalInformation FAILED. Expected %s, got %s./n", "", termEmpty)
// 	}
// }

// TODO: write test for parseClassInformation

// TODO: bigger ones: write Test for parseCourseList

func TestParseName(t *testing.T) {
	testcases := []struct {
		description    string
		input          string
		expectedResult string
	}{
		{"empty string", "", "no name detected"},
		{"Class info row", "Klasse: 09a    Klassenleitung: Umbridge, Dolores    Vertretung: McGonagall, Minerva", "no name detected"},
		{"FachLehrer-row", "Potter, Harry-FachLehrer", "Potter, Harry"},
		{"Fehlzeiten-row", "Potter, Harry-Fehlzeiten\n		-:-/23:6 ja", "Potter, Harry"},
		{"Kurs-row", "Potter, Harry-Kurs", "Potter, Harry"},
		{"Note-row", "Potter, Harry-Note", "Potter, Harry"},
		{"name with dashes and second name", "von Nachname, NameTeil1-NameTeil2 Zweitname-FachLehrer", "von Nachname, NameTeil1-NameTeil2 Zweitname"},
	}

	for _, test := range testcases {
		t.Run(test.description, func(t *testing.T) {
			result := parseName(test.input)
			if result != test.expectedResult {
				t.Errorf("For %s, expected %s but got %s", test.description, test.expectedResult, result)
			}
		})
	}
}

// TODO: write test for parseAVSVGrade

// TODO: write test for parseAVSVGrade

// TODO: write test for parseRepeatedSchoolyears

// TODO: write test for parseTotalAbsents

func TestParseAbsentsFromString(t *testing.T) {
	testcases := []struct {
		description    string
		input          string
		expectedResult global.AbsentStruc
	}{
		{"empty string", "", global.AbsentStruc{Absents: -1, UnexcusedAbsents: -1, AbsentDays: -1, UnexcusedDays: -1}},
		{"invalid format", "", global.AbsentStruc{Absents: -1, UnexcusedAbsents: -1, AbsentDays: -1, UnexcusedDays: -1}},
		{"Absents string without input", "-:-/-:-", global.AbsentStruc{Absents: -1, UnexcusedAbsents: -1, AbsentDays: -1, UnexcusedDays: -1}},
		{"Simple absents string", "-:-/2:2", global.AbsentStruc{Absents: 2, UnexcusedAbsents: 2, AbsentDays: -1, UnexcusedDays: -1}},
		{"Absents with 'ja' string", "-:-/16:2 ja", global.AbsentStruc{Absents: 16, UnexcusedAbsents: 2, AbsentDays: -1, UnexcusedDays: -1}},
		{"Absents with 'keine' string", "-:-/340:340 keine", global.AbsentStruc{Absents: 340, UnexcusedAbsents: 340, AbsentDays: -1, UnexcusedDays: -1}},
		{"Absents with full input", "3:2/40:33 keine", global.AbsentStruc{Absents: 40, UnexcusedAbsents: 33, AbsentDays: 3, UnexcusedDays: 2}},
	}
	for _, test := range testcases {
		t.Run(test.description, func(t *testing.T) {
			result := parseTotalAbsents(test.input)
			if result != test.expectedResult {
				t.Errorf("For %s, expected %+v but got %+v", test.description, test.expectedResult, result)
			}
		})
	}
}

// TODO: write test for parseGradeJump

// TODO: bigger ones: write test for parseCourseInformation

// TODO: write test for multiContentColumnCheck

// TODO: hardcode the string rather than importin and searching for it
// func TestMultiContentColumnCheck(t *testing.T) {
// 	dummyFile := ImportAllSheetsOfXLSXfile("../testData/dummy_Zeugniskonferenzliste.xlsx")
// 	header := courseHeader{16, "WPU", ""}

// 	result := multiContentColumnCheck(dummyFile[0], 6, 4, header)

// 	if result == false {
// 		t.Errorf("multiContentColumnCheck FAILED. Expected %t, got %t./n", false, result)
// 	}
// }

// TODO: write test for countContentInColumn

// TODO: write test for parseMultiColumnCourse

// TODO: write test for parseCourseInfo

// helper functions
// TODO: write test for findRowIndex

// TODO: write test for detectRowNumerPerStudent

// TODO: write test for checkNumberOfStudentRows

// TODO: write test for determineRowsOfStudent

// TODO: write test for findCourse

// TODO: write test for getIndexOfInfoRow

// TODO: write test for multiContentColumnCheck

// TODO: write test for countContentInColumn

// TODO: write test for checkValdityOfStudentDataStructure

// TODO: write test for countEmptyRows

// TODO: write test for checkIfCellExists

type testStringManualInput struct {
	description       string
	input             string
	expectedCourseLvl string
	expectedGrade     int
}

var testManualInputStrings = []testStringManualInput{
	{"empty String", "", "", -1},
	{"testcase A4", "A4", "A", 4},
	{"testcase B1", "B1", "B", 1},
	{"testcase C2", "C2", "C", 2},
	{"testcase G3", "G3", "G", 3},
	{"testcase E5", "E5", "E", 5},
	{"testcase 4A", "4A", "A", 4},
	{"invalid Input AA", "AA", "A", -1},
	{"invalid Input 23", "23", "", -1},
}

func TestParseManualInpurCourseLevel(t *testing.T) {
	for _, test := range testManualInputStrings {
		t.Run(test.description, func(t *testing.T) {
			result := parseManualInpurCourseLevel(test.input)
			if result != test.expectedCourseLvl {
				t.Errorf("For %s, expected %s but got %s", test.description, test.expectedCourseLvl, result)
			}
		})
	}
}

func TestParseManualInputGrade(t *testing.T) {
	for _, test := range testManualInputStrings {
		t.Run(test.description, func(t *testing.T) {
			result := parseManualInputGrade(test.input)
			if result != test.expectedGrade {
				t.Errorf("For %s, expected %d but got %d", test.description, test.expectedGrade, result)
			}
		})
	}
}
