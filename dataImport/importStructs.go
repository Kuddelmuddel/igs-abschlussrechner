package dataimport

// struct to save subject and teacher information of a class from the course header row
type courseHeader struct {
	column  int
	course  string
	teacher string
}

type examHeader struct {
	columnGrade   int
	columnSubject int
}
