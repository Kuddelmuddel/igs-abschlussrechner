package dataimport

import (
	config "igs-abschlussrechner/config"
	"igs-abschlussrechner/global"
	"strings"
)

func MergeConferenceAndExamData(studentData *[][]global.Student, examData map[string]global.ExamDataSource) {
	for _, class := range *studentData {
		for i := range class {
			student := &class[i]

			if student.ClassYear < 9 {
				continue
			}

			if exam, exists := examData[student.Name]; exists {
				student.ReportCardInfo.FinalExams.LUSDExamData = exam
				student.ReportCardInfo.FinalExams.LUSDExamData.DataPresent = true
			}
		}
	}
}

func MergeConferenceListDataAndLUSDGraduationBackupData(studentData *[][]global.Student, lusdGraduationBackupData map[string]global.ExamDataSource) {
	for _, class := range *studentData {
		for i := range class {
			student := &class[i]

			if student.ClassYear < 9 {
				continue
			}

			if backupData, exists := lusdGraduationBackupData[student.Name]; exists {
				student.ReportCardInfo.FinalExams.BackupExamData = backupData
				student.ReportCardInfo.FinalExams.BackupExamData.DataPresent = true
			}
		}
	}
}

// TODO: write TEST (check if Mathematik und Mathe still works)
func MergeConferenceListDataAndCourseLevelData(studentData *[][]global.Student, nonLUSDLvlChangeData *[]global.ImportedLvlChanges) {
	for _, class := range *studentData {
		for i := range class {
			student := &class[i]

			foundEntries := findSubjectInExternalData(student.Name, student.Class, nonLUSDLvlChangeData)
			updateStudentSubjects(&student.ReportCardInfo.Subjects, foundEntries)
		}
	}
}

func findSubjectInExternalData(studentName, class string, externaldata *[]global.ImportedLvlChanges) []global.ImportedLvlChanges {
	var foundStudentEntries []global.ImportedLvlChanges

	for i := range *externaldata {
		entry := &(*externaldata)[i]
		if compareNames(studentName, entry.Name) && compareClass(class, entry.Class) {
			entry.MatchFound = true
			foundStudentEntries = append(foundStudentEntries, *entry)
		}
	}
	return foundStudentEntries
}

func compareClass(lusdClass, externalClass string) bool {
	return strings.EqualFold(strings.TrimPrefix(externalClass, "0"), strings.TrimPrefix(lusdClass, "0"))
}

func compareNames(lusdNametag, externalNametag string) bool {
	nameParts := strings.FieldsFunc(externalNametag, func(r rune) bool {
		return r == ',' || r == ' '
	})

	for _, part := range nameParts {
		if !strings.Contains(lusdNametag, part) {
			return false
		}
	}

	return true
}

func updateStudentSubjects(subjects *[]global.SubjectStruc, foundEntries []global.ImportedLvlChanges) {
	for subIndex := range *subjects {
		subject := &(*subjects)[subIndex]
		for _, entry := range foundEntries {
			if strings.Contains(entry.SubjectFullName, subject.SubjectFullName) {
				updateSubjectData(subject, entry)
			}
		}
	}
}

func updateSubjectData(subject *global.SubjectStruc, entry global.ImportedLvlChanges) {
	subject.LvlChangeNonLUSDData.ListedinNonLUSDData = true
	subject.LvlChangeNonLUSDData.CurrentCourseLvl = entry.CurrentCourseLvl
	subject.LvlChangeNonLUSDData.FutureCourseLvl = entry.FutureCourseLvl
	subject.LvlChangeNonLUSDData.AlreadyObjected = entry.AlreadyObjected
	subject.LvlChangeNonLUSDData.ParentChangeRequest = entry.ParentChangeRequest
}

// TODO: write TEST
func MergeConferenceListDataAndAdditionalData(studentData *[][]global.Student, nonLUSDData *[]global.ImportedAdditionalInfo) {
	for _, class := range *studentData {
		for i := range class {
			student := &class[i]

			for j, additionalInfo := range *nonLUSDData {
				if compareNames(student.Name, additionalInfo.Lastname+", "+additionalInfo.Forename) &&
					compareClass(student.Class, additionalInfo.Class) {

					student.Tendency.ParentsGraduationGoal = getGraduationGoal(additionalInfo.GraduationGoal)
					student.ReportCardInfo.Notenschutz = additionalInfo.Notenschutz
					(*nonLUSDData)[j].MatchFound = true
					break
				}
			}
		}
	}
}

func getGraduationGoal(goal string) string {
	cfg := config.GetConfig()

	switch strings.ToUpper(goal) {
	case "V11":
		return cfg.Versetztung11
	case "MA", "RA":
		return cfg.Realschulabschluss
	case "HA":
		return cfg.Hauptschulabschluss
	case "OHNE RM":
		return "ohne Rückmeldung"
	default:
		return goal
	}
}
