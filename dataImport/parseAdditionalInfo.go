package dataimport

import (
	"igs-abschlussrechner/config"
	"igs-abschlussrechner/global"
	"strings"
)

func ParseAdditionalInfos(additionalInfoList [][]string) []global.ImportedAdditionalInfo {
	var additionalInfos []global.ImportedAdditionalInfo
	cfg := config.GetConfig()

	// detect row and col containing student names and parent goals
	lastNameColumn := cfg.DetectAdditionalInfoFile.DetectionStringLastname
	foreNameColumn := cfg.DetectAdditionalInfoFile.DetectionStringForename
	goalColumn := cfg.DetectAdditionalInfoFile.Detect.Content
	classColumn := cfg.DetectAdditionalInfoFile.DetectionStringClass
	notenschutzColumn := cfg.DetectAdditionalInfoFile.DetectionStringNotenschutz

	rowLastname, colLastname := findRowColIndex(additionalInfoList, lastNameColumn)
	_, colForename := findRowColIndex(additionalInfoList, foreNameColumn)
	_, colGradGoal := findRowColIndex(additionalInfoList, goalColumn)
	_, collClass := findRowColIndex(additionalInfoList, classColumn)
	_, collNotenschutz := findRowColIndex(additionalInfoList, notenschutzColumn)

	// filling GradGoalInfo struct
	for i := rowLastname + 1; i < len(additionalInfoList); i++ {
		var currentStudentData global.ImportedAdditionalInfo
		if len(additionalInfoList[i]) > colForename {
			currentStudentData.Forename = strings.TrimSpace(additionalInfoList[i][colForename])
		}
		if len(additionalInfoList[i]) > colLastname {
			currentStudentData.Lastname = strings.TrimSpace(additionalInfoList[i][colLastname])
		}
		if len(additionalInfoList[i]) > collClass {
			currentStudentData.Class = strings.TrimSpace(additionalInfoList[i][collClass])
		}
		if len(additionalInfoList[i]) > colGradGoal {
			currentStudentData.GraduationGoal = strings.TrimSpace(additionalInfoList[i][colGradGoal])
		}
		if len(additionalInfoList[i]) > collNotenschutz && strings.ToLower(strings.TrimSpace(additionalInfoList[i][collNotenschutz])) == "x" {
			currentStudentData.Notenschutz = true
		}

		if len(currentStudentData.Forename) > 0 && len(currentStudentData.Lastname) > 0 && len(currentStudentData.Class) > 0 && len(currentStudentData.GraduationGoal) > 0 {
			additionalInfos = append(additionalInfos, currentStudentData)
		}
	}
	return additionalInfos
}
