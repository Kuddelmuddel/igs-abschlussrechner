package dataimport

import (
	"igs-abschlussrechner/config"
	"igs-abschlussrechner/global"
	misc "igs-abschlussrechner/miscellaneous"

	"regexp"
	"strconv"
	"strings"

	"github.com/TwiN/go-color"
)

// ParseConferenceList collects general information of the class, information about each student and all taken Courses and returns these information in a struct
func ParseConferenceList(importedData [][][]string) ([][]global.Student, []string) {
	var allParsedStudentsData [][]global.Student
	var parsingWarnings []string

	for sheet := 0; sheet < len(importedData); sheet++ {
		classData, classWarnings := createClassList(importedData[sheet])

		allParsedStudentsData = append(allParsedStudentsData, classData)
		parsingWarnings = append(parsingWarnings, classWarnings...)
	}
	return allParsedStudentsData, parsingWarnings
}

// TODO: Improve function
func createClassList(importedClassData [][]string) ([]global.Student, []string) {
	var classList []global.Student
	var classWarnings []string
	cfg := config.GetConfig()

	// parse general class information
	dateOfFile, years, term := parseTemporalInformation(importedClassData[0][0])
	classTeacher, substituteTeacher, class, classYear, classChar := parseClassInformation(importedClassData[4][0])
	school := importedClassData[1][0]

	// loop through every student in list

	infoRowDetectContent := cfg.DetectConferenceFile.DetectSubjectRowBy
	courseList := parseCourseList(importedClassData, "Name / Fehlzeiten")
	headerRowIndex := findRowIndex(importedClassData, infoRowDetectContent, 0)
	rowsPerStudent, err := detectRowNumerPerStudent(importedClassData)
	misc.CheckError("  ERROR: Could not determine a consistent data structure in exported LUSD file!", err)

	for studentNum := headerRowIndex + rowsPerStudent; studentNum <= len(importedClassData); studentNum += rowsPerStudent + 1 {
		var currentStudent global.Student

		// write general information to Student struct
		currentStudent.DateOfFile = dateOfFile
		currentStudent.School = school
		currentStudent.YearsOfData = years
		currentStudent.Term = partseTerm(term)
		currentStudent.ClassTeacher = classTeacher
		currentStudent.SubstitudeTeacher = substituteTeacher
		currentStudent.Class = class
		currentStudent.ClassYear = classYear
		currentStudent.ClassChar = classChar

		// write specific information to student struct
		dataComplete, dataLackWarning := checkNumberOfStudentRows(importedClassData, studentNum, rowsPerStudent, currentStudent) // function checks, if student rows are complete (if not, student is skipped)
		if !dataComplete {
			classWarnings = append(classWarnings, dataLackWarning)
			studentNum -= (rowsPerStudent - determineRowsOfStudent(importedClassData, studentNum-rowsPerStudent+1)) // line corrects the loop grid to not break the loop due to lacking data
			continue
		}

		currentStudent.Name = parseName(importedClassData[studentNum][0])
		currentStudent.ReportCardInfo.AV = parseAVSVGrade(importedClassData, "AV", courseList, studentNum, rowsPerStudent)
		if currentStudent.ReportCardInfo.AV == "NA" {
			misc.AddWarning(&currentStudent, "• Fehlende Arbeitsverhaltennote (fehlt in exportierter LUSD-Excel-Datei)")
			misc.ConsoleLogPrintln(color.InRed("    * ")+"Missing work attitude grade (in LUSD-data) for "+color.InCyan(currentStudent.Name+" ("+currentStudent.Class+")"), true, true)
			classWarnings = append(classWarnings, "- "+currentStudent.Name+" ("+currentStudent.Class+", "+currentStudent.ClassTeacher+"): Fehlende Arbeitsverhaltennote (fehlt in exportierter LUSD-Excel-Datei)")
		}

		currentStudent.ReportCardInfo.SV = parseAVSVGrade(importedClassData, "SV", courseList, studentNum, rowsPerStudent)
		if currentStudent.ReportCardInfo.SV == "NA" {
			misc.AddWarning(&currentStudent, "• Fehlende Sozialverhaltennote (fehlt in exportierter LUSD-Excel-Datei)")
			misc.ConsoleLogPrintln(color.InRed("    * ")+"Missing social behavior grade (in LUSD-data) for "+color.InCyan(currentStudent.Name+" ("+currentStudent.Class+")"), true, true)
			classWarnings = append(classWarnings, "- "+currentStudent.Name+" ("+currentStudent.Class+", "+currentStudent.ClassTeacher+"): Fehlende Sozialverhaltennote (fehlt in exportierter LUSD-Excel-Datei)")
		}

		currentStudent.History = parseRepeatedSchoolyears(importedClassData, courseList, studentNum, rowsPerStudent, currentStudent.ClassYear)
		if currentStudent.History.RepeatsTotal == -1 {
			misc.AddWarning(&currentStudent, "• Fehlende Schulbesuchsjahre (Daten fehlen in exportierter LUSD-Excel-Datei)")
			misc.ConsoleLogPrintln(color.InRed("    * ")+"Missing school attendance years (in LUSD-data) for "+color.InCyan(currentStudent.Name+" ("+currentStudent.Class+")"), true, true)
			classWarnings = append(classWarnings, "- "+currentStudent.Name+" ("+currentStudent.Class+", "+currentStudent.ClassTeacher+"): Fehlende Schulbesuchsjahre (Daten fehlen in exportierter LUSD-Excel-Datei)")
		}
		currentStudent.ReportCardInfo.GradeJumps = parseGradeJump(importedClassData, courseList, studentNum, rowsPerStudent)

		// find and parse total absents
		indxAbsentsRow := getIndexOfInfoRow(importedClassData, studentNum, rowsPerStudent, "Fehlzeiten") // TODO: hardoce "Fehlzeiten" as const in config file or config package // and improve seaching for the cell content (returning cell string)
		currentStudent.ReportCardInfo.AbsentsTotal = parseTotalAbsents(importedClassData[studentNum-indxAbsentsRow][0])
		if currentStudent.ReportCardInfo.AbsentsTotal.Absents == -1 {
			misc.AddWarning(&currentStudent, "• Fehlende Fehlzeiten (Daten fehlen in LUSD-Daten)")
			misc.ConsoleLogPrintln(color.InRed("    * ")+"Missing absents data (in LUSD-data) for "+color.InCyan(currentStudent.Name+" ("+currentStudent.Class+")"), true, true)
			classWarnings = append(classWarnings, "- "+currentStudent.Name+" ("+currentStudent.Class+", "+currentStudent.ClassTeacher+"): Fehlende Fehlzeiten (Daten fehlen in LUSD-Daten)")
		}
		currentStudent.ReportCardInfo.Subjects, currentStudent.ReportCardInfo.UnratedSubjects = parseCourseInformation(importedClassData, courseList, studentNum, rowsPerStudent)

		if checkForAsterisks(currentStudent.ReportCardInfo) {
			misc.AddWarning(&currentStudent, "• Tendenz wegen *)-Noten bitte manuell prüfen!")
		}

		minSubNumber := cfg.MinimumSubjectsToCalculateTendency
		if len(currentStudent.ReportCardInfo.Subjects) < minSubNumber {
			currentStudent.Warnings.NoCalculationPossible = true
			misc.ConsoleLogPrintln(color.InYellow("    * ")+"Tendency for "+color.InCyan(currentStudent.Name+" ("+currentStudent.Class+")")+" could not be calculated because of too many *)-grades.", true, true)
			classWarnings = append(classWarnings, "- "+currentStudent.Name+" ("+currentStudent.Class+", "+currentStudent.ClassTeacher+"): Tendenz konnte aufgrund zu vieler *)-Noten nicht berechnet werden.")
		}

		classList = append(classList, currentStudent)
	}
	return classList, classWarnings
}

// parseTemporalInformation parses the Date of Process, the schoolyear and the school term from the given string
func parseTemporalInformation(str string) (string, string, string) {
	if len(str) == 0 {
		return "", "", ""
	}
	strSplit := strings.Split(str, "\n")
	fileDate := strSplit[0]
	schoolYearTerm := strings.Split(strSplit[1], " ")
	return fileDate, schoolYearTerm[0], schoolYearTerm[1]
}

// parseClassInformation parses the class teacher, the substitute teacher (if present), the class and class-year, and the class character from the given cell content (string)
func parseClassInformation(str string) (string, string, string, int, string) {
	strSplit := strings.Split(str, "    ")
	classYear := -1
	classTeacher := "noTeacherSpecified"
	var classTrim, subTeacher string

	if len(strSplit) >= 1 { // check for missing data
		classTrim = strings.TrimSpace(strings.Split(strSplit[0], ":")[1])
		classYear = extractClassYear(classTrim)
	}

	classChar := extractClassChar(classTrim)

	if len(strSplit) > 1 && len(strings.Split(strSplit[1], ":")) > 1 { // check for missing data
		classTeacher = strings.TrimSpace(strings.Split(strSplit[1], ":")[1])
	}
	if len(strSplit) > 2 && len(strings.Split(strSplit[2], ":")) > 1 { // check for missing data
		subTeacher = strings.TrimSpace(strings.Split(strSplit[2], ":")[1])
	}

	return classTeacher, subTeacher, strconv.Itoa(classYear) + classChar, classYear, classChar
}

// extractClassYear extracts the class year from the class string.
func extractClassYear(class string) int {
	re := regexp.MustCompile(`\d+`)
	if classRegex := re.FindString(class); classRegex != "" {
		classYear, _ := strconv.Atoi(classRegex)
		return classYear
	}
	return 0 // Default value if not found
}

// extractClassChar extracts the class character (or number) from the class string.
func extractClassChar(class string) string {
	re := regexp.MustCompile(`\D+.*`)
	return re.FindString(class)
}

// parseCourseList returns information of the "course header" row (containing information about all courses)
func parseCourseList(inputArray [][]string, detectionString string) []courseHeader {
	var courseCollection []courseHeader
	index := findRowIndex(inputArray, detectionString, 0)

	for i := 1; i < len(inputArray[index]); i++ {
		var courseData courseHeader

		splitAr := strings.SplitN(inputArray[index][i], "\n", 2)

		if splitAr[0] == "" {
			continue
		}

		courseData.column = i
		courseData.course = splitAr[0]
		if len(splitAr) == 2 {
			courseData.teacher = strings.TrimSpace(strings.ReplaceAll(splitAr[1], "\n", "/"))
		}
		courseCollection = append(courseCollection, courseData)
	}
	return courseCollection
}

func partseTerm(termString string) int {
	switch {
	case strings.Contains(termString, "1.Hj"):
		return 1
	case strings.Contains(termString, "2.Hj"):
		return 2
	}
	return -1
}

// parseName removes all characters after the last "-" and returns all characters befor that (which is the Name of the student). If no dash is in string, "no name detected" is returned.
func parseName(str string) string {
	ar := strings.Split(str, "\n")
	indx := strings.LastIndex(ar[0], "-")
	if indx == -1 || strings.Contains(str, "Klasse:") {
		return "no name detected"
	}
	return str[0:indx]
}

// function parses AV or SV grades from input data. If no grade is found it returns -1
func parseAVSVGrade(data [][]string, avsvSpecifier string, courseList []courseHeader, studentIndex, rowsPerStudent int) string {
	avColumn := findCourse(courseList, avsvSpecifier)
	indexGradeRow := getIndexOfInfoRow(data, studentIndex, rowsPerStudent, "Note")

	if checkIfCellExists(data, studentIndex-indexGradeRow, avColumn) && indexGradeRow != -1 {
		grade := strings.TrimSpace(data[studentIndex-indexGradeRow][avColumn])
		if grade == "*)" {
			return grade
		}
		svGrade, _ := strconv.Atoi(grade)
		if svGrade == 0 {
			return "NA"
		}
		return grade
	}
	return "NA"
}

// function parses how many and which school years were repeated
func parseRepeatedSchoolyears(data [][]string, courseList []courseHeader, studentNum, rowsPerStudent, classYear int) global.StudentHistory {
	var history global.StudentHistory
	indxAbsentsRow := getIndexOfInfoRow(data, studentNum, rowsPerStudent, "Fehlzeiten")
	wdhColumn := findCourse(courseList, "Wdh")

	if wdhColumn == -1 || indxAbsentsRow == -1 {
		history.RepeatsTotal = -1
		history.SchoolAttendanceYears = -1
		return history
	}

	history = parseRepeatedClass(data[studentNum-indxAbsentsRow][wdhColumn])
	history.SchoolAttendanceYears = classYear + history.RepeatsTotal
	return history
}

// function parses and returns, how often and which class a student has repeated. It also returns the total number of repeated classes
func parseRepeatedClass(str string) global.StudentHistory {
	var history global.StudentHistory
	var repeats []global.RepeatedGradeStruc

	repeatsArray := strings.Split(strings.TrimSpace(str), "-------") // info all in one cell, separated by "-------" (and some newlines)
	for i := 0; i < len(repeatsArray); i++ {
		repeatsSplit := strings.Split(strings.TrimSpace(repeatsArray[i]), "/") // info coded in string: "grad/term" eg. 06/1 (6 grade, first term)
		if len(repeatsSplit) > 1 {
			var repeatsAsInt global.RepeatedGradeStruc
			repeatsAsInt.RepeatedGrade, _ = strconv.Atoi(repeatsSplit[0])
			repeatsAsInt.RepeatedTerm, _ = strconv.Atoi(repeatsSplit[1])

			repeats = append(repeats, repeatsAsInt)
		}
	}

	history.RepeatedGrade = repeats

	// check if repeated grade is 0 (means the student is NDHS - and single entry should not be counted as a repeated year)
	if len(repeats) > 0 && repeats[0].RepeatedGrade == 0 {
		history.RepeatsTotal = len(repeats) / 2
	} else {
		history.RepeatsTotal = len(repeats)/2 + len(repeats)%2
	}

	return history
}

func parseGradeJump(data [][]string, courseList []courseHeader, studentIndex, rowsPerStudent int) bool {
	gradeJumpColumn := findCourse(courseList, "Noten-")
	indexGradeRow := getIndexOfInfoRow(data, studentIndex, rowsPerStudent, "Note")

	if checkIfCellExists(data, studentIndex-indexGradeRow, gradeJumpColumn) {
		return strings.Contains(data[studentIndex-indexGradeRow][gradeJumpColumn], "ja")
	}
	return false
}

func parseTotalAbsents(rawdata string) global.AbsentStruc {
	absents := global.AbsentStruc{Absents: -1, UnexcusedAbsents: -1, AbsentDays: -1, UnexcusedDays: -1}

	reObjectAbsents := regexp.MustCompile(`[^\d-]*([0-9-]+):([0-9-]+)/([0-9-]+):([0-9-]+)[^\d-]*`)
	matchedAbsents := reObjectAbsents.FindStringSubmatch(rawdata)

	if len(matchedAbsents) != 5 {
		return absents
	}

	if val, err := strconv.Atoi(matchedAbsents[1]); err == nil {
		absents.AbsentDays = val
	}
	if val, err := strconv.Atoi(matchedAbsents[2]); err == nil {
		absents.UnexcusedDays = val
	}
	if val, err := strconv.Atoi(matchedAbsents[3]); err == nil {
		absents.Absents = val
	}
	if val, err := strconv.Atoi(matchedAbsents[4]); err == nil {
		absents.UnexcusedAbsents = val
	}

	return absents
}

// TODO: Simplify (?) and add a test!
func parseCourseInformation(data [][]string, courseList []courseHeader, studentNum, rowsPerStudent int) ([]global.SubjectStruc, []global.SubjectStruc) {
	var parsedSubjects []global.SubjectStruc
	var parsedUnratedSubjects []global.SubjectStruc

	for i := 0; i < len(courseList); i++ {
		switch {
		case multiContentColumnCheck(data, studentNum, rowsPerStudent, courseList[i]):
			numberOfInfo := countContentInColumn(data, studentNum, rowsPerStudent, courseList[i])
			multiCourseInfo, hasMultiContent := parseMultiColumnCourse(data, studentNum, rowsPerStudent, courseList[i], numberOfInfo)
			for i := 0; i < len(multiCourseInfo); i++ {
				switch {
				case hasMultiContent[i] && multiCourseInfo[i].Grade != 0: // exclude all courses without Grade (grade = 0)
					parsedSubjects = append(parsedSubjects, multiCourseInfo[i])
				case hasMultiContent[i] && multiCourseInfo[i].Grade == 0:
					parsedUnratedSubjects = append(parsedUnratedSubjects, multiCourseInfo[i])
				}
			}
		default:
			courseInfo, hasContent := parseCourseInfo(data, studentNum, rowsPerStudent, courseList[i])
			if hasContent && courseInfo.Grade != 0 { // exclude all courses without Grade (grade = 0)
				parsedSubjects = append(parsedSubjects, courseInfo)
			} else if hasContent && courseInfo.Grade == 0 {
				parsedUnratedSubjects = append(parsedUnratedSubjects, courseInfo)
			}
		}
	}

	return parsedSubjects, parsedUnratedSubjects
}

// function parses and returns the info all courses stacked in one Column (sepearted by "-------" (needs the course ID (column number) and the last row of a student to find the information as well as the number of courses contained
func parseMultiColumnCourse(inputArray [][]string, studentrow, rowsPerStudent int, selectedCourse courseHeader, numberOfInfo int) ([]global.SubjectStruc, []bool) {
	courseInfos := make([]global.SubjectStruc, numberOfInfo)
	hasContent := make([]bool, numberOfInfo)

	for k := 0; k < rowsPerStudent; k++ {
		var cellContent string
		if len(inputArray[studentrow-k]) > selectedCourse.column {
			cellContent = inputArray[studentrow-k][selectedCourse.column]
		}
		splittedContent := strings.Split(cellContent, "-------")

		for i := 0; i < len(splittedContent); i++ {
			// same information for all splitted courses
			courseInfos[i].SubjectAbbreviation = selectedCourse.course
			courseInfos[i].SubjectFullName = misc.ConvertSubAbbToFullName(courseInfos[i].SubjectAbbreviation)

			switch k {
			case 0: // grade
				courseInfos[i].Grade, courseInfos[i].GradeJump = parseGrade(splittedContent, i)
			case 1: // courseLvl
				courseInfos[i].CourseLvl = parseCourseLevel(splittedContent, i)
			case 2: // absent
				courseInfos[i].Absents = parseAbsents(splittedContent[i])
			case 3: // teacher
				courseInfos[i].Teacher = parseTeacher(splittedContent, i, selectedCourse.teacher)
			}
			if courseInfos[i].Teacher == "" || courseInfos[i].Teacher == "in Stufe" || courseInfos[i].Teacher == "sprünge" {
				hasContent[i] = false
			} else {
				hasContent[i] = true
			}
		}
	}
	return courseInfos, hasContent
}

// function parses and returns the course info from the imported Array (needs the course ID (column number) and the last row of a student to find the information)
func parseCourseInfo(inputArray [][]string, studentrow, rowsPerStudent int, selectedCourse courseHeader) (global.SubjectStruc, bool) {
	var courseInfo global.SubjectStruc
	var hasContent bool

	// TODO: return absolute index? (not relative ones?)
	indxTeacher := getIndexOfInfoRow(inputArray, studentrow, rowsPerStudent, "Fachlehrkraft")
	indxCourseLvl := getIndexOfInfoRow(inputArray, studentrow, rowsPerStudent, "Kurs")
	indxGrade := getIndexOfInfoRow(inputArray, studentrow, rowsPerStudent, "Note")
	indxAbsents := getIndexOfInfoRow(inputArray, studentrow, rowsPerStudent, "Fehlzeiten")

	courseInfo.Teacher = parseTeacher(inputArray[studentrow-indxTeacher], selectedCourse.column, selectedCourse.teacher)
	courseInfo.SubjectAbbreviation = selectedCourse.course
	courseInfo.SubjectFullName = misc.ConvertSubAbbToFullName(selectedCourse.course)
	courseInfo.CourseLvl = parseCourseLevel(inputArray[studentrow-indxCourseLvl], selectedCourse.column)
	courseInfo.Grade, courseInfo.GradeJump = parseGrade(inputArray[studentrow-indxGrade], selectedCourse.column)
	if len(inputArray[studentrow]) > selectedCourse.column {
		courseInfo.AlternativeGrade = strings.TrimSpace(inputArray[studentrow-indxGrade][selectedCourse.column])
	}
	if indxAbsents != -1 && checkIfCellExists(inputArray, studentrow-indxAbsents, selectedCourse.column) {
		courseInfo.Absents = parseAbsents(inputArray[studentrow-indxAbsents][selectedCourse.column])
	} else {
		courseInfo.Absents.Absents = -1
		courseInfo.Absents.UnexcusedAbsents = -1
	}

	if courseInfo.Teacher == "" || courseInfo.Teacher == "in Stufe" || courseInfo.Teacher == "sprünge" {
		hasContent = false
		return global.SubjectStruc{}, hasContent
	}
	hasContent = true
	return courseInfo, hasContent

}

// function parses and returns the teacher abbreviation from a given string. If no abbreviation can be fount, the abbreviation in the golbal variable is taken
func parseTeacher(teacherCourseLokalStrArray []string, selectedCol int, teacherCourseGlobal string) string {
	if len(teacherCourseLokalStrArray) > selectedCol && len(strings.TrimSpace(teacherCourseLokalStrArray[selectedCol])) != 0 {
		extrTeacher := strings.ReplaceAll(teacherCourseLokalStrArray[selectedCol], "-------", "")
		extrTeachercleanOfSpaces := strings.TrimSpace(extrTeacher)
		extrTeacherclean := strings.ReplaceAll(extrTeachercleanOfSpaces, "\n", "/")
		return extrTeacherclean
	}
	return strings.TrimSpace(teacherCourseGlobal)
}

// function parses and returns the course level from a given string (the lvl is trailing the '(')
func parseCourseLevel(strArray []string, selectedCol int) string {
	cfg := config.GetConfig()
	clearMCourses := cfg.ClearMCourses

	if len(strArray) > selectedCol {
		braketPosition := strings.IndexRune(strings.TrimSpace(strArray[selectedCol]), '(')
		if braketPosition != -1 {
			course := strArray[selectedCol][braketPosition+1 : braketPosition+2]
			switch clearMCourses {
			case true:
				if course == "m" {
					return ""
				} else {
					return course
				}
			case false:
				return course
			}
		}
	}
	return ""
}

// function parses the grade, converts it to an integer
// Simplify and add Test!
func parseGrade(strArray []string, selectedCol int) (int, bool) {
	if len(strArray) <= selectedCol {
		return -1, false
	}
	switch {
	case strings.Contains(strArray[selectedCol], "(N)"):
		grade, _ := strconv.Atoi(strings.TrimSpace(strings.ReplaceAll(strArray[selectedCol], "(N)", "")))
		return grade, true
	default:
		grade, _ := strconv.Atoi(strings.TrimSpace(strArray[selectedCol]))
		return grade, false
	}
}

func parseAbsents(cellcontent string) global.AbsentStruc {
	var absents global.AbsentStruc

	absentsstring := strings.Split(strings.TrimSpace(cellcontent), "-------") // some cells have more than one absent input, separated by "-------" and some newlines

	if len(absentsstring) > 1 { // check, if a column has more than one input (separeded by "-------"), then sum all of those up
		var absentSum int
		var absentUnexusedSum int

		for i := 0; i < len(absentsstring); i++ {
			if !strings.Contains(absentsstring[i], "/") { // exclude, because the "Wdh in Stufe" is coded with an "/" and is not an absent
				absentsplit := strings.Split(strings.TrimSpace(absentsstring[i]), ":")
				absents, _ := strconv.Atoi(absentsplit[0])
				absentsUnexcused, _ := strconv.Atoi(absentsplit[1])

				absentSum += absents
				absentUnexusedSum += absentsUnexcused
			}
		}
		absents.Absents = absentSum
		absents.UnexcusedAbsents = absentUnexusedSum
		return absents
	} else if len(absentsstring) == 1 {
		absentsplit := strings.Split(strings.TrimSpace(absentsstring[0]), ":")
		if len(absentsplit) > 1 { // if len(absentsplit) is <= 1, then it's empty
			absents.Absents, _ = strconv.Atoi(absentsplit[0])
			absents.UnexcusedAbsents, _ = strconv.Atoi(absentsplit[1])
			return absents
		}
	}

	absents.Absents = -1
	absents.UnexcusedAbsents = -1
	return absents
}

func checkForAsterisks(studentReportcard global.ReportCardInformation) bool {
	for i := 0; i < len(studentReportcard.UnratedSubjects); i++ {
		if studentReportcard.UnratedSubjects[i].AlternativeGrade == "*)" {
			return true
		}
	}
	return false
}
