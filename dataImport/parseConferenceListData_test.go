package dataimport

import (
	config "igs-abschlussrechner/config"
	misc "igs-abschlussrechner/miscellaneous"
	"testing"

	"github.com/spf13/viper"
)

func TestConvertSubAbbToFullName(t *testing.T) {
	config.LoadGlobalConfigVariables("../")

	testcases := []struct {
		inputAbbreviation string
		expectedFullName  string
	}{
		{"", ""},         // Testing empty string
		{"d", "Deutsch"}, // Testing existent abbreviation
		{"D", "Deutsch"}, // Testing case insensitivity
		{"XXX", ""},      // Testing non-existent abbreviation
	}

	mockAbbrevMap := map[string]string{ // Set up the mock abbreviation map
		"d": "Deutsch",
	}
	viper.Set("Fachabkürzungen", mockAbbrevMap)

	// Run test cases
	for _, test := range testcases {
		actualFullName := misc.ConvertSubAbbToFullName(test.inputAbbreviation)
		if actualFullName != test.expectedFullName {
			t.Errorf("For '%s', expected '%s', but got '%s'", test.inputAbbreviation, test.expectedFullName, actualFullName)
		}
	}
}
