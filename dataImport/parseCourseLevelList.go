package dataimport

import (
	"igs-abschlussrechner/config"
	"igs-abschlussrechner/global"
	"strconv"
	"strings"
)

func ParseNonLUSDLvlCanges(lvlChangeList [][]string) []global.ImportedLvlChanges {
	cfg := config.GetConfig()
	var cleanlvlChangeList [][]string
	var lvlChangesList []global.ImportedLvlChanges

	ignoreRowsIfFirstCellisEmpty := cfg.DetectCourseLvlChangeFile.IgnoreIfFirstZellInRowIsEmpty
	if ignoreRowsIfFirstCellisEmpty { // skip rows with empty first cell
		for i := 0; i < len(lvlChangeList); i++ {
			if len(lvlChangeList[i]) != 0 && len(lvlChangeList[i][0]) != 0 {
				cleanlvlChangeList = append(cleanlvlChangeList, lvlChangeList[i])
			}
		}
	} else {
		cleanlvlChangeList = lvlChangeList
	}

	// detect row and col containing student names and parent goals
	NameColumn := cfg.DetectCourseLvlChangeFile.DetectionStringName
	classColumn := cfg.DetectCourseLvlChangeFile.DetectionStringClass
	subjectColumn := cfg.DetectCourseLvlChangeFile.DetectionStringSubject
	teacherColumn := cfg.DetectCourseLvlChangeFile.DetectionStringTeacher
	currentLevelColumn := cfg.DetectCourseLvlChangeFile.DetectionStringCurrentLevel
	FutureLevelColumn := cfg.DetectCourseLvlChangeFile.DetectionStringFutureLevel
	objectionColumn := cfg.DetectCourseLvlChangeFile.DetectionStringObjection
	gradeColumn := cfg.DetectCourseLvlChangeFile.DetectionStringGrade
	changeRequestColumn := cfg.DetectCourseLvlChangeFile.DetectionStringParentChangeRequest

	rowName, colName := findRowColIndex(cleanlvlChangeList, NameColumn)
	_, colClass := findRowColIndex(cleanlvlChangeList, classColumn)
	_, colSubject := findRowColIndex(cleanlvlChangeList, subjectColumn)
	_, colTeacher := findRowColIndex(cleanlvlChangeList, teacherColumn)
	_, colCurrentLevel := findRowColIndex(cleanlvlChangeList, currentLevelColumn)
	_, colFutureLevel := findRowColIndex(cleanlvlChangeList, FutureLevelColumn)
	_, colObjection := findRowColIndex(cleanlvlChangeList, objectionColumn)
	_, colGrade := findRowColIndex(cleanlvlChangeList, gradeColumn)
	_, colChangeRequest := findRowColIndex(cleanlvlChangeList, changeRequestColumn)

	// filling lvlChange struct
	for i := rowName + 1; i < len(cleanlvlChangeList); i++ {
		var currentStudentData global.ImportedLvlChanges

		if colName != -1 && len(cleanlvlChangeList[i]) > colName {
			currentStudentData.Name = strings.TrimSpace(cleanlvlChangeList[i][colName])
		}
		if colClass != -1 && len(cleanlvlChangeList[i]) > colClass {
			currentStudentData.Class = strings.TrimSpace(cleanlvlChangeList[i][colClass])
		}
		if colSubject != -1 && len(cleanlvlChangeList[i]) > colSubject {
			currentStudentData.SubjectFullName = strings.TrimSpace(cleanlvlChangeList[i][colSubject])
			currentStudentData.SubjectAbbreviation = convertSubFullNameToAbb(strings.TrimSpace(cleanlvlChangeList[i][colSubject]))
		}
		if colTeacher != -1 && len(cleanlvlChangeList[i]) > colTeacher {
			currentStudentData.Teacher = strings.TrimSpace(cleanlvlChangeList[i][colTeacher])
		}
		if colCurrentLevel != -1 && len(cleanlvlChangeList[i]) > colCurrentLevel {
			currentStudentData.CurrentCourseLvl = strings.TrimSpace(cleanlvlChangeList[i][colCurrentLevel])
		}
		if colFutureLevel != -1 && len(cleanlvlChangeList[i]) > colFutureLevel {
			currentStudentData.FutureCourseLvl = strings.TrimSpace(cleanlvlChangeList[i][colFutureLevel])
		}
		if colGrade != -1 && len(cleanlvlChangeList[i]) > colGrade {
			currentStudentData.Grade, _ = strconv.Atoi(cleanlvlChangeList[i][colGrade])
		}
		if colObjection != -1 && len(cleanlvlChangeList[i]) > colObjection && strings.ToLower(cleanlvlChangeList[i][colObjection]) == "x" {
			currentStudentData.AlreadyObjected = true
		}
		if colChangeRequest != -1 && len(cleanlvlChangeList[i]) > colChangeRequest && strings.ToLower(cleanlvlChangeList[i][colChangeRequest]) == "x" {
			currentStudentData.ParentChangeRequest = true
		}

		lvlChangesList = append(lvlChangesList, currentStudentData)
	}

	return lvlChangesList
}

// function converts full names of subject to abbreviation
func convertSubFullNameToAbb(subjectFullName string) string {
	cfg := config.GetConfig()
	abbrevMap := cfg.Fachabkürzungen

	reversed_abbrevMap := make(map[string]string)
	for key, value := range abbrevMap {
		reversed_abbrevMap[strings.ToLower(value)] = key
	}

	fullName := strings.ToLower(subjectFullName)
	return abbrevMap[fullName]
}
