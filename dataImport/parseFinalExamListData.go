package dataimport

import (
	config "igs-abschlussrechner/config"
	"igs-abschlussrechner/global"
	misc "igs-abschlussrechner/miscellaneous"
	"strconv"
	"strings"

	"github.com/TwiN/go-color"
)

// ParseConferenceList collects general information of the class, information about each student and all taken Courses and returns these information in a struct
func ParseFinalExamList(importedData [][][]string) map[string]global.ExamDataSource {
	examData := map[string]global.ExamDataSource{}
	for i := 0; i < len(importedData); i++ {
		currentMap := readFinalExamSheet(importedData[i])
		examData = mergeMaps(examData, currentMap) // TODO: check (or write a test), that no data is overwritten!
	}
	return examData
}

func mergeMaps(m1 map[string]global.ExamDataSource, m2 map[string]global.ExamDataSource) map[string]global.ExamDataSource {
	merged := make(map[string]global.ExamDataSource)
	for k, v := range m1 {
		merged[k] = v
	}

	for key, value := range m2 {
		if existingValue, exists := merged[key]; exists {
			merged[key] = mergeExamDataSoureWithoutOverwriting(existingValue, value)
		} else {
			merged[key] = value
		}
	}

	return merged
}

func mergeExamDataSoureWithoutOverwriting(r1, r2 global.ExamDataSource) global.ExamDataSource {
	merged := r1

	if r2.DataPresent {
		merged.DataPresent = r2.DataPresent
	}

	// IdFields:
	if r2.IdFields.StudentName != "" {
		merged.IdFields.StudentName = r2.IdFields.StudentName
	}
	if r2.IdFields.Class != "" {
		merged.IdFields.Class = r2.IdFields.Class
	}
	if r2.IdFields.ClassYear != 0 {
		merged.IdFields.ClassYear = r2.IdFields.ClassYear
	}
	if r2.IdFields.ClassChar != "" {
		merged.IdFields.ClassChar = r2.IdFields.ClassChar
	}
	if r2.IdFields.ClassTeacherAbbreviation != "" {
		merged.IdFields.ClassTeacherAbbreviation = r2.IdFields.ClassTeacherAbbreviation
	}
	if r2.IdFields.ClassTeacherFullName != "" {
		merged.IdFields.ClassTeacherFullName = r2.IdFields.ClassTeacherFullName
	}
	if r2.IdFields.Semester != "" {
		merged.IdFields.Semester = r2.IdFields.Semester
	}

	if r2.GraduationAbbreviation != "" { // add future NA value is included in parse function, add here to (&& e2.GraduationAbbreviation != "NA")
		merged.GraduationAbbreviation = r2.GraduationAbbreviation
	}
	if r2.GraduationFullName != "" { // add future NA value is included in parse function, add here to (&& e2.GraduationAbbreviation != "NA")
		merged.GraduationFullName = r2.GraduationFullName
	}

	if r2.TotalAverage > 0 {
		merged.TotalAverage = r2.TotalAverage
	}

	if r2.MajorsAverage > 0 {
		merged.MajorsAverage = r2.MajorsAverage
	}
	if r2.MinorsAverage > 0 {
		merged.MinorsAverage = r2.MinorsAverage
	}

	if len(r2.ExamSubjects) > 0 {
		merged.ExamSubjects = append(merged.ExamSubjects, r2.ExamSubjects...)
	}

	return merged
}

// TODO: redo and check, what I really nedd in this function (merging happens later! - just one sheet here. (Also hardcode detection scrings into config file))
func readFinalExamSheet(data [][]string) map[string]global.ExamDataSource {
	examData := map[string]global.ExamDataSource{}
	infoRowIndex := findRowIndex(data, "Name", 0) // TODO: hardcode in config package or in config file

	for row := infoRowIndex + 1; row < len(data); row++ {
		if !checkIfCellContainsStudentInfo(data, row, 0) {
			continue
		}

		name := strings.TrimSpace(data[row][0])
		pnIndex := findPNrow(data, row, name)
		znIndex := findZNIndex(data, row, name)

		if pnIndex == -1 {
			continue
		}

		var currentStudentExamData global.ExamDataSource
		currentStudentExamData.ExamSubjects = parseExamSubjectInformation(data, row, pnIndex, znIndex, infoRowIndex)
		haProjectPresentation, projectPresent := parsingPresentationExamData(data, infoRowIndex, znIndex)
		if projectPresent {
			currentStudentExamData.ExamSubjects = append(currentStudentExamData.ExamSubjects, haProjectPresentation)
		}
		currentStudentExamData.TotalAverage = parseLUSDtotalAverage(data, infoRowIndex, znIndex)
		currentStudentExamData.GraduationAbbreviation, currentStudentExamData.GraduationFullName = parseLUSDgraduationType(data, infoRowIndex, znIndex)
		currentStudentExamData.MajorsAverage, currentStudentExamData.MinorsAverage = parsingOtherLUSDAverages(data, infoRowIndex, znIndex)
		if checkIfExamDataPresent(currentStudentExamData) {
			examData[name] = currentStudentExamData
		}
	}
	return examData
}

func checkIfExamDataPresent(examData global.ExamDataSource) bool {
	if len(examData.ExamSubjects) == 0 && examData.TotalAverage == -1 && examData.MajorsAverage == -1 && examData.MinorsAverage == -1 && examData.GraduationAbbreviation == "" {
		return false
	}
	return true
}

func parsingOtherLUSDAverages(data [][]string, infoRowIndex, znIndex int) (float64, float64) {
	for col := 0; col < len(data[infoRowIndex]); col++ {
		if data[infoRowIndex][col] == "DS HF / Rest " && checkIfCellExists(data, znIndex, col) && data[znIndex][col] != "" && data[znIndex][col] != "--/--" {
			averagesCommaCorrected := strings.ReplaceAll(data[znIndex][col], ",", ".")
			averages := strings.Split(averagesCommaCorrected, "/")
			if len(averages) > 1 {
				lusdCalcMajorsAverage, _ := strconv.ParseFloat(averages[0], 64)
				lusdCalcMinorsAverage, _ := strconv.ParseFloat(averages[1], 64)
				return lusdCalcMajorsAverage, lusdCalcMinorsAverage
			}
		}
	}
	return -1, -1
}

func parseLUSDgraduationType(data [][]string, infoRowIndex, znIndex int) (string, string) {
	for col := 0; col < len(data[infoRowIndex]); col++ {
		if data[infoRowIndex][col] == "Abschl.-Art" && checkIfCellExists(data, znIndex, col) && data[znIndex][col] != "" {
			lusdCalcGrad := data[znIndex][col]
			lusdGradFullName := convertGraduationAbbToFullName(lusdCalcGrad)
			return lusdCalcGrad, lusdGradFullName
		}
	}
	return "", ""
}

func convertGraduationAbbToFullName(abb string) string {
	switch abb {
	case config.LusdHaGradAbbr:
		return config.LusdHaGrad
	case config.LusdHaGradQualiAbbr:
		return config.LusdHaGradQuali
	case config.LusdRaGradAbbr:
		return config.LusdRaGrad
	case config.LusdRaGradQualiAbbr:
		return config.LusdRaGradQuali
	default:
		return abb
	}
}

func parseLUSDtotalAverage(data [][]string, infoRowIndex, znIndex int) float64 {
	for col := 0; col < len(data[infoRowIndex]); col++ {
		if data[infoRowIndex][col] == "Gesamt-leistung" && checkIfCellExists(data, znIndex, col) && data[znIndex][col] != "" {
			average := strings.ReplaceAll(data[znIndex][col], ",", ".")
			lusdTotalAverage, _ := strconv.ParseFloat(average, 64)
			return lusdTotalAverage
		}
	}
	return -1
}

func parsingPresentationExamData(data [][]string, infoRowIndex, znIndex int) (global.ExamSubjectStruct, bool) {
	var presentationExam global.ExamSubjectStruct
	var foundPresentation bool

	for col := 0; col < len(data[infoRowIndex]); col++ {
		var err error
		if data[infoRowIndex][col] == "ProP" && checkIfCellExists(data, znIndex, col) && data[znIndex][col] != "" {
			presentationExam.ExamGrade, err = strconv.Atoi(data[znIndex][col])
			if err == nil {
				presentationExam.SubjectAbbreviation = "ProP"
				presentationExam.SubjectFullName = "Hauptschulprojektprüfung" // Fachname ersetzen? (wirklich?) - lieber ne Flag (bool) Präsentationsfach
				foundPresentation = true
			}
		}
	}
	return presentationExam, foundPresentation
}

// TODO: write tests for that!
func parseExamSubjectInformation(data [][]string, row, pnIndex, znIndex, infoRowIndex int) []global.ExamSubjectStruct {
	var examSubjects global.ExamSubjectStruct
	var examSubjectList []global.ExamSubjectStruct
	var sub string

	for col := 2; col < len(data[pnIndex]); col++ {
		if data[pnIndex][col] == "" {
			continue
		}

		examSubjects.ExamGrade, sub = extractExamGrade(data[pnIndex][col])
		// check, if the ZN cell is empty (thats the case, when data is missing)
		if len(data[znIndex]) < len(data[pnIndex]) {
			examSubjects.ReportCardGrade = -1 // empty cell, TODO: Add warning (data missing)
		} else {
			examSubjects.ReportCardGrade, _ = extractExamGrade(data[znIndex][col])
		}

		// extract subject name
		if sub != "" {
			examSubjects.SubjectAbbreviation = sub
			examSubjects.SubjectFullName = misc.ConvertSubAbbToFullName(sub)

		} else {
			examSubjects.SubjectAbbreviation = extractExamSubject(data[infoRowIndex][col])
			examSubjects.SubjectFullName = misc.ConvertSubAbbToFullName(extractExamSubject(data[infoRowIndex][col]))
		}

		// extract Teacher
		examSubjects.Teacher = extractExamTeacher(data[row][col], data[infoRowIndex][col])

		// add Subject to Exam
		examSubjectList = append(examSubjectList, examSubjects)
	}
	return examSubjectList
}

// TODO: write good tests for that!
func findZNIndex(data [][]string, row int, name string) int {
	znIndex := row

	for strings.TrimSpace(data[znIndex][1]) != "ZN" {
		znIndex++

		if !checkIfCellExists(data, znIndex, 1) {
			misc.ConsoleLogPrintln(color.InYellow("    * ")+"Warning (probably not crucial): "+name+" misses ZN-row in exported LUSD file. Skipping student.", true, true)
			return -1
		}
	}
	return znIndex
}

// TODO: write good tests for that!
func findPNrow(data [][]string, startingRow int, name string) int {
	// search for PN row
	pnIndex := startingRow
	for strings.TrimSpace(data[pnIndex][1]) != "PN" {
		pnIndex++

		if !checkIfCellExists(data, pnIndex, 1) || len(data[pnIndex]) == 0 { // falesafe for missing PN rows
			misc.ConsoleLogPrintln(color.InYellow("    * ")+"Warning (probably not crucial): "+name+" misses PN-row in exported LUSD file. Skipping student.", true, true)
			return -1
		}
	}
	return pnIndex
}

func checkIfCellContainsStudentInfo(data [][]string, row, col int) bool {
	switch {
	case len(data) == 0 ||
		len(data[row]) == 0 ||
		data[row][col] == "" ||
		data[row][col] == "Legende:" ||
		data[row][col] == "LN: letzte Leistungsnote, ohne Verrechnung" ||
		data[row][col] == "PN: Prüfungsnote (schriftlich, mündlich oder Präsentation)" ||
		data[row][col] == "ZN: Zeugnisnote":
		return false
	default:
		return true
	}
}

// function extracts and returns the exam grade and the subjet name abbreviation
func extractExamGrade(str string) (int, string) {
	splitstr := strings.Split(str, "-")
	switch {
	case len(splitstr) == 1:
		grade, _ := strconv.Atoi(strings.TrimSpace(str))
		return grade, ""
	case len(splitstr) < 1:
		return -1, ""
	default:
		subject := splitstr[0]
		grade, _ := strconv.Atoi(strings.TrimSpace(splitstr[1]))
		return grade, subject
	}
}

func extractExamSubject(str string) string {
	splitstr := strings.Split(str, "\n")
	return strings.TrimSpace(splitstr[0])
}

func extractExamTeacher(firstStr, secStr string) string {
	splitstr := strings.SplitN(firstStr, "\n", 2)
	if len(splitstr) > 1 {
		cleanstr := strings.ReplaceAll(splitstr[1], "\n", "/")
		return strings.TrimSpace(cleanstr)
	}
	splitstr = strings.SplitN(secStr, "\n", 2)
	switch {
	case len(splitstr) > 1:
		cleanstr := strings.ReplaceAll(splitstr[1], "\n", "/")
		return strings.TrimSpace(cleanstr)
	default:
		return ""
	}
}

// ParseLUSDGraduationBackupResults parses graduation results calculated by the LUSD software
func ParseLUSDGraduationBackupResults(importedData [][][]string) map[string]global.ExamDataSource {
	lusdGraduationMap := map[string]global.ExamDataSource{}
	for i := 0; i < len(importedData); i++ {
		currentMap := readLUSDGraduationResultsBackupSheets(importedData[i])
		lusdGraduationMap = mergeMaps(lusdGraduationMap, currentMap)
	}
	return lusdGraduationMap
}

func readLUSDGraduationResultsBackupSheets(data [][]string) map[string]global.ExamDataSource {
	backupGraduationData := map[string]global.ExamDataSource{}
	infoRowIndex := findRowIndex(data, "UsfBkKurzname", 0)

	for row := infoRowIndex + 1; row < len(data); row++ {
		var currentStudent global.ExamDataSource
		examSubjects := make(map[string]*global.ExamSubjectStruct) // Use a map to hold pointers to subjects

		for col := 0; col < len(data[row]); col++ {
			if !checkIfCellExists(data, row, col) {
				continue
			}

			currentStudent.DataPresent = true

			switch data[infoRowIndex][col] {
			case "StufeSemesterBezeichnung":
				currentStudent.IdFields.Semester = data[row][col]
			case "KlassenName":
				currentStudent.IdFields.Class = data[row][col]
				year, _ := strconv.Atoi(currentStudent.IdFields.Class[:2])
				currentStudent.IdFields.ClassYear = year
				currentStudent.IdFields.ClassChar = currentStudent.IdFields.Class[2:]
			case "PersonalKuerzelDerSchule":
				currentStudent.IdFields.ClassTeacherAbbreviation = data[row][col]
			case "Lehrername":
				currentStudent.IdFields.ClassTeacherFullName = data[row][col]
			case "SchuelerName":
				currentStudent.IdFields.StudentName = data[row][col]
			case "Gesamtleistung":
				average, err := strconv.ParseFloat(data[row][col], 64)
				if err != nil {
					currentStudent.TotalAverage = -1
				} else {
					currentStudent.TotalAverage = average
				}
			case "PF1_Note":
				addExamSubject(examSubjects, "D", "Deutsch", data[row][col])
			case "PF2_Note":
				addExamSubject(examSubjects, "M", "Mathe", data[row][col])
			case "PF3_Note":
				addExamSubject(examSubjects, "PF3", "", data[row][col])
			case "PF4_Note":
				addExamSubject(examSubjects, "PF4", "", data[row][col])
			case "PF3_Fach":
				setSubjectNameAndAbbreviation(examSubjects, "PF3", data[row][col])
			case "PF4_Fach":
				setSubjectNameAndAbbreviation(examSubjects, "PF4", data[row][col])
			case "AbschlussKuerzel":
				currentStudent.GraduationAbbreviation = data[row][col]
			case "AbschlussLangform":
				currentStudent.GraduationFullName = data[row][col]
			default:
				continue
			}
		}

		// Convert the map to a slice and assign it
		for _, subject := range examSubjects {
			currentStudent.ExamSubjects = append(currentStudent.ExamSubjects, *subject)
		}
		backupGraduationData[currentStudent.IdFields.StudentName] = currentStudent
	}

	return backupGraduationData
}

func addExamSubject(subjects map[string]*global.ExamSubjectStruct, abbrev, fullName, gradeStr string) {
	grade, err := strconv.Atoi(gradeStr)
	if err != nil {
		grade = 0
	}
	subject := &global.ExamSubjectStruct{
		SubjectAbbreviation: abbrev,
		SubjectFullName:     fullName,
		ExamGrade:           grade,
	}

	subjects[abbrev] = subject
}

func setSubjectNameAndAbbreviation(examSubjectsMap map[string]*global.ExamSubjectStruct, oldAbbrev, newAbbrev string) {
	if subject, exists := examSubjectsMap[oldAbbrev]; exists {
		delete(examSubjectsMap, oldAbbrev)
		if newAbbrev == "" {
			subject.SubjectAbbreviation = "PPR"
			subject.SubjectFullName = "Hauptschulprojektprüfung"
			examSubjectsMap["Hauptschulprojektprüfung"] = subject
		} else {
			subject.SubjectAbbreviation = newAbbrev
			subject.SubjectFullName = misc.ConvertSubAbbToFullName(newAbbrev)
			examSubjectsMap[newAbbrev] = subject
		}
	}
}
