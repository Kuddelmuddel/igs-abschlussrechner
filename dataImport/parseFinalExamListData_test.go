package dataimport

import (
	"igs-abschlussrechner/global"
	"reflect"
	"testing"
)

func TestMergeMaps(t *testing.T) {
	testcases := []struct {
		description string
		inputMap1   map[string]global.ExamDataSource
		inputMap2   map[string]global.ExamDataSource
		mergedMap   map[string]global.ExamDataSource
	}{
		{
			description: "Test case 1: Merging with non-overlapping keys",
			inputMap1: map[string]global.ExamDataSource{
				"exam1": {
					DataPresent:            true,
					IdFields:               global.IdStruc{},
					GraduationAbbreviation: "MA",
					GraduationFullName:     "Mittlerer Abschluss",
					TotalAverage:           3.3,
					MajorsAverage:          2.1,
					MinorsAverage:          1.5,
					ExamSubjects: []global.ExamSubjectStruct{
						{
							SubjectAbbreviation: "D",
							SubjectFullName:     "Deutsch",
							Teacher:             "ABC",
							ReportCardGrade:     2,
							ExamGrade:           4,
						},
						{
							SubjectAbbreviation: "E",
							SubjectFullName:     "Englisch",
							Teacher:             "BCD",
							ReportCardGrade:     4,
							ExamGrade:           3,
						},
						{
							SubjectAbbreviation: "GL",
							SubjectFullName:     "Gesellschaftslehre",
							Teacher:             "CDE",
							ReportCardGrade:     2,
							ExamGrade:           3,
						},
						{
							SubjectAbbreviation: "M",
							SubjectFullName:     "Mathe",
							Teacher:             "DEF",
							ReportCardGrade:     3,
							ExamGrade:           5,
						},
					},
				},
			},
			inputMap2: map[string]global.ExamDataSource{
				"exam2": {
					DataPresent:            false,
					IdFields:               global.IdStruc{},
					GraduationAbbreviation: "MA",
					GraduationFullName:     "Mittlerer Abschluss",
					TotalAverage:           2.0,
					MajorsAverage:          3.2,
					MinorsAverage:          1.3,
					ExamSubjects: []global.ExamSubjectStruct{
						{
							SubjectAbbreviation: "D",
							SubjectFullName:     "Deutsch",
							Teacher:             "ABC",
							ReportCardGrade:     3,
							ExamGrade:           3,
						},
						{
							SubjectAbbreviation: "E",
							SubjectFullName:     "Englisch",
							Teacher:             "BCD",
							ReportCardGrade:     4,
							ExamGrade:           2,
						},
						{
							SubjectAbbreviation: "GL",
							SubjectFullName:     "Gesellschaftslehre",
							Teacher:             "CDE",
							ReportCardGrade:     3,
							ExamGrade:           1,
						},
						{
							SubjectAbbreviation: "M",
							SubjectFullName:     "Mathe",
							Teacher:             "DEF",
							ReportCardGrade:     3,
							ExamGrade:           2,
						},
					},
				},
			},
			mergedMap: map[string]global.ExamDataSource{
				"exam1": {
					DataPresent:            true,
					IdFields:               global.IdStruc{},
					GraduationAbbreviation: "MA",
					GraduationFullName:     "Mittlerer Abschluss",
					TotalAverage:           3.3,
					MajorsAverage:          2.1,
					MinorsAverage:          1.5,
					ExamSubjects: []global.ExamSubjectStruct{
						{
							SubjectAbbreviation: "D",
							SubjectFullName:     "Deutsch",
							Teacher:             "ABC",
							ReportCardGrade:     2,
							ExamGrade:           4,
						},
						{
							SubjectAbbreviation: "E",
							SubjectFullName:     "Englisch",
							Teacher:             "BCD",
							ReportCardGrade:     4,
							ExamGrade:           3,
						},
						{
							SubjectAbbreviation: "GL",
							SubjectFullName:     "Gesellschaftslehre",
							Teacher:             "CDE",
							ReportCardGrade:     2,
							ExamGrade:           3,
						},
						{
							SubjectAbbreviation: "M",
							SubjectFullName:     "Mathe",
							Teacher:             "DEF",
							ReportCardGrade:     3,
							ExamGrade:           5,
						},
					},
				},
				"exam2": {
					DataPresent:            false,
					IdFields:               global.IdStruc{},
					GraduationAbbreviation: "MA",
					GraduationFullName:     "Mittlerer Abschluss",
					TotalAverage:           2.0,
					MajorsAverage:          3.2,
					MinorsAverage:          1.3,
					ExamSubjects: []global.ExamSubjectStruct{
						{
							SubjectAbbreviation: "D",
							SubjectFullName:     "Deutsch",
							Teacher:             "ABC",
							ReportCardGrade:     3,
							ExamGrade:           3,
						},
						{
							SubjectAbbreviation: "E",
							SubjectFullName:     "Englisch",
							Teacher:             "BCD",
							ReportCardGrade:     4,
							ExamGrade:           2,
						},
						{
							SubjectAbbreviation: "GL",
							SubjectFullName:     "Gesellschaftslehre",
							Teacher:             "CDE",
							ReportCardGrade:     3,
							ExamGrade:           1,
						},
						{
							SubjectAbbreviation: "M",
							SubjectFullName:     "Mathe",
							Teacher:             "DEF",
							ReportCardGrade:     3,
							ExamGrade:           2,
						},
					},
				},
			},
		},
		{
			description: "Test case 2: Merging with overlapping keys and non-default values",
			inputMap1: map[string]global.ExamDataSource{
				"exam1": {
					DataPresent:            true,
					IdFields:               global.IdStruc{},
					GraduationAbbreviation: "MA",
					GraduationFullName:     "Mittlerer Abschluss",
					TotalAverage:           3.3,
					MajorsAverage:          2.1,
					MinorsAverage:          1.5,
					ExamSubjects: []global.ExamSubjectStruct{
						{
							SubjectAbbreviation: "D",
							SubjectFullName:     "Deutsch",
							Teacher:             "ABC",
							ReportCardGrade:     2,
							ExamGrade:           4,
						},
						{
							SubjectAbbreviation: "E",
							SubjectFullName:     "Englisch",
							Teacher:             "BCD",
							ReportCardGrade:     4,
							ExamGrade:           3,
						},
						{
							SubjectAbbreviation: "GL",
							SubjectFullName:     "Gesellschaftslehre",
							Teacher:             "CDE",
							ReportCardGrade:     2,
							ExamGrade:           3,
						},
						{
							SubjectAbbreviation: "M",
							SubjectFullName:     "Mathe",
							Teacher:             "DEF",
							ReportCardGrade:     3,
							ExamGrade:           5,
						},
					},
				},
			},
			inputMap2: map[string]global.ExamDataSource{
				"exam1": {
					DataPresent:            false,
					IdFields:               global.IdStruc{},
					GraduationAbbreviation: "",
					GraduationFullName:     "",
					TotalAverage:           -1,
					MajorsAverage:          2.1,
					MinorsAverage:          0,
					ExamSubjects:           []global.ExamSubjectStruct{},
				},
			},
			mergedMap: map[string]global.ExamDataSource{
				"exam1": {
					DataPresent:            true,
					IdFields:               global.IdStruc{},
					GraduationAbbreviation: "MA",
					GraduationFullName:     "Mittlerer Abschluss",
					TotalAverage:           3.3,
					MajorsAverage:          2.1,
					MinorsAverage:          1.5,
					ExamSubjects: []global.ExamSubjectStruct{
						{
							SubjectAbbreviation: "D",
							SubjectFullName:     "Deutsch",
							Teacher:             "ABC",
							ReportCardGrade:     2,
							ExamGrade:           4,
						},
						{
							SubjectAbbreviation: "E",
							SubjectFullName:     "Englisch",
							Teacher:             "BCD",
							ReportCardGrade:     4,
							ExamGrade:           3,
						},
						{
							SubjectAbbreviation: "GL",
							SubjectFullName:     "Gesellschaftslehre",
							Teacher:             "CDE",
							ReportCardGrade:     2,
							ExamGrade:           3,
						},
						{
							SubjectAbbreviation: "M",
							SubjectFullName:     "Mathe",
							Teacher:             "DEF",
							ReportCardGrade:     3,
							ExamGrade:           5,
						},
					},
				},
			},
		},
		{
			description: "Test case 3: Merging with overlapping keys and default values",
			inputMap1: map[string]global.ExamDataSource{
				"exam1": {
					DataPresent:            true,
					IdFields:               global.IdStruc{},
					GraduationAbbreviation: "MA",
					GraduationFullName:     "Mittlerer Abschluss",
					TotalAverage:           0,
					MajorsAverage:          0,
					MinorsAverage:          -1,
					ExamSubjects: []global.ExamSubjectStruct{
						{
							SubjectAbbreviation: "D",
							SubjectFullName:     "Deutsch",
							Teacher:             "ABC",
							ReportCardGrade:     2,
							ExamGrade:           4,
						},
						{
							SubjectAbbreviation: "E",
							SubjectFullName:     "Englisch",
							Teacher:             "BCD",
							ReportCardGrade:     4,
							ExamGrade:           3,
						},
						{
							SubjectAbbreviation: "GL",
							SubjectFullName:     "Gesellschaftslehre",
							Teacher:             "CDE",
							ReportCardGrade:     2,
							ExamGrade:           3,
						},
						{
							SubjectAbbreviation: "M",
							SubjectFullName:     "Mathe",
							Teacher:             "DEF",
							ReportCardGrade:     3,
							ExamGrade:           5,
						},
					},
				},
			},
			inputMap2: map[string]global.ExamDataSource{
				"exam1": {
					DataPresent:            true,
					IdFields:               global.IdStruc{},
					GraduationAbbreviation: "",
					GraduationFullName:     "",
					TotalAverage:           3.3,
					MajorsAverage:          2.1,
					MinorsAverage:          1.5,
					ExamSubjects:           []global.ExamSubjectStruct{},
				},
			},
			mergedMap: map[string]global.ExamDataSource{
				"exam1": {
					DataPresent:            true,
					IdFields:               global.IdStruc{},
					GraduationAbbreviation: "MA",
					GraduationFullName:     "Mittlerer Abschluss",
					TotalAverage:           3.3,
					MajorsAverage:          2.1,
					MinorsAverage:          1.5,
					ExamSubjects: []global.ExamSubjectStruct{
						{
							SubjectAbbreviation: "D",
							SubjectFullName:     "Deutsch",
							Teacher:             "ABC",
							ReportCardGrade:     2,
							ExamGrade:           4,
						},
						{
							SubjectAbbreviation: "E",
							SubjectFullName:     "Englisch",
							Teacher:             "BCD",
							ReportCardGrade:     4,
							ExamGrade:           3,
						},
						{
							SubjectAbbreviation: "GL",
							SubjectFullName:     "Gesellschaftslehre",
							Teacher:             "CDE",
							ReportCardGrade:     2,
							ExamGrade:           3,
						},
						{
							SubjectAbbreviation: "M",
							SubjectFullName:     "Mathe",
							Teacher:             "DEF",
							ReportCardGrade:     3,
							ExamGrade:           5,
						},
					},
				},
			},
		},
		{
			description: "Test case 4: Merging with many differences",
			inputMap1: map[string]global.ExamDataSource{
				"exam1": {
					DataPresent:            true,
					IdFields:               global.IdStruc{},
					GraduationAbbreviation: "MA",
					GraduationFullName:     "",
					TotalAverage:           3.3,
					MajorsAverage:          0,
					MinorsAverage:          -1,
					ExamSubjects: []global.ExamSubjectStruct{
						{
							SubjectAbbreviation: "D",
							SubjectFullName:     "Deutsch",
							Teacher:             "ABC",
							ReportCardGrade:     2,
							ExamGrade:           4,
						},
						{
							SubjectAbbreviation: "E",
							SubjectFullName:     "Englisch",
							Teacher:             "BCD",
							ReportCardGrade:     4,
							ExamGrade:           3,
						},
						{
							SubjectAbbreviation: "GL",
							SubjectFullName:     "Gesellschaftslehre",
							Teacher:             "CDE",
							ReportCardGrade:     2,
							ExamGrade:           3,
						},
					},
				},
			},
			inputMap2: map[string]global.ExamDataSource{
				"exam1": {
					DataPresent:            true,
					IdFields:               global.IdStruc{},
					GraduationAbbreviation: "",
					GraduationFullName:     "Mittlerer Abschluss",
					TotalAverage:           0,
					MajorsAverage:          2.1,
					MinorsAverage:          1.5,
					ExamSubjects: []global.ExamSubjectStruct{
						{
							SubjectAbbreviation: "M",
							SubjectFullName:     "Mathe",
							Teacher:             "DEF",
							ReportCardGrade:     3,
							ExamGrade:           5,
						},
					},
				},
				"exam2": {
					DataPresent:            true,
					IdFields:               global.IdStruc{},
					GraduationAbbreviation: "MA",
					GraduationFullName:     "Mittlerer Abschluss",
					TotalAverage:           0,
					MajorsAverage:          0,
					MinorsAverage:          -1,
					ExamSubjects: []global.ExamSubjectStruct{
						{
							SubjectAbbreviation: "D",
							SubjectFullName:     "Deutsch",
							Teacher:             "ABC",
							ReportCardGrade:     2,
							ExamGrade:           4,
						},
						{
							SubjectAbbreviation: "E",
							SubjectFullName:     "Englisch",
							Teacher:             "BCD",
							ReportCardGrade:     4,
							ExamGrade:           3,
						},
						{
							SubjectAbbreviation: "GL",
							SubjectFullName:     "Gesellschaftslehre",
							Teacher:             "CDE",
							ReportCardGrade:     2,
							ExamGrade:           3,
						},
						{
							SubjectAbbreviation: "M",
							SubjectFullName:     "Mathe",
							Teacher:             "DEF",
							ReportCardGrade:     3,
							ExamGrade:           5,
						},
					},
				},
			},
			mergedMap: map[string]global.ExamDataSource{
				"exam1": {
					DataPresent:            true,
					IdFields:               global.IdStruc{},
					GraduationAbbreviation: "MA",
					GraduationFullName:     "Mittlerer Abschluss",
					TotalAverage:           3.3,
					MajorsAverage:          2.1,
					MinorsAverage:          1.5,
					ExamSubjects: []global.ExamSubjectStruct{
						{
							SubjectAbbreviation: "D",
							SubjectFullName:     "Deutsch",
							Teacher:             "ABC",
							ReportCardGrade:     2,
							ExamGrade:           4,
						},
						{
							SubjectAbbreviation: "E",
							SubjectFullName:     "Englisch",
							Teacher:             "BCD",
							ReportCardGrade:     4,
							ExamGrade:           3,
						},
						{
							SubjectAbbreviation: "GL",
							SubjectFullName:     "Gesellschaftslehre",
							Teacher:             "CDE",
							ReportCardGrade:     2,
							ExamGrade:           3,
						},
						{
							SubjectAbbreviation: "M",
							SubjectFullName:     "Mathe",
							Teacher:             "DEF",
							ReportCardGrade:     3,
							ExamGrade:           5,
						},
					},
				},
				"exam2": {
					DataPresent:            true,
					IdFields:               global.IdStruc{},
					GraduationAbbreviation: "MA",
					GraduationFullName:     "Mittlerer Abschluss",
					TotalAverage:           0,
					MajorsAverage:          0,
					MinorsAverage:          -1,
					ExamSubjects: []global.ExamSubjectStruct{
						{
							SubjectAbbreviation: "D",
							SubjectFullName:     "Deutsch",
							Teacher:             "ABC",
							ReportCardGrade:     2,
							ExamGrade:           4,
						},
						{
							SubjectAbbreviation: "E",
							SubjectFullName:     "Englisch",
							Teacher:             "BCD",
							ReportCardGrade:     4,
							ExamGrade:           3,
						},
						{
							SubjectAbbreviation: "GL",
							SubjectFullName:     "Gesellschaftslehre",
							Teacher:             "CDE",
							ReportCardGrade:     2,
							ExamGrade:           3,
						},
						{
							SubjectAbbreviation: "M",
							SubjectFullName:     "Mathe",
							Teacher:             "DEF",
							ReportCardGrade:     3,
							ExamGrade:           5,
						},
					},
				},
			},
		},
	}

	for _, test := range testcases {
		t.Run(test.description, func(t *testing.T) {
			result := mergeMaps(test.inputMap1, test.inputMap2)
			if !reflect.DeepEqual(result, test.mergedMap) {
				t.Errorf("For %s, expected %v but got %v", test.description, test.mergedMap, result)
			}
		})
	}
}
