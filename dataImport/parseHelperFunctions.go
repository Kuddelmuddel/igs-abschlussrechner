package dataimport

import (
	"errors"
	"igs-abschlussrechner/config"
	"igs-abschlussrechner/global"
	misc "igs-abschlussrechner/miscellaneous"
	"regexp"
	"strings"

	"github.com/TwiN/go-color"
)

// helper function: finds and returns index of the row containing the specified string str in specified column col
func findRowIndex(ar [][]string, str string, col int) int {
	for row := 0; row < len(ar); row++ {
		if len(ar[row]) > 0 {
			if strings.Contains(ar[row][col], str) {
				return row
			}
		}
	}
	return -1
}

// helper function: function finds a valid student and parses the number of rows per student in input file
func detectRowNumerPerStudent(inputArray [][]string) (int, error) {
	cfg := config.GetConfig()
	infoRowDetectContent := cfg.DetectConferenceFile.DetectSubjectRowBy
	infoIndx := findRowIndex(inputArray, infoRowDetectContent, 0)

	for i := infoIndx; i < len(inputArray); i++ {
		if len(inputArray[i]) == 0 {
			if checkValdityOfStudentDataStructure(inputArray, i+infoIndx+1) {
				return determineRowsOfStudent(inputArray, i+infoIndx+1), nil
			}
		}
	}
	return -1, errors.New("error while importing LUSD data: no consistent subject found to determine data structure")
}

// helper function: function checks, if the there are as many rows for given student as there should be
func checkNumberOfStudentRows(inputArray [][]string, studentNum, rowsPerStudent int, student global.Student) (bool, string) {
	rowsOfStudent := determineRowsOfStudent(inputArray, studentNum-rowsPerStudent+1)

	if rowsOfStudent == rowsPerStudent {
		return true, ""
	}

	incompleteStudent := parseName(inputArray[studentNum-rowsPerStudent+1][0]) + " (" + student.Class + ")"
	misc.ConsoleLogPrintln(color.InRed("    * ")+"Warning: student "+incompleteStudent+" lacks LUSD data - skipping.", true, true)
	warning := "- " + incompleteStudent + ": Fehlende LUSD-Daten, Schüler*In wurde übersprungen!"
	return false, warning
}

// helper function: function counts the number of rows per student in the input Ecxel file
func determineRowsOfStudent(inputArray [][]string, startingRow int) int {
	rowsPerStudent := 0

	for i := startingRow; i < len(inputArray); i++ {
		switch {
		case len(inputArray[i]) == 0:
			return rowsPerStudent
		case i+1 == len(inputArray):
			return rowsPerStudent + 1
		}
		rowsPerStudent++
	}
	return -1
}

// helper function: finds the id of a course by its shortcut (=name) and returns it. If no course with given name exists, function returns -1
func findCourse(allCourses []courseHeader, courseName string) int {
	for i := 0; i < len(allCourses); i++ {
		matched, err := regexp.MatchString(`(`+courseName+`)`, allCourses[i].course)
		misc.CheckError("Failed to compile regular expression in function findCourse, parseHelperFunctions.go", err)

		if matched {
			return allCourses[i].column
		}
	}
	return -1
}

// TODO: check and simplify function!
// helper function:
func getIndexOfInfoRow(inputArray [][]string, lastStudentRow, rowsPerStudent int, rowInfo string) int {
	for i := 0; i < rowsPerStudent; i++ {
		if strings.Contains(inputArray[lastStudentRow-i][0], rowInfo) {
			return i
		}
	}
	return -1
}

// TODO: check function (and simplify?) // add Test!
// helper function: function checks, whether there is more information in one column than usual (this information is separated by "-------"))
func multiContentColumnCheck(inputArray [][]string, studentrow, rowsPerStudent int, selectedCourse courseHeader) bool {
	for i := 0; i < rowsPerStudent; i++ {
		if len(inputArray[studentrow-i]) > selectedCourse.column { // check if row has that much columns TODO: replace with checkIfCellExists function
			cellContent := inputArray[studentrow-i][selectedCourse.column]
			if strings.Contains(cellContent, "-------") {
				return true
			}
		}
	}
	return false
}

// TODO: check function (and simplify?) // add Test!
// helper function: function counts the number of content in a multiContentColumn
func countContentInColumn(inputArray [][]string, studentrow, rowsPerStudent int, selectedCourse courseHeader) int {
	var length int

	for i := 0; i < rowsPerStudent; i++ {
		if len(inputArray[studentrow-i]) > selectedCourse.column { // check if row has that much columns
			cellContent := inputArray[studentrow-i][selectedCourse.column]
			cellLength := len(strings.Split(cellContent, "-------"))
			if cellLength > length {
				length = cellLength
			}
		}
	}
	return length
}

// function checks, if selected student is valid to get data structure info from
func checkValdityOfStudentDataStructure(inputArray [][]string, startRowToCheck int) bool {
	cfg := config.GetConfig()
	var counter int
	var emptyRowPositions []int
	infoRowDetectContent := cfg.DetectConferenceFile.DetectSubjectRowBy
	infoIndx := findRowIndex(inputArray, infoRowDetectContent, 0)

	rowsPerStudent := determineRowsOfStudent(inputArray, startRowToCheck)
	estimatedStudentcount := countEmptyRows(inputArray) + 1

	// find empty rows
	for i := infoIndx + 1; i < len(inputArray); i++ {
		if len(inputArray[i]) == 0 {
			emptyRowPositions = append(emptyRowPositions, i)
		}
	}
	// compare differences of indices with rows of student
	for i := 1; i < len(emptyRowPositions); i++ {
		if emptyRowPositions[i]-emptyRowPositions[i-1]-1 != rowsPerStudent {
			counter++
			if counter > estimatedStudentcount*20/100 {
				return false
			}
		}
	}

	return true
}

// function counts empty rows (first element) in [][]string
func countEmptyRows(inputArray [][]string) int {
	counter := 0
	for i := 0; i < len(inputArray); i++ {
		if len(inputArray[i]) == 0 {
			counter++
		}
	}
	return counter
}

// functions checks, if the given item in [][]string exists or if it is out of range
func checkIfCellExists(data [][]string, row, colmn int) bool {
	return len(data) > row && len(data[row]) > colmn && row != -1 && colmn != -1
}

// finds and returns intex (row and col) containing the specified string str
func findRowColIndex(ar [][]string, str string) (int, int) {
	for row := 0; row < len(ar); row++ {
		for col := 0; col < len(ar[row]); col++ {
			if len(ar[row]) > 0 && len(ar[row]) > col {
				if strings.Contains(ar[row][col], str) {
					return row, col
				}
			}
		}
	}
	return -1, -1
}
