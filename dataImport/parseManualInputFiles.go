package dataimport

import (
	"igs-abschlussrechner/config"
	"igs-abschlussrechner/global"
	misc "igs-abschlussrechner/miscellaneous"
	"regexp"
	"strconv"
	"strings"
)

// ParseConferenceList collects general information of the class, information about each student and all taken Courses and returns these information in a struct
func ParseManualInputFile(manualImport [][][]string) [][]global.Student {
	var allManualStudentsData [][]global.Student

	for sheet := 0; sheet < len(manualImport); sheet++ {
		classData := createManualClassList(manualImport[sheet])
		createManualClassList(manualImport[sheet])

		if len(classData) == 0 {
			continue
		}
		allManualStudentsData = append(allManualStudentsData, classData)
	}
	return allManualStudentsData
}

func createManualClassList(importedClassData [][]string) []global.Student {
	var classList []global.Student

	cfg := config.GetConfig()
	infoRowDetectContent := cfg.DetectManualInputFile.DetectSubjectRowBy

	headerRowIndex := findRowIndex(importedClassData, infoRowDetectContent, 0)
	courseList, examSubjectList := parseManualCourseAndExamSubjectList(importedClassData, infoRowDetectContent)

	classTeacher := "noTeacherSpecified"
	if checkIfCellExists(importedClassData, headerRowIndex-1, 1) {
		classTeacher = importedClassData[headerRowIndex-1][1]
	}

	var class string
	if checkIfCellExists(importedClassData, headerRowIndex-2, 1) {
		class = importedClassData[headerRowIndex-2][1]
	}

	classChar := extractClassChar(class)
	classYear := extractClassYear(class)

	if !checkIfCellExists(importedClassData, headerRowIndex+1, 0) {
		return []global.Student{}
	}

	for row := headerRowIndex + 1; row < len(importedClassData); row++ {
		currentStudent := global.Student{
			Name:         parseManualName(importedClassData[row][0]),
			ClassTeacher: classTeacher,
			ClassYear:    classYear,
			ClassChar:    classChar,
			Class:        class,
		}

		subjectList, examList := processStudentSubjectsAndExamLists(importedClassData[row], courseList, examSubjectList)

		if len(examList) > 0 {
			currentStudent.ReportCardInfo.FinalExams.ManualExamData.DataPresent = true
			currentStudent.ReportCardInfo.FinalExams.ManualExamData.ExamSubjects = examList
		}
		currentStudent.ReportCardInfo.Subjects = subjectList
		classList = append(classList, currentStudent)
	}
	return classList
}

// processStudentSubjectsAndExamLists processes the subjects for a given student row.
func processStudentSubjectsAndExamLists(subjectAndExamRow []string, courseList []courseHeader, examSubjectList []examHeader) ([]global.SubjectStruc, []global.ExamSubjectStruct) {
	var subjectList []global.SubjectStruc
	var examList []global.ExamSubjectStruct
	i := 0

	for column := 1; column < len(subjectAndExamRow); column++ {
		if subjectAndExamRow[column] == "" {
			continue
		}

		switch column {
		case examSubjectList[i].columnGrade:
			var currentExamSubject global.ExamSubjectStruct
			currentExamSubject.ExamGrade = parseManualInputGrade(subjectAndExamRow[column])
			currentExamSubject.SubjectAbbreviation = subjectAndExamRow[column+1]
			currentExamSubject.SubjectFullName = misc.ConvertSubAbbToFullName(subjectAndExamRow[column+1])
			examList = append(examList, currentExamSubject)
		case examSubjectList[i].columnSubject:
			i++ // skip colum (already processed in columnGrade case)
			continue
		default:
			var currentSubject global.SubjectStruc
			currentSubject.SubjectAbbreviation = courseList[column-1].course
			currentSubject.SubjectFullName = misc.ConvertSubAbbToFullName(courseList[column-1].course)
			currentSubject.CourseLvl = parseManualInpurCourseLevel(subjectAndExamRow[column])
			currentSubject.Grade = parseManualInputGrade(subjectAndExamRow[column])

			if currentSubject.Grade != -1 {
				subjectList = append(subjectList, currentSubject)
			}
		}
	}
	return subjectList, examList
}

// parseManualCourseAndExamSubjectList processes a 2D slice of strings to extract course and exam information
func parseManualCourseAndExamSubjectList(inputArray [][]string, detectionString string) ([]courseHeader, []examHeader) {
	var courseCollection []courseHeader
	var examIDCollection []examHeader
	index := findRowIndex(inputArray, detectionString, 0)

	for colIdx, header := range inputArray[index] {
		trimmedHeader := strings.TrimSpace(header)

		switch trimmedHeader {
		case "Schüler*Innen / Fächer", "", "Fach":
			// Skip irrelevant headers
			continue
		case "Note":
			// Capture exam data
			examIDCollection = append(examIDCollection, examHeader{
				columnGrade:   colIdx,
				columnSubject: colIdx + 1,
			})
		default:
			// Capture course data
			courseCollection = append(courseCollection, courseHeader{
				column: colIdx,
				course: trimmedHeader,
			})
		}
	}
	return courseCollection, examIDCollection
}

// function take string, parses name and converts it in LUSD name syntax (Lastname, Surname)
func parseManualName(inputStr string) string {
	switch {
	case inputStr == "":
		return ""
	case strings.Contains(inputStr, ","):
		return inputStr
	case strings.Contains(inputStr, " "):
		return strings.Replace(inputStr, " ", ", ", 1)
	default:
		return inputStr + ", "
	}
}

func parseManualInpurCourseLevel(inputStr string) string {
	switch {
	case strings.Contains(strings.ToUpper(inputStr), "A"):
		return "A"
	case strings.Contains(strings.ToUpper(inputStr), "B"):
		return "B"
	case strings.Contains(strings.ToUpper(inputStr), "C"):
		return "C"
	case strings.Contains(strings.ToUpper(inputStr), "G"):
		return "G"
	case strings.Contains(strings.ToUpper(inputStr), "E"):
		return "E"
	default:
		return ""
	}
}

func parseManualInputGrade(inputStr string) int {

	reObjectClass := regexp.MustCompile(`\d+`)
	gradeString := reObjectClass.FindString(inputStr)
	if len(inputStr) == 0 || gradeString == "" || len(gradeString) > 1 {
		return -1
	}
	grade, _ := strconv.Atoi(gradeString)

	return grade
}
