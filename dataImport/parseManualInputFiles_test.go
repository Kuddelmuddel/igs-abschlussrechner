package dataimport

import (
	"testing"
)

func TestParseManualName(t *testing.T) {
	testcases := []struct {
		inputName          string
		expectedParsedName string
	}{
		{"", ""},                                         // Testing empty string
		{"Benni Schneider", "Benni, Schneider"},          // Testing name without comma
		{"Schneider, Benni", "Schneider, Benni"},         // Testing name with comma
		{"Benni", "Benni, "},                             // Testing first name only
		{"Benni Schneider Lu", "Benni, Schneider Lu"},    // Testing name with more than one space
		{"Benni, Schneider, Lu", "Benni, Schneider, Lu"}, // Testing name with more than coma
	}

	// Run test cases
	for _, test := range testcases {
		result := parseManualName(test.inputName)
		if result != test.expectedParsedName {
			t.Errorf("For parsed name '%s', expected '%s', but got '%s'", test.inputName, test.expectedParsedName, result)
		}
	}
}
