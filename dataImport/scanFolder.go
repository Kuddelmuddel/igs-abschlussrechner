package dataimport

import (
	"errors"
	"igs-abschlussrechner/config"
	misc "igs-abschlussrechner/miscellaneous"
	"os"
	"strings"

	"github.com/TwiN/go-color"
	"github.com/xuri/excelize/v2"
)

// ScanInputDirXLSX scans the input folder and returns a list of all XLSX-files
func ScanInputDirXLSX() ([]string, error) {
	file, err := os.Open("./input/")
	misc.CheckError("  ERROR: Failed to open directory in ScanInputDirXLSX function, dataImport.go", err)
	defer file.Close()

	list, err := file.Readdirnames(0)
	misc.CheckError("  ERROR: Failed to read directory names in ScanInputDirXLSX, dataImport.go", err)

	var xlsxFiles []string
	if len(list) == 0 || (len(list) == 1 && list[0] == ".gitkeep") {
		return xlsxFiles, errors.New("NO FILES TO PROCESS IN INPUT FOLDER ")
	}

	for i := 0; i < len(list); i++ {
		if strings.Contains(list[i], ".xlsx") && !strings.Contains(list[i], ".~lock") {
			xlsxFiles = append(xlsxFiles, list[i])
		}
	}
	return xlsxFiles, nil
}

// IdentifyFiles identifies files according to information from the yaml-config and returns a two dimensional list: [filename][identifier]string.
func IdentifyFiles(fileList []string) [][]string {
	var identifedFiles [][]string
	for i := 0; i < len(fileList); i++ {
		file := "input/" + fileList[i]

		switch {
		case IsFile(file, "ConferenceList"):
			identifedFiles = append(identifedFiles, []string{fileList[i], "ConferenceList"})
			misc.ConsoleLogPrintln(color.InGreen("  * ")+"File containing grades (mandatory): "+color.InCyan(fileList[i]), true, true)

		case IsFile(file, "FinalExamList"):
			identifedFiles = append(identifedFiles, []string{fileList[i], "FinalExamList"})
			misc.ConsoleLogPrintln(color.InGreen("  * ")+"File containing final exam information (optional): "+color.InCyan(fileList[i]), true, true)

		case IsFile(file, "LUSDGraduationResults"):
			identifedFiles = append(identifedFiles, []string{fileList[i], "LUSDGraduationResults"})
			misc.ConsoleLogPrintln(color.InGreen("  * ")+"File containing LUSD graduation results (optional): "+color.InCyan(fileList[i]), true, true)

		case IsFile(file, "CourseLvlChange"):
			identifedFiles = append(identifedFiles, []string{fileList[i], "CourseLvlChange"})
			misc.ConsoleLogPrintln(color.InGreen("  * ")+"File containing course-level changes (optional): "+color.InCyan(fileList[i]), true, true)

		case IsFile(file, "AdditionalInfo"):
			identifedFiles = append(identifedFiles, []string{fileList[i], "AdditionalInfo"})
			misc.ConsoleLogPrintln(color.InGreen("  * ")+"File containing additional Information (optional): "+color.InCyan(fileList[i]), true, true)

		case IsFile(file, "IsManualInput"):
			identifedFiles = append(identifedFiles, []string{fileList[i], "IsManualInput"})
			misc.ConsoleLogPrintln(color.InGreen("  * ")+"File containing manual grade input: "+color.InCyan(fileList[i]), true, true)

		default:
			identifedFiles = append(identifedFiles, []string{fileList[i], "notIdentified"})
			misc.ConsoleLogPrintln(color.InRed("  * ")+color.InCyan(fileList[i])+" could not be identified.", true, true)
		}
	}
	return identifedFiles
}

// IsFile returns true, if a the specified excel file fits to the specifications given in the yaml-config file
func IsFile(filename, identifier string) bool {
	var cell, compareContent, sheet string
	var exact bool
	cfg := config.GetConfig()

	f, err := excelize.OpenFile(filename)
	misc.CheckError("Failed to open excel file in IsFile function, dataImport.go", err)
	sheetMap := f.GetSheetMap()
	sheet = sheetMap[1]

	switch identifier {
	case "FinalExamList":
		exact = true
		cell = cfg.DetectFinalExamFile.Cell
		compareContent = cfg.DetectFinalExamFile.Content
	case "ConferenceList":
		exact = true
		cell = cfg.DetectConferenceFile.Detect.Cell
		compareContent = cfg.DetectConferenceFile.Detect.Content
	case "LUSDGraduationResults":
		exact = true
		cell = cfg.DetectLUSDGraduationResults.Cell
		compareContent = cfg.DetectLUSDGraduationResults.Content
	case "CourseLvlChange":
		exact = false
		cell = cfg.DetectCourseLvlChangeFile.Detect.Cell
		compareContent = cfg.DetectCourseLvlChangeFile.Detect.Content
	case "IsManualInput":
		exact = true
		cell = cfg.DetectManualInputFile.Detect.Cell
		compareContent = cfg.DetectManualInputFile.Detect.Content
	case "AdditionalInfo":
		exact = true
		sheet = cfg.DetectAdditionalInfoFile.XlsxSheetname
		cell = cfg.DetectAdditionalInfoFile.Detect.Cell
		compareContent = cfg.DetectAdditionalInfoFile.Detect.Content
	default:
		return false
	}

	cellContent, err := f.GetCellValue(sheet, cell)
	if err != nil {
		return false
	}

	if exact {
		return cellContent == compareContent
	} else {
		return strings.Contains(cellContent, compareContent)
	}
}

func MandatoryFilesMissing(fillist [][]string) bool {
	for i := 0; i < len(fillist); i++ {
		if fillist[i][1] == "ConferenceList" || fillist[i][1] == "IsManualInput" {
			return false
		}
	}
	return true
}
