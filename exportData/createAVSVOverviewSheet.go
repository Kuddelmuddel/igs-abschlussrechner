package exportdata

import (
	"igs-abschlussrechner/config"
	"igs-abschlussrechner/global"
	misc "igs-abschlussrechner/miscellaneous"
	"strconv"

	"github.com/xuri/excelize/v2"
)

func insertAVSVOverviewSheet(f *excelize.File, studentRepordCard []global.Student) {
	cfg := config.GetConfig()
	zoomScale := cfg.ZoomScale

	sheet := "AVSVÜbersicht"
	_, err := f.NewSheet(sheet)
	misc.CheckError("Failed to create new sheet, createAVSVOverviewSheet.go", err)

	// set page layout
	setSheetFormatPrDefaultRowHeight(f, sheet, 17)
	formatCellWithForAVSVSheet(f, sheet)
	setSheetPrOptionsFitToPage(f, sheet, true)
	var (
		fitToHeight = 1
		fitToWidth  = 1
	)
	err = f.SetPageLayout(sheet, &excelize.PageLayoutOptions{
		FitToHeight: &fitToHeight,
		FitToWidth:  &fitToWidth,
	})
	misc.CheckError("Failed to set page layout, createAVSVOverviewSheet.go", err)

	setSheetViewOptionsZoomscale(f, sheet, 0, zoomScale)

	currentRow := write2ColumnAVSVTable(f, sheet, studentRepordCard, 1)
	currentRow += 16
	writeprintVersionAVSVTable(f, sheet, studentRepordCard, currentRow)
}

func formatCellWithForAVSVSheet(f *excelize.File, sheet string) {
	setColWith(f, sheet, "A", "A", 7.00)
	setColWith(f, sheet, "B", "B", 3.60)
	setColWith(f, sheet, "C", "C", 3.60)
	setColWith(f, sheet, "D", "D", 0.60)
	setColWith(f, sheet, "E", "E", 7.00)
	setColWith(f, sheet, "F", "F", 3.60)
	setColWith(f, sheet, "G", "G", 3.60)
}

func write2ColumnAVSVTable(f *excelize.File, sheet string, studentRepordCard []global.Student, rowNum int) int {
	// ############## create Styles
	tableTitleRow, err := f.NewStyle(&excelize.Style{
		Font: &excelize.Font{
			Size: 10,
			Bold: true,
		},
		Border: []excelize.Border{
			{Type: "top", Color: "000000", Style: 1},
			{Type: "bottom", Color: "000000", Style: 1},
		},
		Alignment: &excelize.Alignment{
			Horizontal: "center",
			Vertical:   "center",
		},
	})
	misc.CheckError("", err)

	tableFirstColumnRow, err := f.NewStyle(&excelize.Style{
		Font: &excelize.Font{
			Size: 11,
		},
		Alignment: &excelize.Alignment{
			Horizontal: "left",
			Vertical:   "center",
		},
	})
	misc.CheckError("", err)

	tableRows, err := f.NewStyle(&excelize.Style{
		Font: &excelize.Font{
			Size: 11,
		},
		Alignment: &excelize.Alignment{
			Horizontal: "center",
			Vertical:   "center",
		},
	})
	misc.CheckError("", err)

	naValueTableRows, err := f.NewStyle(&excelize.Style{
		Font: &excelize.Font{
			Size:  11,
			Color: "b3b3b3",
		},
		Alignment: &excelize.Alignment{
			Horizontal: "center",
			Vertical:   "center",
		},
	})
	misc.CheckError("", err)

	// grey version
	tableFirstColumnRowGray, err := f.NewStyle(&excelize.Style{
		Fill: excelize.Fill{
			Type:    "pattern",
			Color:   []string{"#cccccc"},
			Pattern: 1,
		},
		Font: &excelize.Font{
			Size: 11,
		},
		Alignment: &excelize.Alignment{
			Horizontal: "left",
			Vertical:   "center",
		},
	})
	misc.CheckError("", err)

	tableRowsGray, err := f.NewStyle(&excelize.Style{
		Fill: excelize.Fill{
			Type:    "pattern",
			Color:   []string{"#cccccc"},
			Pattern: 1,
		},
		Font: &excelize.Font{
			Size: 11,
		},
		Alignment: &excelize.Alignment{
			Horizontal: "center",
			Vertical:   "center",
		},
	})
	misc.CheckError("", err)

	naValueTableRowsGray, err := f.NewStyle(&excelize.Style{
		Fill: excelize.Fill{
			Type:    "pattern",
			Color:   []string{"#cccccc"},
			Pattern: 1,
		},
		Font: &excelize.Font{
			Size:  11,
			Color: "b3b3b3",
		},
		Alignment: &excelize.Alignment{
			Horizontal: "center",
			Vertical:   "center",
		},
	})
	misc.CheckError("", err)
	// ############## end Styles

	// Table Header (conference-view)
	setCellValue(f, sheet, cell('A', rowNum), "Name")
	setCellValue(f, sheet, cell('B', rowNum), "Arbeitsverhalten")
	setCellValue(f, sheet, cell('C', rowNum), "Sozialverhalten")
	setCellStyle(f, sheet, cell('A', rowNum), cell('C', rowNum), tableTitleRow)

	setCellValue(f, sheet, cell('E', rowNum), "Name")
	setCellValue(f, sheet, cell('F', rowNum), "Arbeitsverhalten")
	setCellValue(f, sheet, cell('G', rowNum), "Sozialverhalten")
	setCellStyle(f, sheet, cell('E', rowNum), cell('G', rowNum), tableTitleRow)

	rowNumSecCol := rowNum
	// Table Content Part 1 (first column)
	for i := 0; i < len(studentRepordCard)/2; i++ {
		rowNum++
		setCellValue(f, sheet, cell('A', rowNum), studentRepordCard[i].Name)
		setCellValue(f, sheet, cell('B', rowNum), studentRepordCard[i].ReportCardInfo.AV)
		setCellValue(f, sheet, cell('C', rowNum), studentRepordCard[i].ReportCardInfo.SV)

		// format rows
		if (rowNum)%2 == 1 {
			setCellStyle(f, sheet, cell('A', rowNum), cell('A', rowNum), tableFirstColumnRowGray)
			setCellStyle(f, sheet, cell('B', rowNum), cell('C', rowNum), tableRowsGray)
			if studentRepordCard[i].ReportCardInfo.AV == "NA" {
				setCellStyle(f, sheet, cell('B', rowNum), cell('B', rowNum), naValueTableRowsGray)
			}
			if studentRepordCard[i].ReportCardInfo.SV == "NA" {
				setCellStyle(f, sheet, cell('C', rowNum), cell('C', rowNum), naValueTableRowsGray)
			}
		} else {
			setCellStyle(f, sheet, cell('A', rowNum), cell('A', rowNum), tableFirstColumnRow)
			setCellStyle(f, sheet, cell('B', rowNum), cell('C', rowNum), tableRows)
			if studentRepordCard[i].ReportCardInfo.AV == "NA" {
				setCellStyle(f, sheet, cell('B', rowNum), cell('B', rowNum), naValueTableRows)
			}
			if studentRepordCard[i].ReportCardInfo.SV == "NA" {
				setCellStyle(f, sheet, cell('C', rowNum), cell('C', rowNum), naValueTableRows)
			}
		}
	}

	// Table Content Part 2 (second column)
	for i := len(studentRepordCard) / 2; i < len(studentRepordCard); i++ {
		rowNumSecCol++
		setCellValue(f, sheet, cell('E', rowNumSecCol), studentRepordCard[i].Name)
		setCellValue(f, sheet, cell('F', rowNumSecCol), studentRepordCard[i].ReportCardInfo.AV)
		setCellValue(f, sheet, cell('G', rowNumSecCol), studentRepordCard[i].ReportCardInfo.SV)

		// format rows
		if (rowNumSecCol)%2 == 1 {
			setCellStyle(f, sheet, cell('E', rowNumSecCol), cell('E', rowNumSecCol), tableFirstColumnRowGray)
			setCellStyle(f, sheet, cell('F', rowNumSecCol), cell('G', rowNumSecCol), tableRowsGray)
			if studentRepordCard[i].ReportCardInfo.AV == "NA" {
				setCellStyle(f, sheet, cell('F', rowNumSecCol), cell('F', rowNumSecCol), naValueTableRowsGray)
			}
			if studentRepordCard[i].ReportCardInfo.SV == "NA" {
				setCellStyle(f, sheet, cell('G', rowNumSecCol), cell('G', rowNumSecCol), naValueTableRowsGray)
			}
		} else {
			setCellStyle(f, sheet, cell('E', rowNumSecCol), cell('E', rowNumSecCol), tableFirstColumnRow)
			setCellStyle(f, sheet, cell('F', rowNumSecCol), cell('G', rowNumSecCol), tableRows)
			if studentRepordCard[i].ReportCardInfo.AV == "NA" {
				setCellStyle(f, sheet, cell('F', rowNumSecCol), cell('F', rowNumSecCol), naValueTableRows)
			}
			if studentRepordCard[i].ReportCardInfo.SV == "NA" {
				setCellStyle(f, sheet, cell('G', rowNumSecCol), cell('G', rowNumSecCol), naValueTableRows)
			}
		}
	}
	return rowNum
}

func writeprintVersionAVSVTable(f *excelize.File, sheet string, studentRepordCard []global.Student, rowNum int) {
	// ############## create Styles
	tableTitleRow, err := f.NewStyle(&excelize.Style{
		Font: &excelize.Font{
			Size: 10,
			Bold: true,
		},
		Border: []excelize.Border{
			{Type: "top", Color: "000000", Style: 1},
			{Type: "bottom", Color: "000000", Style: 1},
		},
		Alignment: &excelize.Alignment{
			Horizontal: "center",
			Vertical:   "center",
		},
	})
	misc.CheckError("", err)

	tableFirstColumnRow, err := f.NewStyle(&excelize.Style{
		Font: &excelize.Font{
			Size: 11,
		},
		Alignment: &excelize.Alignment{
			Horizontal: "left",
			Vertical:   "center",
		},
	})
	misc.CheckError("", err)

	tableRows, err := f.NewStyle(&excelize.Style{
		Font: &excelize.Font{
			Size: 11,
		},
		Alignment: &excelize.Alignment{
			Horizontal: "center",
			Vertical:   "center",
		},
	})
	misc.CheckError("", err)

	naValueTableRows, err := f.NewStyle(&excelize.Style{
		Font: &excelize.Font{
			Size:  11,
			Color: "b3b3b3",
		},
		Alignment: &excelize.Alignment{
			Horizontal: "center",
			Vertical:   "center",
		},
	})
	misc.CheckError("", err)

	headerStyle, err := f.NewStyle(&excelize.Style{
		Font: &excelize.Font{
			Bold: true,
		},
	})
	misc.CheckError("", err)

	// grey version
	tableFirstColumnRowGray, err := f.NewStyle(&excelize.Style{
		Fill: excelize.Fill{
			Type:    "pattern",
			Color:   []string{"#cccccc"},
			Pattern: 1,
		},
		Font: &excelize.Font{
			Size: 11,
		},
		Alignment: &excelize.Alignment{
			Horizontal: "left",
			Vertical:   "center",
		},
	})
	misc.CheckError("", err)

	tableRowsGray, err := f.NewStyle(&excelize.Style{
		Fill: excelize.Fill{
			Type:    "pattern",
			Color:   []string{"#cccccc"},
			Pattern: 1,
		},
		Font: &excelize.Font{
			Size: 11,
		},
		Alignment: &excelize.Alignment{
			Horizontal: "center",
			Vertical:   "center",
		},
	})
	misc.CheckError("", err)

	naValueTableRowsGray, err := f.NewStyle(&excelize.Style{
		Fill: excelize.Fill{
			Type:    "pattern",
			Color:   []string{"#cccccc"},
			Pattern: 1,
		},
		Font: &excelize.Font{
			Size:  11,
			Color: "b3b3b3",
		},
		Alignment: &excelize.Alignment{
			Horizontal: "center",
			Vertical:   "center",
		},
	})
	misc.CheckError("", err)
	// ############## end Styles

	setCellValue(f, sheet, cell('A', rowNum), "Noten als lange Liste zum drucken (nur diese ist im Druckbereich - die geteilte Darstellung oben ist für den breiten Bildschirm bei der Konferenz optimiert und nicht im Druckbereich)")
	rowNum++
	// define print area
	err = f.SetDefinedName(&excelize.DefinedName{
		Name:     "_xlnm.Print_Area",
		RefersTo: "AVSVÜbersicht!$A$" + strconv.Itoa(rowNum) + ":$D$" + strconv.Itoa(rowNum+2+len(studentRepordCard)),
		Scope:    sheet,
	})
	misc.CheckError("Failed to set print area, ExportReportCards2Excel.go", err)

	// write AVAS-data
	setCellValue(f, sheet, cell('A', rowNum), "Übersicht der Arbeit- und Sozialverhaltensnoten")
	setCellStyle(f, sheet, cell('A', rowNum), cell('A', rowNum), headerStyle)
	rowNum++
	rowNum++
	setCellValue(f, sheet, cell('A', rowNum), "Name")
	setCellValue(f, sheet, cell('B', rowNum), "Arbeitsverhalten")
	setCellValue(f, sheet, cell('C', rowNum), "Sozialverhalten")
	setCellStyle(f, sheet, cell('A', rowNum), cell('C', rowNum), tableTitleRow)

	// Table Content
	for i := 0; i < len(studentRepordCard); i++ {
		rowNum++
		setCellValue(f, sheet, cell('A', rowNum), studentRepordCard[i].Name)
		setCellValue(f, sheet, cell('B', rowNum), studentRepordCard[i].ReportCardInfo.AV == "NA")
		setCellValue(f, sheet, cell('C', rowNum), studentRepordCard[i].ReportCardInfo.SV == "NA")

		// format rows
		if (rowNum)%2 == 1 {
			setCellStyle(f, sheet, cell('A', rowNum), cell('A', rowNum), tableFirstColumnRowGray)
			setCellStyle(f, sheet, cell('B', rowNum), cell('C', rowNum), tableRowsGray)
			if studentRepordCard[i].ReportCardInfo.AV == "NA" {
				setCellStyle(f, sheet, cell('B', rowNum), cell('B', rowNum), naValueTableRowsGray)
			}
			if studentRepordCard[i].ReportCardInfo.SV == "NA" {
				setCellStyle(f, sheet, cell('C', rowNum), cell('C', rowNum), naValueTableRowsGray)
			}
		} else {
			setCellStyle(f, sheet, cell('A', rowNum), cell('A', rowNum), tableFirstColumnRow)
			setCellStyle(f, sheet, cell('B', rowNum), cell('C', rowNum), tableRows)
			if studentRepordCard[i].ReportCardInfo.AV == "NA" {
				setCellStyle(f, sheet, cell('B', rowNum), cell('B', rowNum), naValueTableRows)
			}
			if studentRepordCard[i].ReportCardInfo.SV == "NA" {
				setCellStyle(f, sheet, cell('C', rowNum), cell('C', rowNum), naValueTableRows)
			}
		}
	}
}
