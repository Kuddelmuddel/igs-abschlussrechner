package exportdata

import (
	config "igs-abschlussrechner/config"
	"igs-abschlussrechner/global"
	misc "igs-abschlussrechner/miscellaneous"

	"strconv"
	"strings"

	"github.com/xuri/excelize/v2"
)

func insertOverviewSheet(f *excelize.File, studentRepordCard []global.Student) {
	// create sheet and sheet setup
	sheet := "Übersicht"
	err := f.SetSheetName("Sheet1", sheet)
	misc.CheckError("Failed to set sheet name, insertStudentSheets.go", err)

	// set page layout
	formatCellWithForOverviewsheet(f, sheet) // cell withs
	setSheetPrOptionsFitToPage(f, sheet, true)
	var (
		fitToHeight = 1
		fitToWidth  = 1
		orientation = "landscape"
	)
	err = f.SetPageLayout(sheet, &excelize.PageLayoutOptions{
		Orientation: &orientation,
		FitToHeight: &fitToHeight,
		FitToWidth:  &fitToWidth,
	})
	misc.CheckError("Failed to set page layout, insertStudentSheets.go", err)

	// set zoomlevel
	cfg := config.GetConfig()
	zoomScaleOverview := cfg.ZoomScaleOverview
	setSheetViewOptionsZoomscale(f, sheet, 0, zoomScaleOverview)

	// fill sheet
	overview := createGraduationOverview(studentRepordCard)
	maxLength := max(len(overview.hauptschulabschluss), len(overview.realschulabschluss))

	writeHeader(f, sheet, studentRepordCard)

	// first column (HA and BO)
	writeHATable(f, sheet, overview)
	writeBOTable(f, sheet, maxLength)
	writeLegend(f, sheet, overview, maxLength)

	// second column (RA and OA)
	writeRATable(f, sheet, overview)
	writeOhneAbschluss(f, sheet, overview, maxLength)
	writeNotAssigned(f, sheet, overview, maxLength)

	// third column (V11)
	writeV11Table(f, sheet, overview)
}

// function ceates student tendency Overview struct from given repordcard list
func createGraduationOverview(reportCards []global.Student) gradOverview {
	cfg := config.GetConfig()
	spellOutFOSandQualiInOverview := cfg.SpellOutFOSandQualiInOverview

	var classGraduationOverview gradOverview

	if reportCards[0].ClassYear == 10 {
		classGraduationOverview.grade10 = true
	}

	for i := 0; i < len(reportCards); i++ {
		var newStudent gradInfo
		name := strings.Split(reportCards[i].Name, ",")

		newStudent.surname = name[0]
		if reportCards[i].Warnings.ContainsAsteriskGrades {
			newStudent.forename = name[1] + " *)"
			classGraduationOverview.asteriskGrades = true
		} else {
			newStudent.forename = name[1]
		}

		if reportCards[i].Tendency.WithCompensation {
			newStudent.compensation = "x"
		}

		if spellOutFOSandQualiInOverview { // TODO: nedded? (only for other schools ...)
			switch {
			case strings.Contains(reportCards[i].Tendency.Quali, "Quali"):
				newStudent.quali = "Quali"
			case strings.Contains(reportCards[i].Tendency.Quali, "FOS"):
				newStudent.quali = "FOS"
			}
		} else if strings.Contains(reportCards[i].Tendency.Quali, "Quali") {
			newStudent.quali = "x"
		}

		if reportCards[i].Warnings.MissingGrades {
			newStudent.missingGrades = true
			classGraduationOverview.missingGrades = true
		}

		if reportCards[i].Warnings.ContainsWarnings {
			classGraduationOverview.attention = true
			newStudent.attention = true
		}

		switch reportCards[i].Tendency.GraduationTendency {
		case cfg.Versetztung11:
			classGraduationOverview.v11 = append(classGraduationOverview.v11, newStudent)
		case cfg.Realschulabschluss:
			classGraduationOverview.realschulabschluss = append(classGraduationOverview.realschulabschluss, newStudent)
		case cfg.Hauptschulabschluss:
			classGraduationOverview.hauptschulabschluss = append(classGraduationOverview.hauptschulabschluss, newStudent)
		case cfg.OhneAbschluss:
			classGraduationOverview.ohneAbschluss = append(classGraduationOverview.ohneAbschluss, newStudent)
		default:
			classGraduationOverview.notAssigned = append(classGraduationOverview.notAssigned, newStudent)
		}
	}
	return classGraduationOverview
}

func formatCellWithForOverviewsheet(f *excelize.File, sheet string) {
	emtyCellWidth := 0.74
	nameCellWidth := 4.20
	crossCellWidth := 1.53

	setColWith(f, sheet, "A", "A", emtyCellWidth)
	setColWith(f, sheet, "B", "C", nameCellWidth)
	setColWith(f, sheet, "D", "E", crossCellWidth)

	setColWith(f, sheet, "F", "F", emtyCellWidth)
	setColWith(f, sheet, "G", "H", nameCellWidth)
	setColWith(f, sheet, "I", "J", crossCellWidth)

	setColWith(f, sheet, "K", "K", emtyCellWidth)
	setColWith(f, sheet, "L", "M", nameCellWidth)
	setColWith(f, sheet, "N", "N", crossCellWidth)
}

func writeHeader(f *excelize.File, sheet string, studentRepordCard []global.Student) {
	class := strconv.Itoa(studentRepordCard[0].ClassYear) + studentRepordCard[0].ClassChar

	setCellValue(f, sheet, "B1", "Klasse:")
	setCellValue(f, sheet, "C1", class)

	setCellValue(f, sheet, "G1", "Klassenlehrkraft:")
	setCellValue(f, sheet, "H1", studentRepordCard[0].ClassTeacher)

	setCellValue(f, sheet, "L1", "Datum:")
	setCellValue(f, sheet, "M1", studentRepordCard[0].DateOfFile)
}

func writeHATable(f *excelize.File, sheet string, overviewInfo gradOverview) {
	cfg := config.GetConfig()

	var hauptschulabschluss string
	if overviewInfo.grade10 {
		hauptschulabschluss = cfg.Hauptschulabschluss + " vorhanden/möglich? (prüfen!)"
	} else {
		hauptschulabschluss = cfg.Hauptschulabschluss
	}

	// ############## create Styles
	bgColorStyleHA, err := f.NewStyle(&excelize.Style{
		Fill: excelize.Fill{
			Type:    "pattern",
			Color:   []string{"#d7e4bd"},
			Pattern: 1,
		},
	})
	misc.CheckError("", err)

	bgColorStyleHAcenter, err := f.NewStyle(&excelize.Style{
		Fill: excelize.Fill{
			Type:    "pattern",
			Color:   []string{"#d7e4bd"},
			Pattern: 1,
		},
		Alignment: &excelize.Alignment{
			Horizontal: "center",
			Vertical:   "center",
		},
	})
	misc.CheckError("", err)

	bgColorStyleHeaderHAcenter, err := f.NewStyle(&excelize.Style{
		Fill: excelize.Fill{
			Type:    "pattern",
			Color:   []string{"#92d050"},
			Pattern: 1,
		},
		Font: &excelize.Font{
			Size: 11,
			Bold: true,
		},
		Alignment: &excelize.Alignment{
			Horizontal: "center",
			Vertical:   "center",
		},
	})
	misc.CheckError("", err)

	bgColorStyleHeaderHA, err := f.NewStyle(&excelize.Style{
		Fill: excelize.Fill{
			Type:    "pattern",
			Color:   []string{"#92d050"},
			Pattern: 1,
		},
		Font: &excelize.Font{
			Size: 11,
			Bold: true,
		},
	})
	misc.CheckError("", err)

	bgColorStyleWarning, err := f.NewStyle(&excelize.Style{
		Fill: excelize.Fill{
			Type:    "pattern",
			Color:   []string{"#ff3838"},
			Pattern: 1,
		},
		Font: &excelize.Font{
			Size: 11,
			Bold: true,
		},
		Alignment: &excelize.Alignment{
			Horizontal: "left",
			Vertical:   "center",
		},
	})
	misc.CheckError("", err)

	bgColorStyleAttention, err := f.NewStyle(&excelize.Style{
		Fill: excelize.Fill{
			Type:    "pattern",
			Color:   []string{"#ffbf00"},
			Pattern: 1,
		},
		Font: &excelize.Font{
			Size: 11,
			Bold: true,
		},
		Alignment: &excelize.Alignment{
			Horizontal: "left",
			Vertical:   "center",
		},
	})
	misc.CheckError("", err)
	// ############## end Styles

	setCellValue(f, sheet, "B2", hauptschulabschluss)
	mergeCell(f, sheet, "B2", "E2")
	setCellValue(f, sheet, "B3", "Name")
	setCellValue(f, sheet, "C3", "Vorname")
	if !overviewInfo.grade10 {
		setCellValue(f, sheet, "D3", "Ausgl.")
		setCellValue(f, sheet, "E3", "Quali")
	}

	setCellStyle(f, sheet, "B2", "E2", bgColorStyleHeaderHAcenter)
	setCellStyle(f, sheet, "B3", "E3", bgColorStyleHeaderHA)

	for i := 0; i < len(overviewInfo.hauptschulabschluss); i++ {
		setCellValue(f, sheet, cell('B', 4+i), overviewInfo.hauptschulabschluss[i].surname)  // Nachname
		setCellValue(f, sheet, cell('C', 4+i), overviewInfo.hauptschulabschluss[i].forename) // Vorname
		if !overviewInfo.grade10 {
			setCellValue(f, sheet, cell('D', 4+i), overviewInfo.hauptschulabschluss[i].compensation) // Ausgleich
			setCellValue(f, sheet, cell('E', 4+i), overviewInfo.hauptschulabschluss[i].quali)        // Quali
		}
		if overviewInfo.hauptschulabschluss[i].attention {
			setCellValue(f, sheet, cell('A', 4+i), "!➞") // Quali
			setCellStyle(f, sheet, cell('A', 4+i), cell('A', 4+i), bgColorStyleAttention)
		}
		if overviewInfo.hauptschulabschluss[i].missingGrades {
			setCellValue(f, sheet, cell('A', 4+i), "!➞") // Quali
			setCellStyle(f, sheet, cell('A', 4+i), cell('A', 4+i), bgColorStyleWarning)
		}
		// style
		setCellStyle(f, sheet, cell('B', 4+i), cell('C', 4+i), bgColorStyleHA)
		setCellStyle(f, sheet, cell('D', 4+i), cell('E', 4+i), bgColorStyleHAcenter)
	}
}

func writeBOTable(f *excelize.File, sheet string, maxLength int) {
	// ############## create Styles
	bgColorStyleBO, err := f.NewStyle(&excelize.Style{
		Fill: excelize.Fill{
			Type:    "pattern",
			Color:   []string{"#e6e0ec"},
			Pattern: 1,
		},
	})
	misc.CheckError("", err)

	bgColorStyleHeaderBOcenter, err := f.NewStyle(&excelize.Style{
		Fill: excelize.Fill{
			Type:    "pattern",
			Color:   []string{"#b3a2c7"},
			Pattern: 1,
		},
		Font: &excelize.Font{
			Size: 11,
			Bold: true,
		},
		Alignment: &excelize.Alignment{
			Horizontal: "center",
			Vertical:   "center",
		},
	})
	misc.CheckError("", err)

	bgColorStyleHeaderBO, err := f.NewStyle(&excelize.Style{
		Fill: excelize.Fill{
			Type:    "pattern",
			Color:   []string{"#b3a2c7"},
			Pattern: 1,
		},
		Font: &excelize.Font{
			Size: 11,
			Bold: true,
		},
	})
	misc.CheckError("", err)
	// ############## end Styles

	setCellValue(f, sheet, cell('B', maxLength+6), "Berufsorientierter Abschluss")
	mergeCell(f, sheet, cell('B', maxLength+6), cell('E', maxLength+6))
	setCellValue(f, sheet, cell('B', maxLength+7), "Name")
	setCellValue(f, sheet, cell('C', maxLength+7), "Vorname")
	mergeCell(f, sheet, cell('C', maxLength+7), cell('E', maxLength+7))
	setCellStyle(f, sheet, cell('B', maxLength+6), cell('C', maxLength+6), bgColorStyleHeaderBOcenter)
	setCellStyle(f, sheet, cell('B', maxLength+7), cell('C', maxLength+7), bgColorStyleHeaderBO)
	setCellStyle(f, sheet, cell('B', maxLength+8), cell('C', maxLength+13), bgColorStyleBO)
	for i := 0; i <= 5; i++ { // merge empty cells
		mergeCell(f, sheet, cell('C', maxLength+8+i), cell('E', maxLength+8+i))
	}
}

func writeLegend(f *excelize.File, sheet string, overviewInfo gradOverview, maxLength int) {
	// ############## create Styles
	bgColorStyleWarning, err := f.NewStyle(&excelize.Style{
		Fill: excelize.Fill{
			Type:    "pattern",
			Color:   []string{"#ff3838"},
			Pattern: 1,
		},
		Font: &excelize.Font{
			Size: 11,
			Bold: true,
		},
	})
	misc.CheckError("", err)

	bgColorStyleAttention, err := f.NewStyle(&excelize.Style{
		Fill: excelize.Fill{
			Type:    "pattern",
			Color:   []string{"#ffbf00"},
			Pattern: 1,
		},
		Font: &excelize.Font{
			Size: 11,
			Bold: true,
		},
	})
	misc.CheckError("", err)
	// ############## end Styles

	legrow := maxLength + 15 + len(overviewInfo.notAssigned)
	setCellValue(f, sheet, cell('B', legrow), "Wichtig: Berechnete Tendenzen können Fehler enthalten! Deshalb automatisch berechnete Tendenzen unbedingt prüfen!")
	legrow++

	if overviewInfo.asteriskGrades {
		setCellValue(f, sheet, cell('A', legrow), "*)")
		setCellValue(f, sheet, cell('B', legrow), "Schüler*In mit Sternchennoten auf dem Zeugnis")
		legrow++
	}
	if overviewInfo.attention {
		setCellValue(f, sheet, cell('A', legrow), "!➞")
		setCellValue(f, sheet, cell('B', legrow), "Warnung(en) beachten (siehe Tendenzblatt)")
		setCellStyle(f, sheet, cell('A', legrow), cell('A', legrow), bgColorStyleAttention)
		legrow++
	}
	if overviewInfo.missingGrades {
		setCellValue(f, sheet, cell('A', legrow), "!➞")
		setCellValue(f, sheet, cell('B', legrow), "Schüler*In fehlen Noten aus der LUSD (bei Stufenleitung melden)")
		setCellStyle(f, sheet, cell('A', legrow), cell('A', legrow), bgColorStyleWarning)
	}
}

func writeRATable(f *excelize.File, sheet string, overviewInfo gradOverview) {
	cfg := config.GetConfig()
	realschulabschluss := cfg.Realschulabschluss

	// ############## create Styles
	bgColorStyleRA, err := f.NewStyle(&excelize.Style{
		Fill: excelize.Fill{
			Type:    "pattern",
			Color:   []string{"#c6d9f1"},
			Pattern: 1,
		},
	})
	misc.CheckError("", err)

	bgColorStyleRAcenter, err := f.NewStyle(&excelize.Style{
		Fill: excelize.Fill{
			Type:    "pattern",
			Color:   []string{"#c6d9f1"},
			Pattern: 1,
		},
		Alignment: &excelize.Alignment{
			Horizontal: "center",
			Vertical:   "center",
		},
	})
	misc.CheckError("", err)

	bgColorStyleHeaderRAcenter, err := f.NewStyle(&excelize.Style{
		Fill: excelize.Fill{
			Type:    "pattern",
			Color:   []string{"#00b0f0"},
			Pattern: 1,
		},
		Font: &excelize.Font{
			Size: 11,
			Bold: true,
		},
		Alignment: &excelize.Alignment{
			Horizontal: "center",
			Vertical:   "center",
		},
	})
	misc.CheckError("", err)

	bgColorStyleHeaderRA, err := f.NewStyle(&excelize.Style{
		Fill: excelize.Fill{
			Type:    "pattern",
			Color:   []string{"#00b0f0"},
			Pattern: 1,
		},
		Font: &excelize.Font{
			Size: 11,
			Bold: true,
		},
	})
	misc.CheckError("", err)

	bgColorStyleWarning, err := f.NewStyle(&excelize.Style{
		Fill: excelize.Fill{
			Type:    "pattern",
			Color:   []string{"#ff3838"},
			Pattern: 1,
		},
		Font: &excelize.Font{
			Size: 11,
			Bold: true,
		},
	})
	misc.CheckError("", err)

	bgColorStyleAttention, err := f.NewStyle(&excelize.Style{
		Fill: excelize.Fill{
			Type:    "pattern",
			Color:   []string{"#ffbf00"},
			Pattern: 1,
		},
		Font: &excelize.Font{
			Size: 11,
			Bold: true,
		},
	})
	misc.CheckError("", err)
	// ############## end Styles

	setCellValue(f, sheet, "G2", realschulabschluss)
	mergeCell(f, sheet, "G2", "J2")
	setCellValue(f, sheet, "G3", "Name")
	setCellValue(f, sheet, "H3", "Vorname")
	setCellValue(f, sheet, "I3", "Ausgl.")
	setCellValue(f, sheet, "J3", "Quali")
	setCellStyle(f, sheet, "G2", "G2", bgColorStyleHeaderRAcenter)
	setCellStyle(f, sheet, "G3", "J3", bgColorStyleHeaderRA)
	for i := 0; i < len(overviewInfo.realschulabschluss); i++ {
		setCellValue(f, sheet, cell('G', 4+i), overviewInfo.realschulabschluss[i].surname)      // Nachname
		setCellValue(f, sheet, cell('H', 4+i), overviewInfo.realschulabschluss[i].forename)     // Vorname
		setCellValue(f, sheet, cell('I', 4+i), overviewInfo.realschulabschluss[i].compensation) // Ausgleich
		setCellValue(f, sheet, cell('J', 4+i), overviewInfo.realschulabschluss[i].quali)        // Quali
		if overviewInfo.realschulabschluss[i].attention {
			setCellValue(f, sheet, cell('F', 4+i), "!➞") // Quali
			setCellStyle(f, sheet, cell('F', 4+i), cell('F', 4+i), bgColorStyleAttention)
		}
		if overviewInfo.realschulabschluss[i].missingGrades {
			setCellValue(f, sheet, cell('F', 4+i), "!➞") // Quali
			setCellStyle(f, sheet, cell('F', 4+i), cell('F', 4+i), bgColorStyleWarning)
		}
		// Styles
		setCellStyle(f, sheet, cell('G', 4+i), cell('H', 4+i), bgColorStyleRA)
		setCellStyle(f, sheet, cell('I', 4+i), cell('J', 4+i), bgColorStyleRAcenter)
	}
}

func writeOhneAbschluss(f *excelize.File, sheet string, overviewInfo gradOverview, maxLength int) {
	cfg := config.GetConfig()

	// ############## create Styles
	bgColorStyleNoGrad, err := f.NewStyle(&excelize.Style{
		Fill: excelize.Fill{
			Type:    "pattern",
			Color:   []string{"#ddd9c3"},
			Pattern: 1,
		},
	})
	misc.CheckError("", err)

	bgColorStyleHeaderNoGradcenter, err := f.NewStyle(&excelize.Style{
		Fill: excelize.Fill{
			Type:    "pattern",
			Color:   []string{"#948a54"},
			Pattern: 1,
		},
		Font: &excelize.Font{
			Size: 11,
			Bold: true,
		},
		Alignment: &excelize.Alignment{
			Horizontal: "center",
			Vertical:   "center",
		},
	})
	misc.CheckError("", err)

	bgColorStyleHeaderNoGrad, err := f.NewStyle(&excelize.Style{
		Fill: excelize.Fill{
			Type:    "pattern",
			Color:   []string{"#948a54"},
			Pattern: 1,
		},
		Font: &excelize.Font{
			Size: 11,
			Bold: true,
		},
	})
	misc.CheckError("", err)

	bgColorStyleWarning, err := f.NewStyle(&excelize.Style{
		Fill: excelize.Fill{
			Type:    "pattern",
			Color:   []string{"#ff3838"},
			Pattern: 1,
		},
		Font: &excelize.Font{
			Size: 11,
			Bold: true,
		},
	})
	misc.CheckError("", err)

	bgColorStyleAttention, err := f.NewStyle(&excelize.Style{
		Fill: excelize.Fill{
			Type:    "pattern",
			Color:   []string{"#ffbf00"},
			Pattern: 1,
		},
		Font: &excelize.Font{
			Size: 11,
			Bold: true,
		},
	})
	misc.CheckError("", err)
	// ############## end Styles

	setCellValue(f, sheet, cell('G', maxLength+6), cfg.OhneAbschluss)
	mergeCell(f, sheet, cell('G', maxLength+6), cell('J', maxLength+6))
	setCellValue(f, sheet, cell('G', maxLength+7), "Name")
	setCellValue(f, sheet, cell('H', maxLength+7), "Vorname")
	mergeCell(f, sheet, cell('H', maxLength+7), cell('J', maxLength+7))
	setCellStyle(f, sheet, cell('G', maxLength+6), cell('H', maxLength+6), bgColorStyleHeaderNoGradcenter)
	setCellStyle(f, sheet, cell('G', maxLength+7), cell('H', maxLength+7), bgColorStyleHeaderNoGrad)
	for i := 0; i < len(overviewInfo.ohneAbschluss); i++ {
		setCellValue(f, sheet, cell('G', maxLength+8+i), overviewInfo.ohneAbschluss[i].surname)  // Nachname
		setCellValue(f, sheet, cell('H', maxLength+8+i), overviewInfo.ohneAbschluss[i].forename) // Vorname
		mergeCell(f, sheet, cell('H', maxLength+8+i), cell('J', maxLength+8+i))
		if overviewInfo.ohneAbschluss[i].attention {
			setCellValue(f, sheet, cell('F', maxLength+8+i), "!➞") // Quali
			setCellStyle(f, sheet, cell('F', maxLength+8+i), cell('F', maxLength+8+i), bgColorStyleAttention)
		}
		if overviewInfo.ohneAbschluss[i].missingGrades {
			setCellValue(f, sheet, cell('F', maxLength+8+i), "!➞") // Quali
			setCellStyle(f, sheet, cell('F', maxLength+8+i), cell('F', maxLength+8+i), bgColorStyleWarning)
		}
		// Styles
		setCellStyle(f, sheet, cell('G', maxLength+8), cell('H', maxLength+8+i), bgColorStyleNoGrad)
		mergeCell(f, sheet, cell('H', maxLength+8+i), cell('J', maxLength+8+i))
	}
}

func writeNotAssigned(f *excelize.File, sheet string, overviewInfo gradOverview, maxLength int) {
	// ############## create Styles
	bgColorStyleNoAss, err := f.NewStyle(&excelize.Style{
		Fill: excelize.Fill{
			Type:    "pattern",
			Color:   []string{"#ffa6a6"},
			Pattern: 1,
		},
	})
	misc.CheckError("", err)

	bgColorStyleHeaderNoAsscenter, err := f.NewStyle(&excelize.Style{
		Fill: excelize.Fill{
			Type:    "pattern",
			Color:   []string{"#ff3838"},
			Pattern: 1,
		},
		Font: &excelize.Font{
			Size: 11,
			Bold: true,
		},
		Alignment: &excelize.Alignment{
			Horizontal: "center",
			Vertical:   "center",
		},
	})
	misc.CheckError("", err)

	bgColorStyleHeaderNoAss, err := f.NewStyle(&excelize.Style{
		Fill: excelize.Fill{
			Type:    "pattern",
			Color:   []string{"#ff3838"},
			Pattern: 1,
		},
		Font: &excelize.Font{
			Size: 11,
			Bold: true,
		},
	})
	misc.CheckError("", err)

	bgColorStyleWarning, err := f.NewStyle(&excelize.Style{
		Fill: excelize.Fill{
			Type:    "pattern",
			Color:   []string{"#ff3838"},
			Pattern: 1,
		},
		Font: &excelize.Font{
			Size: 11,
			Bold: true,
		},
	})
	misc.CheckError("", err)

	bgColorStyleAttention, err := f.NewStyle(&excelize.Style{
		Fill: excelize.Fill{
			Type:    "pattern",
			Color:   []string{"#ffbf00"},
			Pattern: 1,
		},
		Font: &excelize.Font{
			Size: 11,
			Bold: true,
		},
	})
	misc.CheckError("", err)
	// ############## end Styles

	if len(overviewInfo.notAssigned) > 0 {
		startRowNotAssigned := maxLength + 10 + len(overviewInfo.ohneAbschluss)
		setCellValue(f, sheet, cell('G', startRowNotAssigned), "Nicht automatisch zugeordnet")
		mergeCell(f, sheet, cell('G', startRowNotAssigned), cell('J', startRowNotAssigned))
		setCellValue(f, sheet, cell('G', startRowNotAssigned+1), "Name")
		setCellValue(f, sheet, cell('H', startRowNotAssigned+1), "Vorname")
		mergeCell(f, sheet, cell('H', startRowNotAssigned+1), cell('J', startRowNotAssigned+1))
		setCellStyle(f, sheet, cell('G', startRowNotAssigned), cell('H', startRowNotAssigned), bgColorStyleHeaderNoAsscenter)
		setCellStyle(f, sheet, cell('G', startRowNotAssigned+1), cell('H', startRowNotAssigned+1), bgColorStyleHeaderNoAss)
		for i := 0; i < len(overviewInfo.notAssigned); i++ {
			setCellValue(f, sheet, cell('G', startRowNotAssigned+2+i), overviewInfo.notAssigned[i].surname)  // Nachname
			setCellValue(f, sheet, cell('H', startRowNotAssigned+2+i), overviewInfo.notAssigned[i].forename) // Vorname
			mergeCell(f, sheet, cell('H', startRowNotAssigned+2+i), cell('J', startRowNotAssigned+2+i))
			if overviewInfo.notAssigned[i].attention {
				setCellValue(f, sheet, cell('F', startRowNotAssigned+2+i), "!➞") // Quali
				setCellStyle(f, sheet, cell('F', startRowNotAssigned+2+i), cell('F', startRowNotAssigned+2+i), bgColorStyleAttention)
			}
			if overviewInfo.notAssigned[i].missingGrades {
				setCellValue(f, sheet, cell('F', startRowNotAssigned+2+i), "!➞") // Quali
				setCellStyle(f, sheet, cell('F', startRowNotAssigned+2+i), cell('F', startRowNotAssigned+2+i), bgColorStyleWarning)
			}
			// Styles
			mergeCell(f, sheet, cell('H', startRowNotAssigned+2+i), cell('J', startRowNotAssigned+2+i))
			setCellStyle(f, sheet, cell('G', startRowNotAssigned+2+i), cell('H', startRowNotAssigned+2+i), bgColorStyleNoAss)
		}
	}
}

func writeV11Table(f *excelize.File, sheet string, overviewInfo gradOverview) {
	cfg := config.GetConfig()

	// ############## create Styles
	bgColorStylev11, err := f.NewStyle(&excelize.Style{
		Fill: excelize.Fill{
			Type:    "pattern",
			Color:   []string{"#fdeada"},
			Pattern: 1,
		},
	})
	misc.CheckError("", err)

	bgColorStylev11center, err := f.NewStyle(&excelize.Style{
		Fill: excelize.Fill{
			Type:    "pattern",
			Color:   []string{"#fdeada"},
			Pattern: 1,
		},
		Alignment: &excelize.Alignment{
			Horizontal: "center",
			Vertical:   "center",
		},
	})
	misc.CheckError("", err)

	bgColorStyleHeaderv11center, err := f.NewStyle(&excelize.Style{
		Fill: excelize.Fill{
			Type:    "pattern",
			Color:   []string{"#ffc000"},
			Pattern: 1,
		},
		Font: &excelize.Font{
			Size: 11,
			Bold: true,
		},
		Alignment: &excelize.Alignment{
			Horizontal: "center",
			Vertical:   "center",
		},
	})
	misc.CheckError("", err)

	bgColorStyleHeaderv11, err := f.NewStyle(&excelize.Style{
		Fill: excelize.Fill{
			Type:    "pattern",
			Color:   []string{"#ffc000"},
			Pattern: 1,
		},
		Font: &excelize.Font{
			Size: 11,
			Bold: true,
		},
	})
	misc.CheckError("", err)

	bgColorStyleWarning, err := f.NewStyle(&excelize.Style{
		Fill: excelize.Fill{
			Type:    "pattern",
			Color:   []string{"#ff3838"},
			Pattern: 1,
		},
		Font: &excelize.Font{
			Size: 11,
			Bold: true,
		},
	})
	misc.CheckError("", err)

	bgColorStyleAttention, err := f.NewStyle(&excelize.Style{
		Fill: excelize.Fill{
			Type:    "pattern",
			Color:   []string{"#ffbf00"},
			Pattern: 1,
		},
		Font: &excelize.Font{
			Size: 11,
			Bold: true,
		},
	})
	misc.CheckError("", err)
	// ############## end Styles

	setCellValue(f, sheet, "L2", cfg.Versetztung11)
	mergeCell(f, sheet, "L2", "N2")
	setCellValue(f, sheet, "L3", "Name")
	setCellValue(f, sheet, "M3", "Vorname")
	setCellValue(f, sheet, "N3", "Ausgl.")
	setCellStyle(f, sheet, "L2", "N2", bgColorStyleHeaderv11center)
	setCellStyle(f, sheet, "L3", "N3", bgColorStyleHeaderv11)

	for i := 0; i < len(overviewInfo.v11); i++ {
		setCellValue(f, sheet, cell('L', 4+i), overviewInfo.v11[i].surname)      // Nachname
		setCellValue(f, sheet, cell('M', 4+i), overviewInfo.v11[i].forename)     // Vorname
		setCellValue(f, sheet, cell('N', 4+i), overviewInfo.v11[i].compensation) // Ausgleich
		if overviewInfo.v11[i].attention {
			setCellValue(f, sheet, cell('K', 4+i), "!➞") // Quali
			setCellStyle(f, sheet, cell('K', 4+i), cell('K', 4+i), bgColorStyleAttention)
		}
		if overviewInfo.v11[i].missingGrades {
			setCellValue(f, sheet, cell('K', 4+i), "!➞") // Quali
			setCellStyle(f, sheet, cell('K', 4+i), cell('K', 4+i), bgColorStyleWarning)
		}
		// Styles
		setCellStyle(f, sheet, cell('L', 4+i), cell('M', 4+i), bgColorStylev11)
		setCellStyle(f, sheet, cell('N', 4+i), cell('N', 4+i), bgColorStylev11center)
	}
}
