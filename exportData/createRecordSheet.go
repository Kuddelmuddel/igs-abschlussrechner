package exportdata

import (
	"fmt"
	config "igs-abschlussrechner/config"
	"igs-abschlussrechner/global"
	misc "igs-abschlussrechner/miscellaneous"
	"log"
	"strconv"
	"strings"

	"github.com/TwiN/go-color"
	"github.com/xuri/excelize/v2"
)

func insertRecord(f *excelize.File, studentRepordCard []global.Student) {
	sheet := "Protokoll"
	_, err := f.NewSheet(sheet)
	misc.CheckError("Failed to create new sheet, createRecordSheet.go", err)

	setSheetViewOptionsGridLines(f, sheet, 0, false)
	var (
		fitToHeight = 1
		fitToWidth  = 1
	)
	err = f.SetPageLayout(sheet, &excelize.PageLayoutOptions{
		FitToHeight: &fitToHeight,
		FitToWidth:  &fitToWidth,
	})
	misc.CheckError("Failed to set page layout, createRecordSheet.go", err)
	setSheetFormatPrDefaultRowHeight(f, sheet, 16)
	formatCellWithForProtocollSheet(f, sheet)

	chapterNum, currentRow := writeProtocolHeader(f, sheet, studentRepordCard, 1, 1)
	chapterNum, currentRow = writePrognosegutachten(f, sheet, studentRepordCard, currentRow, chapterNum)
	chapterNum, currentRow = writeUebergangsberatung(f, sheet, currentRow, chapterNum)
	if !(studentRepordCard[0].ClassYear == 10 && studentRepordCard[0].Term == 2) {
		chapterNum, currentRow = writeErsteinstufungen(f, sheet, currentRow, chapterNum)
		chapterNum, currentRow = writeUmstufungen(f, sheet, studentRepordCard, currentRow, chapterNum)
	}
	chapterNum, currentRow = Notensprung(f, sheet, studentRepordCard, currentRow, chapterNum)
	chapterNum, currentRow = writeZeugnisbemerkungen(f, sheet, currentRow, chapterNum)
	chapterNum, currentRow = writeNachteilsausgleichNotenschutz(f, sheet, currentRow, chapterNum)
	if !(studentRepordCard[0].ClassYear == 10 && studentRepordCard[0].Term == 2) {
		chapterNum, currentRow = writeFoerderplaene(f, sheet, studentRepordCard, currentRow, chapterNum)
	}
	chapterNum, currentRow = writeFoerderschwerpunktLERESE(f, sheet, currentRow, chapterNum)
	chapterNum, currentRow = writeFehlzeiten(f, sheet, currentRow, chapterNum)
	_, currentRow = writeSonstigeErklaerungenBeschluesse(f, sheet, currentRow, chapterNum)
	currentRow = writeSignLines(f, sheet, currentRow)

	// specify print area for proctocol sheet
	err = f.SetDefinedName(&excelize.DefinedName{
		Name:     "_xlnm.Print_Area",
		RefersTo: sheet + "!" + "$A$1:$K$" + strconv.Itoa(currentRow),
		Scope:    sheet,
	})
	misc.CheckError("Failed to set print area, ExportReportCards2Excel.go", err)
}

func formatCellWithForProtocollSheet(f *excelize.File, sheet string) {
	setColWith(f, sheet, "A", "N", 2.05)
	setColWith(f, sheet, "B", "B", 2.54)
	setColWith(f, sheet, "F", "F", 0.5)
	setColWith(f, sheet, "G", "G", 1.50)
	setColWith(f, sheet, "H", "I", 2.4)
	setColWith(f, sheet, "L", "L", 0.5)
}

func writeProtocolHeader(f *excelize.File, sheet string, studentRepordCard []global.Student, rowNum, chapterNum int) (int, int) {
	cfg := config.GetConfig()

	// creating styles for protocol header
	titleStyle, err := f.NewStyle(&excelize.Style{
		Font: &excelize.Font{
			Size: 14,
			Bold: true,
		},
	})
	misc.CheckError("", err)

	subtitleStyle, err := f.NewStyle(&excelize.Style{
		Font: &excelize.Font{
			Size: 14,
		},
	})
	misc.CheckError("", err)

	chapterHeaderStyle, err := f.NewStyle(&excelize.Style{
		Font: &excelize.Font{
			Bold: true,
		},
	})
	misc.CheckError("", err)

	textFieldStyle, err := f.NewStyle(&excelize.Style{
		Font: &excelize.Font{
			Size: 11,
		},
		Alignment: &excelize.Alignment{
			Horizontal: "right",
			Vertical:   "center",
		},
	})
	misc.CheckError("", err)

	timeFieldStyle, err := f.NewStyle(&excelize.Style{
		NumFmt: 20,
		Font: &excelize.Font{
			Size: 11,
		},
		Alignment: &excelize.Alignment{
			Horizontal: "left",
			Vertical:   "center",
		},
	})
	misc.CheckError("", err)

	textboxstyle, err := f.NewStyle(&excelize.Style{
		Font: &excelize.Font{
			Size: 10,
		},
		Border: []excelize.Border{
			{Type: "left", Color: "000000", Style: 1},
			{Type: "top", Color: "000000", Style: 1},
			{Type: "bottom", Color: "000000", Style: 1},
			{Type: "right", Color: "000000", Style: 1},
		},
		Alignment: &excelize.Alignment{
			Vertical: "top",
			WrapText: true,
		},
	})
	misc.CheckError("", err)
	// end of style variables
	setCellValue(f, sheet, cell('A', rowNum), cfg.SchoolName)
	setCellStyle(f, sheet, cell('A', rowNum), cell('A', rowNum), titleStyle)
	rowNum++
	setCellValue(f, sheet, cell('A', rowNum), cfg.SchoolDescription)
	setCellStyle(f, sheet, cell('A', rowNum), cell('A', rowNum), subtitleStyle)
	rowNum++
	rowNum++
	setCellValue(f, sheet, cell('A', rowNum), "Protokoll zur Halbjahres- /Abschlussnotenkonferenz Jahrgänge 8-10")
	setCellStyle(f, sheet, cell('A', rowNum), cell('A', rowNum), chapterHeaderStyle)
	rowNum++
	rowNum++
	setCellValue(f, sheet, cell('A', rowNum), "Datum: ")
	setCellStyle(f, sheet, cell('A', rowNum), cell('B', rowNum), textFieldStyle)
	setCellValue(f, sheet, cell('D', rowNum), "Klasse: ")
	setCellStyle(f, sheet, cell('D', rowNum), cell('D', rowNum), textFieldStyle)

	setCellValue(f, sheet, cell('E', rowNum), strconv.Itoa(studentRepordCard[0].ClassYear)+studentRepordCard[0].ClassChar)

	setCellValue(f, sheet, cell('H', rowNum), "Klassenlehrkraft: ")
	setCellStyle(f, sheet, cell('H', rowNum), cell('H', rowNum), textFieldStyle)
	setCellValue(f, sheet, cell('I', rowNum), studentRepordCard[0].ClassTeacher)
	rowNum++
	rowNum++
	setCellValue(f, sheet, cell('A', rowNum), "Beginn: ")
	setCellStyle(f, sheet, cell('A', rowNum), cell('A', rowNum), textFieldStyle)
	setCellStyle(f, sheet, cell('B', rowNum), cell('B', rowNum), timeFieldStyle)

	setCellValue(f, sheet, cell('D', rowNum), "Ende: ")
	setCellStyle(f, sheet, cell('D', rowNum), cell('D', rowNum), textFieldStyle)
	setCellStyle(f, sheet, cell('E', rowNum), cell('E', rowNum), timeFieldStyle)

	rowNum += 3
	setCellValue(f, sheet, cell('A', rowNum), strconv.Itoa(chapterNum)+". Anwesenheit (siehe Anhang)")
	setCellStyle(f, sheet, cell('A', rowNum), cell('A', rowNum), chapterHeaderStyle)
	rowNum += 3
	chapterNum++
	setCellValue(f, sheet, cell('A', rowNum), strconv.Itoa(chapterNum)+". Tendenzen/Abschlüsse (siehe Liste Anhang)")
	setCellStyle(f, sheet, cell('A', rowNum), cell('A', rowNum), chapterHeaderStyle)
	rowNum += 3
	chapterNum++
	setCellValue(f, sheet, cell('A', rowNum), strconv.Itoa(chapterNum)+". Bemerkungen Tendenzen/Abschlüsse")
	setCellStyle(f, sheet, cell('A', rowNum), cell('A', rowNum), chapterHeaderStyle)
	rowNum++
	chapterNum++
	setCellStyle(f, sheet, cell('A', rowNum), cell('K', rowNum+25), textboxstyle)
	mergeCell(f, sheet, cell('A', rowNum), cell('K', rowNum+25))
	insertLineBreakTip(f, sheet, 'M', rowNum)

	return chapterNum, rowNum + 31
}

func insertLineBreakTip(f *excelize.File, sheet string, colmn byte, rowNum int) {
	infocell, err := f.NewStyle(&excelize.Style{
		Font: &excelize.Font{
			Size: 10,
		},
		Alignment: &excelize.Alignment{
			Horizontal: "left",
			Vertical:   "center",
		},
	})
	misc.CheckError("", err)

	setCellValue(f, sheet, cell(colmn, rowNum), "Hinweis: Zeilenumbruch innerhalb einer Zelle einfügen:")
	setCellValue(f, sheet, cell(colmn, rowNum+1), "Excel: ALT+ENTER")
	setCellValue(f, sheet, cell(colmn, rowNum+2), "LibreOffice: STRG+ENTER")
	setCellStyle(f, sheet, cell(colmn, rowNum), cell(colmn, rowNum+4), infocell)

}

func writePrognosegutachten(f *excelize.File, sheet string, studentRepordCard []global.Student, rowNum, chapterNum int) (int, int) {
	// ############## create Styles
	chapterHeaderStyle, err := f.NewStyle(&excelize.Style{
		Font: &excelize.Font{
			Bold: true,
		},
	})
	misc.CheckError("", err)

	tableHeaderStyle, err := f.NewStyle(&excelize.Style{
		Font: &excelize.Font{
			Size: 10,
			Bold: true,
		},
		Border: []excelize.Border{
			{Type: "left", Color: "000000", Style: 1},
			{Type: "top", Color: "000000", Style: 1},
			{Type: "bottom", Color: "000000", Style: 1},
			{Type: "right", Color: "000000", Style: 1},
		},
		Alignment: &excelize.Alignment{
			Horizontal: "center",
			Vertical:   "center",
		},
	})
	misc.CheckError("", err)

	tableHeaderStyleLeftCol, err := f.NewStyle(&excelize.Style{
		Font: &excelize.Font{
			Size: 10,
			Bold: true,
		},
		Border: []excelize.Border{
			{Type: "left", Color: "000000", Style: 1},
			{Type: "top", Color: "000000", Style: 1},
			{Type: "bottom", Color: "000000", Style: 1},
			{Type: "right", Color: "000000", Style: 1},
		},
		Alignment: &excelize.Alignment{
			Horizontal: "left",
			Vertical:   "center",
		},
	})
	misc.CheckError("", err)

	tablecenterStyle, err := f.NewStyle(&excelize.Style{
		Font: &excelize.Font{
			Size: 10,
		},
		Border: []excelize.Border{
			{Type: "left", Color: "000000", Style: 1},
			{Type: "top", Color: "000000", Style: 1},
			{Type: "bottom", Color: "000000", Style: 1},
			{Type: "right", Color: "000000", Style: 1},
		},
		Alignment: &excelize.Alignment{
			Horizontal: "center",
			Vertical:   "center",
		},
	})
	misc.CheckError("", err)

	tableleftStyle, err := f.NewStyle(&excelize.Style{
		Font: &excelize.Font{
			Size: 10,
		},
		Border: []excelize.Border{
			{Type: "left", Color: "000000", Style: 1},
			{Type: "top", Color: "000000", Style: 1},
			{Type: "bottom", Color: "000000", Style: 1},
			{Type: "right", Color: "000000", Style: 1},
		},
		Alignment: &excelize.Alignment{
			Vertical: "center",
			WrapText: true,
		},
	})
	misc.CheckError("", err)
	// ############## end Styles

	setCellValue(f, sheet, cell('A', rowNum), strconv.Itoa(chapterNum)+". Prognosegutachten / Qualifizierender Realschulabschluss Jahrgang 10")
	setCellStyle(f, sheet, cell('A', rowNum), cell('A', rowNum), chapterHeaderStyle)
	chapterNum++
	rowNum++
	setCellValue(f, sheet, cell('A', rowNum), "Schüler*In")
	mergeCell(f, sheet, cell('A', rowNum), cell('C', rowNum+1))
	setCellStyle(f, sheet, cell('A', rowNum), cell('C', rowNum+1), tableHeaderStyleLeftCol)
	setCellValue(f, sheet, cell('D', rowNum), "Einspruch")
	mergeCell(f, sheet, cell('D', rowNum), cell('E', rowNum))
	setCellStyle(f, sheet, cell('D', rowNum), cell('E', rowNum+1), tableHeaderStyle)
	setCellValue(f, sheet, cell('G', rowNum), "Schüler*In")
	mergeCell(f, sheet, cell('G', rowNum), cell('I', rowNum+1))
	setCellStyle(f, sheet, cell('G', rowNum), cell('I', rowNum+1), tableHeaderStyleLeftCol)
	setCellValue(f, sheet, cell('J', rowNum), "Einspruch")
	mergeCell(f, sheet, cell('J', rowNum), cell('K', rowNum))
	setCellStyle(f, sheet, cell('J', rowNum), cell('K', rowNum+1), tableHeaderStyle)
	rowNum++
	setCellValue(f, sheet, cell('D', rowNum), "Pro")
	setCellValue(f, sheet, cell('E', rowNum), "Kontra")
	setCellValue(f, sheet, cell('J', rowNum), "Pro")
	setCellValue(f, sheet, cell('K', rowNum), "Kontra")

	// add all students with Quali to list (if in grade 10)
	if studentRepordCard[0].ClassYear == 10 && studentRepordCard[0].Term == 2 {
		qualiStudents := extractStudentsWithQuali(studentRepordCard)

		switch {
		case len(qualiStudents) <= 5:
			rowNum++
			for k := 0; k < len(qualiStudents); k++ {

				setCellValue(f, sheet, cell('A', rowNum+k), qualiStudents[k])
				mergeCell(f, sheet, cell('A', rowNum+k), cell('C', rowNum+k))
				setCellStyle(f, sheet, cell('A', rowNum+k), cell('C', rowNum+k), tableleftStyle)
				setCellStyle(f, sheet, cell('D', rowNum+k), cell('E', rowNum+k), tablecenterStyle)

				mergeCell(f, sheet, cell('G', rowNum+k), cell('I', rowNum+k))
				setCellStyle(f, sheet, cell('G', rowNum+k), cell('I', rowNum+k), tableleftStyle)
				setCellStyle(f, sheet, cell('J', rowNum+k), cell('K', rowNum+k), tablecenterStyle)
			}
			rowNum += len(qualiStudents)

			// add empty cells
			for i := 0; i < 5-len(qualiStudents); i++ {
				mergeCell(f, sheet, cell('A', rowNum), cell('C', rowNum))
				setCellStyle(f, sheet, cell('A', rowNum), cell('C', rowNum), tableleftStyle)
				setCellStyle(f, sheet, cell('D', rowNum), cell('E', rowNum), tablecenterStyle)

				mergeCell(f, sheet, cell('G', rowNum), cell('I', rowNum))
				setCellStyle(f, sheet, cell('G', rowNum), cell('I', rowNum), tableleftStyle)
				setCellStyle(f, sheet, cell('J', rowNum), cell('K', rowNum), tablecenterStyle)

				rowNum++
			}
		case len(qualiStudents) <= 10:
			rowNum++
			studentsPart1 := qualiStudents[0:5]
			studentsPart2 := qualiStudents[5:]

			for k := 0; k < len(studentsPart1); k++ {
				setCellValue(f, sheet, cell('A', rowNum+k), studentsPart1[k])
				mergeCell(f, sheet, cell('A', rowNum+k), cell('C', rowNum+k))
				setCellStyle(f, sheet, cell('A', rowNum+k), cell('C', rowNum+k), tableleftStyle)
				setCellStyle(f, sheet, cell('D', rowNum+k), cell('E', rowNum+k), tablecenterStyle)

				if k < len(studentsPart2) {
					setCellValue(f, sheet, cell('G', rowNum+k), studentsPart2[k])
				}
				mergeCell(f, sheet, cell('G', rowNum+k), cell('I', rowNum+k))
				setCellStyle(f, sheet, cell('G', rowNum+k), cell('I', rowNum+k), tableleftStyle)
				setCellStyle(f, sheet, cell('J', rowNum+k), cell('K', rowNum+k), tablecenterStyle)
			}
			rowNum += len(studentsPart1)

		case len(qualiStudents) > 10:
			rowNum++
			studentsPart1 := qualiStudents[0 : len(qualiStudents)/2]
			studentsPart2 := qualiStudents[len(qualiStudents)/2:]

			for k := 0; k < len(studentsPart1); k++ {
				setCellValue(f, sheet, cell('A', rowNum+k), studentsPart1[k])
				mergeCell(f, sheet, cell('A', rowNum+k), cell('C', rowNum+k))
				setCellStyle(f, sheet, cell('A', rowNum+k), cell('C', rowNum+k), tableleftStyle)
				setCellStyle(f, sheet, cell('D', rowNum+k), cell('E', rowNum+k), tablecenterStyle)

				if k < len(studentsPart2) {
					setCellValue(f, sheet, cell('G', rowNum+k), studentsPart2[k])
				}
				mergeCell(f, sheet, cell('G', rowNum+k), cell('I', rowNum+k))
				setCellStyle(f, sheet, cell('G', rowNum+k), cell('I', rowNum+k), tableleftStyle)
				setCellStyle(f, sheet, cell('J', rowNum+k), cell('K', rowNum+k), tablecenterStyle)
			}
			rowNum += len(studentsPart1)
		}
	} else {
		rowNum++
		for i := 0; i < 5; i++ {
			mergeCell(f, sheet, cell('A', rowNum), cell('C', rowNum))
			setCellStyle(f, sheet, cell('A', rowNum), cell('C', rowNum), tableleftStyle)
			setCellStyle(f, sheet, cell('D', rowNum), cell('E', rowNum), tablecenterStyle)

			mergeCell(f, sheet, cell('G', rowNum), cell('I', rowNum))
			setCellStyle(f, sheet, cell('G', rowNum), cell('I', rowNum), tableleftStyle)
			setCellStyle(f, sheet, cell('J', rowNum), cell('K', rowNum), tablecenterStyle)

			rowNum++
		}
	}
	return chapterNum, rowNum + 2
}

func extractStudentsWithQuali(repordCards []global.Student) []string {
	var qualiStudents []string
	for i := 0; i < len(repordCards); i++ {
		if strings.Contains(repordCards[i].Tendency.Quali, "Quali") {
			qualiStudents = append(qualiStudents, repordCards[i].Name)
		}
	}
	return qualiStudents
}

func writeUebergangsberatung(f *excelize.File, sheet string, rowNum, chapterNum int) (int, int) {
	// ############## create Styles
	chapterHeaderStyle, err := f.NewStyle(&excelize.Style{
		Font: &excelize.Font{
			Bold: true,
		},
	})
	misc.CheckError("", err)

	textboxstyle, err := f.NewStyle(&excelize.Style{
		Font: &excelize.Font{
			Size: 10,
		},
		Border: []excelize.Border{
			{Type: "left", Color: "000000", Style: 1},
			{Type: "top", Color: "000000", Style: 1},
			{Type: "bottom", Color: "000000", Style: 1},
			{Type: "right", Color: "000000", Style: 1},
		},
		Alignment: &excelize.Alignment{
			Vertical: "top",
			WrapText: true,
		},
	})
	misc.CheckError("", err)
	// ############## end Styles

	setCellValue(f, sheet, cell('A', rowNum), strconv.Itoa(chapterNum)+". Übergangsberatung (siehe FO Verbleib Schüler)")
	setCellStyle(f, sheet, cell('A', rowNum), cell('A', rowNum), chapterHeaderStyle)
	chapterNum++
	rowNum++
	setCellValue(f, sheet, cell('A', rowNum), "Bemerkungen:")
	mergeCell(f, sheet, cell('A', rowNum), cell('K', rowNum+11))
	setCellStyle(f, sheet, cell('A', rowNum), cell('K', rowNum+11), textboxstyle)
	insertLineBreakTip(f, sheet, 'M', rowNum)

	return chapterNum, rowNum + 13
}

func writeErsteinstufungen(f *excelize.File, sheet string, rowNum, chapterNum int) (int, int) {
	// ############## create Styles
	chapterHeaderStyle, err := f.NewStyle(&excelize.Style{
		Font: &excelize.Font{
			Bold: true,
		},
	})
	misc.CheckError("", err)
	// ############## end Styles

	setCellValue(f, sheet, cell('A', rowNum), strconv.Itoa(chapterNum)+". Ersteinstufungen (siehe Anhang)")
	setCellStyle(f, sheet, cell('A', rowNum), cell('A', rowNum), chapterHeaderStyle)
	chapterNum++

	return chapterNum, rowNum + 3
}

func writeNachteilsausgleichNotenschutz(f *excelize.File, sheet string, rowNum, chapterNum int) (int, int) {
	// ############## create Styles
	chapterHeaderStyle, err := f.NewStyle(&excelize.Style{
		Font: &excelize.Font{
			Bold: true,
		},
	})
	misc.CheckError("", err)

	textboxstyle, err := f.NewStyle(&excelize.Style{
		Font: &excelize.Font{
			Size: 10,
		},
		Border: []excelize.Border{
			{Type: "left", Color: "000000", Style: 1},
			{Type: "top", Color: "000000", Style: 1},
			{Type: "bottom", Color: "000000", Style: 1},
			{Type: "right", Color: "000000", Style: 1},
		},
		Alignment: &excelize.Alignment{
			Vertical: "top",
			WrapText: true,
		},
	})
	misc.CheckError("", err)
	// ############## create Styles

	setCellValue(f, sheet, cell('A', rowNum), strconv.Itoa(chapterNum)+". Nachteilsausgleich/Notenschutz")
	setCellStyle(f, sheet, cell('A', rowNum), cell('A', rowNum), chapterHeaderStyle)
	chapterNum++
	rowNum++
	setCellValue(f, sheet, cell('A', rowNum), "Beantragt (und in Abstimmung genehmigt) für:")
	mergeCell(f, sheet, cell('A', rowNum), cell('K', rowNum+11))
	setCellStyle(f, sheet, cell('A', rowNum), cell('K', rowNum+11), textboxstyle)
	insertLineBreakTip(f, sheet, 'M', rowNum)

	return chapterNum, rowNum + 14
}

func writeUmstufungen(f *excelize.File, sheet string, studentRepordCard []global.Student, rowNum, chapterNum int) (int, int) {
	// ############## create Styles
	chapterHeaderStyle, err := f.NewStyle(&excelize.Style{
		Font: &excelize.Font{
			Bold: true,
		},
	})
	misc.CheckError("", err)

	tableHeaderStyle, err := f.NewStyle(&excelize.Style{
		Font: &excelize.Font{
			Size: 10,
			Bold: true,
		},
		Border: []excelize.Border{
			{Type: "left", Color: "000000", Style: 1},
			{Type: "top", Color: "000000", Style: 1},
			{Type: "bottom", Color: "000000", Style: 1},
			{Type: "right", Color: "000000", Style: 1},
		},
		Alignment: &excelize.Alignment{
			Horizontal: "center",
			Vertical:   "center",
		},
	})
	misc.CheckError("", err)

	tableHeaderStyleLeftCol, err := f.NewStyle(&excelize.Style{
		Font: &excelize.Font{
			Size: 10,
			Bold: true,
		},
		Border: []excelize.Border{
			{Type: "left", Color: "000000", Style: 1},
			{Type: "top", Color: "000000", Style: 1},
			{Type: "bottom", Color: "000000", Style: 1},
			{Type: "right", Color: "000000", Style: 1},
		},
		Alignment: &excelize.Alignment{
			Horizontal: "left",
			Vertical:   "center",
		},
	})
	misc.CheckError("", err)

	tablecenterStyle, err := f.NewStyle(&excelize.Style{
		Font: &excelize.Font{
			Size: 10,
		},
		Border: []excelize.Border{
			{Type: "left", Color: "000000", Style: 1},
			{Type: "top", Color: "000000", Style: 1},
			{Type: "bottom", Color: "000000", Style: 1},
			{Type: "right", Color: "000000", Style: 1},
		},
		Alignment: &excelize.Alignment{
			Horizontal: "center",
			Vertical:   "center",
		},
	})
	misc.CheckError("", err)

	tableleftStyle, err := f.NewStyle(&excelize.Style{
		Font: &excelize.Font{
			Size: 10,
		},
		Border: []excelize.Border{
			{Type: "left", Color: "000000", Style: 1},
			{Type: "top", Color: "000000", Style: 1},
			{Type: "bottom", Color: "000000", Style: 1},
			{Type: "right", Color: "000000", Style: 1},
		},
		Alignment: &excelize.Alignment{
			Vertical: "center",
			WrapText: true,
		},
	})
	misc.CheckError("", err)

	tablecenterStyleMarked, err := f.NewStyle(&excelize.Style{
		Fill: excelize.Fill{
			Type:    "pattern",
			Color:   []string{"#ffbf00"},
			Pattern: 1,
		},
		Font: &excelize.Font{
			Size: 10,
		},
		Border: []excelize.Border{
			{Type: "left", Color: "000000", Style: 1},
			{Type: "top", Color: "000000", Style: 1},
			{Type: "bottom", Color: "000000", Style: 1},
			{Type: "right", Color: "000000", Style: 1},
		},
		Alignment: &excelize.Alignment{
			Horizontal: "center",
			Vertical:   "center",
		},
	})
	misc.CheckError("", err)

	tableleftStyleMarked, err := f.NewStyle(&excelize.Style{
		Fill: excelize.Fill{
			Type:    "pattern",
			Color:   []string{"#ffbf00"},
			Pattern: 1,
		},
		Font: &excelize.Font{
			Size: 10,
		},
		Border: []excelize.Border{
			{Type: "left", Color: "000000", Style: 1},
			{Type: "top", Color: "000000", Style: 1},
			{Type: "bottom", Color: "000000", Style: 1},
			{Type: "right", Color: "000000", Style: 1},
		},
		Alignment: &excelize.Alignment{
			Vertical: "center",
		},
	})
	misc.CheckError("", err)

	legend, err := f.NewStyle(&excelize.Style{
		Font: &excelize.Font{
			Size: 8,
		},
		Alignment: &excelize.Alignment{
			Vertical: "top",
		},
	})
	misc.CheckError("", err)

	tableleftStyleGrey, err := f.NewStyle(&excelize.Style{
		Font: &excelize.Font{
			Size:  10,
			Color: "b3b3b3",
		},
		Border: []excelize.Border{
			{Type: "left", Color: "000000", Style: 1},
			{Type: "top", Color: "000000", Style: 1},
			{Type: "bottom", Color: "000000", Style: 1},
			{Type: "right", Color: "000000", Style: 1},
		},
		Alignment: &excelize.Alignment{
			Vertical: "center",
			WrapText: true,
		},
	})
	misc.CheckError("", err)

	tablecenterStyleGrey, err := f.NewStyle(&excelize.Style{
		Font: &excelize.Font{
			Size:  10,
			Color: "cccccc",
		},
		Border: []excelize.Border{
			{Type: "left", Color: "000000", Style: 1},
			{Type: "top", Color: "000000", Style: 1},
			{Type: "bottom", Color: "000000", Style: 1},
			{Type: "right", Color: "000000", Style: 1},
		},
		Alignment: &excelize.Alignment{
			Horizontal: "center",
			Vertical:   "center",
		},
	})
	misc.CheckError("", err)
	// ############## end Styles

	setCellValue(f, sheet, cell('A', rowNum), strconv.Itoa(chapterNum)+". Umstufungen:")
	setCellStyle(f, sheet, cell('A', rowNum), cell('A', rowNum), chapterHeaderStyle)
	chapterNum++
	rowNum++

	// Table Header (Umstufungen)
	setCellValue(f, sheet, cell('A', rowNum), "Schüler*In")
	mergeCell(f, sheet, cell('A', rowNum), cell('D', rowNum))
	setCellValue(f, sheet, cell('E', rowNum), "Fach (Lehrkraft)")
	mergeCell(f, sheet, cell('E', rowNum), cell('G', rowNum))
	setCellValue(f, sheet, cell('H', rowNum), "alter Kurs")
	setCellValue(f, sheet, cell('I', rowNum), "neuer Kurs")
	setCellValue(f, sheet, cell('J', rowNum), "Bemerkung")
	mergeCell(f, sheet, cell('J', rowNum), cell('K', rowNum))
	setCellStyle(f, sheet, cell('A', rowNum), cell('D', rowNum), tableHeaderStyleLeftCol)
	setCellStyle(f, sheet, cell('E', rowNum), cell('K', rowNum), tableHeaderStyle)
	rowNum++

	for i := 0; i < len(studentRepordCard); i++ {
		for k := 0; k < len(studentRepordCard[i].Record.LevelChanges); k++ {
			setCellValue(f, sheet, cell('A', rowNum), studentRepordCard[i].Record.LevelChanges[k].StudentName)
			mergeCell(f, sheet, cell('A', rowNum), cell('D', rowNum))
			setCellValue(f, sheet, cell('E', rowNum), studentRepordCard[i].Record.LevelChanges[k].SubjectFullName+" ("+studentRepordCard[i].Record.LevelChanges[k].Teacher+")")
			mergeCell(f, sheet, cell('E', rowNum), cell('G', rowNum))
			setCellValue(f, sheet, cell('H', rowNum), studentRepordCard[i].Record.LevelChanges[k].CourseLvl+" (Note: "+strconv.Itoa(studentRepordCard[i].Record.LevelChanges[k].Grade)+")")
			setCellValue(f, sheet, cell('I', rowNum), misc.DetermineChangedLevel(studentRepordCard[i].Record.LevelChanges[k].CourseLvl, studentRepordCard[i].Record.LevelChanges[k].LvlChange))
			if studentRepordCard[i].Record.LevelChanges[k].ParentChangeRequest {
				setCellValue(f, sheet, cell('I', rowNum), misc.DetermineChangedLevel(studentRepordCard[i].Record.LevelChanges[k].CourseLvl, "↧"))
				setCellValue(f, sheet, cell('J', rowNum), "Elternantrag")
			}
			mergeCell(f, sheet, cell('J', rowNum), cell('K', rowNum))
			switch {
			case subjectNotTakenNextYear(studentRepordCard[i].Record.LevelChanges[k].SubjectFullName, studentRepordCard[i].Record.LevelChanges[k].SubjectAbbreviation, studentRepordCard[i].ClassYear, studentRepordCard[i].Term):
				setCellStyle(f, sheet, cell('A', rowNum), cell('D', rowNum), tableleftStyleGrey)
				setCellStyle(f, sheet, cell('E', rowNum), cell('K', rowNum), tablecenterStyleGrey)
				setCellValue(f, sheet, cell('L', rowNum), "→ Fach wird im "+strconv.Itoa(studentRepordCard[i].ClassYear+1)+" Schuljahr nicht unterrichtet")
			case studentRepordCard[i].Record.LevelChanges[k].NotListed:
				var noteText string
				switch {
				case studentRepordCard[i].Record.LevelChanges[k].ListedInLUSD && !studentRepordCard[i].Record.LevelChanges[k].ListedInNonLUSD:
					noteText = "→ Umstufung aus LUSD-Daten generiert, Eintrag fehlt in der Umstufungsliste."
				case !studentRepordCard[i].Record.LevelChanges[k].ListedInLUSD && studentRepordCard[i].Record.LevelChanges[k].ListedInNonLUSD:
					noteText = "→ Umstufung kommt aus der Umstufungsliste (Eintrag wurde nicht über Umstufungsregeln aus LUSD-Daten generiert)."
				}
				setCellStyle(f, sheet, cell('A', rowNum), cell('D', rowNum), tableleftStyleMarked)
				setCellStyle(f, sheet, cell('E', rowNum), cell('K', rowNum), tablecenterStyleMarked)
				setCellValue(f, sheet, cell('L', rowNum), noteText)
			default:
				setCellStyle(f, sheet, cell('A', rowNum), cell('D', rowNum), tableleftStyle)
				setCellStyle(f, sheet, cell('E', rowNum), cell('K', rowNum), tablecenterStyle)
			}
			rowNum++
		}
	}
	setCellValue(f, sheet, cell('A', rowNum), "Markierte Zellen zeigen Unterschiede zwischen der automatisch generierten Liste und der Liste der Stufenleitung an.")
	setCellStyle(f, sheet, cell('A', rowNum), cell('A', rowNum), legend)
	rowNum++

	return chapterNum, rowNum + 2
}

func Notensprung(f *excelize.File, sheet string, studentRepordCard []global.Student, rowNum, chapterNum int) (int, int) {
	// ############## create Styles
	chapterHeaderStyle, err := f.NewStyle(&excelize.Style{
		Font: &excelize.Font{
			Bold: true,
		},
	})
	misc.CheckError("", err)

	tableHeaderStyle, err := f.NewStyle(&excelize.Style{
		Font: &excelize.Font{
			Size: 10,
			Bold: true,
		},
		Border: []excelize.Border{
			{Type: "left", Color: "000000", Style: 1},
			{Type: "top", Color: "000000", Style: 1},
			{Type: "bottom", Color: "000000", Style: 1},
			{Type: "right", Color: "000000", Style: 1},
		},
		Alignment: &excelize.Alignment{
			Horizontal: "center",
			Vertical:   "center",
		},
	})
	misc.CheckError("", err)

	tableHeaderStyleLeftCol, err := f.NewStyle(&excelize.Style{
		Font: &excelize.Font{
			Size: 10,
			Bold: true,
		},
		Border: []excelize.Border{
			{Type: "left", Color: "000000", Style: 1},
			{Type: "top", Color: "000000", Style: 1},
			{Type: "bottom", Color: "000000", Style: 1},
			{Type: "right", Color: "000000", Style: 1},
		},
		Alignment: &excelize.Alignment{
			Horizontal: "left",
			Vertical:   "center",
		},
	})
	misc.CheckError("", err)

	tablecenterStyle, err := f.NewStyle(&excelize.Style{
		Font: &excelize.Font{
			Size: 10,
		},
		Border: []excelize.Border{
			{Type: "left", Color: "000000", Style: 1},
			{Type: "top", Color: "000000", Style: 1},
			{Type: "bottom", Color: "000000", Style: 1},
			{Type: "right", Color: "000000", Style: 1},
		},
		Alignment: &excelize.Alignment{
			Horizontal: "center",
			Vertical:   "center",
		},
	})
	misc.CheckError("", err)

	tableleftStyle, err := f.NewStyle(&excelize.Style{
		Font: &excelize.Font{
			Size: 10,
		},
		Border: []excelize.Border{
			{Type: "left", Color: "000000", Style: 1},
			{Type: "top", Color: "000000", Style: 1},
			{Type: "bottom", Color: "000000", Style: 1},
			{Type: "right", Color: "000000", Style: 1},
		},
		Alignment: &excelize.Alignment{
			Vertical: "center",
			WrapText: true,
		},
	})
	misc.CheckError("", err)
	// ############## end Styles

	setCellValue(f, sheet, cell('A', rowNum), strconv.Itoa(chapterNum)+". Notensprünge:")
	setCellStyle(f, sheet, cell('A', rowNum), cell('A', rowNum), chapterHeaderStyle)
	chapterNum++
	rowNum++

	// Table Header (Notensprünge)
	setCellValue(f, sheet, cell('A', rowNum), "Schüler*In")
	mergeCell(f, sheet, cell('A', rowNum), cell('D', rowNum))
	setCellValue(f, sheet, cell('E', rowNum), "Fach (Lehrkraft)")
	mergeCell(f, sheet, cell('E', rowNum), cell('G', rowNum))
	setCellValue(f, sheet, cell('H', rowNum), "Note (Kurs)")
	setCellValue(f, sheet, cell('I', rowNum), "Begründung")
	mergeCell(f, sheet, cell('I', rowNum), cell('K', rowNum))
	setCellStyle(f, sheet, cell('A', rowNum), cell('D', rowNum), tableHeaderStyleLeftCol)
	setCellStyle(f, sheet, cell('E', rowNum), cell('K', rowNum), tableHeaderStyle)
	rowNum++

	for i := 0; i < len(studentRepordCard); i++ {
		for k := 0; k < len(studentRepordCard[i].Record.GradeJumps); k++ {
			setCellValue(f, sheet, cell('A', rowNum), studentRepordCard[i].Record.GradeJumps[k].StudentName)
			mergeCell(f, sheet, cell('A', rowNum), cell('D', rowNum))
			setCellValue(f, sheet, cell('E', rowNum), studentRepordCard[i].Record.GradeJumps[k].SubjectFullName+" ("+studentRepordCard[i].Record.GradeJumps[k].Teacher+")")
			mergeCell(f, sheet, cell('E', rowNum), cell('G', rowNum))
			if len(studentRepordCard[i].Record.GradeJumps[k].CourseLvl) > 0 {
				setCellValue(f, sheet, cell('H', rowNum), strconv.Itoa(studentRepordCard[i].Record.GradeJumps[k].Grade)+" ("+studentRepordCard[i].Record.GradeJumps[k].CourseLvl+")")
			} else {
				setCellValue(f, sheet, cell('H', rowNum), strconv.Itoa(studentRepordCard[i].Record.GradeJumps[k].Grade))
			}
			mergeCell(f, sheet, cell('I', rowNum), cell('K', rowNum))

			setCellStyle(f, sheet, cell('A', rowNum), cell('D', rowNum), tableleftStyle)
			setCellStyle(f, sheet, cell('E', rowNum), cell('K', rowNum), tablecenterStyle)
			rowNum++
		}
	}
	rowNum++
	return chapterNum, rowNum + 2
}

func writeFoerderplaene(f *excelize.File, sheet string, studentRepordCard []global.Student, rowNum, chapterNum int) (int, int) {
	// ############## create Styles
	chapterHeaderStyle, err := f.NewStyle(&excelize.Style{
		Font: &excelize.Font{
			Bold: true,
		},
	})
	misc.CheckError("", err)

	tableHeaderStyle, err := f.NewStyle(&excelize.Style{
		Font: &excelize.Font{
			Size: 10,
			Bold: true,
		},
		Border: []excelize.Border{
			{Type: "left", Color: "000000", Style: 1},
			{Type: "top", Color: "000000", Style: 1},
			{Type: "bottom", Color: "000000", Style: 1},
			{Type: "right", Color: "000000", Style: 1},
		},
		Alignment: &excelize.Alignment{
			Horizontal: "center",
			Vertical:   "center",
		},
	})
	misc.CheckError("", err)

	tablecenterStyle, err := f.NewStyle(&excelize.Style{
		Font: &excelize.Font{
			Size: 10,
		},
		Border: []excelize.Border{
			{Type: "left", Color: "000000", Style: 1},
			{Type: "top", Color: "000000", Style: 1},
			{Type: "bottom", Color: "000000", Style: 1},
			{Type: "right", Color: "000000", Style: 1},
		},
		Alignment: &excelize.Alignment{
			Horizontal: "center",
			Vertical:   "center",
		},
	})
	misc.CheckError("", err)

	tableHeaderStyleLeftCol, err := f.NewStyle(&excelize.Style{
		Font: &excelize.Font{
			Size: 10,
			Bold: true,
		},
		Border: []excelize.Border{
			{Type: "left", Color: "000000", Style: 1},
			{Type: "top", Color: "000000", Style: 1},
			{Type: "bottom", Color: "000000", Style: 1},
			{Type: "right", Color: "000000", Style: 1},
		},
		Alignment: &excelize.Alignment{
			Horizontal: "left",
			Vertical:   "center",
		},
	})
	misc.CheckError("", err)

	tableleftStyle, err := f.NewStyle(&excelize.Style{
		Font: &excelize.Font{
			Size: 10,
		},
		Border: []excelize.Border{
			{Type: "left", Color: "000000", Style: 1},
			{Type: "top", Color: "000000", Style: 1},
			{Type: "bottom", Color: "000000", Style: 1},
			{Type: "right", Color: "000000", Style: 1},
		},
		Alignment: &excelize.Alignment{
			Vertical: "center",
			WrapText: true,
		},
	})
	misc.CheckError("", err)

	tableleftStyleGrey, err := f.NewStyle(&excelize.Style{
		Font: &excelize.Font{
			Size:  10,
			Color: "cccccc",
		},
		Border: []excelize.Border{
			{Type: "left", Color: "000000", Style: 1},
			{Type: "top", Color: "000000", Style: 1},
			{Type: "bottom", Color: "000000", Style: 1},
			{Type: "right", Color: "000000", Style: 1},
		},
		Alignment: &excelize.Alignment{
			Vertical: "center",
			WrapText: true,
		},
	})
	misc.CheckError("", err)

	tablecenterStyleGrey, err := f.NewStyle(&excelize.Style{
		Font: &excelize.Font{
			Size:  10,
			Color: "cccccc",
		},
		Border: []excelize.Border{
			{Type: "left", Color: "000000", Style: 1},
			{Type: "top", Color: "000000", Style: 1},
			{Type: "bottom", Color: "000000", Style: 1},
			{Type: "right", Color: "000000", Style: 1},
		},
		Alignment: &excelize.Alignment{
			Horizontal: "center",
			Vertical:   "center",
		},
	})
	misc.CheckError("", err)
	// ############## end Styles

	setCellValue(f, sheet, cell('A', rowNum), strconv.Itoa(chapterNum)+". Förderpläne:")
	setCellStyle(f, sheet, cell('A', rowNum), cell('A', rowNum), chapterHeaderStyle)
	chapterNum++
	rowNum++

	// Table Header (Förderpläne)
	setCellValue(f, sheet, cell('A', rowNum), "Schüler*In")
	mergeCell(f, sheet, cell('A', rowNum), cell('D', rowNum))
	setCellValue(f, sheet, cell('E', rowNum), "Fach (Lehrkraft)")
	mergeCell(f, sheet, cell('E', rowNum), cell('I', rowNum))
	setCellValue(f, sheet, cell('J', rowNum), "Kurs/Note")
	mergeCell(f, sheet, cell('J', rowNum), cell('K', rowNum))
	setCellStyle(f, sheet, cell('A', rowNum), cell('D', rowNum), tableHeaderStyleLeftCol)
	setCellStyle(f, sheet, cell('E', rowNum), cell('K', rowNum), tableHeaderStyle)

	rowNum++

	for i := 0; i < len(studentRepordCard); i++ {
		for k := 0; k < len(studentRepordCard[i].Record.SupportPlan); k++ {
			setCellValue(f, sheet, cell('A', rowNum), studentRepordCard[i].Record.SupportPlan[k].StudentName)
			mergeCell(f, sheet, cell('A', rowNum), cell('D', rowNum))
			setCellValue(f, sheet, cell('E', rowNum), studentRepordCard[i].Record.SupportPlan[k].SubjectFullName+" ("+studentRepordCard[i].Record.SupportPlan[k].Teacher+")")
			mergeCell(f, sheet, cell('E', rowNum), cell('I', rowNum))
			setCellValue(f, sheet, cell('J', rowNum), studentRepordCard[i].Record.SupportPlan[k].CourseLvlGrade)
			mergeCell(f, sheet, cell('J', rowNum), cell('K', rowNum))
			if subjectNotTakenNextYear(studentRepordCard[i].Record.SupportPlan[k].SubjectFullName, studentRepordCard[i].Record.SupportPlan[k].SubjectAbbreviation, studentRepordCard[i].ClassYear, studentRepordCard[i].Term) {
				setCellStyle(f, sheet, cell('A', rowNum), cell('D', rowNum), tableleftStyleGrey)
				setCellStyle(f, sheet, cell('E', rowNum), cell('K', rowNum), tablecenterStyleGrey)
				setCellValue(f, sheet, cell('L', rowNum), "→ Fach wird im "+strconv.Itoa(studentRepordCard[i].ClassYear+1)+" Schuljahr nicht unterrichtet")
			} else {
				setCellStyle(f, sheet, cell('A', rowNum), cell('D', rowNum), tableleftStyle)
				setCellStyle(f, sheet, cell('E', rowNum), cell('K', rowNum), tablecenterStyle)
			}

			rowNum++
		}
	}

	return chapterNum, rowNum + 2
}

func subjectNotTakenNextYear(subjectFullName, subjectAbbreviation string, schoolyear int, term int) bool {
	var ignoreyear int
	cfg := config.GetConfig()
	subjectsFallOut := cfg.IgnorelvlChange // TODO: convert already in config package

	ignoreSubs := strings.Split(subjectsFallOut, ";")
	for i := 0; i < len(ignoreSubs); i++ {
		ignoreSubInYear := strings.Split(ignoreSubs[i], ",")
		if len(ignoreSubInYear) == 2 {
			ignoreyear, _ = strconv.Atoi(strings.TrimSpace(ignoreSubInYear[0]))
			ignoreSub := strings.TrimSpace(ignoreSubInYear[1])

			if term == 2 && schoolyear == ignoreyear && (ignoreSub == subjectFullName || ignoreSub == subjectAbbreviation) {
				return true
			}
		} else {
			fmt.Printf(color.InYellow("    * ")+"Warning: invalid entry in yaml-config file: exceptions - ignorelvlChange: %s\n", ignoreSubs[i])
			log.Print("    * Warning: invalid entry in yaml-config file: exceptions - ignorelvlChange: " + ignoreSubs[i] + "\n")
		}
	}
	return false
}

func writeFoerderschwerpunktLERESE(f *excelize.File, sheet string, rowNum, chapterNum int) (int, int) {
	// ############## create Styles
	chapterHeaderStyle, err := f.NewStyle(&excelize.Style{
		Font: &excelize.Font{
			Bold: true,
		},
	})
	misc.CheckError("", err)

	tableHeaderStyle, err := f.NewStyle(&excelize.Style{
		Font: &excelize.Font{
			Size: 10,
			Bold: true,
		},
		Border: []excelize.Border{
			{Type: "left", Color: "000000", Style: 1},
			{Type: "top", Color: "000000", Style: 1},
			{Type: "bottom", Color: "000000", Style: 1},
			{Type: "right", Color: "000000", Style: 1},
		},
		Alignment: &excelize.Alignment{
			Horizontal: "left",
			Vertical:   "center",
		},
	})
	misc.CheckError("", err)

	tableHeaderStyleLeftCol, err := f.NewStyle(&excelize.Style{
		Font: &excelize.Font{
			Size: 10,
			Bold: true,
		},
		Border: []excelize.Border{
			{Type: "left", Color: "000000", Style: 1},
			{Type: "top", Color: "000000", Style: 1},
			{Type: "bottom", Color: "000000", Style: 1},
			{Type: "right", Color: "000000", Style: 1},
		},
		Alignment: &excelize.Alignment{
			Horizontal: "left",
			Vertical:   "center",
			WrapText:   true,
		},
	})
	misc.CheckError("", err)

	textboxstyle, err := f.NewStyle(&excelize.Style{
		Font: &excelize.Font{
			Size: 10,
		},
		Border: []excelize.Border{
			{Type: "left", Color: "000000", Style: 1},
			{Type: "top", Color: "000000", Style: 1},
			{Type: "bottom", Color: "000000", Style: 1},
			{Type: "right", Color: "000000", Style: 1},
		},
		Alignment: &excelize.Alignment{
			Vertical: "top",
			WrapText: true,
		},
	})
	misc.CheckError("", err)
	// ############## end Styles

	setCellValue(f, sheet, cell('A', rowNum), strconv.Itoa(chapterNum)+". Schüler mit Förderschwerpunkt LER, ESE:")
	setCellStyle(f, sheet, cell('A', rowNum), cell('A', rowNum), chapterHeaderStyle)
	chapterNum++
	rowNum++
	setCellValue(f, sheet, cell('A', rowNum), "Schüler*In")
	mergeCell(f, sheet, cell('A', rowNum), cell('C', rowNum))
	setCellStyle(f, sheet, cell('A', rowNum), cell('C', rowNum), tableHeaderStyleLeftCol)
	setCellValue(f, sheet, cell('D', rowNum), "Bemerkung")
	mergeCell(f, sheet, cell('D', rowNum), cell('K', rowNum))
	setCellStyle(f, sheet, cell('A', rowNum), cell('K', rowNum), tableHeaderStyle)
	rowNum++
	for i := 0; i < 6; i++ {
		mergeCell(f, sheet, cell('A', rowNum), cell('C', rowNum))
		mergeCell(f, sheet, cell('D', rowNum), cell('K', rowNum))
		setCellStyle(f, sheet, cell('A', rowNum), cell('K', rowNum), textboxstyle)
		rowNum++
	}
	return chapterNum, rowNum + 2
}

func writeZeugnisbemerkungen(f *excelize.File, sheet string, rowNum, chapterNum int) (int, int) {
	// ############## create Styles
	chapterHeaderStyle, err := f.NewStyle(&excelize.Style{
		Font: &excelize.Font{
			Bold: true,
		},
	})
	misc.CheckError("", err)

	tableHeaderStyleLeftCol, err := f.NewStyle(&excelize.Style{
		Font: &excelize.Font{
			Size: 10,
			Bold: true,
		},
		Border: []excelize.Border{
			{Type: "left", Color: "000000", Style: 1},
			{Type: "top", Color: "000000", Style: 1},
			{Type: "bottom", Color: "000000", Style: 1},
			{Type: "right", Color: "000000", Style: 1},
		},
		Alignment: &excelize.Alignment{
			Horizontal: "left",
			Vertical:   "center",
		},
	})
	misc.CheckError("", err)

	tablecenterStyle, err := f.NewStyle(&excelize.Style{
		Font: &excelize.Font{
			Size: 10,
		},
		Border: []excelize.Border{
			{Type: "left", Color: "000000", Style: 1},
			{Type: "top", Color: "000000", Style: 1},
			{Type: "bottom", Color: "000000", Style: 1},
			{Type: "right", Color: "000000", Style: 1},
		},
		Alignment: &excelize.Alignment{
			Horizontal: "center",
			Vertical:   "center",
		},
	})
	misc.CheckError("", err)

	tableleftStyle, err := f.NewStyle(&excelize.Style{
		Font: &excelize.Font{
			Size: 10,
		},
		Border: []excelize.Border{
			{Type: "left", Color: "000000", Style: 1},
			{Type: "top", Color: "000000", Style: 1},
			{Type: "bottom", Color: "000000", Style: 1},
			{Type: "right", Color: "000000", Style: 1},
		},
		Alignment: &excelize.Alignment{
			Vertical: "center",
			WrapText: true,
		},
	})
	misc.CheckError("", err)

	textboxSmallHeaderstyle, err := f.NewStyle(&excelize.Style{
		Font: &excelize.Font{
			Size: 7,
			Bold: true,
		},
		Border: []excelize.Border{
			{Type: "left", Color: "000000", Style: 1},
			{Type: "top", Color: "000000", Style: 1},
			{Type: "bottom", Color: "000000", Style: 1},
			{Type: "right", Color: "000000", Style: 1},
		},
		Alignment: &excelize.Alignment{
			Horizontal: "center",
			Vertical:   "center",
		},
	})
	misc.CheckError("", err)
	// ############## create Styles

	setCellValue(f, sheet, cell('A', rowNum), strconv.Itoa(chapterNum)+". Zeugnisbemerkungen:")
	setCellStyle(f, sheet, cell('A', rowNum), cell('A', rowNum), chapterHeaderStyle)
	chapterNum++
	rowNum++
	setCellValue(f, sheet, cell('A', rowNum), "Schüler*In")
	mergeCell(f, sheet, cell('A', rowNum), cell('C', rowNum+2))
	setCellStyle(f, sheet, cell('A', rowNum), cell('C', rowNum+2), tableHeaderStyleLeftCol)
	setCellValue(f, sheet, cell('D', rowNum), "Noten-\nschutz\nbei LRS")
	mergeCell(f, sheet, cell('D', rowNum), cell('D', rowNum+2))
	setCellValue(f, sheet, cell('E', rowNum), "Individuelle\nLeistungs-\nbewertung\nsprachliche\nGründe")
	mergeCell(f, sheet, cell('E', rowNum), cell('E', rowNum+2))
	setCellValue(f, sheet, cell('F', rowNum), "Individuelle\nLeistungs-\nbewertung\nsonstige\nGründe")
	mergeCell(f, sheet, cell('F', rowNum), cell('G', rowNum+2))
	setCellValue(f, sheet, cell('H', rowNum), "Sport-\nunterricht\nohne\nBewertung")
	mergeCell(f, sheet, cell('H', rowNum), cell('H', rowNum+2))
	setCellValue(f, sheet, cell('I', rowNum), "Sonstiges")
	mergeCell(f, sheet, cell('I', rowNum), cell('K', rowNum+2))
	setCellStyle(f, sheet, cell('D', rowNum), cell('K', rowNum+2), textboxSmallHeaderstyle)
	rowNum += 3
	for i := 0; i < 10; i++ {
		mergeCell(f, sheet, cell('A', rowNum), cell('C', rowNum))
		mergeCell(f, sheet, cell('F', rowNum), cell('G', rowNum))
		mergeCell(f, sheet, cell('I', rowNum), cell('K', rowNum))
		setCellStyle(f, sheet, cell('A', rowNum), cell('K', rowNum), tableleftStyle)
		setCellStyle(f, sheet, cell('D', rowNum), cell('H', rowNum), tablecenterStyle)

		rowNum++
	}

	// Klassensprecher*In
	rowNum++
	setCellValue(f, sheet, cell('A', rowNum), "Klassensprecher*In:")
	setCellStyle(f, sheet, cell('A', rowNum), cell('A', rowNum), chapterHeaderStyle)
	rowNum++
	setCellValue(f, sheet, cell('A', rowNum), "Klassensprecher*In:")
	mergeCell(f, sheet, cell('A', rowNum), cell('C', rowNum))
	mergeCell(f, sheet, cell('D', rowNum), cell('K', rowNum))
	setCellStyle(f, sheet, cell('A', rowNum), cell('K', rowNum), tableleftStyle)
	rowNum++
	setCellValue(f, sheet, cell('A', rowNum), "stellv. Klassensprecher*In:")
	mergeCell(f, sheet, cell('A', rowNum), cell('C', rowNum))
	mergeCell(f, sheet, cell('D', rowNum), cell('K', rowNum))
	setCellStyle(f, sheet, cell('A', rowNum), cell('K', rowNum), tableleftStyle)

	return chapterNum, rowNum + 3

}

func writeFehlzeiten(f *excelize.File, sheet string, rowNum, chapterNum int) (int, int) {
	// ############## create Styles
	chapterHeaderStyle, err := f.NewStyle(&excelize.Style{
		Font: &excelize.Font{
			Bold: true,
		},
	})
	misc.CheckError("", err)

	tableHeaderStyleLeftCol, err := f.NewStyle(&excelize.Style{
		Font: &excelize.Font{
			Size: 10,
			Bold: true,
		},
		Border: []excelize.Border{
			{Type: "left", Color: "000000", Style: 1},
			{Type: "top", Color: "000000", Style: 1},
			{Type: "bottom", Color: "000000", Style: 1},
			{Type: "right", Color: "000000", Style: 1},
		},
		Alignment: &excelize.Alignment{
			Horizontal: "left",
			Vertical:   "center",
		},
	})
	misc.CheckError("", err)

	tablecenterStyle, err := f.NewStyle(&excelize.Style{
		Font: &excelize.Font{
			Size: 10,
		},
		Border: []excelize.Border{
			{Type: "left", Color: "000000", Style: 1},
			{Type: "top", Color: "000000", Style: 1},
			{Type: "bottom", Color: "000000", Style: 1},
			{Type: "right", Color: "000000", Style: 1},
		},
		Alignment: &excelize.Alignment{
			Horizontal: "center",
			Vertical:   "center",
		},
	})
	misc.CheckError("", err)

	tableleftStyle, err := f.NewStyle(&excelize.Style{
		Font: &excelize.Font{
			Size: 10,
		},
		Border: []excelize.Border{
			{Type: "left", Color: "000000", Style: 1},
			{Type: "top", Color: "000000", Style: 1},
			{Type: "bottom", Color: "000000", Style: 1},
			{Type: "right", Color: "000000", Style: 1},
		},
		Alignment: &excelize.Alignment{
			Vertical: "center",
			WrapText: true,
		},
	})
	misc.CheckError("", err)

	textboxSmallHeaderstyle, err := f.NewStyle(&excelize.Style{
		Font: &excelize.Font{
			Size: 7,
			Bold: true,
		},
		Border: []excelize.Border{
			{Type: "left", Color: "000000", Style: 1},
			{Type: "top", Color: "000000", Style: 1},
			{Type: "bottom", Color: "000000", Style: 1},
			{Type: "right", Color: "000000", Style: 1},
		},
		Alignment: &excelize.Alignment{
			Horizontal: "center",
			Vertical:   "center",
		},
	})
	misc.CheckError("", err)
	// ############## create Styles

	setCellValue(f, sheet, cell('A', rowNum), strconv.Itoa(chapterNum)+". Fehlzeiten")
	setCellStyle(f, sheet, cell('A', rowNum), cell('A', rowNum), chapterHeaderStyle)
	chapterNum++
	rowNum++
	setCellValue(f, sheet, cell('A', rowNum), "Schüler*In")
	mergeCell(f, sheet, cell('A', rowNum), cell('C', rowNum+2))
	setCellStyle(f, sheet, cell('A', rowNum), cell('C', rowNum+2), tableHeaderStyleLeftCol)
	setCellValue(f, sheet, cell('D', rowNum), "Androhung\nAttest-\npflicht")
	mergeCell(f, sheet, cell('D', rowNum), cell('D', rowNum+2))
	setCellValue(f, sheet, cell('E', rowNum), "Attestpflicht")
	mergeCell(f, sheet, cell('E', rowNum), cell('G', rowNum+1))
	setCellValue(f, sheet, cell('E', rowNum+2), "Pro")
	setCellValue(f, sheet, cell('F', rowNum+2), "Kontra")
	mergeCell(f, sheet, cell('F', rowNum+2), cell('G', rowNum+2))
	setCellStyle(f, sheet, cell('E', rowNum+2), cell('G', rowNum+2), textboxSmallHeaderstyle)
	setCellValue(f, sheet, cell('H', rowNum), "Schul-\npsychologe\nein-\ngeschaltet")
	mergeCell(f, sheet, cell('H', rowNum), cell('H', rowNum+2))
	setCellValue(f, sheet, cell('I', rowNum), "Bußgeld-\nbescheid\nwird\nerwirkt")
	mergeCell(f, sheet, cell('I', rowNum), cell('I', rowNum+2))
	setCellValue(f, sheet, cell('J', rowNum), "Sonstiges")
	mergeCell(f, sheet, cell('J', rowNum), cell('K', rowNum+2))
	setCellStyle(f, sheet, cell('D', rowNum), cell('K', rowNum+2), textboxSmallHeaderstyle)
	rowNum += 3
	for i := 0; i < 10; i++ {
		mergeCell(f, sheet, cell('A', rowNum), cell('C', rowNum))
		mergeCell(f, sheet, cell('F', rowNum), cell('G', rowNum))
		mergeCell(f, sheet, cell('J', rowNum), cell('K', rowNum))
		setCellStyle(f, sheet, cell('A', rowNum), cell('K', rowNum), tableleftStyle)
		setCellStyle(f, sheet, cell('D', rowNum), cell('I', rowNum), tablecenterStyle)

		rowNum++
	}

	return chapterNum, rowNum + 2
}

func writeSonstigeErklaerungenBeschluesse(f *excelize.File, sheet string, rowNum, chapterNum int) (int, int) {
	// ############## create Styles
	chapterHeaderStyle, err := f.NewStyle(&excelize.Style{
		Font: &excelize.Font{
			Bold: true,
		},
	})
	misc.CheckError("", err)

	textboxstyle, err := f.NewStyle(&excelize.Style{
		Font: &excelize.Font{
			Size: 10,
		},
		Border: []excelize.Border{
			{Type: "left", Color: "000000", Style: 1},
			{Type: "top", Color: "000000", Style: 1},
			{Type: "bottom", Color: "000000", Style: 1},
			{Type: "right", Color: "000000", Style: 1},
		},
		Alignment: &excelize.Alignment{
			Vertical: "top",
			WrapText: true,
		},
	})

	misc.CheckError("", err)
	// ############## create Styles

	setCellValue(f, sheet, cell('A', rowNum), strconv.Itoa(chapterNum)+". Sonstige Erklärungen oder Beschlüsse der Klassenkonferenz")
	setCellStyle(f, sheet, cell('A', rowNum), cell('A', rowNum), chapterHeaderStyle)
	chapterNum++
	rowNum++
	setCellValue(f, sheet, cell('A', rowNum), "AV/SV-Noten nach Liste besprochen (Siehe Anhang)\n")
	setCellStyle(f, sheet, cell('A', rowNum), cell('K', rowNum+2), textboxstyle)
	mergeCell(f, sheet, cell('A', rowNum), cell('K', rowNum+2))
	insertLineBreakTip(f, sheet, 'M', rowNum)

	return chapterNum, rowNum + 6
}

func writeSignLines(f *excelize.File, sheet string, rowNum int) int {
	// ############## create Styles
	signLineStyle, err := f.NewStyle(&excelize.Style{
		Font: &excelize.Font{
			Size: 10,
		},
		Border: []excelize.Border{
			{Type: "top", Color: "000000", Style: 1},
		},
		Alignment: &excelize.Alignment{
			Vertical: "top",
		},
	})
	misc.CheckError("", err)
	// ############## end Styles

	// Unterschriften
	setCellValue(f, sheet, cell('A', rowNum), "Protokollführer/in Druckbuchstaben")
	mergeCell(f, sheet, cell('A', rowNum), cell('E', rowNum))
	setCellStyle(f, sheet, cell('A', rowNum), cell('E', rowNum), signLineStyle)
	setCellValue(f, sheet, cell('G', rowNum), "Datum, Unterschrift")
	mergeCell(f, sheet, cell('G', rowNum), cell('K', rowNum))
	setCellStyle(f, sheet, cell('G', rowNum), cell('K', rowNum), signLineStyle)

	rowNum += 3
	setCellValue(f, sheet, cell('A', rowNum), "Datum, Unterschrift Klassenlehrkraft")
	mergeCell(f, sheet, cell('A', rowNum), cell('E', rowNum))
	setCellStyle(f, sheet, cell('A', rowNum), cell('E', rowNum), signLineStyle)

	return rowNum
}
