package exportdata

import (
	calc "igs-abschlussrechner/calcTendencies"
	config "igs-abschlussrechner/config"
	"igs-abschlussrechner/global"
	misc "igs-abschlussrechner/miscellaneous"
	"regexp"
	"strconv"
	"strings"

	"github.com/xuri/excelize/v2"
)

const gradeJumpSymbol = "🦘"       // ⤼
const gradeHAIncreaseSymbol = "^" // ˆ
const examSubjectSymbol = "*"

func insertStudentSheets(f *excelize.File, studentRepordCard []global.Student) {
	cfg := config.GetConfig()

	// create a sheet per student in studentreportCard
	for i := 0; i < len(studentRepordCard); i++ {
		sheetName := createSheetName(studentRepordCard[i].Name)

		_, err := f.NewSheet(sheetName)
		misc.CheckError("Failed to create new sheet, insertStudentSheets.go", err)

		// set worksheet defaults
		setSheetViewOptionsGridLines(f, sheetName, 0, false)
		setSheetViewOptionsZoomscale(f, sheetName, 0, cfg.ZoomScale)
		setSheetFormatPrDefaultRowHeight(f, sheetName, 14.166666667)
		formatCellWithForStudentSheet(f, sheetName)

		// set page layout
		setSheetPrOptionsFitToPage(f, sheetName, true)
		var (
			fitToHeight = 1
			fitToWidth  = 1
		)
		err = f.SetPageLayout(sheetName, &excelize.PageLayoutOptions{
			FitToHeight: &fitToHeight,
			FitToWidth:  &fitToWidth,
		})
		misc.CheckError("Failed to set page layout, insertStudentSheets.go", err)

		// import data
		currentrow := writeHeaderInformation(studentRepordCard[i], f, sheetName)
		currentrow = writeSubjectInformation(studentRepordCard[i], f, sheetName, currentrow+1)
		currentrow = writeTendencyInformation(studentRepordCard[i], f, sheetName, currentrow+1)

		setCellValue(f, sheetName, cell('I', 1), "(↦ ab hier nicht im Druckbereich)")
		writeLog(studentRepordCard[i], f, sheetName, 1, 'M')

		// specify print area for worksheet
		if cfg.AutoPrintArea {
			err := f.SetDefinedName(&excelize.DefinedName{
				Name:     "_xlnm.Print_Area",
				RefersTo: sheetName + "!" + "$A$1:$G$" + strconv.Itoa(currentrow),
				Scope:    sheetName,
			})
			misc.CheckError("Failed to set print area, ExportReportCards2Excel.go", err)

		} else {
			err := f.SetDefinedName(&excelize.DefinedName{
				Name:     "_xlnm.Print_Area",
				RefersTo: sheetName + "!" + cfg.PrintArea,
				Scope:    sheetName,
			})
			misc.CheckError("Failed to set print area, ExportReportCards2Excel.go", err)
		}
	}
}

func formatCellWithForStudentSheet(f *excelize.File, sheet string) {
	setColWith(f, sheet, "A", "A", 4.47)
	setColWith(f, sheet, "B", "B", 3.00)
	setColWith(f, sheet, "C", "C", 2.23)
	setColWith(f, sheet, "D", "D", 1.33)
	setColWith(f, sheet, "E", "E", 1.58)
	setColWith(f, sheet, "F", "F", 2.37)
	setColWith(f, sheet, "G", "G", 2.21)
	setColWith(f, sheet, "H", "L", 0.5)
	setColWith(f, sheet, "I", "K", 1.00)
}

func createSheetName(studentNameString string) string {
	// prepare sheet-name variable
	studentName := strings.Replace(studentNameString, ", ", "", 1)
	studentNameNoSpaces := strings.ReplaceAll(studentName, " ", "")
	studentNameNoDashes := strings.ReplaceAll(studentNameNoSpaces, "-", "")
	// clean name from any non-Word characters
	reObject := regexp.MustCompile(`\(`)

	return firstN(reObject.Split(studentNameNoDashes, -1)[0], 31) // cut string, if longer than 31 characters, because of the 31 character limit for sheet names
}

// function returns first n chatachters of a string
func firstN(str string, n int) string {
	i := 0
	for j := range str {
		if i == n {
			return str[:j]
		}
		i++
	}
	return str
}

// function writes header information to excel file (starting at given startrow number) and returns last written row number
func writeHeaderInformation(reportCard global.Student, f *excelize.File, sheet string) int {
	cfg := config.GetConfig()

	// ############## create Styles
	titleStyle, err := f.NewStyle(&excelize.Style{
		Font: &excelize.Font{
			Size: 14,
			Bold: true,
		},
		Alignment: &excelize.Alignment{
			Horizontal: "center",
		},
	})
	misc.CheckError("", err)

	nameCell, err := f.NewStyle(&excelize.Style{
		Font: &excelize.Font{
			Size: 11,
			Bold: true,
		},
		Alignment: &excelize.Alignment{
			Horizontal: "left",
		},
	})
	misc.CheckError("", err)

	headerSpecifier, err := f.NewStyle(&excelize.Style{
		Font: &excelize.Font{
			Size: 11,
		},
		Alignment: &excelize.Alignment{
			Horizontal: "right",
		},
	})
	misc.CheckError("", err)

	headerSpecifierBold, err := f.NewStyle(&excelize.Style{
		Font: &excelize.Font{
			Size: 11,
			Bold: true,
		},
		Alignment: &excelize.Alignment{
			Horizontal: "right",
		},
	})
	misc.CheckError("", err)

	headerContent, err := f.NewStyle(&excelize.Style{
		Font: &excelize.Font{
			Size: 11,
		},
		Alignment: &excelize.Alignment{
			Horizontal: "left",
		},
	})
	misc.CheckError("", err)

	foerderMassnahmenHeader, err := f.NewStyle(&excelize.Style{
		Font: &excelize.Font{
			Size: 11,
			Bold: true,
		},
	})
	misc.CheckError("", err)

	foerderMassnahmenTable, err := f.NewStyle(&excelize.Style{
		Font: &excelize.Font{
			Size: 11,
		},
		Border: []excelize.Border{
			{Type: "left", Color: "000000", Style: 1},
			{Type: "top", Color: "000000", Style: 1},
			{Type: "bottom", Color: "000000", Style: 1},
			{Type: "right", Color: "000000", Style: 1},
		},
		Alignment: &excelize.Alignment{
			Horizontal: "center",
		},
	})
	misc.CheckError("", err)

	headerHighlightedRed, err := f.NewStyle(&excelize.Style{
		Font: &excelize.Font{
			Size:  11,
			Color: "#c9211e"},
	})
	misc.CheckError("", err)

	legendStyle, err := f.NewStyle(&excelize.Style{
		Font: &excelize.Font{
			Size: 7.8,
		},
		Alignment: &excelize.Alignment{
			Vertical: "top",
		},
	})
	misc.CheckError("", err)

	naValue, err := f.NewStyle(&excelize.Style{
		Font: &excelize.Font{
			Size:  11,
			Color: "#b3b3b3",
		},
		Alignment: &excelize.Alignment{
			Horizontal: "left",
		},
	})
	misc.CheckError("", err)
	// ############## create Styles

	rowNum := 1
	setCellValue(f, sheet, cell('A', rowNum), "Tendenzermittlung Schulabschluss")
	mergeCell(f, sheet, "A1", "F1")
	setCellStyle(f, sheet, cell('A', rowNum), cell('A', rowNum), titleStyle)
	setCellValue(f, sheet, cell('G', rowNum), reportCard.DateOfFile)

	rowNum++
	rowNum++

	// Name
	setCellValue(f, sheet, cell('A', rowNum), "Name:  ")
	setCellStyle(f, sheet, cell('A', rowNum), cell('A', rowNum), headerSpecifier)
	setCellValue(f, sheet, cell('B', rowNum), reportCard.Name)
	setCellStyle(f, sheet, cell('B', rowNum), cell('B', rowNum), nameCell)

	// Stand
	setCellValue(f, sheet, cell('E', rowNum), "Stand:  ")
	setCellStyle(f, sheet, cell('E', rowNum), cell('E', rowNum), headerSpecifier)
	setCellValue(f, sheet, cell('F', rowNum), strconv.Itoa(reportCard.Term)+". Hj; "+reportCard.YearsOfData)
	setCellStyle(f, sheet, cell('F', rowNum), cell('F', rowNum), headerContent)
	rowNum++

	// Klasse
	setCellValue(f, sheet, cell('A', rowNum), "Klasse:  ")
	setCellStyle(f, sheet, cell('A', rowNum), cell('A', rowNum), headerSpecifier)
	setCellValue(f, sheet, cell('B', rowNum), reportCard.Class)
	setCellStyle(f, sheet, cell('B', rowNum), cell('B', rowNum), nameCell)

	// Klassenlehrkraft
	setCellValue(f, sheet, cell('E', rowNum), "Klassenlehrkraft:  ")
	setCellStyle(f, sheet, cell('E', rowNum), cell('E', rowNum), headerSpecifier)
	setCellValue(f, sheet, cell('F', rowNum), reportCard.ClassTeacher)
	setCellStyle(f, sheet, cell('F', rowNum), cell('F', rowNum), headerContent)
	rowNum++

	// Schulbesuchsjahre
	setCellValue(f, sheet, cell('A', rowNum), "Schulbesuchsjahre:  ")
	setCellStyle(f, sheet, cell('A', rowNum), cell('A', rowNum), headerSpecifier)
	setCellValue(f, sheet, cell('B', rowNum), replaceM1byNA(reportCard.History.SchoolAttendanceYears))
	if checkForMissingValue(reportCard.History.SchoolAttendanceYears) {
		setCellStyle(f, sheet, cell('B', rowNum), cell('B', rowNum), naValue)
	} else {
		setCellStyle(f, sheet, cell('B', rowNum), cell('B', rowNum), headerContent)
	}

	// Vertretung
	setCellValue(f, sheet, cell('E', rowNum), "Vertretung:  ")
	setCellStyle(f, sheet, cell('E', rowNum), cell('E', rowNum), headerSpecifier)
	setCellValue(f, sheet, cell('F', rowNum), reportCard.SubstitudeTeacher)
	setCellStyle(f, sheet, cell('F', rowNum), cell('F', rowNum), headerContent)
	rowNum++

	// Wiederholungen
	setCellValue(f, sheet, cell('A', rowNum), "Wiederholungen:  ")
	setCellStyle(f, sheet, cell('A', rowNum), cell('A', rowNum), headerSpecifier)
	setCellValue(f, sheet, cell('B', rowNum), replaceM1byNA(reportCard.History.RepeatsTotal))
	if checkForMissingValue(reportCard.History.SchoolAttendanceYears) {
		setCellStyle(f, sheet, cell('B', rowNum), cell('B', rowNum), naValue)
	} else {
		setCellStyle(f, sheet, cell('B', rowNum), cell('B', rowNum), headerContent)
	}

	// print which years where repeated
	var repeatedYears []string
	for i := 0; i < len(reportCard.History.RepeatedGrade); i++ {
		if reportCard.History.RepeatedGrade[i].RepeatedGrade == 0 {
			setCellValue(f, sheet, cell('C', rowNum), "NDHS - Schulbesuchsjahre in Akte prüfen!")
			setCellStyle(f, sheet, cell('C', rowNum), cell('C', rowNum), headerHighlightedRed)

			setCellValue(f, sheet, cell('C', rowNum+1), "(NDHS: Schüler*in nicht deutscher Herkunftssprache)")
			setCellStyle(f, sheet, cell('C', rowNum+1), cell('A', rowNum+1), legendStyle)
		} else {
			repeatedYears = append(repeatedYears, strconv.Itoa(reportCard.History.RepeatedGrade[i].RepeatedGrade)+"/"+strconv.Itoa(reportCard.History.RepeatedGrade[i].RepeatedTerm))
		}
	}

	if len(repeatedYears) > 0 {
		setCellValue(f, sheet, cell('C', rowNum), "(wiederholt: "+strings.Join(repeatedYears, ", ")+")")
	}

	if cfg.IncludeFoerdermassnahmenHeader {
		rowNum++
		rowNum++
		// Fördermaßnahmen
		setCellValue(f, sheet, cell('A', rowNum), "Fördermaßnahmen:")
		setCellStyle(f, sheet, cell('A', rowNum), cell('A', rowNum), foerderMassnahmenHeader)
		rowNum++
		setCellValue(f, sheet, cell('A', rowNum), "LRS")
		setCellValue(f, sheet, cell('B', rowNum), "Dyskalkulie")
		setCellValue(f, sheet, cell('C', rowNum), "NTA")
		setCellValue(f, sheet, cell('D', rowNum), "NS")
		if reportCard.ReportCardInfo.Notenschutz {
			setCellValue(f, sheet, cell('D', rowNum+1), "x")
		}
		setCellValue(f, sheet, cell('E', rowNum), "DaZ")
		setCellValue(f, sheet, cell('F', rowNum), "Sonstiges")
		setCellStyle(f, sheet, cell('A', rowNum), cell('F', rowNum+1), foerderMassnahmenTable)
		rowNum++
		rowNum++

		// Legend
		setCellValue(f, sheet, cell('A', rowNum), "LRS = Lese- und Rechtschreibstörung; DaZ = Deutsch als Zweitsprache; NTA = Nachteilsausgleich; NS = Notenschutz")
		setCellStyle(f, sheet, cell('A', rowNum), cell('A', rowNum), legendStyle)
	}

	rowNum++
	rowNum++

	// Arbeitsverhalten
	setCellValue(f, sheet, cell('A', rowNum), "Arbeitsverhalten:  ")
	setCellStyle(f, sheet, cell('A', rowNum), cell('A', rowNum), headerSpecifierBold)
	setCellValue(f, sheet, cell('B', rowNum), reportCard.ReportCardInfo.AV)
	if reportCard.ReportCardInfo.AV == "NA" {
		setCellStyle(f, sheet, cell('B', rowNum), cell('B', rowNum), naValue)
	} else {
		setCellStyle(f, sheet, cell('B', rowNum), cell('B', rowNum), headerContent)
	}

	// Fehlzeiten
	setCellValue(f, sheet, cell('E', rowNum), "Fehlzeiten (e/ue):  ")
	setCellStyle(f, sheet, cell('E', rowNum), cell('E', rowNum), headerSpecifierBold)
	setCellValue(f, sheet, cell('F', rowNum), replaceM1byNA(reportCard.ReportCardInfo.AbsentsTotal.Absents)+" / "+replaceM1byNA(reportCard.ReportCardInfo.AbsentsTotal.UnexcusedAbsents))
	if checkForMissingValue(reportCard.ReportCardInfo.AbsentsTotal.Absents) {
		setCellStyle(f, sheet, cell('F', rowNum), cell('F', rowNum), naValue)
	} else {
		setCellStyle(f, sheet, cell('F', rowNum), cell('F', rowNum), headerContent)
	}
	rowNum++

	// Sozialverhalten
	setCellValue(f, sheet, cell('A', rowNum), "Sozialverhalten:  ")
	setCellStyle(f, sheet, cell('A', rowNum), cell('A', rowNum), headerSpecifierBold)
	setCellValue(f, sheet, cell('B', rowNum), reportCard.ReportCardInfo.SV)
	if reportCard.ReportCardInfo.SV == "NA" {
		setCellStyle(f, sheet, cell('B', rowNum), cell('B', rowNum), naValue)
	} else {
		setCellStyle(f, sheet, cell('B', rowNum), cell('B', rowNum), headerContent)
	}

	// Fehltage
	if !checkForMissingValue(reportCard.ReportCardInfo.AbsentsTotal.AbsentDays) {
		setCellValue(f, sheet, cell('E', rowNum), "Fehltage (e/ue):  ")
		setCellStyle(f, sheet, cell('E', rowNum), cell('E', rowNum), headerSpecifierBold)
		setCellValue(f, sheet, cell('F', rowNum), replaceM1byNA(reportCard.ReportCardInfo.AbsentsTotal.AbsentDays)+" / "+replaceM1byNA(reportCard.ReportCardInfo.AbsentsTotal.UnexcusedDays))
	}

	rowNum++
	return rowNum
}

// function writes all subjects to excel file (starting at given startrow number) and returns last written row number
func writeSubjectInformation(reportCard global.Student, f *excelize.File, sheet string, rowNum int) int {
	cfg := config.GetConfig()

	// ############## create styles for subject table
	tableTitleRow, err := f.NewStyle(&excelize.Style{
		Font: &excelize.Font{
			Size: 10,
			Bold: true,
		},
		Border: []excelize.Border{
			{Type: "top", Color: "000000", Style: 1},
			{Type: "bottom", Color: "000000", Style: 1},
		},
		Alignment: &excelize.Alignment{
			Horizontal: "center",
			Vertical:   "center",
		},
	})
	misc.CheckError("", err)

	tableFirstColumnTitleRow, err := f.NewStyle(&excelize.Style{
		Font: &excelize.Font{
			Size: 10,
			Bold: true,
		},
		Border: []excelize.Border{
			{Type: "top", Color: "000000", Style: 1},
			{Type: "bottom", Color: "000000", Style: 1},
		},
		Alignment: &excelize.Alignment{
			Horizontal: "left",
			Vertical:   "center",
		},
	})
	misc.CheckError("", err)

	//
	tableFirstColumnBetweentitleRow, err := f.NewStyle(&excelize.Style{
		Font: &excelize.Font{
			Size: 11,
		},
		Border: []excelize.Border{
			{Type: "top", Color: "000000", Style: 1},
			{Type: "bottom", Color: "000000", Style: 1},
		},
		Alignment: &excelize.Alignment{
			Horizontal: "left",
			Vertical:   "center",
		},
	})
	misc.CheckError("", err)

	tableFirstColumnBetweentitleRowInfo, err := f.NewStyle(&excelize.Style{
		Font: &excelize.Font{
			Size: 6,
		},
		Alignment: &excelize.Alignment{
			Horizontal: "left",
			Vertical:   "center",
		},
	})
	misc.CheckError("", err)

	tableFirstColumnRow, err := f.NewStyle(&excelize.Style{
		Font: &excelize.Font{
			Size: 11,
		},
		Alignment: &excelize.Alignment{
			Horizontal: "left",
			Vertical:   "center",
		},
	})
	misc.CheckError("", err)

	tableRows, err := f.NewStyle(&excelize.Style{
		Font: &excelize.Font{
			Size: 11,
		},
		Alignment: &excelize.Alignment{
			Horizontal: "center",
			Vertical:   "center",
		},
	})
	misc.CheckError("", err)

	naValueTableRows, err := f.NewStyle(&excelize.Style{
		Font: &excelize.Font{
			Size:  11,
			Color: "#b3b3b3",
		},
		Alignment: &excelize.Alignment{
			Horizontal: "center",
			Vertical:   "center",
		},
	})
	misc.CheckError("", err)

	tableLastRows, err := f.NewStyle(&excelize.Style{
		Font: &excelize.Font{
			Size: 11,
		},
		Border: []excelize.Border{
			{Type: "bottom", Color: "000000", Style: 1},
		},
		Alignment: &excelize.Alignment{
			Horizontal: "center",
			Vertical:   "center",
		},
	})
	misc.CheckError("", err)

	naValueTableLastRows, err := f.NewStyle(&excelize.Style{
		Font: &excelize.Font{
			Size:  11,
			Color: "#b3b3b3",
		},
		Border: []excelize.Border{
			{Type: "bottom", Color: "000000", Style: 1},
		},
		Alignment: &excelize.Alignment{
			Horizontal: "center",
			Vertical:   "center",
		},
	})
	misc.CheckError("", err)

	tableFirstColumnLastRow, err := f.NewStyle(&excelize.Style{
		Font: &excelize.Font{
			Size: 11,
		},
		Border: []excelize.Border{
			{Type: "bottom", Color: "000000", Style: 1},
		},
		Alignment: &excelize.Alignment{
			Horizontal: "left",
			Vertical:   "center",
		},
	})
	misc.CheckError("", err)

	legendStyleSubjectTable, err := f.NewStyle(&excelize.Style{
		Font: &excelize.Font{
			Size: 9,
		},
		Alignment: &excelize.Alignment{
			Vertical: "top",
		},
	})
	misc.CheckError("", err)

	// grey version
	tableFirstColumnRowGray, err := f.NewStyle(&excelize.Style{
		Fill: excelize.Fill{
			Type:    "pattern",
			Color:   []string{"#cccccc"},
			Pattern: 1,
		},
		Font: &excelize.Font{
			Size: 11,
		},
		Alignment: &excelize.Alignment{
			Horizontal: "left",
			Vertical:   "center",
		},
	})
	misc.CheckError("", err)

	tableRowsGray, err := f.NewStyle(&excelize.Style{
		Fill: excelize.Fill{
			Type:    "pattern",
			Color:   []string{"#cccccc"},
			Pattern: 1,
		},
		Font: &excelize.Font{
			Size: 11,
		},
		Alignment: &excelize.Alignment{
			Horizontal: "center",
			Vertical:   "center",
		},
	})
	misc.CheckError("", err)

	naValueTableRowsGray, err := f.NewStyle(&excelize.Style{
		Fill: excelize.Fill{
			Type:    "pattern",
			Color:   []string{"#cccccc"},
			Pattern: 1,
		},
		Font: &excelize.Font{
			Size:  11,
			Color: "#b3b3b3",
		},
		Alignment: &excelize.Alignment{
			Horizontal: "left",
			Vertical:   "center",
		},
	})
	misc.CheckError("", err)

	tableLastRowsGray, err := f.NewStyle(&excelize.Style{
		Fill: excelize.Fill{
			Type:    "pattern",
			Color:   []string{"#cccccc"},
			Pattern: 1,
		},
		Font: &excelize.Font{
			Size: 11,
		},
		Border: []excelize.Border{
			{Type: "bottom", Color: "000000", Style: 1},
		},
		Alignment: &excelize.Alignment{
			Horizontal: "center",
			Vertical:   "center",
		},
	})
	misc.CheckError("", err)

	naValueTableLastRowsGray, err := f.NewStyle(&excelize.Style{
		Fill: excelize.Fill{
			Type:    "pattern",
			Color:   []string{"#cccccc"},
			Pattern: 1,
		},
		Font: &excelize.Font{
			Size:  11,
			Color: "#b3b3b3",
		},
		Border: []excelize.Border{
			{Type: "bottom", Color: "000000", Style: 1},
		},
		Alignment: &excelize.Alignment{
			Horizontal: "center",
			Vertical:   "center",
		},
	})
	misc.CheckError("", err)

	tableFirstColumnLastRowGray, err := f.NewStyle(&excelize.Style{
		Fill: excelize.Fill{
			Type:    "pattern",
			Color:   []string{"#cccccc"},
			Pattern: 1,
		},
		Font: &excelize.Font{
			Size: 11,
		},
		Border: []excelize.Border{
			{Type: "bottom", Color: "000000", Style: 1},
		},
		Alignment: &excelize.Alignment{
			Horizontal: "left",
			Vertical:   "center",
		},
	})
	misc.CheckError("", err)

	lableConvertedGradesTable, err := f.NewStyle(&excelize.Style{
		Font: &excelize.Font{
			Size: 6,
		},
		Alignment: &excelize.Alignment{
			Vertical: "bottom",
		},
	})
	misc.CheckError("", err)
	// ############## end Styles

	haIncrease := (reportCard.Tendency.GraduationTendency == cfg.Hauptschulabschluss || (reportCard.Tendency.GraduationTendency == cfg.OhneAbschluss) && reportCard.ClassYear == 9) && (reportCard.Term == 1 || (reportCard.Term == 2 && reportCard.ReportCardInfo.FinalExams.TookExams))

	// start of subject Information Block
	setCellValue(f, sheet, cell('A', rowNum), "Fach")
	setCellValue(f, sheet, cell('B', rowNum), "Lehrkraft")
	setCellValue(f, sheet, cell('C', rowNum), "Fehlzeiten")
	setCellValue(f, sheet, cell('D', rowNum), "Kurs")
	setCellValue(f, sheet, cell('E', rowNum), "Note")
	setCellValue(f, sheet, cell('F', rowNum), "Ausfall")
	setCellValue(f, sheet, cell('G', rowNum), "Ausgleich")
	setCellStyle(f, sheet, cell('A', rowNum), cell('A', rowNum), tableFirstColumnTitleRow)
	setCellStyle(f, sheet, cell('B', rowNum), cell('G', rowNum), tableTitleRow)

	setCellValue(f, sheet, cell('I', rowNum-1), "Noten umgerechnet für:")
	mergeCell(f, sheet, cell('I', rowNum-1), cell('K', rowNum-1))
	setCellStyle(f, sheet, cell('I', rowNum-1), cell('I', rowNum-1), lableConvertedGradesTable)

	setCellValue(f, sheet, cell('I', rowNum), cfg.Versetztung11)
	setCellValue(f, sheet, cell('J', rowNum), "RA")
	if reportCard.ClassYear != 10 {
		setCellValue(f, sheet, cell('K', rowNum), "HA")
	}
	setCellStyle(f, sheet, cell('I', rowNum), cell('K', rowNum), tableTitleRow)

	rowNum++

	// loop through subject list (and otherSubjecct List) and write all 7 informations per loop
	for i := 0; i < len(reportCard.ReportCardInfo.Subjects); i++ {
		switch {
		case reportCard.ReportCardInfo.Subjects[i].ExamTaken && reportCard.ReportCardInfo.Subjects[i].GrageFromPreviousYear:
			setCellValue(f, sheet, cell('A', rowNum+i), reportCard.ReportCardInfo.Subjects[i].SubjectFullName+examSubjectSymbol+"(Vorjahresnote)") // TODO: Null bzw. -1 später ersetzten durch "NA" oder so
		case reportCard.ReportCardInfo.Subjects[i].ExamTaken:
			setCellValue(f, sheet, cell('A', rowNum+i), reportCard.ReportCardInfo.Subjects[i].SubjectFullName+examSubjectSymbol)
		default:
			setCellValue(f, sheet, cell('A', rowNum+i), reportCard.ReportCardInfo.Subjects[i].SubjectFullName)
		}
		setCellValue(f, sheet, cell('B', rowNum+i), reportCard.ReportCardInfo.Subjects[i].Teacher)
		if reportCard.ReportCardInfo.AbsentsTotal.Absents == -1 && reportCard.ReportCardInfo.Subjects[i].Absents.Absents == 0 {
			setCellValue(f, sheet, cell('C', rowNum+i), "NA / NA")
		} else {
			setCellValue(f, sheet, cell('C', rowNum+i), replaceM1byNA(reportCard.ReportCardInfo.Subjects[i].Absents.Absents)+" / "+replaceM1byNA(reportCard.ReportCardInfo.Subjects[i].Absents.UnexcusedAbsents))
		}
		setCellValue(f, sheet, cell('D', rowNum+i), reportCard.ReportCardInfo.Subjects[i].CourseLvl+reportCard.ReportCardInfo.Subjects[i].LvlChange)
		switch {
		case reportCard.ReportCardInfo.Subjects[i].ExamTaken:
			if reportCard.ReportCardInfo.Subjects[i].GradeJump {
				setCellValue(f, sheet, cell('E', rowNum+i), strconv.Itoa(reportCard.ReportCardInfo.Subjects[i].Grade)+examSubjectSymbol+" ("+gradeJumpSymbol+strconv.Itoa(reportCard.ReportCardInfo.Subjects[i].GradeNotIncludingExamGrade)+")")
			} else {
				setCellValue(f, sheet, cell('E', rowNum+i), strconv.Itoa(reportCard.ReportCardInfo.Subjects[i].Grade)+examSubjectSymbol+" ("+strconv.Itoa(reportCard.ReportCardInfo.Subjects[i].GradeNotIncludingExamGrade)+")")
			}
		case haIncrease && reportCard.ReportCardInfo.Subjects[i].HAGradeDifferentThanOrginial:
			if reportCard.ReportCardInfo.Subjects[i].GradeJump {
				setCellValue(f, sheet, cell('E', rowNum+i), gradeJumpSymbol+strconv.Itoa(reportCard.ReportCardInfo.Subjects[i].HAGrade)+gradeHAIncreaseSymbol+" ["+strconv.Itoa(reportCard.ReportCardInfo.Subjects[i].Grade)+"]")
			} else {
				setCellValue(f, sheet, cell('E', rowNum+i), strconv.Itoa(reportCard.ReportCardInfo.Subjects[i].HAGrade)+gradeHAIncreaseSymbol+" ["+strconv.Itoa(reportCard.ReportCardInfo.Subjects[i].Grade)+"]")
			}
		default:
			if reportCard.ReportCardInfo.Subjects[i].GradeJump {
				setCellValue(f, sheet, cell('E', rowNum+i), gradeJumpSymbol+strconv.Itoa(reportCard.ReportCardInfo.Subjects[i].Grade))
			} else {
				setCellValue(f, sheet, cell('E', rowNum+i), reportCard.ReportCardInfo.Subjects[i].Grade)
			}
		}
		setCellValue(f, sheet, cell('F', rowNum+i), reportCard.ReportCardInfo.Subjects[i].FailString)
		setCellValue(f, sheet, cell('G', rowNum+i), reportCard.ReportCardInfo.Subjects[i].CompenstaionBy)

		// format rows
		evenUneven := rowNum % 2
		switch {
		case i == len(reportCard.ReportCardInfo.Subjects)-1: // last row
			if (rowNum+i)%2 == evenUneven {
				setCellStyle(f, sheet, cell('A', rowNum+i), cell('A', rowNum+i), tableFirstColumnLastRowGray)
				setCellStyle(f, sheet, cell('B', rowNum+i), cell('G', rowNum+i), tableLastRowsGray)
				if checkForMissingValue(reportCard.ReportCardInfo.Subjects[i].Absents.Absents) || (reportCard.ReportCardInfo.AbsentsTotal.Absents == -1 && reportCard.ReportCardInfo.Subjects[i].Absents.Absents == 0) {
					setCellStyle(f, sheet, cell('C', rowNum+i), cell('C', rowNum+i), naValueTableLastRowsGray)
				}
			} else {
				setCellStyle(f, sheet, cell('A', rowNum+i), cell('A', rowNum+i), tableFirstColumnLastRow)
				setCellStyle(f, sheet, cell('B', rowNum+i), cell('G', rowNum+i), tableLastRows)
				if checkForMissingValue(reportCard.ReportCardInfo.Subjects[i].Absents.Absents) || (reportCard.ReportCardInfo.AbsentsTotal.Absents == -1 && reportCard.ReportCardInfo.Subjects[i].Absents.Absents == 0) {
					setCellStyle(f, sheet, cell('C', rowNum+i), cell('C', rowNum+i), naValueTableLastRows)
				}
			}
		case (rowNum+i)%2 == evenUneven:
			setCellStyle(f, sheet, cell('A', rowNum+i), cell('A', rowNum+i), tableFirstColumnRowGray)
			setCellStyle(f, sheet, cell('B', rowNum+i), cell('G', rowNum+i), tableRowsGray)
			if checkForMissingValue(reportCard.ReportCardInfo.Subjects[i].Absents.Absents) || (reportCard.ReportCardInfo.AbsentsTotal.Absents == -1 && reportCard.ReportCardInfo.Subjects[i].Absents.Absents == 0) {
				setCellStyle(f, sheet, cell('C', rowNum+i), cell('C', rowNum+i), naValueTableRowsGray)
			}
		default:
			setCellStyle(f, sheet, cell('A', rowNum+i), cell('A', rowNum+i), tableFirstColumnRow)
			setCellStyle(f, sheet, cell('B', rowNum+i), cell('G', rowNum+i), tableRows)
			if checkForMissingValue(reportCard.ReportCardInfo.Subjects[i].Absents.Absents) || (reportCard.ReportCardInfo.AbsentsTotal.Absents == -1 && reportCard.ReportCardInfo.Subjects[i].Absents.Absents == 0) {
				setCellStyle(f, sheet, cell('C', rowNum+i), cell('C', rowNum+i), naValueTableRows)
			}
		}

		// add table with converted grades
		setCellValue(f, sheet, cell('I', rowNum+i), calc.ConvertGrade(reportCard.ReportCardInfo.Subjects[i], cfg.Versetztung11, false))
		setCellValue(f, sheet, cell('J', rowNum+i), calc.ConvertGrade(reportCard.ReportCardInfo.Subjects[i], cfg.Realschulabschluss, false))
		if reportCard.ClassYear != 10 {
			setCellValue(f, sheet, cell('K', rowNum+i), calc.ConvertGrade(reportCard.ReportCardInfo.Subjects[i], cfg.Hauptschulabschluss, haIncrease))
		}

		// format rows of converted grades
		evenUneven = rowNum % 2
		switch {
		case i == len(reportCard.ReportCardInfo.Subjects)-1: // last row
			if (rowNum+i)%2 == evenUneven {
				setCellStyle(f, sheet, cell('I', rowNum+i), cell('K', rowNum+i), tableLastRowsGray)
			} else {
				setCellStyle(f, sheet, cell('I', rowNum+i), cell('K', rowNum+i), tableLastRows)
			}
		case (rowNum+i)%2 == evenUneven:
			setCellStyle(f, sheet, cell('I', rowNum+i), cell('K', rowNum+i), tableRowsGray)
		default:
			setCellStyle(f, sheet, cell('I', rowNum+i), cell('K', rowNum+i), tableRows)
		}

	}

	rowNum += len(reportCard.ReportCardInfo.Subjects)
	if len(reportCard.ReportCardInfo.UnratedSubjects) > 0 {
		setCellValue(f, sheet, cell('A', rowNum), "sonstige Fächer:")
		setCellStyle(f, sheet, cell('A', rowNum), cell('A', rowNum), tableFirstColumnTitleRow)
		setCellStyle(f, sheet, cell('B', rowNum), cell('G', rowNum), tableFirstColumnBetweentitleRow)

		rowNum++
		for i := 0; i < len(reportCard.ReportCardInfo.UnratedSubjects); i++ {
			setCellValue(f, sheet, cell('A', rowNum+i), reportCard.ReportCardInfo.UnratedSubjects[i].SubjectFullName)
			setCellValue(f, sheet, cell('B', rowNum+i), reportCard.ReportCardInfo.UnratedSubjects[i].Teacher)
			setCellValue(f, sheet, cell('C', rowNum+i), replaceM1byNA(reportCard.ReportCardInfo.UnratedSubjects[i].Absents.Absents)+" / "+replaceM1byNA(reportCard.ReportCardInfo.UnratedSubjects[i].Absents.UnexcusedAbsents))
			setCellValue(f, sheet, cell('D', rowNum+i), reportCard.ReportCardInfo.UnratedSubjects[i].CourseLvl+reportCard.ReportCardInfo.UnratedSubjects[i].LvlChange)
			setCellValue(f, sheet, cell('E', rowNum+i), reportCard.ReportCardInfo.UnratedSubjects[i].AlternativeGrade)
			setCellValue(f, sheet, cell('F', rowNum+i), reportCard.ReportCardInfo.UnratedSubjects[i].FailString)
			setCellValue(f, sheet, cell('G', rowNum+i), reportCard.ReportCardInfo.UnratedSubjects[i].CompenstaionBy)

			// format rows
			evenUneven := rowNum % 2
			switch {
			case i == len(reportCard.ReportCardInfo.UnratedSubjects)-1: // last row
				if (rowNum+i)%2 == evenUneven {
					setCellStyle(f, sheet, cell('A', rowNum+i), cell('A', rowNum+i), tableFirstColumnLastRowGray)
					setCellStyle(f, sheet, cell('B', rowNum+i), cell('G', rowNum+i), tableLastRowsGray)
					if checkForMissingValue(reportCard.ReportCardInfo.UnratedSubjects[i].Absents.Absents) {
						setCellStyle(f, sheet, cell('C', rowNum+i), cell('C', rowNum+i), naValueTableLastRowsGray)
					}
				} else {
					setCellStyle(f, sheet, cell('A', rowNum+i), cell('A', rowNum+i), tableFirstColumnLastRow)
					setCellStyle(f, sheet, cell('B', rowNum+i), cell('G', rowNum+i), tableLastRows)
					if checkForMissingValue(reportCard.ReportCardInfo.UnratedSubjects[i].Absents.Absents) {
						setCellStyle(f, sheet, cell('C', rowNum+i), cell('C', rowNum+i), naValueTableLastRows)
					}
				}
			case (rowNum+i)%2 == evenUneven:
				setCellStyle(f, sheet, cell('A', rowNum+i), cell('A', rowNum+i), tableFirstColumnRowGray)
				setCellStyle(f, sheet, cell('B', rowNum+i), cell('G', rowNum+i), tableRowsGray)
				if checkForMissingValue(reportCard.ReportCardInfo.UnratedSubjects[i].Absents.Absents) {
					setCellStyle(f, sheet, cell('C', rowNum+i), cell('C', rowNum+i), naValueTableRowsGray)
				}
			default:
				setCellStyle(f, sheet, cell('A', rowNum+i), cell('A', rowNum+i), tableFirstColumnRow)
				setCellStyle(f, sheet, cell('B', rowNum+i), cell('G', rowNum+i), tableRows)
				if checkForMissingValue(reportCard.ReportCardInfo.UnratedSubjects[i].Absents.Absents) {
					setCellStyle(f, sheet, cell('C', rowNum+i), cell('C', rowNum+i), naValueTableRows)
				}
			}
		}
		rowNum += len(reportCard.ReportCardInfo.UnratedSubjects)
	}

	if reportCard.ReportCardInfo.FinalExams.TookExams {
		setCellValue(f, sheet, cell('A', rowNum), "Abschlussprüfungen:")
		setCellStyle(f, sheet, cell('A', rowNum), cell('G', rowNum), tableFirstColumnTitleRow)

		rowNum++
		for i := 0; i < len(reportCard.ReportCardInfo.FinalExams.ExamSubjects); i++ {
			setCellValue(f, sheet, cell('A', rowNum+i), reportCard.ReportCardInfo.FinalExams.ExamSubjects[i].SubjectFullName)
			setCellValue(f, sheet, cell('B', rowNum+i), reportCard.ReportCardInfo.FinalExams.ExamSubjects[i].Teacher)
			setCellValue(f, sheet, cell('E', rowNum+i), reportCard.ReportCardInfo.FinalExams.ExamSubjects[i].ExamGrade)

			// format rows
			evenUneven := rowNum % 2
			switch {
			case i == len(reportCard.ReportCardInfo.FinalExams.ExamSubjects)-1: // last row
				if (rowNum+i)%2 == evenUneven {
					setCellStyle(f, sheet, cell('A', rowNum+i), cell('A', rowNum+i), tableFirstColumnLastRowGray)
					setCellStyle(f, sheet, cell('B', rowNum+i), cell('G', rowNum+i), tableLastRowsGray)
				} else {
					setCellStyle(f, sheet, cell('A', rowNum+i), cell('A', rowNum+i), tableFirstColumnLastRow)
					setCellStyle(f, sheet, cell('B', rowNum+i), cell('G', rowNum+i), tableLastRows)
				}
			case (rowNum+i)%2 == evenUneven:
				setCellStyle(f, sheet, cell('A', rowNum+i), cell('A', rowNum+i), tableFirstColumnRowGray)
				setCellStyle(f, sheet, cell('B', rowNum+i), cell('G', rowNum+i), tableRowsGray)
			default:
				setCellStyle(f, sheet, cell('A', rowNum+i), cell('A', rowNum+i), tableFirstColumnRow)
				setCellStyle(f, sheet, cell('B', rowNum+i), cell('G', rowNum+i), tableRows)
			}
		}
		rowNum += len(reportCard.ReportCardInfo.FinalExams.ExamSubjects)
	}

	setCellValue(f, sheet, cell('A', rowNum), "↧: Abstufung (↧*: auf Elternantrag); ↥: Aufstufung; "+gradeJumpSymbol+": Notensprung; ↦: Umstufung nächstes Schuljahr nicht relevant")
	setCellStyle(f, sheet, cell('A', rowNum), cell('A', rowNum), legendStyleSubjectTable)

	if haIncrease {
		rowNum++
		setCellValue(f, sheet, cell('A', rowNum), "Hinweis zur Tabelle: \nNotenverbesserung im Hauptschulabschlusszeugnis mit "+gradeHAIncreaseSymbol+" gekennzeichnet. In eckigen Klammern [] sind die Noten des Schuljahreszeugnis angegeben. \nPrüfungsnoten sind in den Noten der Tabelle bereits verrechnet und mit "+examSubjectSymbol+" gekennzeichnet. Vorherigen Noten sind in Klammern dahinter angegeben.")
		// add Warning if not 1. Halbjahr ...
		setCellStyle(f, sheet, cell('A', rowNum), cell('A', rowNum), tableFirstColumnBetweentitleRowInfo)
		rowNum++
	}
	rowNum++

	// print info to legend, if level change was ignored for specified subjects
	return rowNum
}

// function writes tendency information to excel file (starting at given startrow number) and returns last written row number
func writeTendencyInformation(reportCard global.Student, f *excelize.File, sheet string, rowNum int) int {
	cfg := config.GetConfig()

	// ############## create styles for tendency cells
	oneDigitDec := "0.0"

	tendendySpecifierCellTop, err := f.NewStyle(&excelize.Style{
		Font: &excelize.Font{
			Size: 11,
			Bold: true,
		},
		Border: []excelize.Border{
			{Type: "left", Color: "000000", Style: 1},
			{Type: "top", Color: "000000", Style: 1},
		},
		Alignment: &excelize.Alignment{
			Horizontal: "right",
			Vertical:   "center",
		},
	})
	misc.CheckError("", err)

	tendendyInfoCellMiddleTop, err := f.NewStyle(&excelize.Style{
		Font: &excelize.Font{
			Size:  11,
			Bold:  true,
			Color: "#c9211e",
		},
		Border: []excelize.Border{
			{Type: "top", Color: "000000", Style: 1},
		},
		Alignment: &excelize.Alignment{
			Horizontal: "center",
			Vertical:   "center",
		},
	})
	misc.CheckError("", err)

	tendendyInfoCellTop, err := f.NewStyle(&excelize.Style{
		Font: &excelize.Font{
			Size:  11,
			Bold:  true,
			Color: "#c9211e",
		},
		Border: []excelize.Border{
			{Type: "top", Color: "000000", Style: 1},
			{Type: "right", Color: "000000", Style: 1},
		},
		Alignment: &excelize.Alignment{
			Horizontal: "center",
			Vertical:   "center",
		},
	})
	misc.CheckError("", err)

	tendendySpecifierCellMiddle, err := f.NewStyle(&excelize.Style{
		Font: &excelize.Font{
			Size: 11,
		},
		Border: []excelize.Border{
			{Type: "left", Color: "000000", Style: 1},
		},
		Alignment: &excelize.Alignment{
			Horizontal: "right",
			Vertical:   "center",
		},
	})
	misc.CheckError("", err)

	tendendyInfoCellMiddleRed, err := f.NewStyle(&excelize.Style{
		Font: &excelize.Font{
			Size:  11,
			Color: "#c9211e",
		},
		Border: []excelize.Border{
			{Type: "right", Color: "000000", Style: 1},
		},
		Alignment: &excelize.Alignment{
			Horizontal: "center",
			Vertical:   "center",
		},
	})
	misc.CheckError("", err)

	tendendyInfoCellMiddleMiddleRedQuali, err := f.NewStyle(&excelize.Style{
		Font: &excelize.Font{
			Size:  11,
			Color: "#c9211e",
		},
		Alignment: &excelize.Alignment{
			Horizontal: "left",
			Vertical:   "center",
		},
	})
	misc.CheckError("", err)

	tendendyInfoCellMiddleMiddleRed, err := f.NewStyle(&excelize.Style{
		Font: &excelize.Font{
			Size:  11,
			Color: "#c9211e",
		},
		Alignment: &excelize.Alignment{
			Horizontal: "center",
			Vertical:   "center",
		},
	})
	misc.CheckError("", err)

	tendendyInfoCellMiddle, err := f.NewStyle(&excelize.Style{
		Font: &excelize.Font{
			Size: 11,
		},
		Border: []excelize.Border{
			{Type: "right", Color: "000000", Style: 1},
		},
		Alignment: &excelize.Alignment{
			Horizontal: "left",
			Vertical:   "center",
		},
	})
	misc.CheckError("", err)

	tendendyInfoCellMiddleMiddel, err := f.NewStyle(&excelize.Style{
		CustomNumFmt: &oneDigitDec,
		Font: &excelize.Font{
			Size: 11,
		},
		Alignment: &excelize.Alignment{
			Horizontal: "center",
			Vertical:   "center",
		},
	})
	misc.CheckError("", err)

	tendendyInfoCellMiddleBottom, err := f.NewStyle(&excelize.Style{
		CustomNumFmt: &oneDigitDec,
		Font: &excelize.Font{
			Size: 11,
		},
		Border: []excelize.Border{
			{Type: "bottom", Color: "000000", Style: 1},
		},
		Alignment: &excelize.Alignment{
			Horizontal: "center",
			Vertical:   "center",
		},
	})
	misc.CheckError("", err)

	tendendySpecifierCellBottom, err := f.NewStyle(&excelize.Style{
		Font: &excelize.Font{
			Size: 11,
		},
		Border: []excelize.Border{
			{Type: "left", Color: "000000", Style: 1},
			{Type: "bottom", Color: "000000", Style: 1},
		},
		Alignment: &excelize.Alignment{
			Horizontal: "center",
			Vertical:   "right",
		},
	})
	misc.CheckError("", err)

	tendendyInfoCellBottom, err := f.NewStyle(&excelize.Style{
		Font: &excelize.Font{
			Size: 11,
		},
		Border: []excelize.Border{
			{Type: "bottom", Color: "000000", Style: 1},
			{Type: "right", Color: "000000", Style: 1},
		},
		Alignment: &excelize.Alignment{
			Horizontal: "left",
			Vertical:   "center",
		},
	})
	misc.CheckError("", err)

	schnittUeberischtTop, err := f.NewStyle(&excelize.Style{
		Font: &excelize.Font{
			Size:  11,
			Bold:  true,
			Color: "#b2b2b2",
		},
		Border: []excelize.Border{
			{Type: "top", Color: "000000", Style: 1},
			{Type: "bottom", Color: "000000", Style: 1},
		},
		Alignment: &excelize.Alignment{
			Horizontal: "center",
			Vertical:   "center",
		},
	})
	misc.CheckError("", err)

	schnittUeberischtTopBlack, err := f.NewStyle(&excelize.Style{
		Font: &excelize.Font{
			Size:  11,
			Bold:  true,
			Color: "#000000",
		},
		Border: []excelize.Border{
			{Type: "top", Color: "000000", Style: 1},
			{Type: "bottom", Color: "000000", Style: 1},
		},
		Alignment: &excelize.Alignment{
			Horizontal: "center",
			Vertical:   "center",
		},
	})
	misc.CheckError("", err)

	schnittUeberischtMiddleLeft, err := f.NewStyle(&excelize.Style{
		NumFmt:       1,
		CustomNumFmt: &oneDigitDec,
		Font: &excelize.Font{
			Size:  11,
			Color: "#b2b2b2",
		},
		Alignment: &excelize.Alignment{
			Horizontal: "right",
			Vertical:   "center",
		},
	})
	misc.CheckError("", err)

	schnittUeberischtMiddleLeftBlack, err := f.NewStyle(&excelize.Style{
		NumFmt:       1,
		CustomNumFmt: &oneDigitDec,
		Font: &excelize.Font{
			Size:  11,
			Color: "#000000",
		},
		Alignment: &excelize.Alignment{
			Horizontal: "right",
			Vertical:   "center",
		},
	})
	misc.CheckError("", err)

	schnittUeberischtMiddleMiddle, err := f.NewStyle(&excelize.Style{
		NumFmt:       1,
		CustomNumFmt: &oneDigitDec,
		Font: &excelize.Font{
			Size:  11,
			Color: "#b2b2b2",
		},
		Alignment: &excelize.Alignment{
			Horizontal: "center",
			Vertical:   "center",
		},
	})
	misc.CheckError("", err)

	schnittUeberischtMiddleMiddleBlack, err := f.NewStyle(&excelize.Style{
		NumFmt:       1,
		CustomNumFmt: &oneDigitDec,
		Font: &excelize.Font{
			Size:  11,
			Color: "#000000",
		},
		Alignment: &excelize.Alignment{
			Horizontal: "center",
			Vertical:   "center",
		},
	})
	misc.CheckError("", err)

	schnittUeberischtBottomLeft, err := f.NewStyle(&excelize.Style{
		NumFmt:       1,
		CustomNumFmt: &oneDigitDec,
		Font: &excelize.Font{
			Size:  11,
			Color: "#b2b2b2",
		},
		Border: []excelize.Border{
			{Type: "bottom", Color: "000000", Style: 1},
		},
		Alignment: &excelize.Alignment{
			Horizontal: "right",
			Vertical:   "center",
		},
	})
	misc.CheckError("", err)

	schnittUeberischtBottomMiddle, err := f.NewStyle(&excelize.Style{
		NumFmt:       1,
		CustomNumFmt: &oneDigitDec,
		Font: &excelize.Font{
			Size:  11,
			Color: "#b2b2b2",
		},
		Border: []excelize.Border{
			{Type: "bottom", Color: "000000", Style: 1},
		},
		Alignment: &excelize.Alignment{
			Horizontal: "center",
			Vertical:   "center",
		},
	})
	misc.CheckError("", err)

	// lusdTop, err := f.NewStyle(&excelize.Style{
	// 	Font: &excelize.Font{
	// 		Size:  8,
	// 		Bold:  true,
	// 		Color: "#666666",
	// 	},
	// 	Border: []excelize.Border{
	// 		{Type: "left", Color: "b2b2b2", Style: 1},
	// 		{Type: "top", Color: "b2b2b2", Style: 1},
	// 		{Type: "right", Color: "b2b2b2", Style: 1},
	// 	},
	// 	Alignment: &excelize.Alignment{
	// 		Horizontal: "center",
	// 		Vertical:   "center",
	// 	},
	// })
	// misc.CheckError("", err)

	// lusdMiddleLeft, err := f.NewStyle(&excelize.Style{
	// 	Font: &excelize.Font{
	// 		Size:  9,
	// 		Color: "#666666",
	// 	},
	// 	Border: []excelize.Border{
	// 		{Type: "left", Color: "b2b2b2", Style: 1},
	// 	},
	// 	Alignment: &excelize.Alignment{
	// 		Horizontal: "right",
	// 		Vertical:   "center",
	// 	},
	// })
	// misc.CheckError("", err)

	// lusdMiddleRight, err := f.NewStyle(&excelize.Style{
	// 	CustomNumFmt: &oneDigitDec,
	// 	Font: &excelize.Font{
	// 		Size:  11,
	// 		Color: "#666666",
	// 	},
	// 	Border: []excelize.Border{
	// 		{Type: "right", Color: "b2b2b2", Style: 1},
	// 	},
	// 	Alignment: &excelize.Alignment{
	// 		Horizontal: "center",
	// 		Vertical:   "center",
	// 	},
	// })
	// misc.CheckError("", err)

	// lusdBottomLeft, err := f.NewStyle(&excelize.Style{
	// 	Font: &excelize.Font{
	// 		Size:  9,
	// 		Color: "#666666",
	// 	},
	// 	Border: []excelize.Border{
	// 		{Type: "bottom", Color: "b2b2b2", Style: 1},
	// 	},
	// 	Alignment: &excelize.Alignment{
	// 		Horizontal: "right",
	// 		Vertical:   "center",
	// 	},
	// })
	// misc.CheckError("", err)

	// lusdBottomRight, err := f.NewStyle(&excelize.Style{
	// 	Font: &excelize.Font{
	// 		Size:  11,
	// 		Color: "#666666",
	// 	},
	// 	Border: []excelize.Border{
	// 		{Type: "bottom", Color: "b2b2b2", Style: 1},
	// 		{Type: "right", Color: "b2b2b2", Style: 1},
	// 	},
	// 	Alignment: &excelize.Alignment{
	// 		Horizontal: "center",
	// 		Vertical:   "center",
	// 	},
	// })
	// misc.CheckError("", err)

	legendStyleSubjectTable, err := f.NewStyle(&excelize.Style{
		Font: &excelize.Font{
			Size:  9,
			Color: "#000000",
		},
		Alignment: &excelize.Alignment{
			Vertical: "top",
		},
	})
	misc.CheckError("", err)

	lusdResultsHeader, err := f.NewStyle(&excelize.Style{
		Font: &excelize.Font{
			Size: 11,
			Bold: true,
		},
		Alignment: &excelize.Alignment{
			Horizontal: "left",
			Vertical:   "center",
		},
	})
	misc.CheckError("", err)
	// ############## end Styles

	setCellValue(f, sheet, cell('A', rowNum), "Tendenz: ")
	setCellStyle(f, sheet, cell('A', rowNum), cell('A', rowNum), tendendySpecifierCellTop)

	switch {
	case reportCard.Warnings.NoCalculationPossible:
		setCellValue(f, sheet, cell('B', rowNum), "nicht berechenbar")
	case reportCard.ClassYear == 10 && reportCard.Tendency.GraduationTendency == cfg.Hauptschulabschluss:
		setCellValue(f, sheet, cell('B', rowNum), reportCard.Tendency.GraduationTendency+"?")
	default:
		setCellValue(f, sheet, cell('B', rowNum), reportCard.Tendency.GraduationTendency)
	}
	mergeCell(f, sheet, cell('B', rowNum), cell('D', rowNum))
	setCellStyle(f, sheet, cell('B', rowNum), cell('D', rowNum), tendendyInfoCellMiddleTop)
	setCellStyle(f, sheet, cell('D', rowNum), cell('D', rowNum), tendendyInfoCellTop)

	rowNum++
	setCellValue(f, sheet, cell('A', rowNum), "Ausgleich/Quali: ")
	setCellStyle(f, sheet, cell('A', rowNum), cell('A', rowNum), tendendySpecifierCellMiddle)
	if !reportCard.Tendency.WithCompensation {
		setCellValue(f, sheet, cell('B', rowNum), "---")
	} else {
		setCellValue(f, sheet, cell('B', rowNum), "mit Ausgleich")
	}
	setCellValue(f, sheet, cell('C', rowNum), reportCard.Tendency.Quali)
	if reportCard.Tendency.Quali == cfg.NaWiQuali {
		setCellStyle(f, sheet, cell('B', rowNum), cell('B', rowNum), tendendyInfoCellMiddleMiddleRed)
		setCellStyle(f, sheet, cell('C', rowNum), cell('C', rowNum), tendendyInfoCellMiddleMiddleRedQuali)
	} else {
		setCellStyle(f, sheet, cell('B', rowNum), cell('C', rowNum), tendendyInfoCellMiddleMiddleRed)
	}
	setCellStyle(f, sheet, cell('D', rowNum), cell('D', rowNum), tendendyInfoCellMiddleRed)

	rowNum++
	setCellValue(f, sheet, cell('A', rowNum), "Elternwunsch: ")
	setCellStyle(f, sheet, cell('A', rowNum), cell('A', rowNum), tendendySpecifierCellMiddle)

	setCellValue(f, sheet, cell('B', rowNum), reportCard.Tendency.ParentsGraduationGoal)
	setCellStyle(f, sheet, cell('B', rowNum), cell('C', rowNum), tendendyInfoCellMiddleMiddel)
	setCellStyle(f, sheet, cell('D', rowNum), cell('D', rowNum), tendendyInfoCellMiddle)

	rowNum++
	setCellValue(f, sheet, cell('A', rowNum), "Ziel des Kindes: ")
	setCellStyle(f, sheet, cell('A', rowNum), cell('A', rowNum), tendendySpecifierCellBottom)
	setCellStyle(f, sheet, cell('B', rowNum), cell('C', rowNum), tendendyInfoCellMiddleBottom)
	setCellStyle(f, sheet, cell('D', rowNum), cell('D', rowNum), tendendyInfoCellBottom)

	rowNum++
	rowNum++

	setCellValue(f, sheet, cell('A', rowNum), "Schnittübersicht")
	setCellStyle(f, sheet, cell('A', rowNum), cell('A', rowNum), schnittUeberischtTopBlack)
	setCellValue(f, sheet, cell('B', rowNum), "V11")
	setCellValue(f, sheet, cell('C', rowNum), "RA")
	setCellValue(f, sheet, cell('D', rowNum), "HA")
	setCellStyle(f, sheet, cell('B', rowNum), cell('D', rowNum), schnittUeberischtTop)

	switch reportCard.Tendency.GraduationTendency {
	case cfg.Versetztung11:
		setCellStyle(f, sheet, cell('B', rowNum), cell('B', rowNum), schnittUeberischtTopBlack)
	case cfg.Realschulabschluss:
		setCellStyle(f, sheet, cell('C', rowNum), cell('C', rowNum), schnittUeberischtTopBlack)
	default:
		if reportCard.ClassYear == 10 {
			setCellStyle(f, sheet, cell('C', rowNum), cell('C', rowNum), schnittUeberischtTopBlack)
		} else {
			setCellStyle(f, sheet, cell('D', rowNum), cell('D', rowNum), schnittUeberischtTopBlack)
		}
	}

	rowNum++
	setCellValue(f, sheet, cell('A', rowNum), "⌀ Gesamtschnitt: ")
	setCellValue(f, sheet, cell('B', rowNum), replaceZeroByDashes(reportCard.CalcResults.V11Results.Averages.AverageAllSubjects))
	setCellValue(f, sheet, cell('C', rowNum), replaceZeroByDashes(reportCard.CalcResults.RAResults.Averages.AverageAllSubjects))
	setCellValue(f, sheet, cell('D', rowNum), replaceZeroByDashes(reportCard.CalcResults.HAResults.Averages.AverageAllSubjects))
	setCellStyle(f, sheet, cell('A', rowNum), cell('A', rowNum), schnittUeberischtMiddleLeft)
	setCellStyle(f, sheet, cell('B', rowNum), cell('D', rowNum), schnittUeberischtMiddleMiddle)
	switch reportCard.Tendency.GraduationTendency {
	case cfg.Versetztung11:
		setCellStyle(f, sheet, cell('A', rowNum), cell('A', rowNum), schnittUeberischtMiddleLeftBlack)
		setCellStyle(f, sheet, cell('B', rowNum), cell('B', rowNum), schnittUeberischtMiddleMiddleBlack)
	case cfg.Realschulabschluss:
		setCellStyle(f, sheet, cell('A', rowNum), cell('A', rowNum), schnittUeberischtMiddleLeftBlack)
		setCellStyle(f, sheet, cell('C', rowNum), cell('C', rowNum), schnittUeberischtMiddleMiddleBlack)
	default:
		if reportCard.ClassYear == 10 {
			setCellStyle(f, sheet, cell('A', rowNum), cell('A', rowNum), schnittUeberischtMiddleLeftBlack)
			setCellStyle(f, sheet, cell('C', rowNum), cell('C', rowNum), schnittUeberischtMiddleMiddleBlack)
		} else {
			setCellStyle(f, sheet, cell('A', rowNum), cell('A', rowNum), schnittUeberischtMiddleLeftBlack)
			setCellStyle(f, sheet, cell('D', rowNum), cell('D', rowNum), schnittUeberischtMiddleMiddleBlack)
		}
	}

	rowNum++
	setCellValue(f, sheet, cell('A', rowNum), "⌀ DME: ")
	setCellValue(f, sheet, cell('B', rowNum), replaceZeroByDashes(reportCard.CalcResults.V11Results.Averages.AverageDME))
	setCellValue(f, sheet, cell('C', rowNum), replaceZeroByDashes(reportCard.CalcResults.RAResults.Averages.AverageDME))
	setCellValue(f, sheet, cell('D', rowNum), replaceZeroByDashes(reportCard.CalcResults.HAResults.Averages.AverageDME))
	setCellStyle(f, sheet, cell('A', rowNum), cell('A', rowNum), schnittUeberischtMiddleLeft)
	setCellStyle(f, sheet, cell('B', rowNum), cell('D', rowNum), schnittUeberischtMiddleMiddle)
	if reportCard.Tendency.GraduationTendency == cfg.Realschulabschluss || (reportCard.Tendency.GraduationTendency == cfg.Realschulabschluss && reportCard.ClassYear == 10) {
		setCellStyle(f, sheet, cell('A', rowNum), cell('A', rowNum), schnittUeberischtMiddleLeftBlack)
		setCellStyle(f, sheet, cell('C', rowNum), cell('C', rowNum), schnittUeberischtMiddleMiddleBlack)
	}

	rowNum++
	setCellValue(f, sheet, cell('A', rowNum), "⌀ restl. Fächer: ")
	setCellValue(f, sheet, cell('B', rowNum), replaceZeroByDashes(reportCard.CalcResults.V11Results.Averages.AverageAllButDME))
	setCellValue(f, sheet, cell('C', rowNum), replaceZeroByDashes(reportCard.CalcResults.RAResults.Averages.AverageAllButDME))
	setCellValue(f, sheet, cell('D', rowNum), replaceZeroByDashes(reportCard.CalcResults.HAResults.Averages.AverageAllButDME))
	setCellStyle(f, sheet, cell('A', rowNum), cell('A', rowNum), schnittUeberischtMiddleLeft)
	setCellStyle(f, sheet, cell('B', rowNum), cell('D', rowNum), schnittUeberischtMiddleMiddle)
	if reportCard.Tendency.GraduationTendency == cfg.Realschulabschluss || (reportCard.Tendency.GraduationTendency == cfg.Hauptschulabschluss && reportCard.ClassYear == 10) {
		setCellStyle(f, sheet, cell('A', rowNum), cell('A', rowNum), schnittUeberischtMiddleLeftBlack)
		setCellStyle(f, sheet, cell('C', rowNum), cell('C', rowNum), schnittUeberischtMiddleMiddleBlack)
	}

	rowNum++
	setCellValue(f, sheet, cell('A', rowNum), "⌀ DME+CH/PH: ")
	setCellValue(f, sheet, cell('B', rowNum), replaceZeroByDashes(reportCard.CalcResults.V11Results.Averages.AverageNaWiQualiDMEandPHorCH))
	setCellValue(f, sheet, cell('C', rowNum), replaceZeroByDashes(reportCard.CalcResults.RAResults.Averages.AverageNaWiQualiDMEandPHorCH))
	setCellValue(f, sheet, cell('D', rowNum), replaceZeroByDashes(reportCard.CalcResults.HAResults.Averages.AverageNaWiQualiDMEandPHorCH))
	setCellStyle(f, sheet, cell('A', rowNum), cell('A', rowNum), schnittUeberischtMiddleLeft)
	setCellStyle(f, sheet, cell('B', rowNum), cell('D', rowNum), schnittUeberischtMiddleMiddle)
	if reportCard.Tendency.GraduationTendency == cfg.Realschulabschluss && reportCard.Tendency.Quali == cfg.NaWiQuali {
		setCellStyle(f, sheet, cell('A', rowNum), cell('A', rowNum), schnittUeberischtMiddleLeftBlack)
		setCellStyle(f, sheet, cell('C', rowNum), cell('C', rowNum), schnittUeberischtMiddleMiddleBlack)
	}

	rowNum++
	setCellValue(f, sheet, cell('A', rowNum), "⌀ restl. Fächer (außer DME+PH/CH): ")
	setCellValue(f, sheet, cell('B', rowNum), replaceZeroByDashes(reportCard.CalcResults.V11Results.Averages.AverageNaWiQualiRestOfSubjects))
	setCellValue(f, sheet, cell('C', rowNum), replaceZeroByDashes(reportCard.CalcResults.RAResults.Averages.AverageNaWiQualiRestOfSubjects))
	setCellValue(f, sheet, cell('D', rowNum), replaceZeroByDashes(reportCard.CalcResults.HAResults.Averages.AverageNaWiQualiRestOfSubjects))
	setCellStyle(f, sheet, cell('A', rowNum), cell('A', rowNum), schnittUeberischtBottomLeft)
	setCellStyle(f, sheet, cell('B', rowNum), cell('D', rowNum), schnittUeberischtBottomMiddle)
	if reportCard.Tendency.GraduationTendency == cfg.Realschulabschluss && reportCard.Tendency.Quali == cfg.NaWiQuali {
		setCellStyle(f, sheet, cell('A', rowNum), cell('A', rowNum), schnittUeberischtMiddleLeftBlack)
		setCellStyle(f, sheet, cell('C', rowNum), cell('C', rowNum), schnittUeberischtMiddleLeftBlack)
	}

	rowNum++
	setCellValue(f, sheet, cell('A', rowNum), "(Alle Durchschnittswerte sind immer abgerundet)")
	setCellStyle(f, sheet, cell('A', rowNum), cell('A', rowNum), legendStyleSubjectTable)

	if reportCard.Warnings.ContainsWarnings || reportCard.Warnings.MissingGrades {
		// ############## create Styles
		warningHeader, err := f.NewStyle(&excelize.Style{
			Font: &excelize.Font{
				Size: 11,
				Bold: true,
			},
			Alignment: &excelize.Alignment{
				Horizontal: "center",
			},
		})
		misc.CheckError("", err)

		warningBox, err := f.NewStyle(&excelize.Style{
			Fill: excelize.Fill{
				Type:    "pattern",
				Color:   []string{"#ffbf00"},
				Pattern: 1,
			},
			Font: &excelize.Font{
				Size: 10,
			},
			Alignment: &excelize.Alignment{
				Horizontal: "left",
				Vertical:   "center",
			},
		})
		misc.CheckError("", err)
		// ############## end Styles

		rowNum++
		setCellValue(f, sheet, cell('C', rowNum), "⚠️ Achtung! ⚠️")
		setCellStyle(f, sheet, cell('C', rowNum), cell('C', rowNum), warningHeader)

		for i := 0; i < len(reportCard.Warnings.WarningMsg); i++ {
			rowNum++
			setCellValue(f, sheet, cell('A', rowNum), reportCard.Warnings.WarningMsg[i])
			setCellStyle(f, sheet, cell('A', rowNum), cell('G', rowNum), warningBox)
		}
		rowNum++
	}

	if reportCard.Comments.ContainsComment {
		// ############## create Styles
		commentHeader, err := f.NewStyle(&excelize.Style{
			Font: &excelize.Font{
				Size: 11,
				Bold: true,
			},
			Alignment: &excelize.Alignment{
				Horizontal: "center",
			},
		})
		misc.CheckError("", err)

		commentBox, err := f.NewStyle(&excelize.Style{
			Fill: excelize.Fill{
				Type:    "pattern",
				Color:   []string{"#fff6d5"},
				Pattern: 1,
			},
			Font: &excelize.Font{
				Size: 10,
			},
			Alignment: &excelize.Alignment{
				Horizontal: "left",
				Vertical:   "center",
			},
		})
		misc.CheckError("", err)
		// ############## end Styles
		rowNum++
		setCellValue(f, sheet, cell('C', rowNum), "Hinweise (FAQs)")
		setCellStyle(f, sheet, cell('C', rowNum), cell('C', rowNum), commentHeader)

		for i := 0; i < len(reportCard.Comments.CommentMsg); i++ {
			rowNum++
			setCellValue(f, sheet, cell('A', rowNum), reportCard.Comments.CommentMsg[i])
			setCellStyle(f, sheet, cell('A', rowNum), cell('G', rowNum), commentBox)
		}
		rowNum++
	}

	// if reportCard.ReportCardInfo.FinalExams.LUSDGraduationDataPresent {
	// 	setCellValue(f, sheet, cell('F', rowNum), "ausgelesene LUSD-Werte")
	// 	mergeCell(f, sheet, cell('F', rowNum), cell('G', rowNum))
	// 	setCellStyle(f, sheet, cell('F', rowNum), cell('G', rowNum), lusdTop)
	// }

	// rowNum++

	// if reportCard.ReportCardInfo.FinalExams.LUSDGraduationDataPresent {
	// 	setCellValue(f, sheet, cell('F', rowNum), "Gesamt:")
	// 	setCellStyle(f, sheet, cell('F', rowNum), cell('F', rowNum), lusdMiddleLeft)
	// 	if reportCard.ReportCardInfo.FinalExams.LUSDcalcTotalAverage != 0 {
	// 		setCellValue(f, sheet, cell('G', rowNum), reportCard.ReportCardInfo.FinalExams.LUSDcalcTotalAverage)
	// 	} else {
	// 		setCellValue(f, sheet, cell('G', rowNum), "---")
	// 	}
	// 	setCellStyle(f, sheet, cell('G', rowNum), cell('G', rowNum), lusdMiddleRight)
	// 	rowNum++
	// 	setCellValue(f, sheet, cell('F', rowNum), "Hauptfächer:")
	// 	setCellStyle(f, sheet, cell('F', rowNum), cell('F', rowNum), lusdMiddleLeft)
	// 	if reportCard.ReportCardInfo.FinalExams.LUSDcalcMajorsAverage != 0 {
	// 		setCellValue(f, sheet, cell('G', rowNum), reportCard.ReportCardInfo.FinalExams.LUSDcalcMajorsAverage)
	// 	} else {
	// 		setCellValue(f, sheet, cell('G', rowNum), "---")
	// 	}
	// 	setCellStyle(f, sheet, cell('G', rowNum), cell('G', rowNum), lusdMiddleRight)
	// 	rowNum++
	// 	setCellValue(f, sheet, cell('F', rowNum), "restl. Fächer:")
	// 	setCellStyle(f, sheet, cell('F', rowNum), cell('F', rowNum), lusdMiddleLeft)
	// 	if reportCard.ReportCardInfo.FinalExams.LUSDcalcMinorsAverage != 0 {
	// 		setCellValue(f, sheet, cell('G', rowNum), reportCard.ReportCardInfo.FinalExams.LUSDcalcMinorsAverage)
	// 	} else {
	// 		setCellValue(f, sheet, cell('G', rowNum), "---")
	// 	}
	// 	setCellStyle(f, sheet, cell('G', rowNum), cell('G', rowNum), lusdMiddleRight)
	// 	rowNum++
	// 	setCellValue(f, sheet, cell('F', rowNum), "Abschluss:")
	// 	setCellStyle(f, sheet, cell('F', rowNum), cell('F', rowNum), lusdBottomLeft)
	// 	setCellValue(f, sheet, cell('G', rowNum), reportCard.ReportCardInfo.FinalExams.LUSDcalcGrad)
	// 	setCellStyle(f, sheet, cell('G', rowNum), cell('G', rowNum), lusdBottomRight)
	// }

	rowNum++
	// TDOD: grau formtieren (lusd kram wiederverwerten von code oben)
	if reportCard.ReportCardInfo.FinalExams.LUSDExamData.DataPresent {
		rowNum++
		setCellValue(f, sheet, cell('A', rowNum), "Werte aus LUSD Ergebnis-Datei für "+reportCard.Name+":")
		setCellStyle(f, sheet, cell('A', rowNum), cell('A', rowNum), lusdResultsHeader)
		rowNum++
		setCellValue(f, sheet, cell('A', rowNum), "Abschluss: ")
		setCellStyle(f, sheet, cell('A', rowNum), cell('A', rowNum), tendendySpecifierCellMiddle)
		setCellValue(f, sheet, cell('B', rowNum), reportCard.ReportCardInfo.FinalExams.GraduationFullName+" ("+reportCard.ReportCardInfo.FinalExams.GraduationAbbreviation+")")
		rowNum++
		setCellValue(f, sheet, cell('A', rowNum), "Gesamtleistung: ")
		setCellStyle(f, sheet, cell('A', rowNum), cell('A', rowNum), tendendySpecifierCellMiddle)
		setCellValue(f, sheet, cell('B', rowNum), reportCard.ReportCardInfo.FinalExams.LUSDExamData.TotalAverage)
		setCellValue(f, sheet, cell('C', rowNum), reportCard.ReportCardInfo.FinalExams.LUSDExamData.MajorsAverage)
		setCellValue(f, sheet, cell('D', rowNum), reportCard.ReportCardInfo.FinalExams.LUSDExamData.MinorsAverage)
		rowNum++
		for _, examSubject := range reportCard.ReportCardInfo.FinalExams.BackupExamData.ExamSubjects {
			setCellValue(f, sheet, cell('A', rowNum), examSubject.SubjectFullName+": ")
			setCellStyle(f, sheet, cell('A', rowNum), cell('A', rowNum), tendendySpecifierCellMiddle)
			setCellValue(f, sheet, cell('B', rowNum), examSubject.ExamGrade)
			rowNum++
		}
	}
	rowNum++
	return rowNum
}

// function writes the tendency-Log to excel sheet starting at startrow
func writeLog(reportCard global.Student, f *excelize.File, sheet string, rowNum int, startCol byte) int {
	cfg := config.GetConfig()

	// ############## create Styles
	logTitle, err := f.NewStyle(&excelize.Style{
		Font: &excelize.Font{
			Size: 10,
			Bold: true,
		},
	})
	misc.CheckError("", err)

	title, err := f.NewStyle(&excelize.Style{
		Font: &excelize.Font{
			Size: 10,
			Bold: true,
		},
		Border: []excelize.Border{
			{Type: "top", Color: "000000", Style: 1},
			{Type: "bottom", Color: "000000", Style: 1},
		},
	})
	misc.CheckError("", err)

	tableTitleRow, err := f.NewStyle(&excelize.Style{
		Font: &excelize.Font{
			Size: 10,
			Bold: true,
		},
	})
	misc.CheckError("", err)

	middleHeader, err := f.NewStyle(&excelize.Style{
		Font: &excelize.Font{
			Size: 10,
			Bold: true,
		},
	})
	misc.CheckError("", err)
	// ############## end Styles

	rowNum++

	setCellValue(f, sheet, cell(startCol, rowNum), "Log")
	setCellStyle(f, sheet, cell(startCol, rowNum), cell(startCol+3, rowNum), logTitle)

	rowNum++
	// evenUneven := rowNum % 2
	for i := 0; i < len(reportCard.Tendency.Log); i++ {
		setCellValue(f, sheet, cell(startCol, rowNum+i), reportCard.Tendency.Log[i][0])
		setCellValue(f, sheet, cell(startCol+1, rowNum+i), reportCard.Tendency.Log[i][1])
		setCellValue(f, sheet, cell(startCol+2, rowNum+i), reportCard.Tendency.Log[i][2])
		switch {
		case reportCard.Tendency.Log[i][0] == cfg.Versetztung11+"-Bedingungen" || reportCard.Tendency.Log[i][0] == cfg.Realschulabschluss+"bedingungen" || reportCard.Tendency.Log[i][0] == cfg.Hauptschulabschluss+"bedingungen":
			setCellStyle(f, sheet, cell(startCol, rowNum+i), cell(startCol+3, rowNum+i), title)
		case reportCard.Tendency.Log[i][0] == "Erfüllt?" || reportCard.Tendency.Log[i][0] == "Fach":
			setCellStyle(f, sheet, cell(startCol, rowNum+i), cell(startCol+3, rowNum+i), tableTitleRow)
		case reportCard.Tendency.Log[i][0] == "Ausfälle":
			setCellStyle(f, sheet, cell(startCol, rowNum+i), cell(startCol+3, rowNum+i), middleHeader)
		}
	}
	return rowNum + len(reportCard.Tendency.Log) + 1
}

func replaceZeroByDashes(number float64) interface{} {
	if number == 0 {
		return "---"
	}
	return number
}
