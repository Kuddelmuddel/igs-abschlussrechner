package exportdata

type gradOverview struct {
	v11                 []gradInfo
	realschulabschluss  []gradInfo
	hauptschulabschluss []gradInfo
	ohneAbschluss       []gradInfo
	notAssigned         []gradInfo
	asteriskGrades      bool
	missingGrades       bool
	attention           bool
	grade10             bool
}

type gradInfo struct {
	surname       string
	forename      string
	compensation  string
	quali         string
	missingGrades bool
	attention     bool
}
