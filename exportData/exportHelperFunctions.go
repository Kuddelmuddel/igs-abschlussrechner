package exportdata

import (
	"log"
	"strconv"

	"github.com/xuri/excelize/v2"
)

// function converts given cell number and letter to combined string (e.g. 'A' and 1 to string "A1")
func cell(char byte, num int) string {
	return string(char) + strconv.Itoa(num)
}

func max(x, y int) int {
	if x < y {
		return y
	}
	return x
}

func setColWith(f *excelize.File, sheet string, startCol string, endCol string, withInCM float64) {
	withInpixel := withInCM * 4.047628635
	err := f.SetColWidth(sheet, startCol, endCol, withInpixel)
	if err != nil {
		log.Fatalf("Error setting column width in sheet %s, columns %s-%s: %v", sheet, startCol, endCol, err)
	}
}

func setCellValue(f *excelize.File, sheet string, cell string, value interface{}) {
	err := f.SetCellValue(sheet, cell, value)
	if err != nil {
		log.Fatalf("Error setting value in sheet %s, cell %s: %v", sheet, cell, err)
	}
}

func setSheetViewOptionsZoomscale(f *excelize.File, sheet string, viewIndex int, zoomScale float64) {
	var (
		zoom = zoomScale
	)
	err := f.SetSheetView(sheet, viewIndex, &excelize.ViewOptions{
		ZoomScale: &zoom,
	})
	if err != nil {
		log.Fatalf("Error setting zoom scale for sheet %s, view %d: %v", sheet, viewIndex, err)
	}
}

func setSheetViewOptionsGridLines(f *excelize.File, sheet string, viewIndex int, gridLines bool) {
	err := f.SetSheetView(sheet, viewIndex, &excelize.ViewOptions{
		ShowGridLines: &gridLines,
	})
	if err != nil {
		log.Fatalf("Error setting grid lines for sheet %s, view %d: %v", sheet, viewIndex, err)
	}
}

func setSheetFormatPrDefaultRowHeight(f *excelize.File, sheet string, rowHeight float64) {
	var (
		height = rowHeight
	)
	err := f.SetSheetProps(sheet, &excelize.SheetPropsOptions{
		DefaultRowHeight: &height,
	})
	if err != nil {
		log.Fatalf("Error setting default row height for sheet %s: %v", sheet, err)
	}
}

func setSheetPrOptionsFitToPage(f *excelize.File, sheet string, fitToPage bool) {
	err := f.SetSheetProps(sheet, &excelize.SheetPropsOptions{
		FitToPage: &fitToPage,
	})

	if err != nil {
		log.Fatalf("Error setting fit to page for sheet %s: %v", sheet, err)
	}
}

func setCellStyle(f *excelize.File, sheet string, startcCell string, endCell string, styleID int) {
	err := f.SetCellStyle(sheet, startcCell, endCell, styleID)
	if err != nil {
		log.Fatalf("Error setting style for cells %s:%s in sheet %s: %v", startcCell, endCell, sheet, err)
	}
}

func mergeCell(f *excelize.File, sheet string, startCell string, endCell string) {
	err := f.MergeCell(sheet, startCell, endCell)
	if err != nil {
		log.Fatalf("Error merging cells %s:%s in sheet %s: %v", startCell, endCell, sheet, err)
	}
}

func replaceM1byNA(value int) string {
	if value == -1 {
		return "NA"
	}
	return strconv.Itoa(value)
}

func checkForMissingValue(value int) bool {
	return value == -1
}
