package exportdata

import (
	"fmt"
	"igs-abschlussrechner/config"
	"igs-abschlussrechner/global"
	misc "igs-abschlussrechner/miscellaneous"
	"log"
	"strconv"

	"strings"

	"github.com/xuri/excelize/v2"
)

// ExportDataToExcelfiles exports reportcard list to excel file
func ExportDataToExcelfiles(classData []global.Student) {
	cfg := config.GetConfig()

	filename := CreateFilename(classData)

	f := excelize.NewFile()
	insertOverviewSheet(f, classData)
	insertAVSVOverviewSheet(f, classData)
	if cfg.PrintRecordSheet {
		insertRecord(f, classData)
	}
	insertStudentSheets(f, classData)

	f.SetActiveSheet(0)
	if cfg.Encrypt {
		err := f.SaveAs(filename, excelize.Options{Password: cfg.Passphrase})
		if err != nil {
			log.Fatal(err)
			fmt.Println(err)
		}
	} else {
		err := f.SaveAs(filename)
		if err != nil {
			log.Fatal(err)
			fmt.Println(err)
		}
	}
}

func CreateFilename(classData []global.Student) string {
	teacherLastName := strings.Split(classData[0].ClassTeacher, ", ")[0]
	nametag := classData[0].Class + "_" + teacherLastName
	datetag := strings.ReplaceAll(classData[0].YearsOfData+"-"+strconv.Itoa(classData[0].Term)+".Hj", "/", "_")
	return "output/" + nametag + "_" + datetag + ".xlsx"
}

func ExportStudentLevelChangesToExcelFile(students [][]global.Student, fileName string) error {
	f := excelize.NewFile()

	sheetName := "Umstufungen"
	err := f.SetSheetName("Sheet1", sheetName)
	misc.CheckError("Failed to rename first sheet, insertStudentSheets.go", err)

	title := "Umstufungen " + strconv.Itoa(students[0][0].Term) + ". Halbjahr " + students[0][0].YearsOfData
	setCellValue(f, sheetName, "A1", title)

	// Set header row
	headers := []string{
		"Schülername", "Klasse", "Fach", "Kurslehrkraft", "aktuelle Kursdifferenzierung", "Endgültige Abstufung", "Halbjahresnote", "Gewünschte Umstufung",
	}
	for i, header := range headers {
		col := string(rune('A' + i))
		setCellValue(f, sheetName, col+"2", header)
	}

	// Write data rows
	row := 3
	for _, classData := range students {
		misc.ClasswiseBatchProcess(&classData, func(student *global.Student) {
			for _, subject := range student.ReportCardInfo.Subjects {
				if subject.LvlChange != "" { // Only process subjects with a level change
					setCellValue(f, sheetName, "A"+strconv.Itoa(row), student.Name)
					setCellValue(f, sheetName, "B"+strconv.Itoa(row), student.Class)
					setCellValue(f, sheetName, "C"+strconv.Itoa(row), subject.SubjectFullName)
					setCellValue(f, sheetName, "D"+strconv.Itoa(row), subject.Teacher)
					setCellValue(f, sheetName, "E"+strconv.Itoa(row), subject.CourseLvl)
					setCellValue(f, sheetName, "F"+strconv.Itoa(row), "") // Placeholder for "Endgültige Abstufung"
					setCellValue(f, sheetName, "G"+strconv.Itoa(row), subject.Grade)
					setCellValue(f, sheetName, "H"+strconv.Itoa(row), misc.DetermineChangedLevel(subject.CourseLvl, subject.LvlChange))
					row++
				}
			}
		})
	}

	return f.SaveAs("output/" + fileName)
}
