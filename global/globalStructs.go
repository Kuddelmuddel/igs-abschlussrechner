package global

type Student struct {
	DateOfFile        string
	School            string
	YearsOfData       string
	Term              int
	ClassTeacher      string
	SubstitudeTeacher string
	Class             string
	ClassYear         int
	ClassChar         string
	Name              string
	ReportCardInfo    ReportCardInformation
	Tendency          TendencyStruc
	CalcResults       CalcResultsStruc
	Record            RecordStruc
	History           StudentHistory
	Warnings          WarningStruc
	Comments          CommentStruc
}

type ReportCardInformation struct {
	AV              string // string to be able to include *) grade
	SV              string // string to be able to include *) grade
	AbsentsTotal    AbsentStruc
	Subjects        []SubjectStruc
	UnratedSubjects []SubjectStruc
	FinalExams      ExamsStruc
	GradeJumps      bool
	Notenschutz     bool
}

type AbsentStruc struct {
	Absents          int
	UnexcusedAbsents int
	AbsentDays       int
	UnexcusedDays    int
}

type SubjectStruc struct {
	SubjectAbbreviation          string
	SubjectFullName              string
	Teacher                      string
	Absents                      AbsentStruc
	CourseLvl                    string
	LvlChange                    string
	Grade                        int
	GradeNotIncludingExamGrade   int
	GradeJump                    bool
	GrageFromPreviousYear        bool
	HAGrade                      int
	HAGradeDifferentThanOrginial bool
	AlternativeGrade             string
	FailString                   string
	CompenstaionBy               string
	ExamTaken                    bool
	LvlChangeNonLUSDData         NonLUSDLvlChangeStruc
}

type NonLUSDLvlChangeStruc struct {
	ListedInLUSD        bool
	ListedinNonLUSDData bool
	AlreadyObjected     bool
	ParentChangeRequest bool
	CurrentCourseLvl    string
	FutureCourseLvl     string
}

type ExamsStruc struct {
	TookExams              bool
	GraduationAbbreviation string
	GraduationFullName     string
	TotalAverage           float64
	MajorsAverage          float64
	MinorsAverage          float64
	ExamSubjects           []ExamSubjectStruct
	LUSDExamData           ExamDataSource
	BackupExamData         ExamDataSource
	ManualExamData         ExamDataSource
	ReplacedMissingData    bool
	DataReplacedBy         []string
}

type ExamDataSource struct {
	DataPresent            bool
	IdFields               IdStruc
	GraduationAbbreviation string
	GraduationFullName     string
	TotalAverage           float64
	MajorsAverage          float64
	MinorsAverage          float64
	ExamSubjects           []ExamSubjectStruct
}

type IdStruc struct {
	StudentName              string
	Class                    string
	ClassYear                int
	ClassChar                string
	ClassTeacherAbbreviation string
	ClassTeacherFullName     string
	Semester                 string
}

type ExamSubjectStruct struct {
	SubjectAbbreviation string
	SubjectFullName     string
	Teacher             string
	ReportCardGrade     int
	ExamGrade           int
}

type TendencyStruc struct {
	GraduationTendency    string
	WithCompensation      bool
	Quali                 string
	Averages              AverageStruct
	Log                   [][3]string
	ParentsGraduationGoal string
}

type CalcResultsStruc struct {
	V11Results TendencyCalcResults
	RAResults  TendencyCalcResults
	HAResults  TendencyCalcResults
}

type TendencyCalcResults struct {
	FailsAndComps    FailsAndCompensations
	AllCompensated   bool
	Log              [][3]string
	FailsLog         [][3]string
	ConditionsMet    bool
	WithCompensation bool
	Quali            string
	Averages         AverageStruct
}

type FailsAndCompensations struct {
	MajorFail                 []FailedSubject
	MinorFail                 []FailedSubject
	PossibleMajorCompensation []CompensationSubject
	MultiMajorCompB           []CompensationSubject
	MultiMajorCompGE          []CompensationSubject
	MultiMajorCompNoLvl       []CompensationSubject
	CompMinorFail6            []CompensationSubject
	PossibleMinorCompensation []CompensationSubject
	MultiMinorComp            []CompensationSubject
}

type FailedSubject struct {
	Name          string
	Teacher       string // important, because of different WPU subjects all called "WPU" (same name, but not the same teacher)
	Compensated   bool
	Compensatedby string
}

type CompensationSubject struct {
	Name string
	Used bool
}

type AverageStruct struct {
	AverageDME                     float64
	AverageAllButDME               float64
	AverageAllSubjects             float64
	AverageNaWiQualiDMEandPHorCH   float64
	AverageNaWiQualiRestOfSubjects float64
	BestAverageDME                 float64
	BestAverageRestOfSubjects      float64
}

type RecordStruc struct {
	LevelChanges []LevelChangeStruc
	SupportPlan  []SupportPlanStruc
	GradeJumps   []GradeJumpStruc
}

type GradeJumpStruc struct {
	StudentName         string
	SubjectAbbreviation string
	SubjectFullName     string
	CourseLvl           string
	Grade               int
	Teacher             string
}

type LevelChangeStruc struct {
	StudentName         string
	SubjectAbbreviation string
	SubjectFullName     string
	CourseLvl           string
	LvlChange           string
	Grade               int
	GradeJumps          bool // TODO: add here (for Proctcoll?)
	Teacher             string
	NotListed           bool
	ListedInLUSD        bool
	ListedInNonLUSD     bool
	ParentChangeRequest bool
}

type SupportPlanStruc struct {
	StudentName         string
	SubjectAbbreviation string
	SubjectFullName     string
	CourseLvlGrade      string
	Teacher             string
}

type StudentHistory struct {
	SchoolAttendanceYears int
	RepeatedGrade         []RepeatedGradeStruc
	RepeatsTotal          int
}

type RepeatedGradeStruc struct {
	RepeatedGrade int
	RepeatedTerm  int
}

type WarningStruc struct {
	ContainsWarnings       bool
	ContainsAsteriskGrades bool
	NoCalculationPossible  bool
	MissingGrades          bool
	WarningMsg             []string
}

type CommentStruc struct {
	ContainsComment bool
	CommentMsg      []string
}

type NonLUSDData struct {
	NonLUSDLvlChangeInfo  []ImportedLvlChanges
	NonLUSDAdditionalInfo []ImportedAdditionalInfo
}

type ImportedLvlChanges struct {
	Name                string
	Class               string
	SubjectFullName     string
	SubjectAbbreviation string
	Teacher             string
	CurrentCourseLvl    string
	FutureCourseLvl     string
	Grade               int
	AlreadyObjected     bool
	ParentChangeRequest bool
	MatchFound          bool
}

type ImportedAdditionalInfo struct {
	Forename       string
	Lastname       string
	Class          string
	GraduationGoal string
	Notenschutz    bool
	MatchFound     bool
}
