package global

func (e *ExamsStruc) FillDataFromSources() {
	if e.LUSDExamData.DataPresent {
		e.GraduationAbbreviation = e.LUSDExamData.GraduationAbbreviation
		e.GraduationFullName = e.LUSDExamData.GraduationFullName
		e.TotalAverage = e.LUSDExamData.TotalAverage
		e.MajorsAverage = e.LUSDExamData.MajorsAverage
		e.MinorsAverage = e.LUSDExamData.MinorsAverage
		e.ExamSubjects = e.LUSDExamData.ExamSubjects
		e.TookExams = true
		return
	}

	if e.BackupExamData.DataPresent {
		e.GraduationAbbreviation = e.BackupExamData.GraduationAbbreviation
		e.GraduationFullName = e.BackupExamData.GraduationFullName
		e.TotalAverage = e.BackupExamData.TotalAverage
		e.MajorsAverage = e.BackupExamData.MajorsAverage
		e.MinorsAverage = e.BackupExamData.MinorsAverage
		e.ExamSubjects = e.BackupExamData.ExamSubjects
		e.TookExams = true
		return
	}

	if e.ManualExamData.DataPresent {
		e.GraduationAbbreviation = e.ManualExamData.GraduationAbbreviation
		e.GraduationFullName = e.ManualExamData.GraduationFullName
		e.TotalAverage = e.ManualExamData.TotalAverage
		e.MajorsAverage = e.ManualExamData.MajorsAverage
		e.MinorsAverage = e.ManualExamData.MinorsAverage
		e.ExamSubjects = e.ManualExamData.ExamSubjects
		e.TookExams = true
		return
	}

	e.TookExams = false
}
