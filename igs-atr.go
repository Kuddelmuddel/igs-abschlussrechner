package main

import (
	"errors"
	"fmt"
	calc "igs-abschlussrechner/calcTendencies"
	"igs-abschlussrechner/config"
	imp "igs-abschlussrechner/dataImport"
	exportdata "igs-abschlussrechner/exportData"
	"igs-abschlussrechner/global"
	misc "igs-abschlussrechner/miscellaneous"
	"log"
	"os"
	"strconv"

	"github.com/TwiN/go-color"
)

func main() {
	// Open or create the log file
	logFile, err := os.OpenFile("output/00_Log.txt", os.O_APPEND|os.O_RDWR|os.O_CREATE, 0644)
	if err != nil {
		log.Panic(err)
	}
	defer logFile.Close()

	// Set up logger
	log.SetOutput(logFile)
	log.SetFlags(log.Lshortfile | log.LstdFlags)

	// Log start of program
	log.Println("### start of programm ###")

	// Display disclaimer
	disclaimerMsg := "################################# DISCLAIMER #################################\n## " + color.InRed("Important: ") + "Tendencies calculated by this program may contain errors!     ##\n## I cannot guarantee the validity of the calculated graduation tendencies. ##\n" + "##############################################################################\n"
	disclamerMsgDE := "########################## DISCLAIMER ###########################\n## Wichtig: Die berechneten Tendenzen können Fehler enthalten! ##\n## Ich garantiere NICHT, dass die Ergebnisse richtig sind!     ##\n" + "#################################################################"
	misc.ConsoleLogPrint(disclaimerMsg, true, false)

	// initiate infosAndWarnings log
	var infosAndWarningsLog []string
	infosAndWarningsLog = append(infosAndWarningsLog, disclamerMsgDE)

	// import yaml-config data
	cfg := config.LoadGlobalConfigVariables(".")

	// Log setup parameters
	// TODO: function to print the used setup criteria to the log file (as below, only for all setup data of yaml file)
	misc.ConsoleLogPrintln(color.InGreen("*")+" Setup parameters:", true, true)
	misc.ConsoleLogPrintln("  encryption: "+strconv.FormatBool(cfg.Encrypt), true, true)
	infosAndWarningsLog = append(infosAndWarningsLog, "\n# Setup parameters:")
	infosAndWarningsLog = append(infosAndWarningsLog, "- Verschlüsselung genutzt: "+strconv.FormatBool(cfg.Encrypt))

	if cfg.Encrypt {
		misc.ConsoleLogPrintln("  encryption passphrase: "+cfg.Passphrase, true, true)
		infosAndWarningsLog = append(infosAndWarningsLog, " - Verschlüsselt mit Passphrase: "+cfg.Passphrase)
	}

	// scan input folder
	misc.ConsoleLogPrintln(color.InGreen("* ")+"Scanning input folder ...", true, true)
	fileListXLSX, err := imp.ScanInputDirXLSX()
	misc.CheckError("  ERROR: No files in input folder!", err)
	identifiedFiles := imp.IdentifyFiles(fileListXLSX)
	if imp.MandatoryFilesMissing(identifiedFiles) {
		errMsg := "could not find mandatory Excel file(s) in input folder"
		err := errors.New(errMsg)
		fmt.Println(color.InRed("  ERROR:"), color.InRed(err))
		log.Panicf("%s", err)
	}

	// Import data from input folder
	infosAndWarningsLog = append(infosAndWarningsLog, "\n# Importierte Dateien")

	var (
		conferenceList, finalExamList, manualInputList, lusdGraduationResults [][][]string
		additionalInfoList, lvlChangeList                                     [][]string
	)

	misc.ConsoleLogPrintln(color.InGreen("*")+" Importing files from input folder:", true, true)
	for _, file := range identifiedFiles {
		fileName := file[0]
		fileLocation := "input/" + fileName
		fileIdentity := file[1]

		logMessage := func(consoleMsg, logInput string) {
			misc.ConsoleLogPrintln(consoleMsg, true, true)
			infosAndWarningsLog = append(infosAndWarningsLog, "- "+logInput)
		}

		switch fileIdentity {
		case "ConferenceList":
			logMessage(color.InGreen("  * ")+"Importing conference list file: "+color.InCyan(fileName), fileName)
			conferenceList = append(conferenceList, imp.ImportAllSheetsOfXLSXfile(fileLocation)...)
		case "FinalExamList":
			logMessage(color.InGreen("  * ")+"Importing list of final exams: "+color.InCyan(fileName), fileName)
			finalExamList = append(finalExamList, imp.ImportAllSheetsOfXLSXfile(fileLocation)...)
		case "LUSDGraduationResults":
			logMessage(color.InGreen("  * ")+"Importing list of LUSD graduation results: "+color.InCyan(fileName), fileName)
			lusdGraduationResults = append(lusdGraduationResults, imp.ImportAllSheetsOfXLSXfile(fileLocation)...)
		case "CourseLvlChange":
			logMessage(color.InGreen("  * ")+"Importing course level changes: "+color.InCyan(fileName), fileName)
			lvlChangeList = append(lvlChangeList, imp.ImportSheetOfXLSXfile(fileLocation, "")...)
		case "AdditionalInfo":
			logMessage(color.InGreen("  * ")+"Importing additional info file: "+color.InCyan(fileName), fileName)
			additionalInfoList = append(additionalInfoList, imp.ImportSheetOfXLSXfile(fileLocation, cfg.DetectAdditionalInfoFile.XlsxSheetname)...)
		case "IsManualInput":
			logMessage(color.InGreen("  * ")+"Importing manual input file: "+color.InCyan(fileName), fileName)
			manualInputList = append(manualInputList, imp.ImportAllSheetsOfXLSXfile(fileLocation)...)
		case "notIdentified":
			logMessage(color.InRed("  * ")+"Skipping (unidentified): "+color.InCyan(fileName), "- ⚠️ WARNING: "+fileName+" not imported (file could not be identified)")
		}
	}

	// Parsing data
	infosAndWarningsLog = append(infosAndWarningsLog, "\n# Warnungen")
	misc.ConsoleLogPrintln(color.InGreen("*")+" Parsing imported data:", true, true)

	misc.ConsoleLogPrintln(color.InGreen("  * ")+"Parsing conference list data", true, true)
	studentsData, parseWarnings := imp.ParseConferenceList(conferenceList)
	if len(parseWarnings) > 0 {
		infosAndWarningsLog = append(infosAndWarningsLog, "## Warnungen beim Import der Konferenzlisten-Daten")
		infosAndWarningsLog = append(infosAndWarningsLog, parseWarnings...)
	}

	if len(manualInputList) != 0 {
		misc.ConsoleLogPrintln(color.InGreen("  * ")+"Parsing student information from manual input file(s)", true, true)
		parsedManualData := imp.ParseManualInputFile(manualInputList)
		studentsData = append(studentsData, parsedManualData...)
	} else {
		misc.ConsoleLogPrintln(color.InYellow("  * ")+"No file(s) containing manual input found in input folder.", true, true)
	}

	var finalExamData map[string]global.ExamDataSource
	if len(finalExamList) != 0 {
		misc.ConsoleLogPrintln(color.InGreen("  * ")+"Parsing final exam list data", true, true)
		finalExamData = imp.ParseFinalExamList(finalExamList)
	} else {
		misc.ConsoleLogPrintln(color.InYellow("  * ")+"No final exam list found in input folder.", true, true)
	}

	var lusdGraduationBackupResultsData map[string]global.ExamDataSource
	if len(lusdGraduationResults) != 0 {
		misc.ConsoleLogPrintln(color.InGreen("  * ")+"Parsing LUSD graduation results (as backup)", true, true)
		lusdGraduationBackupResultsData = imp.ParseLUSDGraduationBackupResults(lusdGraduationResults)
	} else {
		misc.ConsoleLogPrintln(color.InYellow("  * ")+"No LUSD graduation results found in input folder.", true, true)
	}

	var externalInformation global.NonLUSDData
	if len(lvlChangeList) != 0 {
		misc.ConsoleLogPrintln(color.InGreen("  * ")+"Parsing course level changes from non-LUSD file(s)", true, true)
		externalInformation.NonLUSDLvlChangeInfo = imp.ParseNonLUSDLvlCanges(lvlChangeList)
	} else {
		misc.ConsoleLogPrintln(color.InYellow("  * ")+"No file(s) containing course level changes found in input folder.", true, true)
	}

	if len(additionalInfoList) != 0 {
		misc.ConsoleLogPrintln(color.InGreen("  * ")+"Parsing additional information from external file(s)", true, true)
		externalInformation.NonLUSDAdditionalInfo = imp.ParseAdditionalInfos(additionalInfoList)
	} else {
		misc.ConsoleLogPrintln(color.InYellow("  * ")+"No file(s) containing additional information found in input folder.", true, true)
	}

	// Merging imported data
	misc.ConsoleLogPrintln(color.InGreen("* ")+"Merging imported data", true, true)
	misc.ConsoleLogPrint(color.InGreen("  * ")+"Merging final exam data and conference list data ... ", true, true)
	if len(finalExamData) != 0 {
		imp.MergeConferenceAndExamData(&studentsData, finalExamData)
		misc.ConsoleLogPrint(color.InGreen("done!\n"), true, true)
	} else {
		misc.ConsoleLogPrint(color.InYellow("no final exam data, skipping.\n"), true, true)
	}

	misc.ConsoleLogPrint(color.InGreen("  * ")+"Merging LUSD graduation results data (backup) and conference list data ... ", true, true)
	if len(lusdGraduationBackupResultsData) != 0 {
		imp.MergeConferenceListDataAndLUSDGraduationBackupData(&studentsData, lusdGraduationBackupResultsData)
		misc.ConsoleLogPrint(color.InGreen("done!\n"), true, true)
	} else {
		misc.ConsoleLogPrint(color.InYellow("no LUSD graduation results data, skipping.\n"), true, true)
	}

	misc.ConsoleLogPrint(color.InGreen("  * ")+"Merging non-LUSD course level change data and conference list data ... ", true, true)
	if len(externalInformation.NonLUSDLvlChangeInfo) != 0 {
		imp.MergeConferenceListDataAndCourseLevelData(&studentsData, &externalInformation.NonLUSDLvlChangeInfo)
		misc.ConsoleLogPrint(color.InGreen("done!\n"), true, true)
	} else {
		misc.ConsoleLogPrint(color.InYellow("no non-LUSD Course level change data, skipping.\n"), true, true)
	}

	misc.ConsoleLogPrint(color.InGreen("  * ")+"Merging additional non-LUSD data and conference list data ... ", true, true)
	if len(externalInformation.NonLUSDAdditionalInfo) != 0 {
		imp.MergeConferenceListDataAndAdditionalData(&studentsData, &externalInformation.NonLUSDAdditionalInfo)
		misc.ConsoleLogPrint(color.InGreen("done!\n"), true, true)
	} else {
		misc.ConsoleLogPrint(color.InYellow("no additional non-LUSD data, skipping.\n"), true, true)
	}

	// processing data: calculate tendencies and averages
	misc.ConsoleLogPrintln(color.InGreen("* ")+"Determining tendencies", true, true)
	processingSteps := []struct {
		processFunc func(*global.Student)
		stepName    string
	}{
		// TODO: remove the returns, that are not necessary here ...
		{calc.ChoseAndCheckExamData, "Choose and check exam data source"},
		{calc.IncreaseGradesForEligibleHAStudents, "Increase non-level grades, if taken HA exam"},                  // TODO: add Test
		{calc.FindAllFails, "Finding failed subjects"},                                                             // TODO: add Test
		{calc.FindPossibleCompensations, "Finding all possible compensations"},                                     // TODO: add Test
		{calc.CompensateFails, "Checking if all failed subjects can be compensated and adding compensation flags"}, // TODO: add Test
		{calc.CheckingTendencyCriteria, "Checking tendency criteria"},                                              // TODO: add Test
		{calc.CalculatingAllAverages, "Calculating averages"},
		{calc.AddingQuali, "Checking Quali criteria"},                                                          // TODO: add Test
		{calc.AddingTendencyDataToReportcard, "Writing tendency specific data to report card"},                 // TODO: add Test
		{calc.AddingFailsToSubjectsInReportcard, "Writing tendency specific fails to subjects in report card"}, // TODO: add Test
	}
	for _, step := range processingSteps {
		misc.ProcessAllStudentData(&studentsData, step.processFunc, step.stepName)
	}

	// preparing data for export (additional processing and checking for exceptions)
	misc.ConsoleLogPrintln(color.InGreen("* ")+"Preparing data for export ...", true, true)

	misc.ConsoleLogPrintln(color.InGreen("  * ")+"Marking courses for level change", true, true)
	for _, studentData := range studentsData {
		misc.ClasswiseBatchProcess(&studentData, calc.MarkLVLChance)
	}

	misc.ConsoleLogPrintln(color.InGreen("  * ")+"Checking for inconsistencies and adding Warnings and comments", true, true)
	infosAndWarningsLog = append(infosAndWarningsLog, "\n## Warnungen zur Verarbeitung der LUSD-Daten")
	for _, studentData := range studentsData {
		toAddToInfosAndWarningsLog := calc.AddingWarningsAndCommentsToStudentData(&studentData)
		if len(toAddToInfosAndWarningsLog) > 0 {
			infosAndWarningsLog = append(infosAndWarningsLog, toAddToInfosAndWarningsLog...)
		}
	}

	misc.ConsoleLogPrintln(color.InGreen("  * ")+"Writing data to record", true, true)
	for _, studentData := range studentsData {
		misc.ClasswiseBatchProcess(&studentData, calc.AddingRecordToStudentData)
	}

	misc.ConsoleLogPrintln(color.InGreen("  * ")+"Sorting subjects by list in config file", true, true)
	for _, studentData := range studentsData {
		calc.SortSubjectsOfStudentData(&studentData)
	}

	misc.ConsoleLogPrintln(color.InGreen("  * ")+"Checking, if all non-LUSD data could be assigned to respective students ...", true, true)
	infosAndWarningsLog = append(infosAndWarningsLog, calc.AuditNonLUSDData(externalInformation)...)

	misc.ConsoleLogPrintln(color.InGreen("* ")+"Exporting data to excel files", true, true)
	for _, studentData := range studentsData {
		misc.ConsoleLogPrintln(color.InGreen("  * ")+"Exporting file "+color.InCyan(exportdata.CreateFilename(studentData)), true, true)
		// TODO: clean up and simplify export functions
		exportdata.ExportDataToExcelfiles(studentData)
	}

	if cfg.GenerateLvlChangeList {
		filenameLevelCanges := "Umstufungen.xlsx"
		misc.ConsoleLogPrintln(color.InGreen("* ")+"Exporting level changes to "+color.InCyan(filenameLevelCanges), true, true)
		err := exportdata.ExportStudentLevelChangesToExcelFile(studentsData, filenameLevelCanges)
		if err != nil {
			panic(err)
		}
	}

	misc.ConsoleLogPrint(color.InGreen("* ")+"Writing log and warnings to "+color.InCyan("output/00_Log_Infos-und-Warnungen.md")+"\n", true, true)
	misc.Write2txtFile("output/00_Infos_and_Warnungs.md", infosAndWarningsLog)
	log.Print("Program stopped without errors\n\n")
}
