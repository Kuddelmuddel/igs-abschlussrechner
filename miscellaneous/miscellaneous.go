package miscellaneous

import (
	"fmt"
	"igs-abschlussrechner/config"
	"igs-abschlussrechner/global"
	"log"
	"os"
	"regexp"
	"strings"

	"github.com/TwiN/go-color"
)

// helper function to add warnings to student struc
func AddWarning(student *global.Student, warningMsg string) {
	student.Warnings.WarningMsg = append(student.Warnings.WarningMsg, warningMsg)
	student.Warnings.ContainsWarnings = true
}

// helper function to process all student data
func ProcessAllStudentData(data *[][]global.Student, processFunc func(*global.Student), stepName string) {
	ConsoleLogPrintln(color.InGreen("  * ")+stepName, true, true)
	for i := range *data {
		ClasswiseBatchProcess(&(*data)[i], processFunc)
	}
}

func ClasswiseBatchProcess(classData *[]global.Student, processFunc func(*global.Student)) {
	for i := range *classData {
		processFunc(&(*classData)[i])
	}
}

// Write2txtFile writes string array to text file
func Write2txtFile(filename string, input []string) {
	f, err := os.Create(filename)
	if err != nil {
		log.Fatal(err)
	}

	for _, line := range input {
		_, err := f.WriteString(line + "\n")
		if err != nil {
			log.Fatal(err)
		}
	}
	defer f.Close()
}

// function writes (println) message to console and log (depending on flags)
func ConsoleLogPrintln(message string, writeToConsole, writeToLog bool) {
	if writeToConsole {
		fmt.Println(message)
	}
	if writeToLog {
		log.Println(removeColors(message))
	}
}

// function writes (print) message to console and log (depending on flags)
func ConsoleLogPrint(message string, writeToConsole, writeToLog bool) {
	if writeToConsole {
		fmt.Print(message)
	}
	if writeToLog {
		log.Print(removeColors(message))
	}
}

func removeColors(s string) string {
	re := regexp.MustCompile(`\x1b\[\d+m`)
	return re.ReplaceAllString(s, "")
}

// CheckError checks, weather an error occurred or not. If an error occurs, it prints the error message
func CheckError(message string, err error) {
	if err != nil {
		fmt.Printf(color.InRed("%s")+"\nerror: %s", message, err)
		log.Fatalf("%s - %s", message, err)
	}
}

// determines the new course level after level change
func DetermineChangedLevel(level string, change string) string {
	var newlevel string
	switch {
	case strings.Contains(change, "↥"):
		switch level {
		case "B":
			newlevel = "A"
		case "C":
			newlevel = "B"
		case "G":
			newlevel = "E"
		}
	case strings.Contains(change, "↧"):
		switch level {
		case "A":
			newlevel = "B"
		case "B":
			newlevel = "C"
		case "E":
			newlevel = "G"
		}
	default:
		newlevel = "?"
	}
	return newlevel
}

// function converts subject abbreviations to full names of subject
func ConvertSubAbbToFullName(subjectAbbreviation string) string {
	cfg := config.GetConfig()
	abbrevMap := cfg.Fachabkürzungen
	abbreviation := strings.ToLower(subjectAbbreviation)

	if abbrevMap[abbreviation] == "" {
		// TODO: Add warning to InfosAndWarnings and console output (make infosandwarnings variable global?)
		// Message: subject abbreviation "..." is missing in Fachabkürzungen in config file. Add subject there!
		return subjectAbbreviation
	}
	return abbrevMap[abbreviation]
}
